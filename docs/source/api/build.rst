``gwas_catalog_parser.build`` sub-package
=========================================

``gwas_catalog_parser.build.build`` module
------------------------------------------

.. autofunction:: gwas_catalog_parser.build.build::build_gwas_catalog


``gwas_catalog_parser.build.variants`` module
---------------------------------------------

.. autofunction:: gwas_catalog_parser.build.variants::cache_variants
