.. gwas-catalog-parser documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gwas-catalog-parser
==============================

The `gwas-catalog-parser <https://gitlab.com/cfinan/gwas-catalog-parser>`_ collection of resources and scripts to parse the `GWAS catalog <https://www.ebi.ac.uk/gwas/>`_ into a relational database.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started
   schema

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api
   examples

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
