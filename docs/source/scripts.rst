==============
Python scripts
==============

Below is a list of all the command line endpoints installed with the gwas-catalog-parser.

``gwas-catalog-build``
----------------------

.. argparse::
   :module: gwas_catalog_parser.build.build
   :func: _init_cmd_args
   :prog: gwas-catalog-build

Example usage
~~~~~~~~~~~~~

Here is an example of running an update to the GWAS catalog against GRCh37:

.. code-block:: console

   $ gwas-catalog-build -vv -a GRCh37 --gwas-catalog-section gwas_cat_b37
   === gwas-catalog-build (gwas_catalog_parser v0.1.0a0) ===
   [info] 11:03:26 > assembly value: GRCh37
   [info] 11:03:26 > cache_dir value: /home/user/.cache/gwas_catalog
   [info] 11:03:26 > config value: /home/user/.genomic_data.ini
   [info] 11:03:26 > gwas_catalog_section value: gwas_cat_b37
   [info] 11:03:26 > no_map_variants value: False
   [info] 11:03:26 > tmpdir value: None
   [info] 11:03:26 > verbose value: 2
   [info] 11:03:26 > variant cache database path: /home/user/.cache/gwas_catalog/variants_grch37.db
   [info] 11:03:27 > There are 427870 raw association (0 loaded/427870 unloaded)
   [info] 11:03:27 > downloading abbreviations
   [info] 11:03:41 > added 0 abbreviations
   [info] 11:03:41 > downloading/updating the GWAS catalog study data
   [info] 11:03:55 > added 34876 rows
   [warning] 11:03:55 > rows added to existing studies: 1251
   [info] 11:03:55 > downloading/updating the GWAS catalog association data
   [info] 11:05:51 > added 146764 rows
   [warning] 11:05:51 > rows added to existing studies: 33915
   [info] 11:05:51 > downloading/updating the GWAS catalog ancestry data
   [info] 11:06:02 > added 35549 rows
   [warning] 11:06:02 > rows added to existing studies: 138
   [info] 11:06:02 > Importing pubmed data - this may take some time...
   [info] 11:06:27 > There were 418 pubmed entries added (0 bad IDs)
   [info] 11:06:27 > Importing study data...
   [info] 11:06:35 > There were 33625 studies entries added
   [info] 11:06:35 > Importing phenotype data...
   [info] 11:13:00 > There were 73041 phenotypes entries added
   [info] 11:13:00 > inserting associations...
   [info] 11:16:54 > added 146764 association rows
   [info] 11:16:54 > inserting variants...
   @ 2023-07-19 11:16:55: Read the chain file:  /home/user/Dropbox/chain_files/GRCh38_to_GRCh37.chain.gz
   @ 2023-07-19 14:15:46: Read the chain file:  /home/user/Dropbox/chain_files/GRCh38_to_GRCh37.chain.gz
   [info] 14:24:31 > added 148433 variants (140932 mapped, 7501 unmapped)
   [info] 14:24:31 > Importing genes from Ensembl...
   [info] 14:24:31 > Importing orphan genes...
   [info] 14:24:53 > Importing gene synonyms from HGNC...
   [info] 14:24:53 > There were 0 gene synonyms added
   [info] 14:24:53 > There were 309 genes added (0 ensembl genes/309 orphans)
   [info] 14:24:53 > Importing associated genes...
   [info] 14:28:09 > There were 192368 associated genes added (11075 missed)
   [warning] 14:28:09 > UMLS not available, skipping phenotype mapping...
   [info] 14:28:09 > There were 0 exact mappings (0%)
   [info] 14:28:09 > There were 0 metamap mappings (0%)
   [info] 14:28:09 > There were 0 substring mappings (0%)
   [info] 14:28:09 > There were 0 missed mappings (0%)
   [info] 14:28:09 > *** END ***

A further example of running against GRCh38

.. code-block:: console

   $ gwas-catalog-build -vv -a GRCh38 --gwas-catalog-section gwas_cat_b38
   === gwas-catalog-build (gwas_catalog_parser v0.1.0a0) ===
   [info] 14:29:42 > assembly value: GRCh38
   [info] 14:29:42 > cache_dir value: /home/user/.cache/gwas_catalog
   [info] 14:29:42 > config value: /home/user/.genomic_data.ini
   [info] 14:29:42 > gwas_catalog_section value: gwas_cat_b38
   [info] 14:29:42 > no_map_variants value: False
   [info] 14:29:42 > tmpdir value: None
   [info] 14:29:42 > verbose value: 2
   [info] 14:29:42 > variant cache database path: /home/user/.cache/gwas_catalog/variants_grch38.db
   [info] 14:29:44 > There are 427870 raw association (0 loaded/427870 unloaded)
   [info] 14:29:44 > downloading abbreviations
   [info] 14:29:58 > added 0 abbreviations
   [info] 14:29:58 > downloading/updating the GWAS catalog study data
   [info] 14:30:12 > added 34876 rows
   [warning] 14:30:12 > rows added to existing studies: 1251
   [info] 14:30:12 > downloading/updating the GWAS catalog association data
   [info] 14:32:09 > added 146764 rows
   [warning] 14:32:09 > rows added to existing studies: 33915
   [info] 14:32:09 > downloading/updating the GWAS catalog ancestry data
   [info] 14:32:20 > added 35549 rows
   [warning] 14:32:20 > rows added to existing studies: 138
   [info] 14:32:20 > Importing pubmed data - this may take some time...
   [info] 14:32:30 > There were 418 pubmed entries added (0 bad IDs)
   [info] 14:32:30 > Importing study data...
   [info] 14:32:39 > There were 33625 studies entries added
   [info] 14:32:39 > Importing phenotype data...
   [info] 14:39:28 > There were 73041 phenotypes entries added
   [info] 14:39:28 > inserting associations...
   [info] 14:43:34 > added 146764 association rows
   [info] 14:43:34 > inserting variants...
   [info] caching variants:  58%|++++++++        | 85836/146764 [1:30:03<57:33, 17.64 variants/s]

The relevant sections in the ``~/.genomic_config.ini`` file are shown below:

.. code-block:: ini

   [general]
   seed = 1984
   email = user@gmail.com
   # Your path may be different here
   webdriver = /home/software/bin/chromedriver

   [species_synonyms]
   homo_sapiens = human
   homo sapiens = human
   humans = human

   [assembly_synonyms.human]
   GRCh38 = b38
   GRCh37 = b37

   [gwas_cat_b38]
   # An SQLAlchemy connection URL, see:
   # https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
   # All connection options here should be prefixed with gwas_cat
   gwas_cat.url = sqlite:////data/gwas_catalog/gwas_cat_grch38.db

   [gwas_cat_b37]
   # An SQLAlchemy connection URL, see:
   # https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
   # All connection options here should be prefixed with gwas_cat
   gwas_cat.url = sqlite:////data/gwas_catalog/gwas_cat_grch37.db

   [chain_files.human.b38]
   b37 = /home/user/Dropbox/chain_files/GRCh38_to_GRCh37.chain.gz


``gwas-catalog-cache-variants``
-------------------------------

.. argparse::
   :module: gwas_catalog_parser.build.variants
   :func: _init_cmd_args
   :prog: gwas-catalog-cache-variants
