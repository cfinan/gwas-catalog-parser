GWAS catalog schema
===================

The GWAS catalogue database in this repository is built using SQLAlchemy and has the following schema.

.. image:: /_static/gwas_catalog_schema.png
    :alt: The GWAS catalog schema

.. include:: ./data_dict/gwas_catalog_tables.rst
