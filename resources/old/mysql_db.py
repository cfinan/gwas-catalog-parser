"""
Functions for quering the gwas_catalog database from MySQL
"""

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_all_tables(conn):
    """
    Make sure all the tables in the database are created, this will require that
    """
    cur = conn.cursor
