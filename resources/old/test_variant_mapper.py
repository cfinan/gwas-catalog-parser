#!/usr/bin/env python
"""
Test the mapping of the variants and the parsing of association data from the
GWAS catalog
"""
from pymisc import progress
from ensembl_python_tools import rest, variants
from urllib.error import HTTPError
# import pymysql
import sqlite3
import pprint as pp
import re


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def mapvars(varlist, restc, prog):
    buffer_size = len(varlist)
    prog.static_msg("testing with '{0}'".format(buffer_size))
    res = []
    try:
        # Rest serach the variants
        res = restc.post_variants('human', varlist)
        res.extend(res)
        print("PASS")
    except HTTPError as e:
        print("FAIL {0}".format(str(e)))
        if buffer_size == 1:
            return []

        next_buffer_size = int(buffer_size/2) + 1
        # Now loop through individually
        for v in range(0, len(varlist), next_buffer_size):
            start = v
            end = min(v+next_buffer_size-1, buffer_size)
            print("start={0}; end={1}".format(start, end))
            res.extend(mapvars(varlist[start:end], restc, prog))
    return res


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dict_factory(cursor, row):
    """
    A custom factory callback that s supplied to the SQLite connections to
    mimick a DictCursor. This will be called interally from the cursor
    See:
    https://docs.python.org/3/library/sqlite3.html#sqlite3.Connection.row_factory

    Parameters
    ----------

    cursor : :obj:`Cursor`
        The cursor object
    row :`tuple`
        The row tuple to be `dict`

    Returns
    -------

    d :`dict`
        The row represented in dictionary format
    """
    # Initialise an empty dict that will form the row
    d = {}

    # Loop through the returned columns in the cursor and map to the tuple
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


# defaults_file = "/home/rmjdcfi/.my.cnf"
# defaults_section = "gwas_catalog_dtadb1"
# conn = pymysql.connect(use_unicode=True,
#                        charset="utf8",
#                        local_infile=True,
#                        cursorclass=pymysql.cursors.DictCursor,
#                        read_default_file=defaults_file,
#                        read_default_group=defaults_section)
conn = sqlite3.connect(
    "/home/rmjdcfi/code/gwas-catalog-parser/data/gwas_cat.db")
conn.row_factory = dict_factory
cur = conn.cursor()

# Get the number of snps
sql = """
SELECT count(DISTINCT(snps)) AS nvars FROM raw_download
"""
nrow = cur.execute(sql).fetchone()['nvars']

# select snps, snp_id_current, chr_id, chr_pos, strongest_snp_risk_allele, pvalue_mlog, or_or_beta, ci_text from raw_download where snps LIKE "%,%"
# First get some PMIDs
sql = """
SELECT DISTINCT(snps) FROM raw_download
"""
server = 'http://grch37.rest.ensembl.org'
debug = False
#server = 'http://rest.ensembl.org'
restc = rest.EnsemblRestClient(server='http://grch37.rest.ensembl.org',
                               debug=debug)
cur.execute(sql)

prog = progress.RemainingProgress(nrow)
counter = 0
max_counter = 200
var_buffer = []
non_rsid = []

all_res = []
for i in prog.progress(cur):
    # print(counter)
    if counter == max_counter:
        # pp.pprint(var_buffer)
        # Rest serach the variants
        res = restc.get_variants('human', var_buffer)
        all_res.extend(res)

        # single_res = restc.get_variant('homo_sapiens', var_buffer[0])
        var_buffer = []
        counter = 0
        # break

    # We will only search for variants that have an rsID otherwise they caused
    # the vep lookup to fail and fall back to splitting the variants up into
    # increasingly small lists
    # var_id = variants.process_var_id(i['snps'])
    var_id = re.sub(r'\s+', '', i['snps'])

    if variants.is_rsid(var_id) is True:
        # res = restc.get_variant('human', var_id)
        # all_res.append(res)

        # Test to see if they are an rsId or not
        var_buffer.append(var_id)
        counter += 1
    else:
        non_rsid.append(var_id)

print(len(all_res))
# pp.pprint(non_rsid)
# all_res = []
# for i in prog.progress(cur):
#     if counter == max_counter:
#         pp.pprint(var_buffer)
#         # Rest serach the variants
#         try:
#             res = restc.post_variants('human', var_buffer)
#             all_res.extend(res)
#         except HTTPError:
#             # Now loop through individually
#             for v in var_buffer:
#                 try:
#                     res = restc.get_variant('human', v)
#                     pp.pprint(res)
#                 except HTTPError:
#                     print("recaught and moving on")

#         # single_res = restc.get_variant('homo_sapiens', var_buffer[0])
#         var_buffer = []
#         # pp.pprint(single_res)
#         break
#     # Test to see if they are an rsId or not
#     var_buffer.append(i['snps'])
#     counter += 1
