"""
Dialogs used in the GUI
"""
import wx
import sys


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DownloadProgressDialog(wx.ProgressDialog):
    """
    A download dialog box
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, parent):
        """
        Initialise

        Parameters
        ----------

        parent : :obj:`gui_main.MainFrame`
            The parent main frame of the application
        """

        # Initialise the dialog from the superclass with some preset options
        # Note that this can't be aborted at present. Also, we are using the
        # default maximum of 100 (percent) and the percent is calculated from
        # a custom Update method
        super(DownloadProgressDialog, self).__init__("GWAS Catalog Download",
                                                     "Downloading...",
                                                     parent=parent,
                                                     style=0
                                                     | wx.PD_APP_MODAL
                                                     | wx.PD_ESTIMATED_TIME
                                                     | wx.PD_REMAINING_TIME
                                                     | wx.PD_AUTO_HIDE)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def Update(self, current, total):
        """
        Overide the dialogs update method

        Parameters
        ----------

        current :`int`
            The number of bytes that have been downloaded
        total :`int`
            The total number of bytes that need downloading
        """

        # The dialog operates on percentage, so we calculate the percent
        # downloaded to send to the dialog
        perc_download = current / float(total) * 100

        # Now make the current and total bytes into MB and make them pretty
        # to display in the dialog
        current = self.make_mb(current)
        total = self.make_mb(total)

        # Update the dialog with a call to the super class Update method
        super(DownloadProgressDialog, self).Update(
            perc_download,
            "Downloading...{0:.2f} MB/{1:.2f} MB".format(current, total))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def make_mb(self, data_bytes):
        """
        A helper function to turn bytes into MB

        Parameters
        ----------

        data_bytes :`int`
            The data in bytes to convert to MB
        """
        return round((data_bytes/1024)/1024, 2)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UpdateProgressDialog(wx.ProgressDialog):
    """
    A general update progress dialog box
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, parent, calls=1000):
        """
        Initialise

        Parameters
        ----------

        parent : :obj:`gui_main.MainFrame`
            The parent main frame of the application
        calls :`int`
            The number of calls to Update that it will take before the
            ProgressDialog updates, this is to slow the dialog down as too many
            updates will slow the application
        """

        # The updates calls and the current calls
        self.calls = calls
        self._cur_call = 0

        # Initialise the dialog from the superclass with some preset options
        # Note that this can't be aborted at present. Also, we are using the
        # default maximum of 100 (percent) and the percent is calculated from
        # a custom Update method
        super(UpdateProgressDialog, self).__init__("Performing Update",
                                                   "Updating...",
                                                   parent=parent,
                                                   style=0
                                                   | wx.PD_APP_MODAL
                                                   | wx.PD_ESTIMATED_TIME
                                                   | wx.PD_REMAINING_TIME
                                                   | wx.PD_ELAPSED_TIME
                                                   | wx.PD_AUTO_HIDE)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def make_mb(self, data_bytes):
        """
        A helper function to turn bytes into MB

        Parameters
        ----------

        data_bytes :`int`
            The data in bytes to convert to MB
        """
        return round((data_bytes/1024)/1024, 2)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def SetRange(self, maximum, calls=1):
        """
        Set the dialog range
        """

        # First set the calls and max sure we are not below the minimum
        self.calls = min(maximum, calls)
        self._cur_call = 0
        super().SetRange(maximum)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def Update(self, count, msg, display_count=True, suffix=""):
        """
        Overide the dialogs update method

        Parameters
        ----------

        current :`int`
            The number of bytes that have been downloaded
        total :`int`
            The total number of bytes that need downloading    
        """

        # We do not want the count to get to the end until we call destroy
        count = min(count, self.GetRange() - 1)

        if self.calls == self._cur_call:
            if display_count is True and suffix != "MB":
                msg = "{0}{1} {2}/{3} {2}".format(msg, count,
                                                  suffix, self.GetRange())
            elif display_count is True:
                msg = "{0}{1:.2f} {2}/{3:.2f} {2}".format(
                    msg,
                    UpdateProgressDialog.make_mb(count),
                    suffix,
                    UpdateProgressDialog.make_mb(self.GetRange()))

            # Update the dialog with a call to the super class Update method
            super(UpdateProgressDialog, self).Update(count, msg)
            self._cur_call = 0

        # Increment the current call
        self._cur_call += 1


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FatalErrorDialog(wx.MessageDialog):
    """
    A general handler for fatal error dialogs
    """
    def __init__(self, parent, error_message):
        """
        Initialise

        Parameters
        ----------

        parent : :obj:`gui_main.MainFrame`
            The parent main frame of the application
        error_msg :`str` (optional)
            The error message to display
        """

        # Initialise the dialog from the superclass with some preset options
        super(FatalErrorDialog, self).__init__(parent,
                                               "A fatal error occured with "
                                               "the error message: {0}".format(error_message),
                                               'Fatal Error',
                                               wx.OK | wx.ICON_ERROR
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def handle_fatal_error(parent, msg="A fatal error has occured"):
    """
    A helper function for handling fatal errors in the application. Most of
    these, will happen during initialisation. Note that calling this will call
    sys.exit(1)

    Parameters
    ----------

    parent : :obj:`gui_main.MainFrame`
        The parent main frame of the application
    msg :`str` (optional)
        The error message to display
    """

    print(msg, file=sys.stderr)
    dlg = FatalErrorDialog(parent, msg)
    dlg.ShowModal()
    dlg.Destroy()
    parent.Close()
    sys.exit(1)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_dialog(parent):
    """
    A helper function for initialisation of the GWAS catalogue data

    Parameters
    ----------

    parent : :obj:`gui_main.MainFrame`
        The parent main frame of the application
    """

    msg = "There is no data or it was not initialised correctly, need to initialise (this may take some time)...."
    print(msg, file=sys.stderr)

    dlg = wx.MessageDialog(parent, msg,
                           'Initialisation', wx.OK | wx.ICON_INFORMATION)
    dlg.ShowModal()
    dlg.Destroy()
