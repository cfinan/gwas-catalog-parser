#!/usr/bin/env python
from __future__ import print_function

from pymisc import htools
from pymisc import progress
from gwas_database import parsers
from umls import metamap
import csv
import pprint as pp
# import os
# import re


# cut -f7-8,30,35 alternative.1 | sort -u > uniq_traits.txt
studies_file = "/home/rmjdcfi/code/gwas_databases/data/uniq_traits.txt"
outfile = "/home/rmjdcfi/code/gwas_databases/data/test_mappings.txt"

prog = progress.RateProgress()
start_at = 0
break_at = -1
# args = ['-L', '2018AA', '-Z', '2018AA', '-sAIy', '--conj', '-V', 'USAbase']
# args = ['-L', '2018AA', '-Z', '2018AA', '-sAI', '--conj', '-V', 'USAbase']

with metamap.MetamapServer() as server:
    # Create the parsers that we need
    # parser for the mapped trait column
    mtparser = parsers.GwasCatMappedTrait(server)

    with open(studies_file, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter="\t")
        header = htools.HeaderTools(reader.next())

        with open(outfile, 'w') as fo:
            fo.write("ID\tGROUPS\tTEXT\tCUI\tTERM\tSTY(STYGROUP)\n")
            # Loop through the body of the file
            for c, row in enumerate(prog.progress(reader)):
                if c < start_at:
                    continue
                row = header.map_to_header(row)
                # pp.pprint(row)
                # Parse the mapped_trait column
                try:
                    for i in mtparser.parse(row['MAPPED_TRAIT']):
                        cui = i[2].cui
                        # print(cui)
                        # pp.pprint(cui.stys[0].groups)
                        stys = ["{0} ({1})".format(s.term, s.groups[0][1])
                                for s in cui.stys]
                        fo.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\n".format(
                            c,
                            ",".join(i[0]),
                            i[1],
                            cui.cui,
                            cui.term,
                            "|".join(stys),
                            row['STUDY ACCESSION']))
                except ValueError:
                    pass
                if c == break_at:
                    break
                # mapped_traits = preprocess_mapped_trait(row['MAPPED_TRAIT'])
                # for i in mapped_traits:
                #     mm = server.run(i, args)
                #     fo.writelines("{0}\n\n".format(i))
                #     fo.writelines(pp.pformat(mm))
                #     fo.writelines("\n\n")

                # fo.writelines("{0}\n\n".format(row['STUDY']))
                # mm = server.run(row['STUDY'], args)
                # fo.writelines(pp.pformat(mm))

                # term = "moderate and severe diarrhea"
                # mm = server.run(term, args)
                # fo.writelines("{0}\n\n".format(term))
                # fo.writelines(pp.pformat(mm))
                # fo.writelines("\n\n")

                # term = "renal and pancreatic cancer"
                # mm = server.run(term, args)
                # fo.writelines("{0}\n\n".format(term))
                # fo.writelines(pp.pformat(mm))
                # fo.writelines("\n\n")

                # pp.pprint(mm['Utterances'].keys())
                # break

#         for row in prog.progress(reader):
#             # fo.writelines("\n\n******* NEW_LINE ********\n\n")
#             row = header.map_to_header(row)
#             fo.writelines(pp.pformat(row))
#             # pp.pprint(row)

#             mapped_traits = preprocess_mapped_trait(row['MAPPED_TRAIT'])
#             for i in mapped_traits:
#                 fo.writelines("\n\n{0}\n\n".format(i))
#                 # print(i)
#                 parsed = tagger.parse(i)
#                 fo.writelines(pp.pformat(parsed))
#                 # pp.pprint(parsed)

#                 # Find CUIs
#                 m = umls_parse.CuiPhraseNgramMapper(uq, parsed)

#                 phrase_maps = [pm for pm in m.phrase_map()]
#                 phrase_maps.sort(key=lambda x: x[0], reverse=True)
#                 for pm in phrase_maps:
#                     fo.writelines("\n\n*** PHRASE MAP ***\n\n")
#                     # print("*** PHRASE MAP ***")
#                     fo.writelines(pp.pformat(pm))
#                     # pp.pprint(pm)

#                     for st in semantic_tagger.get_tags(pm):
#                         fo.writelines("\n\n*** SEMANTIC TAG ***\n\n")
#                         fo.writelines(pp.pformat(st))
#                         fo.writelines("\n\n")
#                     break

#             # for i in ["DISEASE/TRAIT", "MAPPED_TRAIT", "STUDY"]:
#             #     fo.writelines("\n\n{0}\n\n".format(i))
#             #     # print(i)
#             #     parsed = tagger.parse(row[i])
#             #     fo.writelines(pp.pformat(parsed))
#             #     # pp.pprint(parsed)

#             #     # Find CUIs
#             #     m = umls_parse.CuiPhraseNgramMapper(uq, parsed)

#             #     phrase_maps = [pm for pm in m.phrase_map()]
#             #     phrase_maps.sort(key=lambda x: x[0], reverse=True)
#             #     for pm in phrase_maps:
#             #         fo.writelines("\n\n*** PHRASE MAP ***\n\n")
#             #         # print("*** PHRASE MAP ***")
#             #         fo.writelines(pp.pformat(pm))
#             #         # pp.pprint(pm)
#             #         break

#             # break

#             # for k, v in sorted(count_mapped_trait.items(), key=lambda kv: kv[1]):
#             #     fo.writelines("{0}\t{1}\n".format(k, v))
