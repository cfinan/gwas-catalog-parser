#!/usr/bin/env python
"""
A rough parse of the gwas atlas summary file to map to CUIs
"""
from umls import metamap
from pymisc import progress
import os
import gzip
import csv
import pprint as pp

args = ['-L',
        '2018AA',
        '-Z',
        '2018AA',
        '-sAIy',
        '--conj',
        '-V',
        'USAbase']

infile = os.path.abspath("../data/gwasATLAS_v20190117.txt.gz")
outfile = os.path.abspath("../data/gwasATLAS_v20190117_rough_parse.txt")
prog = progress.RateProgress()

with gzip.open(infile, 'rt') as csvfile:
    reader = csv.reader(csvfile, delimiter="\t")
    header = next(reader)

    outheader = list(header)
    outheader.append("cui")
    outheader.append("term")
    outheader.append("stys")
    with open(outfile, "w") as csvout:
        writer = csv.writer(csvout, delimiter="\t")
        writer.writerow(outheader)
        with metamap.MetamapServer() as server:
            for row in prog.progress(reader):
                all_mappings = []
                newrow = dict((i, row[c]) for c, i in enumerate(header))
                # pp.pprint(row)

                try:
                    res = server.run(newrow['uniqTrait'], args)
                except ValueError:
                    writer.writerow(row + ["", "", ""])
                    continue
                # print(res)

                for p in res[0][0].phrases:
                    # pp.pprint(p)
                    for m in p.mappings:
                        for i in m.candidates:
                            writer.writerow(row + [i.cui.cui, i.cui.term, "|".join([sty.term for sty in i.cui.stys])])

