"""
Exceptions that are issued by GWAS repository modules
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DownloadError(Exception):
    """
    Called when a GWAS repository can't be downloaded
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, response_code, class_name, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The error message to output
        response : :obj:`str`
            The response code from the webserver
        class_name : :obj:`str`
            The name of the class that the search attribute is missing from
        """

        # Store attributes
        self.message = message
        self.response_code = response_code
        self.class_name = class_name

        # Call the Exception superclass
        super(DownloadError, self).__init__(message, *args)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RowformatError(Exception):
    """
    Called when a there is a problem with a row from one of the GWAS
    repositories
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The error message to output
        """

        # Store attributes
        self.message = message

        # Call the Exception superclass
        super(RowformatError, self).__init__(message, *args)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ParseError(Exception):
    """
    Called when a there is a problem with parsing a row
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The error message to output
        """

        # Store attributes
        self.message = message

        # Call the Exception superclass
        super(ParseError, self).__init__(message, *args)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DbValueError(Exception):
    """
    Called when a there is a problem with parsing a row
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The error message to output
        """

        # Store attributes
        self.message = message

        # Call the Exception superclass
        super(DbAttributeError, self).__init__(message, *args)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ParseWarning(Warning):
    """
    Called when a there is a potential problem with parsing a row
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, *args):
        """
        Initialise

        Paramerters
        -----------

        message : :obj:`str`
            The warning message to output
        """

        # Store attributes
        self.message = message

        # Call the Exception superclass
        super(ParseWarning, self).__init__(message, *args)
