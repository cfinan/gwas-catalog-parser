#!/usr/bin/env python
"""
Test that the pubmed ID search code is working
"""

from umls import pubmed_util, umls_query
import pymysql
import pprint as pp
import os
import csv
import sys

email = "c.finan@ucl.ac.uk"
defaults_file = "/home/rmjdcfi/.my.cnf"
defaults_section = "gwas_catalog_dtadb1"
conn = pymysql.connect(use_unicode=True,
                       charset="utf8",
                       local_infile=True,
                       cursorclass=pymysql.cursors.DictCursor,
                       read_default_file=defaults_file,
                       read_default_group=defaults_section)

cur = conn.cursor()

# First get some PMIDs
sql = """
SELECT pubmed_id FROM raw_download GROUP BY pubmed_id
"""
cur.execute(sql)
pmids = list(set([i['pubmed_id'] for i in cur]))
# pp.pprint(pmids)
print(len(pmids))
# print(pmids[0:2])
# pq = pubmed_util.PubmedIdQuery(email, pmids[0:2], verbose=True)
# pq.search()
# pq.fetch()
# res = pq.get_results()
# pp.pprint(res)
batch = 100
# pp.pprint(pubmed_util.MedlineRecord.MAIN_FIELD_HEADER)
# sys.exit(1)
with open(os.path.abspath("../data/pubmed_test.medline"), 'w') as out:
    writer = csv.writer(out, delimiter="\t")
    writer.writerow(pubmed_util.MedlineRecord.MAIN_FIELD_HEADER)
    with open(os.path.abspath("../data/mesh_terms.txt"), 'w') as mhout:
        mesh_writer = csv.writer(mhout, delimiter="\t")
        mesh_writer.writerow(['pubmed_id', 'mesh_term', 'modifiers', 'major_term'])
        for i in range(0, len(pmids), batch):
            start = i
            end = min(i+batch-1, len(pmids))
            print("START={0}, END={1}".format(start, end))
            test = pmids[start:end]
            pq = pubmed_util.PubmedIdQuery(email, test, verbose=True)
            pq.search()
            pq.fetch()
            res = pq.get_results()

            for j in res:
                # print(res)
                main_data = j.get_main_fields()
                # pp.pprint(main_data)
                try:
                    for c, m in enumerate(main_data['mesh_terms']):
                        mesh_writer.writerow([main_data['pubmed_id'], m[0], m[1], m[2]])
                        #  cuis = umlsq.match_term(m[0], sab="MSH")
                        # concepts = [con.cui for con in cuis]
                        # stys = [sty.term for con in cuis for sty in con.stys]
                except TypeError:
                    pass
                row = [main_data[k]
                       for k in pubmed_util.MedlineRecord.MAIN_FIELD_HEADER]
                writer.writerow(row)
            # break
