"""
Parser classes for various bits of GWAS databases
"""
from __future__ import print_function
import pprint as pp

import os
import re
import csv
import itertools

# pp.pprint(os.environ)
# Will change for a proper environment variable
ABV_FILE = os.path.join(
    os.environ['HOME'],
    "code/gwas_databases/data/abreviations.txt")

# The classifications of effect types
OR_EFFECT = "or"
BETA_EFFECT = "beta"
LOG_OR_EFFECT = "log_or"
UNKNOWN_EFFECT = "unknown"


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FullCatalogParser(object):
    """
    A parse class that acts on the whole catalogue
    """

    # The expected header columns, this is in sort order
    EXP_HEADER = ['95% CI (TEXT)',
                  'CHR_ID',
                  'CHR_POS',
                  'CNV',
                  'CONTEXT',
                  'DATE',
                  'DATE ADDED TO CATALOG',
                  'DISEASE/TRAIT',
                  'DOWNSTREAM_GENE_DISTANCE',
                  'DOWNSTREAM_GENE_ID',
                  'FIRST AUTHOR',
                  'GENOTYPING TECHNOLOGY',
                  'INITIAL SAMPLE SIZE',
                  'INTERGENIC',
                  'JOURNAL',
                  'LINK',
                  'MAPPED_GENE',
                  'MAPPED_TRAIT',
                  'MAPPED_TRAIT_URI',
                  'MERGED',
                  'OR or BETA',
                  'P-VALUE',
                  'P-VALUE (TEXT)',
                  'PLATFORM [SNPS PASSING QC]',
                  'PUBMEDID',
                  'PVALUE_MLOG',
                  'REGION',
                  'REPLICATION SAMPLE SIZE',
                  'REPORTED GENE(S)',
                  'RISK ALLELE FREQUENCY',
                  'SNPS',
                  'SNP_GENE_IDS',
                  'SNP_ID_CURRENT',
                  'STRONGEST SNP-RISK ALLELE',
                  'STUDY',
                  'STUDY ACCESSION',
                  'UPSTREAM_GENE_DISTANCE',
                  'UPSTREAM_GENE_ID']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, fobj, metamap):
        """
        Initialise

        Parameters
        ----------

        fobj : :obj:`File`
            A file like object to access the catalog .tsv file (must be v1.0.2)
            format

        Raises
        ------

        ValueError
            If the header of the GWAS catalog file does not match what is
            expected
        """

        # Create a csv reader object
        self._reader = csv.reader(fobj, delimiter="\t")

        # The metamap server object
        self._metamap = metamap

        # Get the heaer from the file we will check that the columns are good
        self._header = next(self._reader)

        # Create a mapped trait parser
        self._mapped_trait_parser = GwasCatMappedTrait(self._metamap)

        # These are the various caches that will be used to store mapped
        # trait/variant data
        self._trait_cache = {}
        self._variant_cache = {}

        # Make sure the header is valid
        if sorted(self._header) != self.__class__.EXP_HEADER:
            raise ValueError("Can't identify header columns")
        # pp.pprint(self._header)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self):
        """
        Generator to parse each record of the GWAS catalogue file

        Yields
        ------

        record : :obj:`GwasCatalogRecord`
            A GWAS catalog record
        """

        for row in self._reader:
            mapped_row = dict([(i, row[c])
                               for c, i in enumerate(self._header)])

            mapped_traits = self._parse_mapped_trait(
                mapped_row['MAPPED_TRAIT'])
            yield mapped_traits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_mapped_trait(self, mapped_trait):
        """
        Deals with the mapped trait parsing
        """

        all_mappings = []

        for i in mapped_trait.split(','):
            all_mappings.append(self._mapped_trait_parser.parse(i))

        return all_mappings


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseParser(object):
    """
    A base parser to hold common elements
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, metamap):
        """
        A metamap server obejct
        """
        self.metamap = metamap
        self.abv = self._read_abv()
        self.args = ['-L',
                     '2018AA',
                     '-Z',
                     '2018AA',
                     '-sAIy',
                     '--conj',
                     '-V',
                     'USAbase']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _read_abv(self):
        """
        https://www.ebi.ac.uk/gwas/docs/abbreviations
        Read the abreviations file into a dict
        """

        abv = {}
        with open(ABV_FILE) as fo:
            for i in fo:
                try:
                    key, value = i.split(":")
                    abv[key.strip()] = value.strip()
                except ValueError:
                    # unpacking error
                    pass
        return abv

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _run_metamap(self, text):
        """
        Run metamap on the text
        """

        # TODO: Need to catch some errors here
        return self.metamap.run(text, self.args)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_phrases(self, result):
        """
        Get the phrases from a metamap result
        """

        # We expect there to be 1 document that contains 1 utterance
        try:
            if len(result) > 1 or len(result[0]) > 1:
                raise ValueError("unexpected number of documents or utterances")
        except IndexError as e:
            print(e)
            pp.pprint(result)
            return []

        return result[0][0].phrases


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatMappedTrait(BaseParser):
    """
    A parser for the mapped trait column
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self, text):
        """
        Parse the mapped trait column
        """

        mapped_traits = self._preprocess_mapped_trait(text)
        # pp.pprint(mapped_traits)
        mapped_traits = self._apply_cheats(mapped_traits)
        # pp.pprint(mapped_traits)
        # all_mappings = []
        for c, i in enumerate(mapped_traits):
            # pp.pprint(i)
            # print(len(i) + 1)
            # print(list(range(1, len(i) + 1)))
            for j in range(1, len(i)):
                # print(j)
                # print(i[j])
                i[j] = self._parse(i[j])
        # print(all_mappings)
        return mapped_traits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse(self, term):
        """
        Do the parsing for a single term 
        """
        pt = self._run_metamap(term)
        all_mappings = []

        for p in self._get_phrases(pt):
            # pp.pprint(p)
            for m in p.mappings:
                # We could get one or more mappings but as WSD is active
                # hopefully not too often!
                # I want to test the mapping for certain keywords that give an
                # clue as to what they are
                # pp.pprint(m)
                # pp.pprint(m.candidates)
                mapping_type = self._get_mapping_type(m.text)
                # pp.pprint((self._get_mapping_type(m.text), m.candidates))
                all_mappings.extend(
                    [(mapping_type, m.text, c, term) for c in m.candidates])

        return all_mappings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_mapping_type(self, mapping_phrase):
        """
        Look for certain key words in the mapping that indicate what it is 
        with regards to the phenotype as a whole
        """
        mapping_types = []
        if re.search(r'measurement$', mapping_phrase):
            mapping_types.append("QTL")

        if re.search(r'^response to', mapping_phrase):
            mapping_types.append("RESPONSE")

        if len(mapping_types) == 0:
            mapping_types.append("TRAIT")

        return mapping_types

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _preprocess_mapped_trait(self, mapped_trait):
        """
        Pre-processed the mapped traits to make them more ameanable to mapping
        """

        # The mapped trait column is , delimited in the case of > 1 mapped
        # trait
        mapped_traits = [i.strip() for i in mapped_trait.split(",")]

        return mapped_traits

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _apply_cheats(self, mapped_traits):
        """
        Cheats are text fudges that I have developed through experience
        """
        # pp.pprint(mapped_traits)
        for c, i in enumerate(mapped_traits):
            # remove -based
            # TODO: Need a unicode hyphen
            # mapped_traits[c] = re.sub(r'-based', '', i)

            # dehyphonate -indiced/-adjusted
            # mapped_traits[c] = re.sub(r'-(induced|adjusted)', r' \1', i)

            # remove :
            mapped_traits[c] = re.sub(r':', ' ', i)

            # A(2) -> A2
            mapped_traits[c] = re.sub(r'A\s*\(2\)', 'A2', i)

            # Here we define some potential caviats that are present in some of the
            # EFO terms, for simplicity we only deal with 1 possible caviat per
            # term and do not deal with the potential for nexted caviats
            # The net result of this loop is that mapped traits is a list of lists
            # where the sub list has the caviat as the first argument (or None) and
            # the subterms as the remaining arguments
            m = re.search(r'[-\u002D\u058A\u05BE\u1400\u1806\u2010-\u2015\u2E17\u2E1A\u2E3A\u2E3B\u2E40\u301C\u3030\u30A0\uFE31\uFE32\uFE58\uFE63\uFF0D](based|induced|adjusted)', i)
            if m:
                mapped_traits[c] = [m.group(1)]
                mapped_traits[c].extend(re.split(r'[-\u002D\u058A\u05BE\u1400\u1806\u2010-\u2015\u2E17\u2E1A\u2E3A\u2E3B\u2E40\u301C\u3030\u30A0\uFE31\uFE32\uFE58\uFE63\uFF0D](?:based|induced|adjusted)\s*', i))
            else:
                mapped_traits[c] = [None, i]

        return mapped_traits


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_unique_publication_genes(genes):
    """
    Take the gene list that needs to be queried and process to get a unique
    set of gene IDs that will need to be mapped back to Ensembl Genes. The
    unique genes will be used to do lookups on, this is because the gene Xref
    only has a GET and no POST so is very slow

    Parameters
    ----------
    genes :list of tuple
        The tuples are named tuples with a raw_download_id and a reported_genes
        attribute

    Returns
    -------
    unique_genes :dict
        The keys are the gene names to search for and the values are sets of
        raw_download_ids that are associated with those gene names
    """
    nulls = ['NR', 'NULL', 'intergenic', '']
    unique_genes = {}

    # Loop through all the raw download genes that need importing, so, these
    # do not have any Ensembl Gene ID info associated with them
    for pg in genes:
        try:
            # Remove any excess whitespace
            genes = re.sub(r'\s+', '', pg.reported_genes).strip()
        except TypeError:
            # could be None
            pass

        # Also check for a string based "NULL" value
        if genes in nulls:
            genes = None

        try:
            # Multiple genes will be separated by a comma
            genes = genes.split(',')
        except AttributeError:
            continue

        # Loop through all the individual gene names and then store then as
        # keys and associate the raw download ID as a value. Note that the
        # "None" entries will also have a key but it will be ignored when
        # querying Ensembl
        for g in genes:
            try:
                unique_genes[g].add(pg.raw_download_id)
            except KeyError:
                unique_genes[g] = set()
                unique_genes[g].add(pg.raw_download_id)

    return unique_genes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def search_genes(rc, unique_genes, max_batch=1000):
    """
    Parse a list of reported genes to get Ensembl Gene IDs for them 

    Parameters
    ----------

    rc : :obj:`EnsemblRestClient`
        The connection to the Ensembl REST server
    genes :`list` of `dict`
        A list of variant associations taken from a raw download of the GWAS
        catalogue to lookup against the Ensembl REST server and clean up. The
        expected keys in the dict are:
        {'chr_id': '1',
         'chr_pos': '177920345',
         'ci_text': 'unit decrease',
         'or_or_beta': '0.048511162',
         'p_value': 3e-46,
         'pvalue_mlog': 45.52287874528034,
         'raw_download_id': 195,
         'reported_genes': 'NR',
         'risk_allele_frequency': '0.80425773765243',
         'snps': 'rs543874',
         'strongest_snp_risk_allele': 'rs543874-A'},
    max_batch :`int`
        The batch size to POST to the REST server, the default is the maximum
        (1000). If more are passed then, they are processed in batches of this
        size
    """

    # Now we can go through the cache and do the searches, the IDs are mapped
    # with the XREF get lookup and then we post the mapped IDs in batches of
    # 1000 to get the coordinate information
    mapped_genes = {}
    for rg, ids in unique_genes.items():
        try:
            gids = rc.get_ensembl_xref('human', rg, object_type="gene")
        except Exception:
            mapped_genes.setdefault(None, [])
            mapped_genes[None].extend([(i, rg, None) for i in ids])

        if len(gids) > 0:
            for gid in gids:
                mapped_genes.setdefault(gid['id'], [])
                mapped_genes[gid['id']].extend([(rdid, rg, gid['id'])
                                                for rdid in ids])
        else:
            mapped_genes.setdefault(None, [])
            mapped_genes[None].extend([(i, rg, None) for i in ids])

        # If the mapped genes are on or above our target batch (they may be
        # above as we have no way of knowing how many ensembl gene IDs will be
        # mapped to a listed gene ID. Then we post the ensembl gene IDs to get
        # the coordinates
        if len(mapped_genes) >= max_batch:
            for i in get_gene_coords(rc, mapped_genes):
                yield i

            # pp.pprint(ens_coords)
            mapped_genes = {}

    # Get the coords for any remaining genes
    if len(mapped_genes) > 0:
        for i in get_gene_coords(rc, mapped_genes):
            yield i

        mapped_genes = {}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_coords(rc, mapped_genes):
    """
    Return the gene coordinates for a bunch of mapped genes

    Parameters
    ----------

    rc : :obj:`EnsemblRestClient`
        A rest client to query the gene IDs
    mapped_genes :`dict`
        A dicts with ensembl gene IDs as keys and lists of mappings as values
        the mappings are tuples with rawdownload_id [0], listed_gene_id [1] and
        ensembl_gene_id [2]. If there has been no ensembl gene ID mapping then
        it will be None

    Yields
    -------

    mappings :`dict`
        The dict is primarily the Ensembl coordinate data with a couple of new
        keys added for the raw_download ID and the listed_gene_id. If there was
        no mapping then the dict will just contain raw_download ID and the
        listed_gene_id
    """

    ens_coords = rc.get_id_data([i for i in mapped_genes.keys()
                                 if i is not None])
    for i in sorted([e for v in mapped_genes.values() for e in v], key=lambda x: x[0]):
        try:
            coord = ens_coords[i[2]]

            try:
                # I noticed that some of the ensembl coords entries do not have a
                # description field, for consistency we make sure it exists
                coord['description']
            except KeyError:
                coord['description'] = None

            coord_copy = coord.copy()
        except KeyError:
            coord_copy = {}

        coord_copy['listed_gene_id'] = i[1]
        coord_copy['raw_download_id'] = i[0]
        yield coord_copy


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_associations(rest, variants, max_batch=200):
    """
    Parse a list of variants based on IDs.

    Parameters
    ----------

    rest : :obj:`EnsemblRestClient`
        The connection to the Ensembl REST server
    variants :`list` of `dict`
        A list of variant associations taken from a raw download of the GWAS
        catalogue to lookup against the Ensembl REST server and clean up. The
        expected keys in the dict are:
        {'chr_id': '1',
         'chr_pos': '177920345',
         'ci_text': 'unit decrease',
         'or_or_beta': '0.048511162',
         'p_value': 3e-46,
         'pvalue_mlog': 45.52287874528034,
         'raw_download_id': 195,
         'reported_genes': 'NR',
         'risk_allele_frequency': '0.80425773765243',
         'snps': 'rs543874',
         'strongest_snp_risk_allele': 'rs543874-A'},
    max_batch :`int`
        The batch size to POST to the REST server, the default is the maximum
        (200). If more are passed then, they are processed in batches of this
        size
    """

    # This will hold the processed data with the original snp as keys and their
    # data as values. There are some records that are haplotypes. These will be
    # represented multiple times in this stucture. However, this will be
    # flattened again before returning
    processed = {}

    # This will hold dicts of processed batches that will eventually be
    # flattened and returned
    all_processed = []

    for counter, assoc in enumerate(variants):
        # pp.pprint(assoc)
        proc_assoc = {}
        # Attempt to guess the effect type based on the population description
        # and the confidence interval text
        proc_assoc['effect_type'] = effect_type_guess(
            assoc.initial_sample_size,
            assoc.replication_sample_size,
            assoc.ci_text)

        # Get the processed variants list and store the number of variants in
        # the association
        proc_assoc['proc_variants'] = process_variants(
            assoc.strongest_snp_risk_allele)

        # This will hold the ensembl mapped variants after we have queried them
        proc_assoc['ensembl_variants'] = {}

        proc_assoc['nvars'] = len(proc_assoc['proc_variants'])

        effect_size, direction, units, se, ci_lower, ci_upper, effect_flags = \
            process_effect_size(assoc.or_or_beta, assoc.ci_text)

        # Store the processed effects
        proc_assoc['effect_size'] = effect_size
        proc_assoc['effect_direction'] = direction
        proc_assoc['effect_units'] = units
        proc_assoc['standard_error'] = se
        proc_assoc['ci_lower'] = ci_lower
        proc_assoc['ci_upper'] = ci_upper
        proc_assoc['effect_flags'] = effect_flags

        # Just coping values over
        proc_assoc['mlog_pvalue'] = assoc.pvalue_mlog
        proc_assoc['raw_download_id'] = assoc.raw_download_id
        proc_assoc['chr_name_cat'] = assoc.chr_id

        # Now cast selected fields, and also rename then to match the database
        # fields
        for v in [('p_value', 'pvalue', float),
                  ('risk_allele_frequency', 'effect_allele_freq', float),
                  ('chr_pos', 'start_pos_cat', int)]:
            try:
                proc_assoc[v[1]] = v[2](getattr(assoc, v[0]))
            except (TypeError, ValueError):
                proc_assoc[v[1]] = None

        # Now after we have built the processed record up, we want to add it to
        # a dict of all processed records in this batch. With the key being the
        # unmapped var_id and the values being a list that could contain one or
        # more associations tagged to that var_id
        for v in proc_assoc['proc_variants']:
            try:
                processed[v[0]].append(proc_assoc)
            except KeyError:
                processed[v[0]] = []
                processed[v[0]].append(proc_assoc)

        # Processes a batch for variants when we have reached max_batch size
        if (counter+1) % max_batch == 0:
            all_processed.extend(_process_ensembl_variant(rest, processed))
            processed = list()

    # Now make sure we have no left over variants to process
    if len(processed) > 0:
        all_processed.extend(_process_ensembl_variant(rest, processed))

    # Now we want to flatten into a list, in the case of haplotypes we do not
    # want them to be represented multiple times, so we keep track of what
    # raw_download_ids have already been flattened
    seen_rid = set()

    # Sort and yield individual RIDS
    for i in sorted(all_processed, key=lambda x: x['raw_download_id']):
        if i['raw_download_id'] not in seen_rid:
            seen_rid.add(i['raw_download_id'])
            yield i


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_ensembl_variant(rest, gwas_cat_vars):
    """
    Search for the variants in Ensembl and merge the Ensembl data back into
    the GWAS catalog data

    Parameters
    ----------

    rest : :obj:`EnsemblRestClient`
        The connection to the Ensembl REST server
    gwas_cat_vars :`dict`
        A dict with variant IDs as keys and processed GWAS catalog association
        data as values, this will be updated with the ensembl data if it
        matches with a key entry

    Returns
    -------

    gwas_cat_vars :`dict`
        A dict with variant IDs as keys and processed GWAS catalog association
        data as values, this will be updated with the ensembl data if it
        matches with a key entry.
    """
    # This will get the variant IDs that we want to search for using
    # the rest API, we exclude None entries
    search_vars = [k for k in gwas_cat_vars.keys() if k is not None]

    # Do rest search
    ens_vars = rest.get_variants('human', search_vars)

    # merge rest results into processed data. To do that we loop
    # through, the rest results. The format of the rest results is a
    # dict or var_ids as keys and a dict var_info as values
    for variant, var_data in ens_vars.items():
        # The GWAS catalog could potentially contain an old synonym,
        # for the variant name so there is no guanentee that they will
        # match the keys in the rest search, if not we search through
        # the variant synonyms for a match
        try:
            # Attempt to assignt he variant, if that fails we will get
            # a KeyError so we will look in the synonyms
            _add_ensembl_variant(gwas_cat_vars, variant, var_data)
        except KeyError:
            # It must be in the synonyms:
            try:
                for syn in var_data['synonyms']:
                    try:
                        # Attempt to assign, if it works then move on
                        _add_ensembl_variant(gwas_cat_vars, syn, var_data)
                        break
                    except KeyError:
                        pass
            except TypeError:
                # I had a strange error that I need to understand
                pp.pprint(search_vars)
                pp.pprint(ens_vars)
                print(len(search_vars))
                pp.pprint(var_data)
                print(variant)
                raise

    return list(itertools.chain(*gwas_cat_vars.values()))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _add_ensembl_variant(gwas_cat_vars, ens_var_id, ens_var_data):
    """
    A helper function to update the gwas catalog variant list with mapped
    ensembl data

    Parameters
    ----------

    gwas_cat_vars :`dict`
        A dict with variant IDs as keys and processed GWAS catalog association
        data as values, this will be updated with the ensembl data if it
        matches with a key entry
    ens_var_id :`str`
        The variant identifier from ensembl that will be searched in
        gwas_cat_vars
    ens_var_data :`dict`
        Variant information from ensembl for the ens_var_id

    Raises
    ------

    KeyError
        If ens_var_data is not located in gwas_cat_vars
    """

    # We attempt to use the ensembl variant ID as a key so we can assign the
    # mapped variant and the risk allele to each matching association. This
    # will throw a KeyError if it can't be found
    for a in gwas_cat_vars[ens_var_id]:
        # If we get here then the variant is in the processed list. SO we want
        # to update the association with the Ensembl data. There could be more
        # than one variant in an association (i.e. haplotype) so we initialise
        # a list as a default value
        a.setdefault('ensembl_variants', {})

        # We assign a copy of the var data as we will be updating it with the
        # effect allele for the association. So whilst different associations
        # may map to the same variant their effect alleles may be different and
        # the dict memory ref will be incorrect in these cases. Note that this
        # is a shallow copy (I think) but that is OK as the nested structures
        # are not touched
        assign_ens_var_data = ens_var_data.copy()
        # a['ensembl_variants'].append(assign_ens_var_data)
        a['ensembl_variants'][ens_var_id] = assign_ens_var_data

        # This fiddly bit of code looks for the effect allele of the variant
        # that matches the ensembl variant and assigns it to the mapped ensembl
        # variant
        try:
            assign_ens_var_data['effect_allele'] = [e[1] for e in a['proc_variants']
                                                    if e[0] == ens_var_id][0]
        except IndexError:
            assign_ens_var_data['effect_allele'] = None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_effect_size(effect_size, ci_text):
    """
    Does some post processing on the effect size columns. This updates beta
    values to be negative if that are decreasing in value. This also processes
    the 95% confidence interval into its upper/lower bounds and creates a
    direction of effect variable (for betas) which will either be +/-. The
    units for the beta will also be captured if they are available

    Parameters
    ----------

    effect_size :`str`
        The unsigned effect size (or_or_beta column) from the GWAS catalog
    ci_text :`str`
        The ci_text column from the GWAS catalog, this is used to determine the
        effect direction

    returns
    -------

    effects :`tuple`
        The tuple has the following elements, [0] signed effect size (float),
        [1] effect direction (str) [2] effect units (str) [3] SE (float)
        [4] CI lower (float) [5] CI upper (float) [6] flags (str). If any of
        these attributes are absent then they are replaced with None
    """

    # Precompile as these will be used in a match and a sub
    ci_reg = re.compile(r'^\s*\[?(\d+(?:\.\d+))\s*-\s*(\d+(?:\.\d+))\]?')
    unit_reg = re.compile(r'(.*?)(increase|decrease)')

    # Attempt to match the CIs
    try:
        ci_match = ci_reg.match(ci_text)
    except TypeError:
        # ci_text is None
        try:
            effect_size = float(effect_size)
        except TypeError:
            pass

        return effect_size, None, None, None, None, None, None

    try:
        # This will fail with an attribute error if not present
        ci_lower = float(ci_match.group(1))
        ci_upper = float(ci_match.group(2))

        # Remove the CIs to make the following REGEXs easier
        ci_text = ci_reg.sub('', ci_text)

        # Calculate the standard error from the CIs
        se = (ci_upper - ci_lower) / 3.92
    except AttributeError:
        # If we fail then everything is None
        ci_lower = None
        ci_upper = None
        se = None

    # Now attempt to match the units and direction
    unit_match = unit_reg.search(ci_text)
    try:
        # The units may not be present so will end up being '' if not
        # (and direction is). If neither are present then we get an
        # AttributeError
        units = unit_match.group(1).strip()
        direction = unit_match.group(2)

        # Remove the units, anything that is left is flags
        ci_text = unit_reg.sub('', ci_text)

        # If units is empty make them None
        if units == '':
            units = None

        # Sign the effect size in the case of decreasing effect direction
        if direction == 'decrease' and effect_size is not None:
            effect_size = "-{0}".format(effect_size)
    except AttributeError:
        units = None
        direction = None

    # Make sure the effect size is a float
    # TODO: Catch AttributeError or TypeError
    try:
        effect_size = float(effect_size)
    except TypeError:
        # Effect size is None
        pass

    # Finally clean any flags and make None if there are not any
    ci_text = re.sub(r'[\(\)]', '', ci_text.strip())
    if ci_text == '':
        ci_text = None
    return effect_size, direction, units, se, ci_lower, ci_upper, ci_text


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_variants(variants):
    """
    cleans up the variant string, note that this may be a , separated list of
    variants
    """

    # First remove all whitespace, I would not expect to see whitespace in their
    # variant names
    variants = re.sub(r'\s+', '', variants)

    # Now split on , (commas) or ; (semicolon)
    variants = re.split(r'[,;]', variants)
    proc_variants = []
    for i in variants:
        match = re.match(r'(.*?)-([DIRATCGatcgdir?]+)', i)
        try:
            var = match.group(1)
            risk_allele = match.group(2).upper()
        except AttributeError:
            match = re.match(r'(rs\d+)([DIRATCGatcgdir?]+)', i)
            try:
                var = match.group(1)
                risk_allele = match.group(2).upper()
            except AttributeError:
                var = i
                risk_allele = None
                pp.pprint(variants)
                print(i)
                # raise

        if risk_allele == "?":
            risk_allele = None
        proc_variants.append((var, risk_allele))

    return proc_variants


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def effect_type_guess(dis_pop, rep_pop, ci_text):
    """
    Has a guess at the effect type based on data in the population fields and
    in in the confidence interval test field
    """

    # Count the evidence for an odds ratio and a beta estimate
    or_evidence = 0
    beta_evidence = 0

    # search the population fields for case/control designation
    for i in [dis_pop, rep_pop]:
        # print(i)
        try:
            if re.search(r'\bcases?\b', i, re.IGNORECASE) and \
               re.search(r'\bcontrols?\b', i, re.IGNORECASE):
                or_evidence += 1
        except TypeError:
            # Could be None
            pass

    # Search the effect description for an indication that the effect estimate
    # is signed
    try:
        if re.search(r'increase|decrease', ci_text, re.IGNORECASE):
            beta_evidence += 1
    except TypeError:
        # could be none
        pass

    # Now evaluate the evidence and make a decision on the effect type
    # guess
    # If we have evidence for both then we are assuming a log_or
    if or_evidence > 0 and beta_evidence > 0:
        return LOG_OR_EFFECT
    elif or_evidence > 0:
        return OR_EFFECT
    elif beta_evidence > 0:
        return BETA_EFFECT
    else:
        return UNKNOWN_EFFECT
