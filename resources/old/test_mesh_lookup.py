#!/usr/bin/env python
"""
Test the lookup of the MESH terms
"""
from umls import umls_query
from pymisc import progress
import csv
import pymysql

defaults_file = "/home/rmjdcfi/.my.cnf"
defaults_section = "gwas_catalog_dtadb1"
infile = "/home/rmjdcfi/code/gwas-catalog-parser/data/mesh_terms.txt"
outfile = "/home/rmjdcfi/code/gwas-catalog-parser/data/mesh_terms_mapped.txt"

umls_conn = pymysql.connect(use_unicode=True,
                            charset="utf8",
                            cursorclass=pymysql.cursors.DictCursor,
                            read_default_file=defaults_file,
                            read_default_group="umls_2018ab_dtadb1")
umlsq = umls_query.UmlsQuery(umls_conn)

uniq_header = ['cui_id', 'cui', 'term', 'ncui', 'probcui', 'semantic_types',
               'all_terms']
with open(infile) as csvin:
    nlines = sum([1 for i in csvin])
    csvin.seek(0)
    prog = progress.RemainingProgress(nlines)
    reader = csv.reader(csvin, delimiter="\t")
    header = next(reader)
    with open(outfile, 'w') as csvout:
        writer = csv.writer(csvout, delimiter="\t")
        writer.writerow(uniq_header + header)

        # Now read in the file
        for row in prog.progress(reader):
            cuis = [cui for cui in umlsq.match_uniq_term_from_term(row[1])]

            if len(cuis) > 0:
                for i in cuis:
                    writer.writerow([i[h] for h in uniq_header] + row)
            else:
                    writer.writerow(["" for h in uniq_header] + row)
