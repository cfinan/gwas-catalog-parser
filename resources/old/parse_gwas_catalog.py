#!/usr/bin/env python
"""
Parse the entire GWAS catalogue into a decent state
"""
from gwas_catalog_parser import parsers
from pymisc import progress
from umls import metamap
import argparse
import pprint as pp
import sys
parser = argparse.ArgumentParser(description="parse the gwas catalogue")

parser.add_argument("outdir", help="the directory to output the processed "
                    "GWAS catalogue files")
parser.add_argument("infile", nargs="?", help="The input GWAS catalogue file, "
                    "this expects v1.0.2 format. If this is not provided then"
                    "the file will be downloaded")
parser.add_argument('-v', '--verbose', action="store_true",
                    help="indicate progress")

args = parser.parse_args()
pp.pprint(args)

try:
    fobj = open(args.infile)
except TypeError:
    # Not supplied so we need to download
    print("auto-downloading not implemented yet, please supply a valid file",
          file=sys.stderr)
    sys.exit(1)
except FileNotFoundError:
    print("can't find GWAS catalogue file '{0}'".format(args.infile),
          file=sys.stderr)
    sys.exit(1)

# Create the full catalog parser
prog = progress.RateProgress(verbose=args.verbose)

with metamap.MetamapServer() as server:
    p = parsers.FullCatalogParser(fobj, server)
    for r in prog.progress(p.parse()):
        pp.pprint(r)
    # pp.pprint(r)
