"""
Functions used to initialise the database by downloading the required datasets
"""
from gwas_catalog_parser import dialogs, constants as con, \
    gwas_database_download, parsers
from umls import pubmed_util
from merit.ensembl.rest import MAX_POST_SIZE
from merit.ensembl import variants
from simple_progress import progress
import sys
import tempfile
import os
import csv
import hashlib
import pprint as pp
import re
import shutil


MSG_PREFIX = '[info]'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_gwas_cat_data(parent, existing_ids=None, dlg=None):
    """
    A full data initialiser for the GWAS catalog data. This is designed to be
    called from the GUI and will perform the data download with dialogs. Run
    pubmed abstract lookups and perform the variant rsID validation. All the
    data are written to temp files. only records not already in the GWAS
    catalogue raw_downlaod table are processed

    Parameters
    ----------

    parent : :obj:`gui_main.MainFrame`
        The main frame of the application
    dlg : :obj:`dialogs.UpdateProgressDialog` (optional)
        A dialog to indicate update progress. If not supplied one is created
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("initialising...")

    # So now we make sure the initialised option is False, that way if anything
    # goes wrong then the initialisation will be re attempted on the next
    # startup, this will be toggles to True at the end of this function
    # parent.gwas_cat_db.update_initialised_option(False)

    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent, calls=0)

    # Get a list of existing IDs if none have been provided
    if existing_ids is None or len(existing_ids) == 0:
        existing_ids = [i for i in download_existing_ids(parent,
                                                         dlg=dlg)]
    fd, ofn = tempfile.mkstemp()
    os.close(fd)
    msg.msg("Downloading to: {0}".format(ofn))

    # First initialise the download
    gui_download = gwas_database_download.GwasCatGuiFetch(dlg,
                                                          outfile=ofn,
                                                          verbose=True)

    # Wrap in a general exception try/except, and when it does destroy the
    # dialog and initialise and error dialog
    gui_download.download()

    # Get the GWAS catalog data
    import_required_data(parent, ofn, existing_ids, dlg=dlg)

    # Now update any pubmed IDs that are not represented int he pubmed table
    # note that this does not look for IDs that are updated by pubmed or any
    # mesh terms that have not been fully incorporated (i.e. if a download
    # fails
    import_pubmed_ids(parent, dlg=dlg)

    # Now we look at variant annotation and processing the association data
    import_association_data(parent, dlg=dlg)

    # Now we look at mapping the reported genes to Ensembl Gene IDs
    import_publication_genes(parent, dlg=dlg)

    # Now import the potential download locations of the full datasets
    import_full_data_download_locations(parent, dlg=dlg)

    # Now download the abbreviations table
    import_gwas_cat_abbreviations(parent)

    # TODO: Metamap update
    dlg.Destroy()
    os.unlink(ofn)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_existing_ids(parent, dlg=None):
    """
    This controls the downloading of the existing raw_data hashes in the
    database. These will allow the determination of records that we have and
    what will need to be added

    Parameters
    ----------
    parent : :obj:`gui_main.MainFrame`
        The main frame of the application
    dlg : :obj:`dialogs.UpdateProgressDialog` (optional)
        A dialog to indicate existing ID download progress. If not supplied one
        is created

    Returns
    -------
    data : :obj:`list` of `str`
        A list where each element is a 32 character MD5 representing a
        raw data row
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("downloading existing IDs...")

    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Initialise empty data
    data = []

    # First determine the count of rows in the database
    total_records = parent.gwas_cat_db.get_raw_record_count()
    msg.msg("total_records={0}".format(total_records))

    if total_records > 0:
        msg_str = "Getting existing raw data..."
        msg.msg(msg_str)
        dlg.SetRange(total_records, con.UPDATE_DIALOG_STEPS)

        # Now download
        # TODO: Should this be a generator?
        for counter, i in enumerate(parent.gwas_cat_db.get_all_identifers()):
            dlg.Update(counter+1, "Downloading IDs...", suffix="IDs")
            data.append(i)

    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_required_data(parent, download_file, existing_ids, dlg=None):
    """
    Check the download file against the existing data and import rows that are
    not present into the database. This will launch a dialog that will detail
    the import progress if there are records to be imported. Note, at present
    no checks for changed rows are carried out although this may change in
    future

    Parameters
    ----------
    parent : :obj:`gui_main.MainFrame`
        The main frame of the application
    download_file :`str`
        The path the the download file to check
    existing_data :`list` of `str`
        The rows existing gwas catalog accession numbers.
    dlg : :obj:`dialogs.UpdateProgressDialog` (optional)
        A dialog to indicate import progress. If not supplied one is created
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing GWAS catalog data...")

    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Get the filtered file
    filtered_file, update_count = filter_existing_ids(parent,
                                                      download_file,
                                                      existing_ids, dlg=dlg)
    msg.msg("Need to import '{0}' rows".format(update_count))

    # If there is stuff that needs updating
    if update_count > 0:
        dlg.SetRange(update_count, con.UPDATE_DIALOG_STEPS)

        # Get the current autocommit value and make sure we turn it off, we
        # will then commit in one go at the end and return auto commit to
        # it's previous value
        with open(filtered_file, 'r') as csvin:
            reader = csv.reader(csvin, delimiter="\t")

            # Skip the header
            next(reader)
            insert_rows = []

            for count, row in enumerate(reader):
                dlg.Update(count, "Importing...", suffix="rows")

                # Don't update the dialog too often as it slows things down
                # I will update the dialog everytime I insert into the database
                if (count % 1000) == 0:
                    # Do the insert the empty the row buffer
                    parent.gwas_cat_db.insert_raw_download_record(insert_rows)
                    insert_rows = list()

                # Add the next row after making sure the NULLs are currectly
                # dealt with, NR and NS are effectively NULL values in the
                # GWAS catalog
                row = [i if i not in con.GWAS_CAT_DOWNLOAD_NULLS else None
                       for i in row]
                insert_rows.append(tuple(row))

        # Is there still stuff in the buffer, if so insert it
        if len(insert_rows) > 0:
            parent.gwas_cat_db.insert_raw_download_record(insert_rows)

    # Remove the temp file that was used to filter the rows
    os.unlink(filtered_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_existing_ids(parent, download_file, existing_ids, dlg=None):
    """
    When given a GWAS catalogue download file, this filters the file
    for GWAS catalogue IDs (hashes) that are not present in existing
    IDs, it also adds some addional data on the download date and
    a md5hash of the record

    Parameters
    ----------
    parent : :obj:`gui_main.MainFrame`
        The main frame of the application
    download_file :`str`
        The path the the download file to check
    existing_data :`list` of `str`
        The rows existing gwas catalog accession numbers.
    dlg : :obj:`dialogs.UpdateProgressDialog` (optional)
        A dialog to indicate import progress. If not supplied one is created
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("filtering existing IDs...")

    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Existing IDs id a tuple of (accession, hash), I am going to use the
    # combination of both to determine what needs updating, the hash will be
    # the main one but, if we have loads of new hashes tied to existing
    # accessions then this is potentially problematic at it means that there
    # have been large scale changes to the file that potentially make it
    # useless
    # The * operator with zip essentially does the inverse
    existing_accessions = set([])
    existing_hashes = set([])

    # When a record is set aside for importing we will store it's MD5 hash in
    # here, I have noticed there is some duplicated MD5s in the GWAS catalog
    # so I want to make sure that they do not make it in
    seen_hashes = set([])

    # Zip fails if the existing IDs is empty
    if len(existing_ids) > 0:
        # list(zip(*existing_ids))
        existing_accessions, existing_hashes = list(zip(*existing_ids))
        existing_accessions = set(existing_accessions)
        existing_hashes = set(existing_hashes)

    # Will hold the number of records that will need adding
    update_count = 0

    # TODO: Remove for now
    # updated_with_existing_accession = 0
    fd, ofn = tempfile.mkstemp()

    # Calculate how many rows were downloaded
    with open(download_file, 'r') as infile:
        downloaded_rows = sum([1 for i in infile]) - 1

    # (re-)initialise the dialog to update the progress
    dlg.SetRange(downloaded_rows, con.UPDATE_DIALOG_STEPS)
    # dlg.calls = min(con.UPDATE_DIALOG_STEPS, downloaded_rows)

    # So we start by opening the download file and seeing what rows need
    # importing into the raw data
    with open(download_file, 'r') as csvin:
        reader = csv.reader(csvin, delimiter="\t")
        header = next(reader)

        # Make sure the header is what we expect
        if header != con.EXP_HEADER:
            raise ValueError("incorrect header in GWAS catclogue download")

        # Open the TEMP file that will hold the records to be imported
        with os.fdopen(fd, "w") as csvout:
            writer = csv.writer(csvout, delimiter="\t")
            writer.writerow(header + ['phen_validated',
                                      'pop_validated',
                                      'assoc_validated',
                                      'modified_epoch_date',
                                      'modified_by',
                                      'displayed',
                                      'md5hash'])

            # Get the update time
            update_time = con.epoch_time()

            for count, row in enumerate(reader):
                dlg.Update(count, "Filtering...", suffix="rows")

                # Get the MD5 sum of the row
                md5hash = hashlib.md5(
                    con.MD5_JOIN.join(row).encode('utf-8')).hexdigest()
                # mapped_row = dict([(header[c], i) for c, i in enumerate(row)])

                if md5hash not in existing_hashes and md5hash not in seen_hashes:
                    # Is the accession present even if the hash is not
                    # if mapped_row['STUDY ACCESSION'] in existing_accessions:
                    #     updated_with_existing_accession += 1

                    writer.writerow(row + [0, 0, 0,
                                           update_time, parent.user_email,
                                           1, md5hash])
                    update_count += 1
                    seen_hashes.add(md5hash)

        # Now make sure that we have not got too many existing accessions
        # if updated_with_existing_accession > c.ALLOW_EXISTING_ACCESSION:
        #     raise RuntimeError("there are '{0}' fors where the accession is"
        #                   "already in the database, these may be large "
        #                  "updates to the GWAS catalogue, at present we "
        #                  "can't handle this!".\
        # format(updated_with_existing_accession))
    return ofn, update_count


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_pubmed_ids(parent, pubmed_ids=None, dlg=None):
    """
    This controls the downloading and importing of pubmed data into the
    database. of the existing raw_data hashes in the
    database. These will allow the determination of records that we have and
    what will need to be added

    Parameters
    ----------
    parent : :obj:`gwas_catalog_parser.MainFrame`
        The main frame of the application
    pubmed_ids :`list` of `int` (optional)
        Pubmed IDs to search for. If it is None or 0 length then a database
        search for pubmed IDs that need updating is performed
    dlg : :obj:`UpdateProgressDialog` (optional)
        A dialog to indicate existing ID download progress. If not supplied one
        is created
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing pubmed IDs...")

    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # If the pmids list is None then we search for any pubmed IDs that needs
    # importing
    if pubmed_ids is None or len(pubmed_ids) == 0:
        pubmed_ids = [i for i in parent.gwas_cat_db.get_missing_pubmed_ids()]
    # print(pubmed_ids)
    if len(pubmed_ids) > 0:
        msg.msg("Downloading pubmed data...{0} IDs".format(len(pubmed_ids)))
        print(msg, file=sys.stderr)

        # The pubmed records will be downloaded in batches and inserted into
        # the database, so we will update the progress meter to reflect the
        # batches. We will look up the mesh data separately to get CUIs
        batches = list(range(0, len(pubmed_ids), con.PUBMED_DOWNLOAD_SIZE))
        msg.msg("{0} pubmed batches...".format(len(batches)))

        dlg.SetRange(len(batches), calls=1)

        # Open the file to write the MeSH heading to
        # with os.fdopen(fd) as csvout:
        for counter, i in enumerate(batches):
            dlg.Update(counter+1, "Searching Pubmed IDs...", suffix="batches")

            # Extract the batch of pubmed IDs that we need
            start = i
            end = min(i + con.PUBMED_DOWNLOAD_SIZE-1, len(pubmed_ids))
            pq = pubmed_util.PubmedIdQuery(parent.user_email,
                                           pubmed_ids[start:end],
                                           verbose=False)
            pq.search()
            pq.fetch()
            res = pq.get_results()

            # This will hold the pubmed data for a single batch
            pubmed_data = []
            mesh_data = []

            dlg.Update(counter+1, "Importing Searching for MeSH IDs...",
                       suffix="batches")
            msg.msg("search batch {0}...{1}".format(counter+1, len(res)))
            # Loop through the results and write the pubmed data to the
            # database, then look up all the mesh headers to get CUIs
            for c, j in enumerate(res):
                main_data = j.get_main_fields()

                # We grab the mesh terms and store them then we replace the
                # mesh terms with the number of mesh terms
                try:
                    main_data['nmesh_terms'] = len(main_data['mesh_terms'])
                except TypeError:
                    main_data['nmesh_terms'] = 0

                try:
                    # Loop through all the MeSH terms and look them up in the
                    # UMLS to get CUIs
                    for m in main_data['mesh_terms']:
                        cuis = parent.umls_db.match_uniq_term_from_term(m[0])

                        for i in cuis:
                            mesh_data.append(
                                {'pubmed_id': main_data['pubmed_id'],
                                 'mesh_term': m[0], 'modifiers': m[1],
                                 'major_term': m[2], 'cui': i.cui,
                                 'term': i.term, 'ncui': i.ncui,
                                 'probcui': i.probcui,
                                 'semantic_types': i.semantic_types,
                                 'all_terms': i.all_terms})
                except TypeError:
                    pass

                del main_data['mesh_terms']
                pubmed_data.append(main_data)

            dlg.Update(counter+1, "Updating the database...",
                       suffix="batches")

            # pp.pprint(mesh_data)
            # If we get to here then we have processed the batch so we want to
            # update the dialog and write to the database
            parent.gwas_cat_db.insert_pubmed_record(pubmed_data)
            parent.gwas_cat_db.insert_mesh_record(mesh_data)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_association_data(parent, dlg=None):
    """
    Process the association data from the raw download. This will only process
    raw_download_ids that do not appear in the associations table

    Parameters
    ----------

    parent : :obj:`gwas_catalog_parser.MainFrame`
        The main frame of the application
    dlg : :obj:`UpdateProgressDialog` (optional)
        A dialog to indicate existing ID download progress. If not supplied one
        is created
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing association data, this may take some time...")

    # Generate a dialog if none has been passed
    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Check that the assembly of the REST connection matches any rest
    # assemblies that have been used before i.e. we have not mixed up GRCh37
    # and GRCh38
    check_ensembl_assembly(parent.gwas_cat_db, parent.ensembl_rest)

    # Now we query the database to determine how many records we will need to
    # process, this is used to setup the progress dialog box
    missing_assoc = [a for a in parent.gwas_cat_db.get_missing_associations()]
    nrows = len(missing_assoc)
    msg.msg("need to search for {0} associations...".format(nrows))

    if nrows > 0:
        dlg.SetRange(nrows, calls=1)

        row_buffer = []

        # process outstanding associations
        for counter, assoc in enumerate(missing_assoc):
            row_buffer.append(assoc)

            if (counter+1) % MAX_POST_SIZE == 0:
                dlg.Update(counter+1, "parsing...", suffix="assocs")
                _process_assoc_batch(parent, row_buffer)
                row_buffer = []

        # Finally, if there are records in the row buffer then, make sure they
        # are processed
        if len(row_buffer) > 0:
            dlg.Update(counter+1, "parsing...", suffix="assocs")
            _process_assoc_batch(parent, row_buffer)
            row_buffer = []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_ensembl_assembly(db, rc):
    """
    Checks that the assembly in the database is comparable with the Ensembl
    assembly

    Parameters
    ----------

    db : :obj:``
        The interface to the gwas_catalog database
    rc : :obj:`EnsemblRestClient`
        The ensembl rest client to query for the assembly
    """

    # Get the assembly from the rest client
    rest_assembly = rc.get_assembly('human')

    # Get the assembly from the database
    gwas_cat_assembly = db.get_ensembl_assembly_option()

    # If the assembly in the database has been set, i.e. this is not a first
    # time initialisation
    if gwas_cat_assembly is not None:
        # strip any patches from the assembly names and compare assembly
        ra = re.sub(r'\.p\d+$', '', rest_assembly['assembly_name'])
        gca = re.sub(r'\.p\d+$', '', gwas_cat_assembly)

        if ra != gca:
            raise ValueError(
                "the rest assembly '{0}' needs to be the same as the "
                "gwas_catalog database assembly '{1}'".format(ra,
                                                              gca))
    else:
        # Insert an assembly into the database
        db.update_ensembl_assembly_option(rest_assembly['assembly_name'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_assoc_batch(parent, batch):
    """
    Processes a batch of association data into the database

    Parameters
    ----------

    parent : :obj:`gwas_catalog_parser.MainFrame`
        The main frame of the application
    batch :`list` of `dict`
        The associations to process and import into the database
    """

    # These will hold the rows that need to be inserted into the database
    assoc_insert = []

    # Parse the associations, parse associations yields processed association
    # records
    for pa in parsers.parse_associations(parent.ensembl_rest,
                                         batch):
        assoc_var_insert = []
        # Generate an associtation row for inserting into the database
        # This is just for code simplicity when defining the row
        pa['analysis_id'] = None

        # We initialise the association as validated and then unvalidate
        # when we encounter problems
        assoc_validated = 1

        # The associated variant row is a bit more tricky due to the potential
        # 1->many relationship, so we start by looping, through the
        # proc_variants field as this will contain both ensembl mapped variants
        # and unmapped variants
        try:
            # We loop through all the variants that we processed in the
            # association record from the GWAS catalog, not all of these will
            # have been mapped to Ensembl
            for proc_var in pa['proc_variants']:
                # We initialise the row with the raw download ID
                assoc_var = [pa['raw_download_id']]

                # This is the validation state fot the associated variant, we
                # initialise to unvalidated and then validate if we can find an
                # Ensembl mapping
                var_validated = 0

                # This will hold the number of sites for each variant
                # (i.e. ambigious mappings in the genome). Whilst we only store
                # one, this can be used as a flag to highlight problematic
                # variant sites
                nsites = None

                try:
                    # Now attempt to find an Ensembl mapping for the current
                    # processed variant. iIf there is not one then this will
                    # generate a KeyError
                    ens_var = pa['ensembl_variants'][proc_var[0]]

                    # Update the number of sites (genomic mappings) for the
                    # variant
                    nsites = len(ens_var['mappings'])

                    # We only use the first mapping for the variant I may
                    # change this in future to ensure that we do not have
                    # variant regions or even lift over the GWAS catalog one
                    # Also, checking that the effect allele is in the mapping
                    # allele string is probably a good idea
                    mapping = ens_var['mappings'][0]
                    alleles = variants.process_alleles(
                        mapping['allele_string'])
                    uni_id = variants.get_uni_id(
                        mapping['seq_region_name'],
                        mapping['start'],
                        alleles)
                    assoc_var = {'uni_id': uni_id,
                                 'var_id': ens_var['name'],
                                 'chr_name': mapping['seq_region_name'],
                                 'start_pos': mapping['start'],
                                 'end_pos': mapping['end'],
                                 'strand': mapping['strand'],
                                 'ref_allele': alleles[0],
                                 'alt_alleles': "/".join(alleles[1:]),
                                 'minor_allele': ens_var['minor_allele'],
                                 'global_maf': ens_var['MAF'],
                                 'worst_consequence':
                                 ens_var['most_severe_consequence'],
                                 'nsites': nsites}
                    effect_allele = ens_var['effect_allele']

                    # if we do not have an effect allele then we unvalidate the
                    # association
                    if effect_allele is None:
                        assoc_validated = 0

                    # If we get here then the variant is validated
                    var_validated = 1
                except (KeyError, IndexError):
                    # All the Ensembl mappings are None
                    assoc_var = {'uni_id': None,
                                 'var_id': None,
                                 'chr_name': None,
                                 'start_pos': None,
                                 'end_pos': None,
                                 'strand': None,
                                 'ref_allele': None,
                                 'alt_alleles': None,
                                 'minor_allele': None,
                                 'global_maf': None,
                                 'worst_consequence': None,
                                 'nsites': None}
                    assoc_validated = 0
                    effect_allele = proc_var[1]

                # Now finish updating the row and add it to the import batch
                assoc_var['chr_name_cat'] = pa['chr_name_cat']
                assoc_var['start_pos_cat'] = pa['start_pos_cat']
                assoc_var['listed_var_id'] = proc_var[0]
                assoc_var['effect_allele'] = effect_allele
                assoc_var['validated'] = var_validated
                assoc_var_insert.append(assoc_var)
        except TypeError:
            # 'proc_variants' could be None in which case there is nothing else
            # we can do other than set the association to unvalidated
            assoc_validated = 0

        # If the association is still validated at this stage, it means that
        # we managed to map all of the variants to Ensembl. However, that is
        # only part of the story we also need to make sure that we have a full
        # complement of effect and precision information, if not then we
        # unvalidate
        if assoc_validated == 1:
            for i in ['effect_size', 'standard_error', 'mlog_pvalue']:
                if pa[i] is None:
                    assoc_validated = 0
                    break

        # delete the fields that we do not want from the association
        del pa['ensembl_variants']
        del pa['proc_variants']
        del pa['chr_name_cat']
        del pa['start_pos_cat']
        pa['validated'] = assoc_validated
        pa['associated_variant'] = assoc_var_insert
        # Update the association data
        assoc_insert.append(pa)

    # Here we have finished processing a batch so we insert into
    # the database and move on to defining the next batch
    parent.gwas_cat_db.insert_association_record(assoc_insert)
    # parent.gwas_cat_db.insert_associated_variant_record(assoc_var_insert)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_publication_genes(parent, dlg=None):
    """
    Process the reported genes from the raw download. This will only process
    raw_download_ids that do not appear in the reported_genes table or entries
    where the reported gene has not been mapped to an Ensembl ID

    Parameters
    ----------

    parent : :obj:`gwas_catalog_parser.MainFrame`
        The main frame of the application
    dlg : :obj:`UpdateProgressDialog` (optional)
        A dialog to indicate existing ID download progress. If not supplied one
        is created
    """

    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing publication genes...")

    # Generate a dialog if none has been passed
    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Used to build the database insert row
    col_atts = [('raw_download_id', 'raw_download_id'),
                ('listed_gene_id', 'listed_gene_id'),
                ('id', 'ensembl_gene_id'),
                ('display_name', 'display_id'),
                ('description', 'description'),
                ('seq_region_name', 'chr_name'), ('start', 'start_pos'),
                ('end', 'end_pos'), ('strand', 'strand'),
                ('biotype', 'biotype')]

    # Check that the assembly of the REST connection matches any rest
    # assemblies that have been used before i.e. we have not mixed up GRCh37
    # and GRCh38
    check_ensembl_assembly(parent.gwas_cat_db, parent.ensembl_rest)

    # Now get the missing reported genes:
    # pub_genes = [i for i in parent.gwas_cat_db.get_missing_publication_gene()
    #              if i['listed_gene_id'] is None]
    pub_genes = [i for i in parent.gwas_cat_db.get_missing_publication_gene()]
    msg.msg("need to import {0} publication genes...".format(len(pub_genes)))

    # If we have some genes to search for
    if len(pub_genes) > 0:
        # Make sure the dialog is set up correctly
        dlg.SetRange(len(pub_genes), calls=1)
        dlg.Update(0, "Mapping gene IDs...", suffix="entries")

        # This gets the unique genes
        unique_genes = parsers.get_unique_publication_genes(pub_genes)

        gene_buffer = []

        # Keeps track of the number of IDs that have been returned this is so
        # the progress monitor does not overrun
        returned_ids = set()
        for counter, pg in enumerate(
                parsers.search_genes(parent.ensembl_rest, unique_genes)):

            insert_record = {}
            for m, r in col_atts:
                try:
                    insert_record[r] = pg[m]
                except KeyError:
                    insert_record[r] = None

            gene_buffer.append(insert_record)
            returned_ids.add(insert_record['raw_download_id'])

            if len(returned_ids) % 200 == 0:
                # pp.pprint(gene_buffer)
                dlg.Update(counter+1, "Mapping gene IDs...", suffix="entries")
                parent.gwas_cat_db.insert_publication_gene_record(gene_buffer)

                # pp.pprint(gene_buffer)
                gene_buffer = []

        if len(gene_buffer) > 0:
            dlg.Update(counter+1, "Mapping gene IDs...", suffix="entries")
            parent.gwas_cat_db.insert_publication_gene_record(gene_buffer)

            # pp.pprint(gene_buffer)
            gene_buffer = []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_full_data_download_locations(parent, dlg=None):
    """
    Import the full data download locations and attempt to link them to study
    pubmed IDs and potentially GWAS catalogue accessions. So this will download
    GWASAtlas data, Grasp Data and GWAS Catalogue data

    Parameters
    ----------
    parent : :obj:`gui_main.`
    """

    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing download_resources...")

    # Generate a dialog if none has been passed
    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Get all the available download resources so we can not import
    # resources that have already been downloaded. Note that this
    # does not check for any potential updates of the resources I will
    # have to implement that in future
    download_resources = \
        set([(r.pubmed_id, r.resource_name)
             for r in parent.gwas_cat_db.get_download_resources()])

    # Import the GRASP data
    import_grasp(parent, download_resources=download_resources)

    # Import the GWAS atlas download locations
    import_gwas_atlas(parent, download_resources=download_resources)

    # Import the GWAS catalogue download locations
    import_gwas_cat_full_summary(parent,
                                 dlg=dlg,
                                 download_resources=download_resources)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_grasp(parent, download_resources=set([])):
    """
    The links from the grasp website
    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing GRASP links...")

    to_import = []
    for r in gwas_database_download.download_grasp_summary():
        test = (r['pubmed_id'], r['resource_name'])

        if test not in download_resources:
            to_import.append(r)

        to_import.append(r)

    # Now import
    parent.gwas_cat_db.insert_download_file(to_import)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_gwas_atlas(parent, download_resources=set([])):
    """
    The GWAS Atlas download location import
    """

    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing GWAS atlas links...")

    download_dir = tempfile.mkdtemp()

    webdriver = gwas_database_download.initialise_chrome(
        driver_path=parent.webdriver,
        download_dir=download_dir)

    download_file = gwas_database_download.download_gwas_atlas_summary(
        webdriver, download_dir)

    # Shut down the browser
    webdriver.quit()

    # To import
    to_import = []
    for r in gwas_database_download.parse_gwas_atlas_summary(download_file):
        test = (r['pubmed_id'], r['resource_name'])

        if test not in download_resources:
            to_import.append(r)

    # Now import
    parent.gwas_cat_db.insert_download_file(to_import)

    # Delete the download directory
    shutil.rmtree(download_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_gwas_cat_full_summary(parent, dlg=None,
                                 download_resources=set([])):
    """

    """

    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing GWAS catalogue links...")

    # I know this is > than the numbers with full data
    # download_max = 2
    if dlg is None:
        dlg = dialogs.UpdateProgressDialog(parent)

    # Get a could of the full datasets
    ndata_sets = gwas_database_download.get_gwas_cat_full_data_count()

    dlg.SetRange(ndata_sets, calls=1)
    dlg.Update(0, "Getting GWAS catalogue full data listing...",
               suffix="entries")

    to_import = []
    seen_downloads = set([])
    for r in gwas_database_download.download_gwas_cat_full_summary(
            download_max=ndata_sets):
        # if we have not seen this study before then increment the
        # dialog
        if r['notes'] not in seen_downloads:
            dlg.Update(len(seen_downloads)+1,
                       "Getting GWAS catalogue data listing...",
                       suffix="entries")
            seen_downloads.add(r['notes'])

        test = (r['pubmed_id'], r['resource_name'])

        if test not in download_resources:
            to_import.append(r)

    # Now import
    parent.gwas_cat_db.insert_download_file(to_import)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_gwas_cat_abbreviations(parent):
    """

    """
    msg = progress.Msg(prefix=MSG_PREFIX, file=sys.stderr)
    msg.msg("importing GWAS catalogue abbreviations...")

    webdriver = gwas_database_download.initialise_chrome(
        driver_path=parent.webdriver,
        download_dir=os.environ['HOME'],
        headless=True)

    all_abv = set(list(parent.gwas_cat_db.get_all_abbreviations()))

    to_import = []
    for r in gwas_database_download.download_gwas_cat_abbreviations(webdriver):
        test = (r['abbreviation'], r['expanded'])

        if test not in all_abv:
            to_import.append(r)

    # Shut down the browser
    webdriver.quit()

    # Insert the abbreviations
    parent.gwas_cat_db.insert_abbreviation(to_import)
