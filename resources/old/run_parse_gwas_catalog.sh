#!/bin/bash
OUTDIR=../data/test_output
TEST_FILE=../data/gwas_catalog_v1.0.2-associations_e96_r2019-04-06.tsv
mkdir -p "$OUTDIR"

# Demonstrates parsing the GWAS catalogue
./parse_gwas_catalog.py -v "$OUTDIR" "$TEST_FILE"

echo "*** END ***"
