#!/usr/bin/env python
"""
Test REGEX for the ci text column in the GWAS catalog
"""
import re
import pprint as pp


def process_ci_text(i):
    ci_match = re.match(r'^\s*\[?(\d+(?:\.\d+))\s*-\s*(\d+(?:\.\d+))\]?', i)
    original = i
    try:
        ci_lower = float(ci_match.group(1))
        ci_upper = float(ci_match.group(2))
        i = re.sub(r'^\s*\[?(\d+(?:\.\d+))\s*-\s*(\d+(?:\.\d+))\]?', '', i)
        se = (ci_upper - ci_lower) / 3.92
    except AttributeError:
        ci_lower = None
        ci_upper = None
        se = None

    unit_match = re.search(r'(.*?)(increase|decrease)', i)
    try:
        units = unit_match.group(1).strip()
        direction = unit_match.group(2)
        i = re.sub(r'(.*?)(increase|decrease)', '', i)

        if units == '':
            units = None
    except AttributeError:
        units = None
        direction = None

    i = re.sub(r'[\(\)]', '', i.strip())
    return (original, units, direction, ci_lower, ci_upper, se, i)


x = ['[1.59-3.83] percentage SD increase', 'unit increase', 'unit',
     '[16.174-22.372] iu/ml increase GCTG', '1.62-1.8', '[1.61-3.12]',
     '[1.41-2.29] (GENEVA cohort)', '[1.41-3.49] ms increase',
     '[12.4-24.67] beats per minute increase',
     '[1.2-2.6] mm Hg increase in DBP', '[1.1-1.25] (BRCA1 carriers)',
     '[0.42-2.38] μmol/l decrease (replication)',
     'decrease']

for i in x:
    pp.pprint(process_ci_text(i))
    print("")
