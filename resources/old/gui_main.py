"""
GUI components for the GWAS catalog GUI parser
"""
from gwas_catalog_parser import dialogs, gwas_catalog_db, initialise_db
import wx
# import MySQLdb
# import sys


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ErrorFrame(wx.Frame):
    """
    This is a frame that is launched in case any errors happen during program
    initiation, i.e. before the main frame is set. It exists purely to convey a
    fatal error dialog
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, error_message):
        """
        Initialise

        Parameters
        ----------

        error_message : :obj:``
            A message to error out with
        """

        # wx.Frame.__init__(self, None, -1, "Ontology Mapper", size=(1400,1000))
        wx.Frame.__init__(self, None, -1,
                          "Initialise Error", size=(1400, 1000))

        dialogs.handle_fatal_error(
            self, "Initialisation error: {0}".format(error_message))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MainFrame(wx.Frame):
    """
    The main frame that holds the application, I have written a custom one that
    inherits from wx.Frame. It is called with the formatted data,program
    arguments the column names for the data and a sqlalchemy connection to the
    ontology database
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, gwas_cat_db, umls_db, ensembl_rest, user_email,
                 metamap=None, webdriver=None):
        """
        Initialise

        Parameters
        ----------

        gwas_cat_db : :obj:``
            An object for interacting with the GWAS catalog database
        umls_db : :obj:``
            An object for interacting with the UMLS database
        cache_db : :obj:``
            An object for interacting with the SQLite cache
        ensembl_rest :obj:`EnsemblRestClient`
            An Ensembl REST client object
        email :`str`
            The user E-mail address, used in pubmed queries
        metamap :`umls.MetamapServer` (optional)
            An interface to the metamap server. If this is not provided then
            metamap suggestions are disabled
        """

        # Store the parameters
        self.gwas_cat_db = gwas_cat_db
        self.umls_db = umls_db
        # self.cache_db = cache_db
        self.ensembl_rest = ensembl_rest
        self.user_email = user_email
        self.metamap = metamap
        self.webdriver = webdriver

        # Indicate if metamap is available, will be disabled in the menus
        self.have_metamap = False
        if self.metamap is not None:
            self.have_metamap = True

        # wx.Frame.__init__(self, None, -1, "Ontology Mapper", size=(1400,1000))
        wx.Frame.__init__(self, None, -1,
                          "GWAS catalog Annotate", size=(1400, 1000))

        # Initialise the status bar (pre loading of data)
        self._init_status_bar()

        # Now we want to download the base data for the overall display grid
        # if there is no data in the database then the download will be
        # initialised
        self._init_data()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_data(self):
        """
        Initialise the data for the main data grid in the application, this
        involves downloading it from the database and if there is no data in
        the database then initialising it from the GWAS catalog download
        """

        # First see if the database is initialised and also get the count of
        # the rows in the database. If initialisation is False or the row count
        # is 0 then we will need to initialise the data
        is_init = self.gwas_cat_db.get_initialised_option()
        gwas_cat_counts = self.gwas_cat_db.get_raw_record_count()

        if is_init is False or gwas_cat_counts == 0:
            # Inform the user of initialisation
            dialogs.initialise_dialog(self)

            # We perform a full data initialisation
            # Here I am going to catch any error, this is a bit lazy but really
            # any error that occurs is going to be problematic
            # try:
            initialise_db.initialise_gwas_cat_data(self)
            # except Exception as e:
            #     dialogs.handle_fatal_error(
            #         self,
            #         "There was a import error: {0}".format(str(e)))

        # First make sure the cache database is OK and does not need updating
        # gwas_catalog_db.check_cache(self)

        # Use the database connector to get all the data
        # try:
        # data = gwas_catalog_db.download_database_raw_data(self)
        # except MySQLdb.Error as e:
        #     dialogs.handle_fatal_error(
        #         self,
        #         "There was a database error: {0}".format(str(e)))
        # # Now get the initialisation value. This indicates that there were
        # # potentially problems, with a previous initialisation of the database
        # # i.e. not all records were downloaded etc...If this was the case
        # # then we want to try again and import anything that did not get
        # # imported, peviously
        # is_init = self.gwas_cat_db.get_initialised_option()
        # is_init = self.gwas_cat_db.set_initialised_option(True)
        # is_init = True # Temp overide
        # # If initialisation has not been fully performed then we re-initialise
        # # note that this will only initialise stuff that has not been done
        # # already

    # #########################################################################
    # ######### METHODS FOR INITIALISING COMPONENTS OF THE MAIN FRAME #########
    # #########################################################################

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_status_bar(self):
        """
        Initialise the Statusbar at the bottom of the main frame
        """

        # Create the custom status bar and assign it to the frame
        # it will initialise with loading...
        self.status_bar = MainStatusBar(self)
        self.SetStatusBar(self.status_bar)

        # Now make sure that the user email and the database connection are set
        self.status_bar.set_email(self.user_email)
        self.status_bar.set_database(self.gwas_cat_db.session)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MainStatusBar(wx.StatusBar):
    """
    A status bar for the application, I have subclassed it so we can set the
    various fields in it a bit easier
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, parent):
        """
        Initialise

        Parameters
        ----------

        parent : :obj:`wx.Frame`
            The frame that will display the status bar
        """
        wx.StatusBar.__init__(self, parent, -1)

        # This status bar has three fields
        self.SetFieldsCount(5)

        # Sets the three fields to be relative widths to each other.
        self.SetStatusWidths([-1, -1, -1, -1, -1])

        # Total records will be the denominator
        self.total_records = 0

        # Upon initialisation we set the status of all 0 field to Loading...
        self.SetStatusText("Loading Data...", 0)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_total_records(self, records):
        self.total_records = records

        # The 0 is the field number
        self.SetStatusText("Total records: %i" % records, 0)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_displayed_records(self, records):
        self.SetStatusText("Displayed records: {0}/{1}".format(
            records, self.total_records), 1)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_updated_records(self, records):
        self.SetStatusText("Updated records: {0}/{1}".format(
            records, self.total_records), 2)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_email(self, email):
        self.SetStatusText("user: {0}".format(email), 3)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_database(self, dbconn):
        self.SetStatusText(
            "db driver: {0}".format(dbconn.__class__.__module__), 4)
