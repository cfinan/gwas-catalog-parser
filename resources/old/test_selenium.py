"""
Tests for headless selenium to download files
"""

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import tempfile
import glob
import gzip
import csv
import pprint as pp

download_dir = tempfile.mkdtemp()

chrome_options = Options()
# Downloads do not work in headless mode - see here for potential workarounds
# https://github.com/TheBrainFamily/chimpy/issues/108
# chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--disable-gpu")

prefs = {"profile.default_content_settings.popups": 0,
         "download.default_directory": download_dir,
         "directory_upgrade": True}
chrome_options.add_experimental_option("prefs", prefs)

driver = webdriver.Chrome(options=chrome_options)

url = "https://atlas.ctglab.nl/"
page = driver.get(url)        #"LatestLink" is the link I gave you
# driver.find_element_by_class_name('enter').click()     #clicks yes
elements = driver.find_element_by_xpath('//h4[text() = "Database release"]')
download = elements.find_element_by_xpath('//a[@class="release" and contains(text(), ".txt.gz")]')

download.click()
print(driver.current_url)        #This shows that the link has changed to include "&enter=yes" at the 
print(page)

try:
    download_file = glob.glob(os.path.join(download_dir,'*.txt.gz'))[0]
except IndexError:
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_gwas_atlas_summary(fn):
    """
    parse the GWAS Atlas summary file
    """

    col_map = [('id', 'resource_id', str), ('PMID', 'pubmed_id', int),
               ('ChapterLevel', 'trait_area', str),
               ('SubchapterLevel', 'trait_subarea', str),
               ('Domain', 'trait_domain', str), ('Trait', 'trait', str),
               ('uniqTrait', 'uniq_trait', str),
               ('Genome', 'genome_assembly', str),
               ('Population', 'population', str),
               ('Consortium', 'consortium', str),
               ('Ncase', 'ncases', int), ('Ncontrol', 'ncontrols', int),
               ('N', 'nsamples', int), ('File', 'file_download', str),
               ('Website', 'website', str)]

    with gzip.open(fn, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        header = next(reader)
        for i in reader:
            row = dict([(header[idx], i[idx]) for idx in range(len(header))])

            import_row = {}
            for rc, ic, cast in col_map:
                row[rc] = row[rc].strip()
                if row[rc] == '':
                    row[rc] = None
                try:
                    import_row[ic] = cast(row[rc])
                except (TypeError, ValueError):
                    # Could be bad pubmed IDs or NoneTypes
                    import_row[ic] = None
                    pass

            if import_row['pubmed_id'] is None:
                pass
                #pp.pprint(import_row)


parse_gwas_atlas_summary(download_file)
