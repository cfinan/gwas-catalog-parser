#!/usr/bin/env python
"""
Test processing the reported genes with the ensembl rest API
"""
from ensembl_python_tools import rest
from pymisc import progress
import csv
import os
import re
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def lookup_gene(rc, gene, ids):
    """
    Find the ensembl gene ID for the reported gene

    Parameters
    ----------

    genes :`list` of `str`
        The genes to look up
    """
    ids = list(ids)

    try:
        gids = rc.get_ensembl_xref('human', gene, object_type="gene")
    except Exception:
        return [(i, gene, None) for i in ids]

    if len(gids) > 0:
        return [(rdid, gene, gid['id']) for gid in gids for rdid in ids]
    else:
        return [(i, gene, None) for i in ids]


infile = os.path.abspath('../data/reported_genes.txt')
outfile = os.path.abspath('../data/proc_reported_genes.txt')
rc = rest.EnsemblRestClient(server="http://grch37.rest.ensembl.org")

nulls = ['NR', 'NULL', 'intergenic', '']
# fobj = open(infile)
# nlines = sum([1 for i in fobj])
# print(nlines)
# fobj.close()

with open(infile) as csvin:
    reader = csv.reader(csvin, delimiter="\t")
    header = next(reader)

    gene_cache = {}

    for i in reader:
        # pp.pprint(i)
        rdid = i[0]
        genes = re.sub(r'\s+', '', i[1]).strip()
        if genes in nulls:
            genes = None

        try:
            genes = genes.split(',')
        except AttributeError:
            # writer.writerow(i + [''])
            continue

        for g in genes:
            try:
                gene_cache[g].add(rdid)
            except KeyError:
                gene_cache[g] = set()

# Progress
prog = progress.RemainingProgress(len(gene_cache))
with open(outfile, 'w') as csvout:
    writer = csv.writer(csvout, delimiter="\t")
    writer.writerow(header + ['ens_gene'])

    for k, v in prog.progress(gene_cache.items(),
                              default_msg="processing genes..."):        
        # Loop through all the genes
        for g in lookup_gene(rc, k, v):
            # print(g)
            writer.writerow([g[0], g[1], g[2]])
