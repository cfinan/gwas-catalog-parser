#!/bin/bash
mysql -uroot -p -D umls_2018ab --host=127.0.0.1 --port=3311 <<EOF > ../data/medra_diseases.txt
select C.CUI, C.STR, S.STY, R.CUI2 AS P, M.STR AS P_TERM
from MRCONSO C
JOIN MRSTY S
ON C.CUI = S.CUI
JOIN MRREL R
ON R.CUI1 = C.CUI
JOIN MRCONSO M
ON M.CUI = R.CUI2
WHERE C.SAB = "MDR" AND  C.TTY = "PT" AND STY IN ("Disease or Syndrome", "Mental or Behavioral Dysfunction") AND R.REL = "PAR" AND R.SAB = "MDR" AND M.SAB="MDR"
EOF
