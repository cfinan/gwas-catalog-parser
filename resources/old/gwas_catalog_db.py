"""
Database classes for interacting with the gwas catalog database and variou
caches
"""

from gwas_catalog_parser import gwas_database_download, parsers, constants as c, \
    dialogs, gwas_catalog_orm as gco
# from ensembl_python_tools.rest import MAX_POST_SIZE
# from ensembl_python_tools import variants
from merit.ensembl.rest import MAX_POST_SIZE
from merit.ensembl import variants

# Catch specific SQL Alchemy errors
from sqlalchemy.orm import exc
from sqlalchemy import or_

# Downloads are via a tempfile
import tempfile
import pprint as pp
import os
import sys
import sqlite3
import csv
import hashlib
import wx
import time
import re


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatalogDB(object):
    """
    methods for interacting with the database
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, session):
        """
        Initialise

        Parameters
        ----------

        conn : :obj:`Connection`
            A DBI complient database connection
        """

        self.session = session

        # Enseure all the tables are created
        gco.Base.metadata.create_all(
            self.session.get_bind())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_initialised_option(self):
        """
        Return the initialised option

        Returns
        -------
        initialised :`bool`
            True is initalised False is un-initialised
        """

        q = self.session.query(gco.Option.initialised).\
            filter(gco.Option.option_id == 1)

        try:
            # We only expect a single option
            return bool(q.one()[0])
        except exc.NoResultFound:
            # The option is not initialised, in which case initialise the row
            # and then return False to indicate that the data is not
            # initialised
            self.session.add(gco.Option(initialised=0))
            self.session.commit()
            return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_ensembl_assembly_option(self):
        """
        Return the Ensembl Assembly option

        Returns
        -------

        ensembl_assembly :str
            True is initalised False is un-initialised
        """

        q = self.session.query(gco.Option.ensembl_assembly).\
            filter(gco.Option.option_id == 1)

        try:
            # We only expect a single option
            return q.one()[0]
        except exc.NoResultFound:
            # The option is not initialised, in which case initialise the row
            # and then return False to indicate that the data is not
            # initialised
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_all_identifers(self):
        """
        Get GWAS catalog raw data identifiers, these are the accessions and the
        MD5 hashes of the rows

        Yields
        ------
        ids :`tuple`
            A tuple where the first element is the gwas_catalog_accession and
            the second element is a 32 character MD5 of the row
        """

        q = self.session.query(gco.RawDownload.gwas_catalog_accession,
                               gco.RawDownload.md5hash)

        for acc, md5_hash in q:
            yield acc, md5_hash

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_raw_record_count(self):
        """
        Get GWAS catalog raw data records

        Returns
        -------
        count :`int`
            The number of raw records in the raw_download table
        """
        return self.session.query(gco.RawDownload).count()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_missing_pubmed_ids(self):
        """
        Yield pubmed IDs in the GWAS catalog that do not have any pubmed data
        present in the pubmed table

        Yields
        ------

        pubmed_id :`int`
            Pubmed IDs in raw_download that do not have any data in the
            database
        """

        q = self.session.query(gco.RawDownload.pubmed_id).\
            outerjoin(gco.Pubmed,
                      gco.Pubmed.pubmed_id == gco.RawDownload.pubmed_id).\
            filter(gco.Pubmed.pubmed_id == None).\
            group_by(gco.RawDownload.pubmed_id)

        for i in q:
            yield i.pubmed_id

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_missing_associations(self):
        """
        Yield association data that is not represented in the association.
        By extension the associated_variant data is unlikely to be present and
        the publication_gene data.

        Yields
        ------

        association_fields : tuple
            The data fields from raw_download that arerelevent to the
            association
        """

        q = self.session.query(gco.RawDownload.raw_download_id,
                               gco.RawDownload.chr_id,
                               gco.RawDownload.chr_pos,
                               gco.RawDownload.strongest_snp_risk_allele,
                               gco.RawDownload.snps,
                               gco.RawDownload.risk_allele_frequency,
                               gco.RawDownload.p_value,
                               gco.RawDownload.pvalue_mlog,
                               gco.RawDownload.or_or_beta,
                               gco.RawDownload.ci_text,
                               gco.RawDownload.initial_sample_size,
                               gco.RawDownload.replication_sample_size).\
            outerjoin(
                gco.Association,
                gco.Association.raw_download_id == gco.RawDownload.raw_download_id).\
            filter(gco.Association.raw_download_id == None)

        for i in q:
            yield i

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_missing_publication_gene(self):
        """
        Yield association data that is not represented in the association.
        By extension the associated_variant data is unlikely to be present and
        the publication_gene data.

        Yields
        ------

        association_fields :`dict`
            The data fields relevent to the association
        """

        # We get the reported gene for anything where we have not been able
        # to download the Ensembl Gene ID or it is not present at all. In the
        # case where we have no entry the listed_gene_id will be NULL, if it
        # has been searched but is not present then it will be defined
        q = self.session.query(gco.RawDownload.raw_download_id,
                               gco.RawDownload.reported_genes,
                               gco.PublicationGene.listed_gene_id,
                               gco.PublicationGene.ensembl_gene_id).\
            outerjoin(
                gco.PublicationGene,
                gco.PublicationGene.raw_download_id == gco.RawDownload.raw_download_id).\
            filter(or_(gco.PublicationGene.raw_download_id == None,
                       gco.PublicationGene.ensembl_gene_id == None))

        for i in q:
            yield i

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_download_resources(self):
        """
        Yield a tuple of the pubmed_id and resource name for the available
        download locations, this can be used to determine if they have already
        been imported
        """
        q = self.session.query(gco.DownloadLinks.pubmed_id,
                               gco.DownloadLinks.resource_name)

        for i in q:
            yield i

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_all_abbreviations(self):
        """
        Yield a tuple the abreviation and it's expansion
        """
        q = self.session.query(gco.Abbreviations.abbreviation,
                               gco.Abbreviations.expanded)

        for i in q:
            yield i

    # #########################################################################
    # ######################## Database Inserters #############################
    # #########################################################################

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_raw_download_record(self, records):
        """
        Inserts a raw download record into the raw_download table

        Parameters
        ----------

        records :`list` of `tuple`
            Multiple GWAS catalog records. Each element in the list is a record
            that needs to be inserted. The order of the fields must appear in
            exactly the same order as they appear in the file with the
            validation fields, last updated on the end and a md5 hash.
        """

        cols = list(gco.RawDownload.__table__.columns)
        # print(cols)
        # print(type(cols))
        ncols = len(cols)

        for i in records:
            # print(len(i), len(gco.RawDownload.__table__.columns))
            kwargs = {}
            for idx in range(1, ncols):
                # print(cols[idx])
                kwargs[cols[idx].name] = i[idx-1]
            self.session.add(gco.RawDownload(**kwargs))
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_pubmed_record(self, records):
        """
        Inserts a set of pubmed records into the pubmed table

        Parameters
        ----------

        records :list of dict
            Multiple pubmed records. Each element in the list is a dict with
            the keys being database column names and the values being the data
        """

        for r in records:
            # Now issue the insert
            self.session.add(gco.Pubmed(**r))
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_mesh_record(self, records):
        """
        Inserts a set of mesh mappings from pubmed

        Parameters
        ----------

        records :list of dict
            Multiple Mesh records that are associated with a pubmed ID
        """
        for r in records:
            # Now issue the insert
            self.session.add(gco.Mesh(**r))
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_association_record(self, records):
        """
        inserts a set of association records into the association table

        Parameters
        ----------

        records :`list` of `tuple`
            Multiple association records. Each element in the list is a record
            that needs to be inserted. The order of the fields must appear in
            exactly the same order as they appear in the table.
        """
        # pp.pprint(records)
        for r in records:
            r['associated_variant'] = [gco.AssociatedVariant(**av)
                                       for av in r['associated_variant']]
            assoc = gco.Association(**r)
            # Now issue the insert
            self.session.add(assoc)
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_publication_gene_record(self, records):
        """

        """
        for r in records:
            # Now issue the insert
            self.session.add(gco.PublicationGene(**r))
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_download_file(self, records):
        """

        """
        for r in records:
            # Now issue the insert
            self.session.add(gco.DownloadLinks(**r))
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def insert_abbreviation(self, records):
        """

        """
        for r in records:
            # Now issue the insert
            self.session.add(gco.Abbreviations(**r))
        self.session.commit()

    # #########################################################################
    # ######################### Database Updaters #############################
    # #########################################################################

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update_initialised_option(self, initialised):
        """
        Update the initialised option

        Parameters
        ----------

        initialised :`bool`
            True is initalised False is un-initialised
        """

        # First get the initialised option row
        init_row = self.session.query(gco.Option).\
            filter(gco.Option.option_id == 1).one()
        init_row.initalised = int(initialised)
        self.session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def update_ensembl_assembly_option(self, assembly):
        """
        Update the initialised option

        Parameters
        ----------

        ensembl_assembly :str
            The ensembl assembly option
        """

        # First get the initialised option row
        init_row = self.session.query(gco.Option).\
            filter(gco.Option.option_id == 1).one()
        init_row.ensembl_assembly = assembly
        self.session.commit()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class MySQLGwasCatalogDB(GwasCatalogDB):
#     # create database gwas_catalog CHARACTER SET utf8 COLLATE utf8_unicode_ci;
#     BIND_PARAM = "%s"

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def create_study(self):
#         """
#         Check and create a study table
#         """
#         if super(MySQLGwasCatalogDB, self).create_study() is False:
#             sql = """
#                   CREATE TABLE study (
#                   study_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
#                   pubmed_id INT NOT NULL,
#                   other_id VARCHAR(50),
#                   gwas_catalog_accession VARCHAR(20),
#                   INDEX(pubmed_id),
#                   INDEX(gwas_catalog_accession))
#                   ENGINE=Aria"""
#             self.cur.execute(sql)



#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def create_study(self):
#         """
#         Check and create a study table
#         """
#         if super(SQLiteGwasCatalogDB, self).create_study() is False:
#             sql = """
#                   CREATE TABLE study (
#                   study_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
#                   pubmed_id INTEGER NOT NULL,
#                   other_id VARCHAR(50),
#                   gwas_catalog_accession VARCHAR(20))"""
#             self.cur.execute(sql)

#             # Helper for creating indexes
#             self.create_indexes('study', ['pubmed_id',
#                                           'gwas_catalog_accession'])


    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def get_raw_records(self, records=[]):
    #     """
    #     Get GWAS catalog raw data records

    #     Parameters
    #     ----------

    #     records :obj:`list` or `str` (optional)
    #         A list of GWAS catalog study accession IDs to search for. If this
    #         is not supplied then all of them are returned

    #     Yields
    #     ------

    #     record :`tuple`
    #         A record row from the database
    #     """

    #     sql = """
    #     SELECT * FROM raw_download
    #     """

    #     if len(records) > 0:
    #         # TODO: Untested
    #         params = ",".join(["%s"] * len(records))
    #         sql = "{0} WHERE study_accession IN({1})".format(sql, params)
    #         self.cur.execute(sql, tuple(records))
    #     else:
    #         self.cur.execute(sql)

    #     for i in self.cur:
    #         yield i

