#!/usr/bin/env python
"""
Test the parsing of the GWAS catalog abbreviations web page
"""

from gwas_catalog_parser import gwas_database_download

# gwas_database_download.download_gwas_cat_abbreviations()
# gwas_database_download.download_grasp_links()
gwas_database_download.download_cat_full_data_links()
