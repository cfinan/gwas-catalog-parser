#!/usr/bin/env python
"""
GUI tools to annotate the GWAS catalog and place it in a database
"""
from gwas_catalog_parser import gwas_catalog_db, gui_main, constants as c, utils
from umls import umls_query

import os
# import sys
import configparser
import argparse
import re
import hashlib

# Required python libraries
import pymysql
import wx
import pprint as pp
import sys


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == "__main__":
    # Version number
    version = 0.1

    # We have the opportunity to specify alternative configuration files
    parser = argparse.ArgumentParser(
        description="A tool to annotate the GWAS catalog")
    parser.add_argument('-c', '--config', type=str,
                        default=c.GWAS_CATALOG_CONFIG,
                        help="an optional alternative location for the gwas "
                        "catalog config file")
    parser.add_argument("-d", "--defaults", type=str, default=c.DB_DEFAULTS,
                        help="An optional alternative location for the "
                        "database defaults file that contains the "
                        "connection parameters")
    args = parser.parse_args()

    # Make sure the GWAS catalog root directory is created and the cache
    # database directory present
    utils.make_app_directory(c.GWAS_CATALOG_HOME)
    utils.make_app_directory(c.DB_CACHE_DIR)

    try:
        # Attempt to open the config file
        open(args.config).close()
    except FileNotFoundError:
        utils.launch_error_frame("Can't find config "
                                 "file expected at '{0}".format(args.config))

    config = configparser.ConfigParser()
    config.read(args.config)

    # Get the database connection info, if these fail then an error frame is
    # launched to inform the user
    umls_conn_info = utils.get_database_connection_info(config,
                                                        c.CONFIG_UMLS_SECTION,
                                                        c.CONFIG_UMLS_PREFIX)
    gwas_cat_db_conn_info = utils.get_database_connection_info(
        config,
        c.CONFIG_GWAS_CAT_SECTION,
        c.CONFIG_GWAS_CAT_PREFIX)

    # Now get the e-mail address, this is used to access pubmed remotely
    user_email = utils.get_section_value(config, c.CONFIG_USER_EMAIL)
    utils.check_email(user_email)

    # # Now get the ensembl REST server to use
    ensembl_rest = utils.get_section_value(config, c.CONFIG_ENSEMBL_REST)
    ensembl_rest = utils.get_rest(ensembl_rest)

    # Connect to the databases, if any of the connections fail the the error
    # frame is launched
    # Connect to UMLS, if it is an SQLite connection it must exist as the UMLS
    # database is not created on the fly
    umls_session = utils.get_database_connection(umls_conn_info,
                                                 c.CONFIG_UMLS_PREFIX,
                                                 must_exist=True)

    # Get the interface to the UMLS
    umls_db = umls_query.UmlsQuerySQLAlchemy(umls_session())

    # # The GWAS catalog database connection, if it is SQLite then it will be
    # # created from scratch if not already present
    gwas_cat_sess_maker = utils.get_database_connection(
        gwas_cat_db_conn_info,
        c.CONFIG_GWAS_CAT_PREFIX,
        must_exist=False)

    # get the database interaction object
    gwas_cat_db = gwas_catalog_db.GwasCatalogDB(gwas_cat_sess_maker())

    # Attempt to get the webdriver, this may fail if not setup, but we
    # allow that
    try:
        webdriver = config[c.GENERAL_SECTION][c.WEBDRIVER_OPTION]
    except KeyError:
        print("no webdriver configuarion found", file=sys.stderr)
        webdriver = None

    # gwas_cat_db = None
    # if gwas_cat_db_conn_type == c.DB_TYPE_MYSQL:
    #     gwas_cat_db = gwas_catalog_db.MySQLGwasCatalogDB(gwas_cat_conn)
    # elif gwas_cat_db_conn_type == c.DB_TYPE_SQLITE:
    #     gwas_cat_db = gwas_catalog_db.SQLiteGwasCatalogDB(gwas_cat_conn)
    # else:
    #     # Unknown type, we should have errored out before this but just in case
    #     launch_error_frame("database error: unknown database type"
    #                        " '{0}'".format(gwas_cat_db_conn_type))

    # # Now we open the connection cache get the cache for the specified database
    # # connection
    # try:
    #     conn_cache = gwas_catalog_db.DBConnectionCache()
    #     cache_db = conn_cache.get_conn_for_hash(gwas_cat_db_conn_hash)
    # except FileExistsError as e:
    #     # If the database can't be created we want to error out
    #     launch_error_frame("cache error: {0}".format(str(e)))
    # # except Exception as e:
    # #     # Just catch anything else:
    # #     launch_error_frame("unknown cache error: {0}".format(str(e)))

    # Now launch the GUI
    app = wx.App()

    # # TODO: temp set to None, will probably remove
    # cache_db = None
    # f = gui_main.MainFrame(gwas_cat_db, umls_db, cache_db,
    #                        ensembl_rest, user_email)
    f = gui_main.MainFrame(gwas_cat_db, umls_db, ensembl_rest, user_email,
                           webdriver=webdriver)

    f.Show()
    app.MainLoop()
