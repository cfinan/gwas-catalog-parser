#!/usr/bin/env python
import argparse
import sys
import os
import wx
import wx.grid as Grid
from wx.lib.itemspicker import ItemsPicker,IP_REMOVE_FROM_CHOICES,IP_DEFAULT_STYLE,EVT_IP_SELECTION_CHANGED,IP_SORT_CHOICES
import pprint
import csv
import re
from sqlalchemy import create_engine,MetaData,select,and_
from sqlalchemy.exc import ProgrammingError
from sqlite3 import Connection as SQLite3Connection

import nltk
from nltk.corpus import stopwords
import string
from collections import Counter

pp=pprint.PrettyPrinter()
#wx.lib.colourdb.updateColourDB()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Functions Here
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'''
A convienience function for reading the data in from a file
'''
def GetDataFromFile(args,ontology_options):
    # Will hold all the data read in from the file
    data=[]

    # Use CSV to read the file, note that this does not deal with unicode
    # you need unicode.csv for that
    with open(args.file,"r") as csvfile:
        # We are assuming that there is a tab delimiter and no quoting
        reader=csv.reader(csvfile,delimiter=args.file_delimiter)
        header=reader.next()
        stored_header=list(header)

        # We look into this column for existing terms
        onto_terms_idx=None
        try:
            # If we find an existing terms column then we make a note of
            # the column number and remove it from the stored_header (that will
            # end up being the column names on the grid)
            onto_terms_idx=header.index(args.existing_term_col)
            stored_header.pop(onto_terms_idx)
        except ValueError:
            pass

        # Also check to see if we have a # terms column is so strip it as this
        # data will be added back in by the application. '# Terms' is the number
        # of terms associated with a row
        nterm_idx=None
        try:
            nterm_idx=stored_header.index(mapvar.GRID_NTERM_COL)
            stored_header.pop(nterm_idx)
        except ValueError:
            pass

        # The # terms column will have now either been removed or is not present
        # so we now add it in so it will be the penaltimate column in the grid
        stored_header.append(mapvar.GRID_NTERM_COL)

        # 'Updated' is an indicator if the row has been manually updated and/or
        # bulk updated
        # We check if the update column if it is present in the file, if so
        # we will parse it and adjust the "Updated" status of the row, this is
        # to make sure the status is maintained during different sessions of 
        # annotating the same file. We will also ensure the the Update status
        # is on the end of each row so it is consistant during different executions
        # of the application
        update_idx=None
        try:
            update_idx=stored_header.index(mapvar.GRID_UPDATE_COL)
            stored_header.pop(update_idx)
        except ValueError:
            pass

        # as with the number of term column add it back at the end
        stored_header.append(mapvar.GRID_UPDATE_COL)

        # Same with the status column
        status_idx=None
        try:
            status_idx=stored_header.index(mapvar.GRID_STATUS_COL)
            stored_header.pop(status_idx)
        except ValueError:
            pass
        stored_header.append(mapvar.GRID_STATUS_COL)
        
        # We will populate sets of the ontologies and modifiers listed in the
        # file as some of these may not be present in the ontology database and
        # the program will need to know they exists (the job of ontology options)
        file_ontologies=set()
        file_modifiers=set()

        # Loop through each row in the file
        for row in reader:
            # This will hold all the ontology terms associated with the row
            all_terms=[]
           
            # Now we perform the same pop/append operations in the same order
            # as we did for the header, otherwise things will get mixed up
            # So first any ontology terms, if there are none then onto_terms_idx
            # Will be None and the operation below should generate a TypeError
            try:
                existing_terms=row.pop(onto_terms_idx)
                all_terms=ParseOntologyString(existing_terms,
                                              default_ontology=args.default_ontology,
                                              delimiter=args.ontology_delimiter)

                # loop through all the terms gathering modifiers and ontology names
                for t in all_terms:
                    file_ontologies.add(t.GetOntologyName())

                    # Will add modifier code when modifiers are implemented
            except TypeError:
                pass

            # Any "number of terms" columns are striped
            try:
                row.pop(nterm_idx)
            except TypeError:
                pass
                
            # Add our term count and an empty string to represent the data for
            # the '# Terms'
            row.append(len(all_terms))

            # our update column
            update_string=""
            try:
                update_string=row.pop(update_idx)
            except TypeError:
                pass
                
            # Although we potentially store a formatted update string here 
            # we will 'formally' add the and updates below, this will 
            # essentially overwrite this string, this step is included so
            # we do not generate any errors with the header being a different
            # length to the data
            row.append(update_string)

            status_string=mapvar.ROW_STATUS_NONE
            try:
                status_string=row.pop(status_idx)
            except TypeError:
                pass

            row.append(status_string)


            # Now create out ontology row
            row=OntologyRow(row,all_terms,stored_header,ontology_options)

            # Make sure any updates are added to the set
            if len(update_string)>0:
                row.updates=set([i.strip() for i in update_string.split(args.ontology_delimiter)])
                # Indicate that it has been updated in the past by setting the
                # update flag. This should be done through a method call
                row.mappings_changed=True
                row.BuildUpdateString()

            # Make sure the status of the row is set correctly it will default to
            # None (no status)
            row.SetStatus(status_string)

            # Sore the row are an ontology row which is just a container for the
            # row data and the ontology terms
            data.append(row)

    # Return the data and the header (i.e. the column names
    # for the grid)
    return data,stored_header,file_ontologies,file_modifiers

'''
Parses an ontology string into a list of ontology_data objects
'''
def ParseOntologyString(string,default_ontology="MESH",delimiter="|"):
    onto_objects=[]
    onto_list=string.split(delimiter)
    ont_name_regex=re.compile(r'\[\s*ONTOLOGY:\s*(.+?)\s*\]')
    term_id_regex=re.compile(r'\[\s*TERM_ID:\s*(.+?)\s*\]')
    term_name_regex=re.compile(r'\[\s*TERM_NAME:\s*(.+?)\s*\]')

    # Loop through the list and do some checks
    for ont in onto_list:
        ont=ont.strip()
        
        # if the string is empty move on:
        if len(ont)==0:
            continue

        # The ontology string should be encosed in []
        # if not then something is wrong
        if ont[0]!="[" or ont[-1]!="]":
            raise(RuntimeError("[fatal] problem parsing ontology string: %s" % ont))

        # Remove outer braces and any white space
        ont=ont[1:len(ont)-1].strip()

        # Now match for ontology name, term id, and term_name the bare minimum
        # is the term name
        try:
            onto_name=ont_name_regex.search(ont).group(1)
        except AttributeError:
            onto_name=default_ontology

        try:
            term_id=term_id_regex.search(ont).group(1)
        except AttributeError:
            term_id=None

        try:
            term_name=term_name_regex.search(ont).group(1)
        except AttributeError:
            raise(RuntimeError("[fatal] An ontology term must have a name at: %s" % ont))

        # if we get to here then we have enough data to create an ontology term
        # object 
        onto_objects.append(OntologyTerm(term_name,onto_name,id=term_id))
    return(onto_objects)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Classes Here
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'''
Some static variables that are used in the application. mapvar stands for 
mapper variables. This is so all the 'Fixed variables' that may be used at
various points in the program are all in one place.
'''
class mapvar(object):
    # These variables indicate the state that the GUI is in at any one time
    GRID_SINGLE_SELECTION=1
    GRID_MULTI_SELECTION=2
#    BASE_SEARCH_STATE=3
#    HIGHLIGHT_SEARCH_STATE=4
#    FILTER_SEARCH_STATE=6
#    HIGHLIGHT_FILTER_SEARCH_STATE=7

    # These are the options that are displayed in the bulk update dialog
    BU_TEST="Test"
    BU_APPEND="Append"
    BU_REPLACE="Replace"
    BU_REMOVE="Remove"

    # The options that dictate how a column will match during a bulk update
    BU_CASE_SENSITIVE='Case-sensitive'
    BU_CASE_INSENSITIVE='Case-insensitive'
    BU_EXACT_TEXT='Exact text'
    BU_CONTAINS_TEXT='Contains text'

    # A text string to indicate all columns in the application
    ALL_COLUMNS="All"

    # These are the w.InfoBar styles that are used for the infobars that are
    # displayed after the user has done a TEST or REAL bulk update
    IB_BU_TEST=wx.ICON_WARNING
    IB_BU_REAL=wx.ICON_ERROR

    # This is the grid row colour for TEST bulk update rows that match the 
    # serach criteria
#    attr=Grid.GridTableBase()
    GRID_BU_TEST_BACKGROUND_COLOUR="orange"
    GRID_BU_TEST_TEXT_COLOUR="black"
    GRID_BU_UPDATE_BACKGROUND_COLOUR="red"
    GRID_BU_UPDATE_TEXT_COLOUR="white"
    GRID_REGEX_SEARCH_BACKGROUND_COLOUR=wx.Colour(0,180,200,alpha=128)
    GRID_REGEX_SEARCH_TEXT_COLOUR="Black"

    GRID_STATUS_FINISHED_BACKGROUND_COLOUR=None
    GRID_STATUS_FINISHED_TEXT_COLOUR="light grey"

    GRID_STATUS_IGNORE_BACKGROUND_COLOUR=None
    GRID_STATUS_IGNORE_TEXT_COLOUR="light grey"

    GRID_STATUS_REVIEW_BACKGROUND_COLOUR=None
    GRID_STATUS_REVIEW_TEXT_COLOUR="red"

    GRID_STATUS_REFINE_BACKGROUND_COLOUR=None
    GRID_STATUS_REFINE_TEXT_COLOUR="blue"

    GRID_MANUAL_UPDATE_BACKGROUND_COLOUR="grey"
    GRID_MANUAL_UPDATE_TEXT_COLOUR="white"
    GRID_AUTO_UPDATE_BACKGROUND_COLOUR="light grey"
    GRID_AUTO_UPDATE_TEXT_COLOUR="black"


    # These are the extra columns that are added to the data to allow the data
    # to be sorted on run time parameters
    GRID_NTERM_COL="# Terms"
    GRID_UPDATE_COL="Update"
    GRID_STATUS_COL="Status"
    GRID_EXTRA_COLS=[GRID_NTERM_COL,GRID_UPDATE_COL,GRID_STATUS_COL]

    # This is the assumed ontology when previous ontologies are supplied to the 
    # program without any ontology tag (a string at the end)
    # of the term that is enclosed in []
    DEFAULT_ONTOLOGY="MESH"

    # Preexisting TERMS should be supplied to the program in flattened form with
    # this as the delimiter
    DEFAULT_ONTOLOGY_DELIMITER="|"

    # When the data is read in from a text file this is the default delimiter
    DEFAULT_FILE_DELIMITER="\t"

    # This is the default column name, which when present the preexisting terms 
    # will be extracted from
    DEFAULT_CURRENT_TERM_COL="TERMS"

    ROW_STATUS_FINISHED="Finished"
    ROW_STATUS_REVIEW="Review"
    ROW_STATUS_REFINE="Refine"
    ROW_STATUS_IGNORE="Ignore"
    ROW_STATUS_NONE="None"
    ROW_STATUS_LIST=[ROW_STATUS_FINISHED,ROW_STATUS_REVIEW,ROW_STATUS_REFINE,ROW_STATUS_IGNORE,ROW_STATUS_NONE]
    ROW_UPDATE_MANUAL="MANUAL"
    ROW_UPDATE_AUTO="AUTO"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# A representation of an ontology term
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''
A simple class that acts as a container for an ontology term
'''
class OntologyTerm(object):
    def __init__(self,value,ontology_name,trees=None,id=None):
        self.value=value
        self.ontology_name=ontology_name
        self.trees=trees
        self.id=id

    '''
    Useful for debugging
    '''
    def __str__(self):                                                                                                                                
        return "Ontology Name: %s, Term: %s ID: %s" % (str(self.ontology_name),str(self.value),str(self.id))                                                                  
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The three methods below are used to determine equality of objects
    # They are required as different term objects are created at different times
    # during execution but actually represent the same term, so the normal
    # python == != or set() operations will not work
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    == Compares the text/numeric attributes. NotImplemented acts as a fallback
    to the python == in the event that 2 objects of different classes are compared
    '''
    def __eq__(self,other):
        if isinstance(other,OntologyTerm):
            return self.value==other.value and self.id==other.id and self.ontology_name==other.ontology_name
        return NotImplemented

    '''
    != Basically calls __eq__ and inverts the sign
    '''
    def __ne__(self, other):
        result = self.__eq__(other)
        if result is NotImplemented:
            return result
        return not result

    '''                                                                                                                                               
    Sets use hashes to determine inclusion and for set operations                                                                                     
    so overide this as well                                                                                                                           
    see http://stackoverflow.com/questions/390250/elegant-ways-to-support-equivalence-equality-in-python-classes                                      
    '''                                                                                                                                               
    def __hash__(self):                                                                                                                               
        # for a simple case when all attributes ate text or numbers                                                                                   
        """Override the default hash behavior (that returns the id or the object)"""                                                                  
        return hash(tuple(sorted(self.__dict__.items()))) 

    '''
    Returns a formatted ontology value which is the term with
    the ontology it is from in [] brackets after it. This is
    called by the custom OntoPicker that I sub classed from 
    ItemPicker and is used to provide the name that is used
    in the OntoPicker widget
    --when searching for Vitamin D
    UnicodeEncodeError: 'ascii' codec can't encode character u'\xa0' in position 69: ordinal not in range(128)
    '''
    def GetPickerValue(self):
#        return "%s [%s]" % (str(self.value),str(self.ontology_name))
        return "%s [%s]" % (str(self.value.encode('utf8')),str(self.ontology_name.encode('utf8')))

    '''
    This returns an ontology string that is suitable for writing to a file that
    will be read in by the application (specifically the ParseOntologyString function)
    '''
    def GetWriteValue(self):
        id_value=""
        if self.id!=None:
            id_value="[TERM_ID:%s]" % str(self.id)
        return "[[ONTOLOGY:%s][TERM_NAME:%s]%s]" % (str(self.ontology_name),str(self.value),id_value)

    '''
    Return the ontology name for the term
    '''
    def GetOntologyName(self):
        return self.ontology_name


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# A row of data that will be mapped to ontologies
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''
Represents a row of data. The application works row-wise. This means that 
ontology terms are assigned to rows not cells/columns. This is just a 
container for the row data that is read in from the file/database and the
row ontology mappings. The ontology mappings should be a list of OntologyData
objects
'''
class OntologyRow(object):
    def __init__(self,
                 row_data,
                 ontology_mappings,
                 colnames,
                 ontology_options,
                 nterm_col=mapvar.GRID_NTERM_COL,
                 update_col=mapvar.GRID_UPDATE_COL
):
        self.row_data=row_data
        self.ontology_mappings=set(ontology_mappings)
        self.ontology_searches=set()
        self.colnames=colnames

        # Store the options object, this will dictate what ontology mappings and
        # stored ontology searches that the ontology row will provide when asked
        self.ontology_options=ontology_options
        
        # Check that the length of the column names is the
        # same as the rows
        if len(row_data)!=len(colnames):
            raise(RuntimeError("[fatal] column names length (%s) does not match row length (%i)" % (len(self.row_data),len(self.colnames))))

        # These are columns that are derived from the data not
        # necessarily suppled
        self.nterm_col=self.colnames.index(nterm_col)
        self.update_col=self.colnames.index(update_col)

        self.colour=[None]
        self.selected=False
        self.mappings_changed=False
        self.default_mappings_changed=self.mappings_changed
        self.default_mappings=set()

        # This keeps track of the types of updates doe to the 
        # i.e. MANUAL/BULK so they can be written on the grid
        # and be used for sorting subsetting data etc
        self.updates=set()

        # Default
        self.status=mapvar.ROW_STATUS_NONE

        self.bulk_update_test_hit=False
        self.bulk_update_hit=False
        self.regexp_search_hit=False

    '''
    Set the status of the row
    '''
    def SetStatus(self,status):
        self.status=status
        
        # And modify the actual data in the row
        self.row_data[self.colnames.index(mapvar.GRID_STATUS_COL)]=self.status

    '''
    Get the status of the row
    '''
    def GetStatus(self):
        return self.status

    # '''
    # Set the colour of the row, used to colour the row in the event of
    # a test update
    # '''
    # def AppendColour(self,colour=None):
    #     self.colour.append(colour)
    
    # '''
    # This essentially clears the colour stack and resets it to the default
    # colour and the new colour that is passed
    # '''
    # def SetColour(self,colour=None):
    #     self.colour=[None,colour]

    # '''
    # Remove the last colour from the colour stack
    # '''
    # def PopColour(self):
    #     if len(self.colour)>1:
    #         self.colour.pop(-1)

    # '''
    # Get the colour of the row
    # '''
    # def GetColour(self):
    #     # Irrespective of the highlight colours that have been applied to
    #     # a frozen row, if someone asks them for a colour it will always
    #     # be None (which hopefully be interpreted as the default background)
    #     if self.frozen==True:
    #         return None
    #     return self.colour[-1]

    '''
    Return a tuple of (background colour,text colour) of the row depending
    on it's search status, update status or row status. Returns None if
     none of these apply, this should be interpreted as the grid default 
    background colours
    '''
    def GetColours(self):
        if self.regexp_search_hit==True:
            return (mapvar.GRID_REGEX_SEARCH_BACKGROUND_COLOUR,mapvar.GRID_REGEX_SEARCH_TEXT_COLOUR)
        elif self.bulk_update_test_hit==True:
            return (mapvar.GRID_BU_TEST_BACKGROUND_COLOUR,mapvar.GRID_BU_TEST_TEXT_COLOUR)
        elif self.bulk_update_hit==True:
            return (mapvar.GRID_BU_UPDATE_BACKGROUND_COLOUR,mapvar.GRID_BU_UPDATE_TEXT_COLOUR)
        elif self.status==mapvar.ROW_STATUS_FINISHED:
            return (mapvar.GRID_STATUS_FINISHED_BACKGROUND_COLOUR,mapvar.GRID_STATUS_FINISHED_TEXT_COLOUR)
        elif self.status==mapvar.ROW_STATUS_IGNORE:
            return (mapvar.GRID_STATUS_IGNORE_BACKGROUND_COLOUR,mapvar.GRID_STATUS_IGNORE_TEXT_COLOUR)
        elif self.status==mapvar.ROW_STATUS_REVIEW:
            return (mapvar.GRID_STATUS_REVIEW_BACKGROUND_COLOUR,mapvar.GRID_STATUS_REVIEW_TEXT_COLOUR)
        elif self.status==mapvar.ROW_STATUS_REFINE:
            return (mapvar.GRID_STATUS_REFINE_BACKGROUND_COLOUR,mapvar.GRID_STATUS_REFINE_TEXT_COLOUR)
        elif mapvar.ROW_UPDATE_MANUAL in self.updates:
            return (mapvar.GRID_MANUAL_UPDATE_BACKGROUND_COLOUR,mapvar.GRID_MANUAL_UPDATE_TEXT_COLOUR)
        elif mapvar.ROW_UPDATE_MANUAL in self.updates:
            return (mapvar.GRID_AUTO_UPDATE_BACKGROUND_COLOUR,mapvar.GRID_AUTO_UPDATE_TEXT_COLOUR)
        return None
        
    '''
    Bulk update test_hit
    '''
    def SetBulkUpdateTestHit(self,hit):
        self.bulk_update_test_hit=hit

    def SetBulkUpdateHit(self,hit):
        self.bulk_update_hit=hit
        
    def SetRegexpHit(self,hit):
        self.regexp_search_hit=hit

    '''
    Return True or False depending on if the ontology mappings have been chnaged
    '''
    def HaveMappingsChanged(self):
        return self.mappings_changed

    '''
    Sets the ontology mappings for the row. This is not quite a straight forward
    as just assigning the variable. The mappings list will come from either a 
    bulk update or an item picker selection change. However, we do not want to
    loose any mappings that are not currently visible (i.e. they have been 
    removed from view in the options). so we workout the visible mappings and 
    compare the mappings list to that so we only add mappings that are new and 
    remove mappings that WERE visable but are not there anyone because the user
    has removed them from the picker or update.
    '''
    def SetOntologyMappings(self,mappings_list,update_type="MANUAL"):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        all_terms=self.GetAllOntologyMappings()
        viewable_terms=set(self.GetOntologyMappings())
        picked_terms=set(mappings_list)

        self.mappings_changed=True
        self.ontology_mappings=self._FilterMappingsSets(all_terms,viewable_terms,picked_terms)
        self.updates.add(update_type)
        self.BuildUpdateString()

    '''
    Update mappings WITHOUT considering the global display options
    '''
    def SetAllOntologyMappings(self,mappings_list,update_type="MANUAL"):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        self.mappings_changed=True
        self.ontology_mappings=set(mappings_list)
        self.updates.add(update_type)
        self.BuildUpdateString()

    '''
    Set the ontology searches taking global options into consideration. see 
    comments for SetOntologyMappings
    '''
    def SetOntologySearches(self,mappings_list):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        all_terms=set(self.GetAllOntologySearches())
        viewable_terms=set(self.GetOntologySearches())
        picked_terms=set(mappings_list)
        self.ontology_searches=self._FilterMappingsSets(all_terms,viewable_terms,picked_terms)

    '''
    Update mappings WITHOUT considering the global display options
    '''
    def SetAllOntologySearches(self,mappings_list):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        self.ontology_searches=set(mappings_list)

    '''
    Determines which terms can be updated/removed, this is farmed out to a 
    separate function as it is used by the ontology searches and the ontology
    mappings (although the searches are not as important)
    '''
    def _FilterMappingsSets(self,all_terms,viewable_terms,picked_terms):
        remove_terms=viewable_terms.difference(picked_terms)
        add_terms=picked_terms.difference(viewable_terms)
        all_terms=all_terms.difference(remove_terms)
        all_terms=all_terms.union(add_terms)        
        return all_terms

    '''
    Will add ontology mappings to existing mappings 
    '''
    def AppendOntologyMappings(self,mappings_list,update_type=mapvar.ROW_UPDATE_MANUAL):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        start_len=len(self.ontology_mappings)
        # make sure the mappings_list is a set and then create a union    
        self.ontology_mappings=self.ontology_mappings.union(set(mappings_list))

        if start_len!=len(self.ontology_mappings):
            self.updates.add(update_type)
            self.BuildUpdateString()
            self.mappings_changed=True

    '''
    Remove any mapings in the mappings_list from the Ontology mappings
    '''
    def RemoveOntologyMappings(self,mappings_list,update_type=mapvar.ROW_UPDATE_MANUAL):
        # If a row status is finished then we can't update it in any way
        if self.status==mapvar.ROW_STATUS_FINISHED:
            return

        start_len=len(self.ontology_mappings)
        # make sure the mappings_list is a set and then look at the difference
        # this should return ontology_mappins that are NOT in the mappings_list
        self.ontology_mappings=self.ontology_mappings.difference(set(mappings_list))

        if start_len!=len(self.ontology_mappings):
            self.updates.add(update_type)
            self.BuildUpdateString()
            self.mappings_changed=True        

    '''
    Builds the string for the update column
    '''
    def BuildUpdateString(self):
        self.row_data[self.update_col]="|".join(self.updates)

    '''
    Builds a flattened delimited string of all the ontology terms and ontology
    names for a row-wise output. Requires a delimiter to separate multiple terms
    Note at the moment this doesn't account for quoting so the ontology terms
    must NOT contain the delimiter
    '''
    def BuildOntologyString(self,delimiter):
        ontology_text=set()
        # Loop through all the ontology mappings and get their string values
        for term in self.ontology_mappings:
            ontology_text.add(term.GetWriteValue())

        return delimiter.join(ontology_text)

    '''
    Return the column names that apply to the data in the row. If allcols=True
    the # terms column and the update columns are included
    '''
    def GetColNames(self,allcols=False):
        cols=list(self.colnames)
        if allcols==False:
            cols.pop(cols.index(mapvar.GRID_NTERM_COL))
            cols.pop(cols.index(mapvar.GRID_UPDATE_COL))
            
        return cols

    '''
    Return the ontology mappings as a set of ontology term objects. Note that
    this respects the options
    '''
    def GetOntologyMappings(self):
        return [i for i in self.ontology_mappings if i.GetOntologyName() in self.ontology_options.GetSelectedOntologies()]

    '''
    Return all the ontology mappings irrespective of what the options say
    '''
    def GetAllOntologyMappings(self):
        return self.ontology_mappings

    '''
    Return the ontology searches as a set of ontology term objects. Note that
    this respects the options
    '''
    def GetOntologySearches(self):
        return [i for i in self.ontology_searches if i.GetOntologyName() in self.ontology_options.GetSelectedOntologies()]

    '''
    Get all ontology searches irrespective of what the options say
    '''
    def GetAllOntologySearches(self):
        return self.ontology_searches


#---------------------------------------------------------------------------
# THE MAIN FRAME IN THE APPLICATION
#---------------------------------------------------------------------------

'''
The main frame that holds the application, I have written a custom one that
inherits from wx.Frame. It is called with the formatted data,program arguments
the column names for the data and a sqlalchemy connection to the ontology
database
'''
class OntoFrame(wx.Frame):                                                 
    def __init__(self,data,args,colnames,ontodb,ontology_options):
        # Call the super class 
#        wx.Frame.__init__(self, None, -1, "Ontology Mapper", size=(1400,1000))
        wx.Frame.__init__(self, None, -1, "Ontology Mapper")

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # STORE THE PASSED VARIABLES
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Store the column names
        self.colnames=colnames

        # This is the interface to the ontology database where we search for
        # ontology terms
        self.ontodb=ontodb

        # The arguments the user gave to the program
        self.args=args

        # The ontology options that control what is displayed in the application
        self.ontology_options=ontology_options
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # SET THE STATE OF THE GUI AND SELECTION
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.STATE_HIGHLIGHT=False
        self.STATE_FILTERED=False
        self.GRID_MULTISELECT=False
        self.SEARCH_BUFFER=[]
        self.FILTER_BUFFER=[]
        self.UPDATED_RECORDS=0
        self.UPDATES_SINCE_SAVE=False
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # OTHER VARIABLES THAT NEED INITIALISING ON FRAME CREATION
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.term_idx=colnames.index(mapvar.GRID_NTERM_COL)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Initialise the Statusbar at the bottom of the main frame
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.sb=OntoStatusBar(self)
        self.SetStatusBar(self.sb)
        self.sb.SetTotalRecords(len(data))
        self.sb.SetDisplayedRecords(len(data))
        self.sb.SetUpdatedRecords(self.UPDATED_RECORDS)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Initialise the tool bar
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The toolbar flags and the size of the icons
        TBFLAGS=wx.TB_HORIZONTAL | wx.NO_BORDER | wx.TB_FLAT | wx.TB_TEXT
        tsize=(24,24)
        self.toolbar=self.CreateToolBar( TBFLAGS )
        
        # The bitmaps from the base provider, these should be present on all
        # systems
        save_bmp=wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE,wx.ART_TOOLBAR,tsize)
        save_as_bmp=wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE_AS,wx.ART_TOOLBAR,tsize)
#        bulk_update_bmp=wx.ArtProvider.GetBitmap(wx.ART_FIND_AND_REPLACE, wx.ART_TOOLBAR, tsize)
#        modify_all_bmp=wx.ArtProvider.GetBitmap(wx.ART_HELP_SIDE_PANEL, wx.ART_TOOLBAR, tsize)
        options_bmp=wx.ArtProvider.GetBitmap(wx.ART_LIST_VIEW,wx.ART_TOOLBAR,tsize)
#        smart_search_bmp=wx.ArtProvider.GetBitmap(wx.ART_QUESTION,wx.ART_TOOLBAR,tsize)

        # Set the toolbar size to the same size as the bitmaps and create some IDs
        # so the event handler knows what tool was clicked
        self.toolbar.SetToolBitmapSize(tsize)
        self.save_id=wx.NewId()
        self.save_as_id=wx.NewId()
#        self.bulk_update_id=wx.NewId()
#        self.modify_all_id=wx.NewId()
        self.opt_id=wx.NewId()
#        self.smart_search_id=wx.NewId()

        self.toolbar.AddLabelTool(self.save_id,"Save",save_bmp,shortHelp="Save",longHelp="Save the file")
        self.toolbar.AddLabelTool(self.save_as_id,"Save As",save_as_bmp,shortHelp="Save As",longHelp="Save the file")
#        self.toolbar.AddLabelTool(self.bulk_update_id,"Bulk Update",bulk_update_bmp,shortHelp="Bulk Update",longHelp="Launch bulk update")
#        self.toolbar.AddLabelTool(self.modify_all_id,"Modify All",modify_all_bmp,shortHelp="Modify All In View",longHelp="Modify all in view")
        self.toolbar.AddLabelTool(self.opt_id,"Ontology Options",options_bmp,shortHelp="Ontology Options",longHelp="Open ontology options")
#        self.toolbar.AddLabelTool(self.smart_search_id,"Smart Search",smart_search_bmp,shortHelp="Smart Search",longHelp="Launch Smart Search Dialog")
        
#        self.Bind(wx.EVT_TOOL, self.OnBulkUpdate, id=self.bulk_update_id)
        self.Bind(wx.EVT_TOOL, self.OnToolBarClick, id=self.save_id)
        self.Bind(wx.EVT_TOOL, self.OnToolBarClick, id=self.save_as_id)
#        self.Bind(wx.EVT_TOOL, self.OnSmartSearch, id=self.smart_search_id)
        self.Bind(wx.EVT_TOOL, self.OnOptionsClick, id=self.opt_id)

        # Final thing to do for a toolbar is call the Realize() method. This
        # causes it to render (more or less, that is).
        self.toolbar.Realize()
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # SET UP THE PANEL AND THE SPITTER THAT WILL HOLD EVERYTHING
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The application main frame will essentially be divided into two with
        # a movable sash type diveder between them the left side will have
        # a grid containing the data and the right side will have a textbox
        # That provides a full display of the currently selected grid cell 
        # contents. Below that is an item picker widget that will have ontology
        # mapping selections and assignments for the current row.

        # Place a panel inside the frame this will hold a splitter window, this 
        # will allow tab traversal to work
        panel=wx.Panel(self,-1,style=wx.TAB_TRAVERSAL)
        panel_sizer=wx.BoxSizer(wx.VERTICAL)

        # Add the splitter to the panel and the panel sizer
        self.splitter=wx.SplitterWindow(panel,-1,style = wx.SP_LIVE_UPDATE)

        # p1 is the panel that holds the grid and the grid search bar. p2 holds
        # cell and ontology info
        p1 = wx.Panel(self.splitter)
        p2 = wx.Panel(self.splitter)

        # Now assign the windows to the splitter
        self.splitter.SetMinimumPaneSize(20)
        self.splitter.SplitVertically(p1, p2)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # INFO BARS
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Create the InfoBar.  It starts out in a hidden state so it
        # won't be visible until we need it. The info bar is activated after a 
        # test bulk update or a regexp search and indicates how many records 
        # would have been changed if the bulk update had been carried out or 
        # rows that match the regexp search. The infobar is customised to 
        # contain two buttons. The close button dismisses the infobar and clears
        # the highlighting of the matching rows. The filter button also does this
        # but filters the grid view to just the matching rows. This will then 
        # display a new info bar indicating a filtered view. When this is closed
        # the display returns to normal.
        self.search_info_bar=wx.InfoBar(panel) # Note panel is the parent

        # Now we want to add our custom buttons, we determine which button is 
        # clicked and which event binding is called via the ID on the button
        # so we create a unique ID for each button.
        close_id=wx.NewId()
        filter_id=wx.NewId()

        # Add the buttons to the infobar
        self.search_info_bar.AddButton(close_id,label="Close")
        self.search_info_bar.AddButton(filter_id,label="Filter")

        # Now bind the button events via the IDs to the correct methods
        self.search_info_bar.Bind(wx.EVT_BUTTON, self.OnCloseInfoBar,id=close_id)
        self.search_info_bar.Bind(wx.EVT_BUTTON, self.OnFilterInfoBar,id=filter_id)

        # Finally add the infobar to the panel sizer so it will display properly
        # when called
        panel_sizer.Add(self.search_info_bar,0,wx.EXPAND)

        # Now we add an info bar to display when we are in a filtered state this
        # simply has a close button that we will bind to self.OnCloseInfoBar.
        # self.OnCloseInfoBar knows how to handle the various state permutations
        self.filter_info_bar=wx.InfoBar(panel)
        self.filter_info_bar.Bind(wx.EVT_BUTTON, self.OnCloseInfoBar)
        panel_sizer.Add(self.filter_info_bar,0,wx.EXPAND)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # ADD PANELS TO THE SIZERS ~ THIS HAS TO HAPPEN AFTER THE INFO BAR SETUP
        # SO THEY ARE AT THE TOP OF THE SCREEN AND NOT THE BOTTOM
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        panel_sizer.Add(self.splitter, 1 ,wx.EXPAND)
        panel.SetSizer(panel_sizer)

        # The parent sizers for both panels are vertical so the widgets are 
        # stacked
        p1sizer = wx.BoxSizer(wx.VERTICAL)
        p2sizer = wx.BoxSizer(wx.VERTICAL)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # SETUP SHORT CUT KEYS AND OnClose methods
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ctrl_q_id=wx.NewId()
        ctrl_w_id=wx.NewId()
        ctrl_a_id=wx.NewId()
        ctrl_n_id=wx.NewId()
        ctrl_p_id=wx.NewId()
#        ctrl_b_id=wx.NewId()

        self.Bind(wx.EVT_MENU, self.OnCtrlQ, id=ctrl_q_id)
        self.Bind(wx.EVT_MENU, self.OnCtrlW, id=ctrl_w_id)
        self.Bind(wx.EVT_MENU, self.OnCtrlA, id=ctrl_a_id)
        self.Bind(wx.EVT_MENU, self.OnNextRow, id=ctrl_n_id)
        self.Bind(wx.EVT_MENU, self.OnPreviousRow, id=ctrl_p_id)
#        self.Bind(wx.EVT_MENU, self.OnBulkUpdate, id=ctrl_b_id)
#        self.Bind(wx.EVT_MENU, self.OnSmartSearch, id=self.smart_search_id)

        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,ord('Q'),ctrl_q_id),
                                         (wx.ACCEL_CTRL,ord('W'),ctrl_w_id),
                                         (wx.ACCEL_CTRL,ord('A'),ctrl_a_id),
                                         (wx.ACCEL_CTRL,ord('N'),ctrl_n_id),
                                         (wx.ACCEL_CTRL,ord('P'),ctrl_p_id),
#                                         (wx.ACCEL_CTRL,ord('B'),ctrl_b_id),
                                         (wx.ACCEL_CTRL,ord('S'),self.save_id)
#                                         (wx.ACCEL_CTRL,ord('R'),self.smart_search_id)
])
#        accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL,  ord('Q'), randomId )])
        self.SetAcceleratorTable(accel_tbl)    

        # Bind a window close event to an onclose handler that can check the
        # save status and act accordingly
        self.Bind(wx.EVT_CLOSE,self.OnClose)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # A search controls that will do REGEXP search of the data in the grid
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # A Label inidicating the job of the search box
        label_regexp=wx.StaticText(p1, -1, "Data Search:")
        p1sizer.Add(label_regexp, 0, wx.EXPAND | wx.ALL,5)
        
        # All the grid search options are placed in one sizer next to each other
        search_sizer=wx.BoxSizer(wx.HORIZONTAL)

        # Search Box above the grid that allows a regular expression search of 
        # the data in the grid
        self.regexp_search=wx.SearchCtrl(p1, style=wx.TE_PROCESS_ENTER)
        self.regexp_search.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN,self.OnGridSearch)
        self.regexp_search.Bind(wx.EVT_TEXT_ENTER,self.OnGridSearch)
        search_sizer.Add(self.regexp_search, 1, wx.EXPAND)

        # A drop down list indicating the column to search or ALL columns
        label=wx.StaticText(p1, -1, "Column:")
        search_sizer.Add(label,0,wx.ALIGN_CENTER | wx.LEFT,5)

        self.regexp_search_column_list=wx.ComboBox(p1,-1,
                                                   value=mapvar.ALL_COLUMNS,
                                                   choices=[mapvar.ALL_COLUMNS]+colnames,
                                                   style=wx.CB_READONLY | wx.TE_PROCESS_ENTER)
        search_sizer.Add(self.regexp_search_column_list, 0, wx.EXPAND)

        # A check box indicating we want the search to be case sensitive
        self.regexp_search_case=wx.CheckBox(p1, -1, "Ignore Case: ",style=wx.ALIGN_RIGHT)
        self.regexp_search_case.SetValue(True) # Set the default to case insensitive
        search_sizer.Add(self.regexp_search_case, 0, wx.EXPAND)

        # Add the search sizer to the panel 1 sizer so it will appear above the grid
        p1sizer.Add(search_sizer, 0, wx.EXPAND | wx.ALL,5)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Initialise the "options" objects 
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.bulk_update_options=BulkUpdateOptions(colnames)
        self.smart_search_options=SmartSearchOptions()
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#        self.cell_change=False

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Build the GRID that will hold the data to be annotated
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Now build the grid and assign it to the left panel. The grid is
        # added to the sizer otherwise it does not expand to fill the panel
        self.grid=OntoGrid(p1,data,colnames)
        self.grid.Bind(Grid.EVT_GRID_SELECT_CELL,self.OnSelectCell)
        self.grid.EnableEditing(False)
        p1sizer.Add(self.grid, 1, wx.EXPAND | wx.ALL, 5)
        p1.SetSizer(p1sizer)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The manual ontology search box on the right panel
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # A Label inidicating the job of the item picker
        l3=wx.StaticText(p2, -1, "Manual Ontology Search:")
        p2sizer.Add(l3, 0, wx.EXPAND | wx.ALL,5)
        
        # Holds the manual search box and ontology combo box
        man_search_sizer=wx.BoxSizer(wx.HORIZONTAL)
        
        # # Search Box 
        # self.t2=wx.SearchCtrl(p2, style=wx.TE_PROCESS_ENTER)
        # self.t2.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN,self.OnSearch)
        # self.t2.Bind(wx.EVT_TEXT_ENTER,self.OnSearch)
        # man_search_sizer.Add(self.t2,1,wx.EXPAND)
        # Search Box 
        self.t2=wx.SearchCtrl(p2, style=wx.TE_PROCESS_ENTER)
        self.t2.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN,self.OnSearch)
        self.t2.Bind(wx.EVT_TEXT_ENTER,self.OnSearch)
        man_search_sizer.Add(self.t2,1,wx.EXPAND)

        # A drop down list indicating the column to search or ALL columns
        ontlabel=wx.StaticText(p2, -1, "Ontology:")
        man_search_sizer.Add(ontlabel,0,wx.ALIGN_CENTER | wx.LEFT,5)
#        print([mapvar.ALL_COLUMNS]+[list(self.ontology_options.GetSelectedOntologies())])
        self.ontology_list=wx.ComboBox(p2,-1,
                                       value=mapvar.ALL_COLUMNS,
                                       choices=[mapvar.ALL_COLUMNS]+list(self.ontology_options.GetSelectedOntologies()),
                                       style=wx.CB_READONLY | wx.TE_PROCESS_ENTER)
        man_search_sizer.Add(self.ontology_list, 0, wx.EXPAND)



#        # A check box indicating we want the search to be case sensitive
#        self.regexp_search_case=wx.CheckBox(p2, -1, "Ignore Case: ",style=wx.ALIGN_RIGHT)
#        self.regexp_search_case.SetValue(True) # Set the default to case insensitive
#        man_search_sizer.Add(self.regexp_search_case, 0, wx.EXPAND)

        p2sizer.Add(man_search_sizer,0,wx.EXPAND | wx.ALL,5)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The text display box that displays the contents of the cell in the 
        # grid that has the focus
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Now onto the components on the right hand side of the splitter
        # A Label inidicating the job of the text box
        l1 = wx.StaticText(p2, -1, "Text Data:")

        # The text box that holds the contents of the corrent grid cell 
        self.t1 = wx.TextCtrl(p2, -1,style=wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_PROCESS_TAB)
        self.t1.Bind(wx.EVT_RIGHT_DOWN,self.OnRightClick)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The ontology term item picker that displays the mapped terms and 
        # quick search results for the current row on the grid
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # A Label inidicating the job of the item picker
        l2=wx.StaticText(p2, -1, "Select Ontology Mapping:")

        self.ip = OntoPicker(p2,-1, 
                            'Searched Ontology Terms:', 'Assigned Ontology Terms:',
                             ipStyle=IP_REMOVE_FROM_CHOICES|IP_SORT_CHOICES)
        self.ip.Bind(EVT_IP_SELECTION_CHANGED, self.OnPickerSelectionChange)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # The radio box that indicates the status of the row
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(p2, -1, style=wx.LI_HORIZONTAL)

        # Add the radio box
        self.status_rb=wx.RadioBox(p2, -1,
                                   label="Row Status:", 
                                   pos=wx.DefaultPosition,
                                   size=wx.DefaultSize,
                                   style=wx.BORDER_SUNKEN | wx.RA_SPECIFY_COLS,
                                   choices=mapvar.ROW_STATUS_LIST )
        # Set a tooltip
        self.status_rb.SetToolTip(wx.ToolTip("Set the status of the current row"))

        # Make sure the radio box option is what is the current bulk update
        # options selections, this allows the same options to be maintained
        # between bulk update calls as it is hightly likely the user will
        # want to have the same options between calls
        self.status_rb.SetStringSelection( mapvar.ROW_STATUS_NONE )
        line_below = wx.StaticLine(p2, -1, style=wx.LI_HORIZONTAL)

        # Bind the toggle exent so to store the selection in the bulk update 
        # options
        self.status_rb.Bind(wx.EVT_RADIOBOX,self.OnStatusChange)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Add all the components to the sizer and set the sizer into the right
        # panel
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        p2sizer.Add(l1, 0, wx.EXPAND | wx.ALL,5)
        p2sizer.Add(self.t1, 1, wx.EXPAND | wx.ALL,5)
        p2sizer.Add(l2, 0, wx.EXPAND | wx.ALL,5)
        p2sizer.Add(self.ip, 1, wx.EXPAND | wx.ALL,5)
        p2sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)
        p2sizer.Add(self.status_rb,0,wx.EXPAND | wx.ALL,5)
        p2sizer.Add(line_below, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        p2.SetSizer(p2sizer)
        self.p2=p2


        # Set the display in the right panel, everything is initialised to the
        # first cell of the first row on the grid
        self.current_row=self.grid._table.GetOntologyRow(0)
        self.prev_row=0
        self.current_value=self.grid.GetCellValue(0,0)
        self.cell_change=False

        self.UpdateRightPanel(0,0)
        self.SetSelectedRow(0,0)
        self.UpdateStatusBar()

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Get the display sizes and set the frame size to start the application
        # maximised with the splitter sash in the middle
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        displays=(wx.Display(i) for i in range(wx.Display.GetCount()))
        sizes=[display.GetGeometry().GetSize() for display in displays]
        self.SetSize(sizes[0])
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ######################### END OF THE CONSTRUCTOR #######################

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # CLOSING THE WINDOW EVENT HANDLERS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    '''
    This intercepts the EVT_CLOSE so we can offer the user the opportunity to
    save thier work. This means that we will have to handle closeing of any 
    database connections (I think)
    '''
    def OnClose(self,e):
        # If there are any updates offer the user the chance to save them
        if self.UPDATES_SINCE_SAVE==True:
            # Ask the user if they want to save
            dlg = wx.MessageDialog(self, 
                                   'There are unsaved changes, do you want to save?',
                                   'Save Changes?',
                                   wx.OK | wx.CANCEL | wx.ICON_QUESTION
                               )
            if dlg.ShowModal()==wx.ID_OK:
                if self.args.outfile==None:
                    self.LaunchSaveDialog()
                else:
                    # We already have the filename sorted and we just want to save
                    self.SaveFile()
            # Distroy the question dialog
            dlg.Destroy()

        # Now write the ontology options to the database so they will be
        # remembered for next time
        self.ontology_options.Store()

        # If connected to the database I want to close connections here
        self.ontodb.Close()
        
        # KILL THE MAINFRAME. Do not use self.Close() as it crashes see:
        # http://stackoverflow.com/questions/10833163/how-to-kill-a-wxpython-application-when-user-clicks-a-frames-close
        self.Destroy()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The tool bar event handlers
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    Called when the user clicks an item on the toolbar
    '''
    def OnToolBarClick(self,e):
#        print(self.GetClientSize())
#        print("Clicked ID %i" % e.GetId())

        # If the user has clicked save as or save (and there is no output file 
        # set)
        if e.GetId()==self.save_as_id or (e.GetId()==self.save_id and self.args.outfile==None):
            self.LaunchSaveDialog()
        elif e.GetId()==self.save_id:
            # We already have the filename sorted and we just want to save
            self.SaveFile()            
#        self.options_id=wx.NewId()

    '''
    Called when the user pressed CTRL-R or clicks the scmart search icon
    '''
    def OnSmartSearch(self,e):
        print('Smart Searching....')
        # The bulk update options is created once at the begining of the 
        # execution of the program so all settings are persistant. Each
        # time the bulk update option is activated the current row data
        # is passed to it
        self.smart_search_options.SetRow(self.current_row)

        # Build the dialog and give it the bulk update options data and make
        # sure that when it is displayed it is in the center of the screen
        dlg=SmartSearchDialog(self, -1, 
                              data=self.smart_search_options,
                              ontodb=self.ontodb,
                              title="Smart Search",
#                             size=(800, 800),
                              style=wx.DEFAULT_DIALOG_STYLE)
        dlg.CenterOnScreen()

        # This does not return until the dialog is closed. So the main frame
        # can not have focus while the dialog is active
        val=dlg.ShowModal()

        # Destroy the dialog so it will be created fresh upon the next bulk 
        # update call
        dlg.Destroy()    

        # When the dialog returns it should provide 1 of two wx IDs that will 
        # indicate if the user clicked the OK button or the cancel button we
        # test this here
        if val==wx.ID_OK:
            print("Clicked OK")
#            self.DoBulkUpdate()
#            self.grid.Reset()

            # msg="%%s%i%s record(s) that%%shave been modified are highlighted in %%s. Close this dialog to remove highlighting" % (len(self.SEARCH_BUFFER),filter_string)
            # # This is so the program knows when there are highlighted rows
            # # so the next time there are any highlight operations the 
            # # program will know to cancel the previous ones
            # self.STATE_HIGHLIGHT=True

            # # If we are only testing then issue a test info bar
            # if self.bulk_update_options.update_type==mapvar.BU_TEST:
            #     msg=msg % ("Test bulk update active. "," would ","orange")
            #     flag=wx.ICON_WARNING 
            # elif self.bulk_update_options.update_type==mapvar.BU_APPEND:
            #     msg=msg % ("Bulk update active (APPEND). "," ","red")
            #     flag=wx.ICON_ERROR
            # elif self.bulk_update_options.update_type==mapvar.BU_REPLACE:
            #     msg=msg % ("Bulk update active (REPLACE). "," ","red")
            #     flag=wx.ICON_ERROR
            # elif self.bulk_update_options.update_type==mapvar.BU_REMOVE:
            #     msg=msg % ("Bulk update active (REMOVE). "," ","red")
            #     flag=wx.ICON_ERROR
            # self.search_info_bar.ShowMessage(msg,flag)
        else:
            # The user clicked cancel, we really do not have to test for this
            pass

    '''
    Launches global options dialog for the mapper, this dialog allows the user 
    to modify the ontology terms that can be viewed and the modifiers that are
    available with in the program
    '''
    def OnOptionsClick(self,e):
#        print("CLICKED OPTIONS")
#        print("why am i not called?")
        dlg=OptionsDialog(self,
                          -1, 
                          data=self.ontology_options,
                          title="Ontology Options",
                          style=wx.DEFAULT_DIALOG_STYLE)
        dlg.CenterOnScreen()

        # This does not return until the dialog is closed. So the main frame
        # can not have focus while the dialog is active
        val=dlg.ShowModal()

        # Destroy the dialog so it will be created fresh upon the next bulk 
        # update call
        dlg.Destroy()    

        # Detect that the user clicked ok, not strictly necessary here as there
        # is no cancel button
        if val==wx.ID_OK:
            # Now update the right panel so selection changes are seen
            self.UpdateRightPanel(self.grid.GetGridCursorRow(),self.grid.GetGridCursorCol())
            print("Clicked OK")

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The info bar event handlers
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    Called when the user clicks the close button on the any info bar
    '''
    def OnCloseInfoBar(self,e):
        # If we are in a highlighted and filtered state, then this means that
        # the user is conducting a search from a filtered state and is closing
        # the search info bar. So we want to remove the highlighting
        # from the matching rows and re-introduce the filter info bar so they 
        # have the opportunity to close the filtered view
        if self.STATE_HIGHLIGHT==True and self.STATE_FILTERED==True:
            self.RemoveHighlighting()
            
            # Launch the filtered info bar
            self.ShowFilteredInfoBar(update_grid=False)
        elif self.STATE_HIGHLIGHT==True:
            self.RemoveHighlighting()
        elif self.STATE_FILTERED==True:
            # Reset to the unfiltered data
            self.grid.UnsetFilteredData()
            self.filter_info_bar.Dismiss()
            self.STATE_FILTERED=False
            self.UpdateStatusBar()
        else:
            raise(RuntimeError("[fatal] not sure how this has happened!"))
        

    '''
    Called when the user clicks the filter button on the search info bar
    '''
    def OnFilterInfoBar(self,e):
        self.FILTER_BUFFER=self.SEARCH_BUFFER
        self.ShowFilteredInfoBar(update_grid=True)
        self.UpdateStatusBar()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The keyboard shortcut event handlers
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    Sets the focus to the source list of the ontology term item picker
    '''
    def OnCtrlQ(self, event):
        # If there are items in the source list then set the focus and 
        # the first item to be selected
        if self.ip._source.GetCount()>0:
            self.ip._source.SetFocus()
            self.ip._source.SetSelection(0)
            
        # Also make sure that the desination list unselected so we get 
        # a defined behaviour when pressing enter 
        if self.ip._dest.GetSelections()>0:
            for i in self.ip._dest.GetSelections():
                self.ip._dest.Deselect(i)

    '''
    Sets the focus to the destination list of the ontology term item picker
    '''
    def OnCtrlW(self, event):
        # ~Select the destination list and desect the source list
        if self.ip._dest.GetCount()>0:
            self.ip._dest.SetFocus()
            self.ip._dest.SetSelection(0)

        if self.ip._source.GetSelections():
            for i in self.ip._source.GetSelections():
                self.ip._source.Deselect(i)

    '''
    Searches the ontology database using the strinbg selection in the textbox
    as the search term
    '''
    def OnCtrlA(self, event):
        # Attempt to get a selected string and if there is some data then do
        # a search
        string=self.t1.GetStringSelection()
        if len(string)>0:
            self.DoSearch(string)

    '''
    Called when the user types Ctrl-P and moves to the previous row
    '''
    def OnPreviousRow(self, event):
        self.IncrementRow(-1)

    '''
    Called when the user types Ctrl-N and moves to the next row
    '''
    def OnNextRow(self, event):
        self.IncrementRow(1)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Other event handlers
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    Called When the user changes the status of the row
    '''
    def OnStatusChange(self,e):
        self.current_row.SetStatus(e.GetString())
        self.UPDATES_SINCE_SAVE=True
        self.UpdateStatusBar()
        self.UpdateRightPanel(self.grid.GetGridCursorRow(),self.grid.GetGridCursorCol())

    '''
    Called when the user right clicks onto the TextCtrl in the second (right 
    hand panel). It generates a simple menu giving the option to search for
    the selected string the TextCtrl (the same as typing Ctrl-A)
    '''
    def OnRightClick(self,event):
        menu=SearchPopupMenu(self.t1.GetStringSelection(),self)
        self.t1.PopupMenu(menu, event.GetPosition())
        menu.Destroy()

    '''
    This is called when the user pressed Enter in the right hand panel search
    box (or clicks the search icon). It does a manual search of the ontology
    databaase using of the contents of the search box
    '''
    def OnSearch(self,event):
        string=self.t2.GetValue()
        self.DoSearch(string)

    '''
    Called when the user conducts a regex grid search and presses enter or 
    clicks the search icon
    '''
    def OnGridSearch(self,e):
        # First check if we are in a 'Normal' state if not then we will have
        # to reset the state to normal so things to not get confused
        if self.STATE_HIGHLIGHT==True:
            self.RemoveHighlighting()

        # The search may happen from a filtered state in which case we need
        # to remove the filtered info bar before continuing 
        filter_string="" # displayed in  the search results message
        if self.STATE_FILTERED==True:
            self.filter_info_bar.Dismiss()
            filter_string=" filtered"

        search_string=self.regexp_search.GetValue()
        
        regexp=re.compile(r'%s' % search_string)
        if self.regexp_search_case.GetValue()==True:
            regexp=re.compile(r'%s' % search_string,re.IGNORECASE)

        # Get the value of the column dropdown
        column=self.regexp_search_column_list.GetValue()

        # The default serach columns are all columns
        search_columns=range(0,len(self.colnames))

        # First check if we have selected a single column
        if column!=mapvar.ALL_COLUMNS:
            search_columns=[self.colnames.index(column)]

        # Loop through all the data and find columns that match the search 
        # string
        for row in self.grid._table.data:
            # Loop through all the selected 
            for col_idx in search_columns:
                if regexp.search(str(row.row_data[col_idx])):
                    row.SetRegexpHit(True)
                    self.SEARCH_BUFFER.append(row)
                    # Once we have matched once in a column then move on 
                    # so we do not have the same row duplicated
                    break

        # Make the search info bar the active info bar
        self.active_info_bar=self.search_info_bar
        msg="Regexp search active. %i%s record(s) match the search. Close this dialog to remove highlighting" % (len(self.SEARCH_BUFFER),filter_string)
        flag=wx.ICON_QUESTION
        self.active_info_bar.ShowMessage(msg,flag)

        # Make sure we are now in a highlight search state
        self.STATE_HIGHLIGHT=True

        # The rows will be coloured after the refresh
        self.grid.ForceRefresh()
    
    '''
    Called when a grid cell recieves focus and is responsible for initiating 
    all the tasks associated with changing of the grid cell focus, such as 
    changing the display on the right hand panel 2 and adding the custom row wise
    selection.
    See this regarding multiple selection
    https://groups.google.com/forum/#!topic/wxpython-users/wbOTVbGqBXc
    '''
    def OnSelectCell(self, evt):
        r=evt.GetRow()
        c=evt.GetCol()

        if evt.Selecting():
            self.SetSelectedRow(r,c)
            self.UpdateRightPanel(r,c)

    '''
    Called when the user moves an ontology term in the ontology options picker
    '''
    def OnPickerSelectionChange(self, e):
        if self.cell_change==False:
            self.current_row.SetOntologyMappings(self.ip.GetSelectionObjects())
            self.current_row.SetOntologySearches(self.ip.GetItemObjects())

            # Update the number of terms column to reflect ALL the terms
            # assigned to the row, not just those that we are displaying
            self.grid.SetCellValue(self.grid.GetGridCursorRow(),self.term_idx,str(len(self.current_row.GetAllOntologyMappings())))

            self.UPDATES_SINCE_SAVE=True
            self.UpdateStatusBar()

    '''
    Called when the user selects Ctrl-b or when they click on the bulk update
    icon on the toolbar. This launches the bulk update dialog
    '''
    def OnBulkUpdate(self,event):
        # First check if we are in a 'Normal' state if not then we will have
        # to reset the state to normal so things do not get confused
        if self.STATE_HIGHLIGHT==True:
            self.RemoveHighlighting()

        # The search may happen from a filtered state in which case we need
        # to remove the filtered info bar before continuing it will get
        # redisplayed after the search
        filter_string="" # displayed in  the search results message
        if self.STATE_FILTERED==True:
            self.filter_info_bar.Dismiss()

            # This is a modifier for the message in the dialog
            filter_string=" filtered"

        # The bulk update options is created once at the begining of the 
        # execution of the program so all settings are persistant. Each
        # time the bulk update option is activated the current row data
        # is passed to it
        self.bulk_update_options.SetRow(self.current_row)

        # Build the dialog and give it the bulk update options data and make
        # sure that when it is displayed it is in the center of the screen
        dlg=BulkUpdateDialog(self, -1, 
                             data=self.bulk_update_options,
                             title="Bulk Update",
#                             size=(800, 800),
                             style=wx.DEFAULT_DIALOG_STYLE)
        dlg.CenterOnScreen()

        # This does not return until the dialog is closed. So the main frame
        # can not have focus while the dialog is active
        val=dlg.ShowModal()

        # Destroy the dialog so it will be created fresh upon the next bulk 
        # update call
        dlg.Destroy()    

        # When the dialog returns it should provide 1 of two wx IDs that will 
        # indicate if the user clicked the OK button or the cancel button we
        # test this here
        if val==wx.ID_OK:
            self.DoBulkUpdate()
            self.grid.Reset()

            msg="%%s%i%s record(s) that%%shave been modified are highlighted in %%s. Close this dialog to remove highlighting" % (len(self.SEARCH_BUFFER),filter_string)
            # This is so the program knows when there are highlighted rows
            # so the next time there are any highlight operations the 
            # program will know to cancel the previous ones
            self.STATE_HIGHLIGHT=True

            # If we are only testing then issue a test info bar
            if self.bulk_update_options.update_type==mapvar.BU_TEST:
                msg=msg % ("Test bulk update active. "," would ","orange")
                flag=wx.ICON_WARNING 
            elif self.bulk_update_options.update_type==mapvar.BU_APPEND:
                msg=msg % ("Bulk update active (APPEND). "," ","red")
                flag=wx.ICON_ERROR
            elif self.bulk_update_options.update_type==mapvar.BU_REPLACE:
                msg=msg % ("Bulk update active (REPLACE). "," ","red")
                flag=wx.ICON_ERROR
            elif self.bulk_update_options.update_type==mapvar.BU_REMOVE:
                msg=msg % ("Bulk update active (REMOVE). "," ","red")
                flag=wx.ICON_ERROR
            self.search_info_bar.ShowMessage(msg,flag)
        else:
            # The user clicked cancel, we really do not have to test for this
            pass

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Functions called by the event handlers to do stuff
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    The saving process id broken down into several functions as it can be called 
    in different ways. This is called when a save as dailog needs to be displayed
    this can happen if the user clicks save as or if the user clicks save and 
    there is no output file. It can also happen when the user has given an output
    file on the command line but it can't be written to. This inturn calls SaveFile
    SaveFile is the function that will call this function when the file can't be 
    written to
    '''
    def LaunchSaveDialog(self):
        default_file=""
        default_dir=self.args.base_dir

        # If there is an outfile specified (i.e. the user clicked save as
        # after a save). Then we use the basedir of the outfile and the 
        # filename of the outfile as our defaults
        if args.outfile!=None:
            args.outfile=os.path.abspath(args.outfile)

            # Split the output path up into file and dir
            default_dir=os.path.dirname(args.outfile)
            default_file=os.path.basename(args.outfile)

        # Launch the dialog
        dlg=wx.FileDialog(self,
                          message="Save file as ...",
                          defaultDir=default_dir, 
                          defaultFile=default_file,
                          style=wx.SAVE
            )

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal()==wx.ID_OK:
            args.outfile=dlg.GetPath()
                
            # Now save the file
            self.SaveFile()

            # Destroy the dialog. Don't do this until you are done with it!
            # BAD things can happen otherwise!
            dlg.Destroy()

    '''
    Performs a file save operaton called by the toolbar event handler
    '''
    def SaveFile(self):
        # We do not want to write all the lines associated with the grid
        # specifically we want to remove the # Terms column. We should have the
        # index of this stored from the constructor
        write_header=list(self.colnames)
        write_header.pop(self.term_idx)

        # Get the index of the Update column I will want to insert the 
        # term string in the column before it. The update column should be
        # the last column but I will do it this way to make sure I see any 
        # errors
        update_idx=max(0,self.colnames.index(mapvar.GRID_UPDATE_COL)-1)
        write_header[update_idx:0]=[self.args.existing_term_col]

        # Try to open the file for writing and catch any IO errors, this is so I
        # the saving of the file doesn't blindly fail, this should keep asking the
        # user until the file is saved or the user clicks cancel
        try:
            fh=open(self.args.outfile,'wb')
        except IOError:
            # If there was a problem opening the file then tell the user and ask
            # for a differnt location
            dlg=wx.MessageDialog(self,"Problem saving to %s" % args.outfile,"Problem Saving File",wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            self.LaunchSaveDialog()
            return
        
        # Now write the file
        with fh as csvfile:
            writer=csv.writer(csvfile,delimiter=self.args.file_delimiter)
        
            # Write the header minus the # terms column
            writer.writerow(write_header)

            # To get the data for the file we want to access the default data in the 
            # table object as the user may have clicked save when in a filtered view
            for row in self.grid._default_table.data:
                # We make a copy of the row data so that we can change it for 
                # writing without changing the contents of data in the row object
                write_row=list(row.row_data)
                
                # Remove the # Terms column
                write_row.pop(self.term_idx)

                # The ontology row object can build a text string from the ontology
                # terms, we take this and add it to our writing row
                write_row[update_idx:0]=[row.BuildOntologyString(self.args.ontology_delimiter)]

                # And finally output
                writer.writerow(write_row)

        self.UPDATES_SINCE_SAVE=False
        self.UpdateToolBar()
        

    '''
    Updates the status bar at the bottom of the application
    '''
    def UpdateStatusBar(self):
        self.sb.SetDisplayedRecords(self.grid._table.GetNumberRows())
        self.sb.SetUpdatedRecords(sum([1 for i in self.grid._default_table.data if i.HaveMappingsChanged()]))
        self.UpdateToolBar()

    '''
    Updates the toolbar status
    '''
    def UpdateToolBar(self):
        # Grey out or enable save and save as if there have been no updates
        self.toolbar.EnableTool(self.save_as_id,self.UPDATES_SINCE_SAVE)
        self.toolbar.EnableTool(self.save_id,self.UPDATES_SINCE_SAVE)


    '''
    Shows the filtered view info bar so the user knows that only part of the 
    data is displayed and can exit out of the view to show the whole lot. This
    is subed out as it can be called from several event handlers. The 
    update_grid flag, indicates if the grid needs to be updated with the 
    filtered contents in addition to displaying the infobar. The grid update
    is determined by how the info bar is called i.e. from a cancelled(close)
    search for from a filter call
    '''
    def ShowFilteredInfoBar(self,update_grid=False):
        # We only want to attempt to filter if there is some macthing data
        # this is a bit of a fudge as I can't access the button on the info
        # bar to grey it out
        if len(self.FILTER_BUFFER)>0:
            self.STATE_FILTERED=True

            if update_grid==True:
                self.grid.SetFilteredData(self.FILTER_BUFFER)
                self.RemoveHighlighting()

            msg="Filtered view active. %i record(s) are displayed. Close this dialog to restore all the data" % len(self.FILTER_BUFFER)
            flag=wx.ICON_INFORMATION
            self.filter_info_bar.ShowMessage(msg,flag)
        
    '''
    '''
    def IncrementRow(self,increment):
        r=self.grid.GetGridCursorRow()
        c=self.grid.GetGridCursorCol()
        r=r+increment
        self.grid.SetGridCursor(r,c)
        self.grid.SetFocus()

    '''
    Perform a quick search, this is the result from CTRL-A/right click search or
    a manual search
    '''
    def DoSearch(self,string):
        string=string.strip()
        
        # Get the contents of the drop down list to see what ontologies are 
        # selected
        # Get the value of the column dropdown
        drop_down_value=self.ontology_list.GetValue()

        # The default search ontologies are all ontologies
        search_ontology=list(self.ontology_options.GetSelectedOntologies())

        # First check if we have selected a single column
        if drop_down_value!=mapvar.ALL_COLUMNS:
            search_ontology=[drop_down_value]

#        print(search_ontology)

        results=self.ontodb.search_like_terms(string,search_ontology)
        if len(results)>0:
            self.current_row.SetAllOntologySearches(results)
#            self.current_row.ontology_searches=results
            # This ensures that the item picker only displays results that 
            # comply with the global options
            self.ip.SetItemObjects(self.current_row.GetOntologySearches())
#            self.ip.SetItemObjects(results)

    '''
    This resets the application to a Normal base state, in other words removes
    any infobars and clears any highlighting. It also optionaly restores the
    full data grid if the data are in a filtered state, this is controlled by
    the unfilter flag
    '''
    def RemoveHighlighting(self):
        # Remove any colour formatting of the search rows
        self.RollbackDataFormat(self.SEARCH_BUFFER)
        self.STATE_HIGHLIGHT=False

        # Clear the search buffer
        self.SEARCH_BUFFER=[]

        # Dismiss the active info bar and launch the filtered info bar
        self.search_info_bar.Dismiss()


    '''
    Called when the cell cursor is moved around the grid. Note that this
    controls the green selected row colour by manipulating the selected
    variable. This variable is tested in the grid table when the 
    overridden GetAttr() variable is called
    '''
    def SetSelectedRow(self,r,c):
        self.previous_row=self.current_row
        self.previous_row.selected=False
        self.current_row=self.grid._table.GetOntologyRow(r)
        self.current_row.selected=True
        self.current_value=self.grid.GetCellValue(r,c)
        self.UpdateRightPanel(r,c)
        self.grid.ForceRefresh()

    '''
    Updates all the GUI components on the right hand panel when the user moves
    # the row selection
    '''
    def UpdateRightPanel(self,row,col):
        self.t1.SetValue(self.current_value)

        # Get row ontology term data that resects the global ontology options
        onto_mappings=self.current_row.GetOntologyMappings()
        onto_searches=self.current_row.GetOntologySearches()
        
#        onto_mappings=self.current_row.ontology_mappings
#        onto_searches=self.current_row.ontology_searches
        self.cell_change=True
        self.ip.SetSelectionObjects(onto_mappings)
        self.ip.SetItemObjects(onto_searches)
        self.cell_change=False

        # Make sure the status radiobox displays the correct value
        self.status_rb.SetStringSelection(self.current_row.GetStatus())
        self.UpdateFinishedLogic()

    def UpdateFinishedLogic(self):
        if self.current_row.GetStatus()==mapvar.ROW_STATUS_FINISHED:
            self.t1.Disable()
            self.ip.Disable()
            self.t2.Disable()
            self.ontology_list.Disable()
        else:
            self.t1.Enable()
            self.ip.Enable()
            self.t2.Enable()
            self.ontology_list.Enable()
            
        
    '''
    Remove the formatting on the row. Note that this accepts a list of rows
    this is so we can choose what rows we want to remove the formatting on
    '''
    def RollbackDataFormat(self,row_list):
        # Removes the current formatting from the row so any previous fomatting
        # is restored
        for row in row_list:
            row.SetBulkUpdateTestHit(False)
            row.SetBulkUpdateHit(False)
            row.SetRegexpHit(False)

    '''
    This actually does the bulk update according to the bulk update options
    '''
    def DoBulkUpdate(self):
        # To store the matches in the event of a test run
        self.SEARCH_BUFFER=[]

        # Get a list of the columns we want to match against. These will be
        # column objects and they hold all the data we require for matching
        # within them
        self.bulk_update_options.GetSelectedColumns()

        # Now prepare the columns in terms of the case sensitivity this is 
        # done here to avoid any repetition with in the loop. I will pre-compile
        # a regular expression according to the options given in the bulk
        # update dialog
        for col in self.bulk_update_options.GetSelectedColumns():
            # Compile regular expressions for each column
#            if col.GetMatchTypeString()==mapvar.BU_EXACT_TEXT:
            if col.GetCaseSensitiveString()==mapvar.BU_CASE_SENSITIVE:
                col.SetRegex( re.compile( r'%s' % col.GetValue() ) )
            elif col.GetCaseSensitiveString()==mapvar.BU_CASE_INSENSITIVE:
                col.SetRegex( re.compile( r'%s' % col.GetValue(), re.IGNORECASE ))
            else:
                raise(RuntimeError('[fatal] unknown case option %s' % col.GetCaseSensitiveString()))
#            elif col.GetMatchTypeString()==mapvar.BU_CONTAINS_TEXT:
#                if col.GetCaseSensitiveString()==mapvar.BU_CASE_SENSITIVE:
#                    col.SetRegex( re.compile( "%s" % re.escape( col.GetValue() ) ) )
#                elif col.GetCaseSensitiveString()==mapvar.BU_CASE_INSENSITIVE:
#                    col.SetRegex( re.compile( "%s" % re.escape( col.GetValue() ), re.IGNORECASE ))
#                else:
#                    raise(RuntimeError('[fatal] unknown case option %s' % col.GetCaseSensitiveString()))

        # Loop through each row in turn, notice we use table not default table, so we are only
        # searching what is on the screen
        for row in self.grid._table.data:
            # The default is the row matches
            row_match=True

            # Now loop through the test columns that the user selected 
            # in the bulk update dialog we must have a match in all the 
            # selected columns in order for a row to be flagged as matching
            # as soon as we fail then we say the row does not match and there
            # is no point checking any other rows
            for col in self.bulk_update_options.GetSelectedColumns():
                if col.GetRegex().search(row.row_data[col.GetIndex()])==None:
                    row_match=False
                    break
                    
            # Check to see if the row matches
            if row_match==True:
                # If we are testing then we want to set the test background
                # colour of the row and store the row that matches so we can
                # restore the colour when we exit test mode
                if self.bulk_update_options.GetUpdateTypeString()==mapvar.BU_TEST:
                    row.SetBulkUpdateTestHit(True)
                    self.SEARCH_BUFFER.append(row)
                elif self.bulk_update_options.GetUpdateTypeString()==mapvar.BU_APPEND:
                    row.AppendOntologyMappings(self.bulk_update_options.GetSelectedTerms(),update_type="BULK")
                    row.SetBulkUpdateHit(True)
                    self.SEARCH_BUFFER.append(row)
                elif self.bulk_update_options.GetUpdateTypeString()==mapvar.BU_REPLACE:
                    row.SetAllOntologyMappings(self.bulk_update_options.GetSelectedTerms(),update_type="BULK")
                    row.SetBulkUpdateHit(True)
                    self.SEARCH_BUFFER.append(row)
                elif self.bulk_update_options.GetUpdateTypeString()==mapvar.BU_REMOVE:
                    row.RemoveOntologyMappings(self.bulk_update_options.GetSelectedTerms(),update_type="BULK")
                    row.SetBulkUpdateHit(True)
                    self.SEARCH_BUFFER.append(row)
                else:
                    raise(RuntimeError("[fatal] I am not sure how this has happened"))        
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# END OF FRAME CLASS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# A custom grid table for storing and manipulating ontology data
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''
This is the underlaying data structure that is used to populate and update
the grid in the GUI. It exposes some methods that are overriden to provide
the data to fill each cell. It accepts the data and the column names for 
the grid.
'''
class OntoTable(Grid.PyGridTableBase):
    def __init__(self,parent_grid,data,colnames):
        # The base class must be initialized *first*
        Grid.PyGridTableBase.__init__(self)
        
        # Store the passed objects
        self.parent_grid=parent_grid
        self.data = data
        self.colnames = colnames

        # We need to store the row length and column length to
        # see if the table has changed size
        self._rows = self.GetNumberRows()
        self._cols = self.GetNumberCols()

    '''
    Returns the number of columns
    '''
    def GetNumberCols(self):
        return len(self.colnames)

    '''
    Returns the number of rows in the data
    '''
    def GetNumberRows(self):
        return len(self.data)

    def GetColLabelValue(self, col):
        return self.colnames[col]

    # wrong needs fixing although it does not change that much
    def GetRowLabelValue(self, row):
        return row
#        return "row %03d" % int(self.data[row][0])

    def GetOntologyRow(self,row):
        return self.data[row]

    def GetValue(self, row, col):
        return str(self.data[row].row_data[col])

    def GetRawValue(self, row, col):
        return self.data[row].row_data[col]

    def SetValue(self, row, col, value):
        self.data[row].row_data[col]=value

    def ResetView(self, grid):
        """
        (Grid) -> Reset the grid view.   Call this to
        update the grid if rows and columns have been added or deleted
        """
        grid.BeginBatch()

        for current, new, delmsg, addmsg in [
            (self._rows, self.GetNumberRows(), Grid.GRIDTABLE_NOTIFY_ROWS_DELETED, Grid.GRIDTABLE_NOTIFY_ROWS_APPENDED),
            (self._cols, self.GetNumberCols(), Grid.GRIDTABLE_NOTIFY_COLS_DELETED, Grid.GRIDTABLE_NOTIFY_COLS_APPENDED),
        ]:

            if new < current:
                msg = Grid.GridTableMessage(self,delmsg,new,current-new)
                grid.ProcessTableMessage(msg)
            elif new > current:
                msg = Grid.GridTableMessage(self,addmsg,new-current)
                grid.ProcessTableMessage(msg)
                self.UpdateValues(grid)

        grid.EndBatch()

        self._rows = self.GetNumberRows()
        self._cols = self.GetNumberCols()
#        # update the column rendering plugins
#        self._updateColAttrs(grid)

        # update the scrollbars and the displayed part of the grid
        grid.AdjustScrollbars()
        grid.ForceRefresh()

    '''
    This is called by the grid everytime it is refreshed in anyway and over
    riding this method allows some low-level cell colouring operations to be
    carried out almost "in the background". I am not sure what 
    some_extra_parameter is. If this returns None then the grid will give the
    cell the default attributes
    '''
    def GetAttr(self, row, col, some_extra_parameter):
        # Get the row colours, the row will 'know' what colour it should be
        colours=self.data[row].GetColours()

        # See if the row number being updated is the same as the 
        # grid cursor if it is then set it the selected colour and set the 
        # text colour
        if self.data[row].selected==True:
            # Create a grid attribute object these can't be reused, i tried and
            # had all sorts of errors
            attr=Grid.GridCellAttr()
            attr.SetBackgroundColour(self.parent_grid.GetSelectionBackground())
            
            # Make the slection text the same colour as the row background (if it has colour)
            if colours and colours[0]:
                attr.SetTextColour(colours[0])
            return attr
        
        if colours:
            attr=Grid.GridCellAttr()
            if colours[0]:
                # If there are background colours to apply
                attr.SetBackgroundColour(colours[0])
            if colours[1]:
                attr.SetTextColour(colours[1])
            return attr
        return None

    def UpdateValues(self, grid):
        """Update all displayed values"""
        # This sends an event to the grid table to update all of the values
        msg = Grid.GridTableMessage(self, Grid.GRIDTABLE_REQUEST_VIEW_GET_VALUES)
        grid.ProcessTableMessage(msg)

    def SortColumn(self, col,selectedRow=0,reverse=False):
        """
        col -> sort the data based on the column indexed by col
        """
        current_row=self.data[selectedRow]
#        name = self.colnames[col]
        _data = []

        for row in self.data:
            _data.append((row.row_data[col], row))

        _data.sort(reverse=reverse)
        self.data = []
        for sortvalue, row in _data:
            self.data.append(row)
        return self.data.index(current_row)

    '''
    Returns the max string widths (in characters) for the columns in the table
    These are used to set the maximum column widths in the event of very long 
    columns
    '''
    def GetColStringWidths(self):
        max_widths=[0]*self.GetNumberCols()
    
        for row in self.data:
            max_widths=[max(max_widths[c],len(str(col))) for c,col in enumerate(row.row_data)]
        return max_widths

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The GUI grid that holds the data to be mapped
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''
A custom grid class to hold the existing data that will be mapped to ontology
terms
'''
class OntoGrid(Grid.Grid):
    def __init__(self, parent, data, colnames, plugins=None):
        # The base class must be initialized *first*
        Grid.Grid.__init__(self, parent, -1)
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Store the data that has been passed
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.parent=parent
        self.colnames=colnames
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Initialise and set the tables
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self._table = OntoTable(self,data,colnames)
        self._default_table=self._table        
        self.SetTable(self._table)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Bind events to the handlers 
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.Bind(Grid.EVT_GRID_LABEL_LEFT_DCLICK, self.OnLabelLeftDClicked)
        self.Bind(Grid.EVT_GRID_LABEL_RIGHT_DCLICK, self.OnLabelRightDClicked)
        self.Bind(Grid.EVT_GRID_LABEL_LEFT_CLICK, self.OnLabelLeftClicked)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.Bind(Grid.EVT_GRID_CELL_LEFT_CLICK, self.OnCellLeftClick)
        wx.EVT_MOTION(self.GetGridWindow(), self.OnMouseMotion)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Store variables used to set optimum column size this is used in 
        # preference to AutoSizeColumn as I can use it to set a maximum
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # As all the cells are the same font I can get away with this
        # otherwise I can consider using this GridTableBase.GetAttr
        self.test_font=self.GetCellFont(0,0)
        self.test_char="0"
        self.padding_pixels=15
        self.test_dc=wx.ScreenDC()
        self.test_dc.SetFont(self.test_font)
        self.max_string_size=50
        
        # This gets the maximum column size in pixels for a column that is 
        # max_string_size pixels long
        self.max_col_width=self.test_dc.GetTextExtent(self.test_char*self.max_string_size)[0]

        # Get some default column widths from the default table string widths
        self.default_col_widths=self.GuessColumnWidths( self._default_table.GetColStringWidths() )
        self.SetColumnWidths( self.default_col_widths )
        
        # This is margins to the right and the bottom of the grid and NOT each cell
        # I need a custom editor for that....will look into it
        self.SetMargins(20,2)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ######################## END OF THE CONSTRUCTOR ########################

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # EVENT HANDLERS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    '''
    Don't allow any dragging on the window, this stops cell multiple selections
    through click and drag
    '''
    def OnMouseMotion(self,e):
        # mouse being dragged? If so eat the event
        if e.Dragging():      
            pass                    
        else:
            # no dragging, pass on to the window
            e.Skip()           

    '''
    A single left click on the grid, if the control key is pressed then the 
    event is consumed as this would lead to an additional selection in 
    addition to the cursor selection
    '''
    def OnCellLeftClick(self,e):
        if e.ControlDown():   # the edit control needs this key
            pass
        else:
            e.Skip()

    '''
    Capture and discard a shift selection e.g multiple cell selections
    '''
    def OnKeyDown(self,e):
        keycode=e.GetKeyCode()
        if e.ShiftDown():
            pass
        if keycode==wx.WXK_TAB:
            # Make the tab key move focus to the next control
            # rather than moving cell
            self.Navigate()
#            pass
        else:
            e.Skip()

    '''
    A grid label double left click
    '''
    def OnLabelLeftDClicked(self, evt):
        # Did we click on a row or a column?
        row, col = evt.GetRow(), evt.GetCol()
        if row == -1:
            new_selection=self._table.SortColumn(col)
            self.Reset()
            self.SetGridCursor(new_selection,col)

    '''
    A grid label double right click
    '''
    def OnLabelRightDClicked(self, evt):
        # Did we click on a row or a column?
        row, col = evt.GetRow(), evt.GetCol()
        if row == -1:
            new_selection=self._table.SortColumn(col,reverse=True)
            self.Reset()
            self.SetGridCursor(new_selection,col)

    '''
    A grid label left click, all label left clicks are consumed otherwise
    they will select all cells in a row or a column
    '''
    def OnLabelLeftClicked(self,e):
        pass

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # OTHER FUNCTIONS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '''
    Set the column widths to the defaults calculated using the DC. This accepts 
    pixel widths for each column and compares with the max width then sets the
    appropriate width
    '''
    def SetColumnWidths(self,pixel_widths):
        for c,col in enumerate(self.colnames):
            self.SetColSize(c,min(self.default_col_widths[c]+(self.padding_pixels*2),self.max_col_width))
        # Now store the sizes as size objects

    '''
    Guessstimates the text extents based on column character widths. From this
    I can tell if any columns are going to exceed a set maximum column width
    and if they are I can fix them to the maximum.
    See: http://stackoverflow.com/questions/24609197/how-to-make-text-fit-in-cells-in-a-wxpython-grid
    takes a list of string widths in characters and returns a list of pixel 
    widths for each column
    '''
    def GuessColumnWidths(self,string_widths):
        column_data=[self.test_dc.GetTextExtent(self.test_char*i)[0] for i in string_widths]
        label_data=[self.test_dc.GetTextExtent(self.GetColLabelValue(c))[0] for c,col in enumerate(self.colnames)]
        return [max(column_data[c],label_data[c]) for c,col in enumerate(self.colnames)]

    '''
    This essentially swaps the tables in the grid so we have a display of the
    filtered data. The original table is already backed up by the constructor
    so it can be swapped back out with UnsetFilteredData
    '''
    def SetFilteredData(self,data):
        self._table=OntoTable(self,data,self.colnames)
        self.SetTable(self._table)

        # Get the column widths in pixels from the table string widths
        pixel_widths=self.GuessColumnWidths( self._table.GetColStringWidths() )
        self.SetColumnWidths(pixel_widths)
        self.Reset()

    '''
    Returns the grid to the default data that it was called with
    '''
    def UnsetFilteredData(self):
        self._table=self._default_table
        self.SetTable(self._table)
        self.SetColumnWidths( self.default_col_widths )
        self.Reset()

    '''
    reset the view based on the data in the table. Call this when rows are added
    or destroyed
    '''
    def Reset(self):
        self._table.ResetView(self)

    '''
    Taken from http://webcache.googleusercontent.com/search?q=cache:c8c7Cq7d4egJ:w3facility.org/question/how-do-i-control-the-viewport-of-a-grid-in-wxpython/+&cd=7&hl=en&ct=clnk&gl=uk
    '''
    def SetGridCursor(self, irow, icol):
        # move the cursor to the cell as usual
        self.GoToCell(irow, icol)
        # scroll to keep irow in the middle of the page
        ppunit = self.GetScrollPixelsPerUnit()
        cell_coords = self.CellToRect(irow, icol)
        y = cell_coords.y / ppunit[1]  # convert pixels to scroll units
        scrollPageSize = self.GetScrollPageSize(wx.VERTICAL)
        scroll_coords = (0, y - scrollPageSize / 2)
        self.Scroll(scroll_coords)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# A status bar at the bottom of the application
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'''
A status bar for the application, I have subclassed it so we can set the various
fields in it a bit easier
'''
class OntoStatusBar(wx.StatusBar):
    def __init__(self, parent):
        wx.StatusBar.__init__(self, parent, -1)

        # This status bar has three fields
        self.SetFieldsCount(3)
        # Sets the three fields to be relative widths to each other.
        self.SetStatusWidths([-1, -1, -1])
        self.total_records=0

    def SetTotalRecords(self,records):
        self.total_records=records
        self.SetStatusText("Total records: %i" % records, 0)

    def SetDisplayedRecords(self,records):
        self.SetStatusText("Displayed records: %i/%i" % (records,self.total_records), 1)

    def SetUpdatedRecords(self,records):
        self.SetStatusText("Updated records: %i/%i" % (records,self.total_records), 2)



'''
Builds and controls the logic for the bulk update options dialog that is displayed
if the user selects Ctrl-b or the bulk update option
'''
class BulkUpdateDialog(wx.Dialog):
    def __init__(
            self, parent, ID, 
            title="", 
            data=None, 
            size=wx.DefaultSize, 
            pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE
            ):

        # Instead of calling wx.Dialog.__init__ we precreate the dialog
        # so we can set an extra style that must be set before
        # creation, and then we create the GUI object using the Create
        # method.
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        # This next step is the most important, it turns this Python
        # object into the real wrapper of the dialog (instead of pre)
        # as far as the wxPython extension is concerned.
        self.PostCreate(pre)
        self.data=data

        # Now continue with the normal construction of the dialog
        # contents
        # The main sizer we will add everything into
        sizer=wx.BoxSizer(wx.VERTICAL)

        #####################################################################
        ##### Options dictating if we want to replace ontology terms in #####
        ##### matching rows or just append to them                      #####
        #####################################################################

        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        # Add the radio box
        self.update_rb=wx.RadioBox(self, -1,
                                   label="Term Update Type:", 
                                   pos=wx.DefaultPosition,
                                   size=wx.DefaultSize,
                                   style=wx.BORDER_SUNKEN | wx.RA_SPECIFY_COLS,
                                   choices=self.data.update_types )
        # Set a tooltip
        self.update_rb.SetToolTip(wx.ToolTip("Append to existing terms or replace existing terms"))

        # Make sure the radio box option is what is the current bulk update
        # options selections, this allows the same options to be maintained
        # between bulk update calls as it is hightly likely the user will
        # want to have the same options between calls
        self.update_rb.SetStringSelection( self.data.GetUpdateTypeString() )

        # Bind the toggle exent so to store the selection in the bulk update 
        # options
        self.update_rb.Bind(wx.EVT_RADIOBOX,self.OnUpdateTypeChange)
        sizer.Add(self.update_rb,0,wx.EXPAND | wx.ALL,5)

        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.BOTTOM, 5)
        
        #####################################################################
        ##### Options dictating if we want to replace ontology terms in #####
        ##### matching rows or just append to them                      #####
        #####################################################################
        label = wx.StaticText(self, -1, "Query Columns:")
        label.SetHelpText("These columns will be will be searched for matches to text in the respective columns of the current row ")
        sizer.Add(label, 0, wx.EXPAND | wx.ALL,5)

        # This sizer is a horizontal sizer that puts the column selection next
        # to the column options
        column_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # Create an item picker for the columns
        self.column_picker=OntoPicker(self,-1, 
                           'All Columns:', 'Search Columns:',
                             ipStyle=IP_REMOVE_FROM_CHOICES)
        column_sizer.Add(self.column_picker,1,wx.EXPAND | wx.ALL,5)

        # Add the data to the column picker according to what is in the bulk
        # update options object
        self.column_picker.SetItemObjects(self.data.GetUnselectedColumns())
        self.column_picker.SetSelectionObjects(self.data.GetSelectedColumns())

        # Bind
        self.column_picker.Bind(EVT_IP_SELECTION_CHANGED, self.OnColumnSelectionChange)
        self.column_picker._dest.Bind(wx.EVT_LISTBOX,self.OnColumnSelection)

        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(self, -1, style=wx.LI_VERTICAL)
        column_sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.BOTTOM, 5)
        
        ##########################################################
        ##### Some options for the currently selected column #####
        ##########################################################
        options_sizer=wx.BoxSizer(wx.VERTICAL)

        # Label indicating the currently selected column options
        label=wx.StaticText(self, -1, "Search Text (Regexp):")
        label.SetHelpText("These options dictate how we match text for the currently selected column, the text is editable and is treated as a regular expression")
        options_sizer.Add(label, 0, wx.EXPAND | wx.ALL,5)

        # Create a read only text box to hold the serach text associated with 
        # a column
#        self.search_text_box=wx.TextCtrl(self,-1,style=wx.TE_READONLY)
        self.search_text_box=wx.TextCtrl(self,-1)
        self.search_text_box.Bind(wx.EVT_TEXT,self.OnSearchTextChanged)
        options_sizer.Add(self.search_text_box, 0, wx.EXPAND | wx.ALL,5)

        # The radio box groups that dictate how we want to match the data in 
        # the selected column
        self.case_rb=wx.RadioBox(self, -1,
                                 label="Case Sensitivity:",
                                 pos=wx.DefaultPosition,
                                 size=wx.DefaultSize,
                                 style=wx.RA_SPECIFY_ROWS,

                                 choices=self.data.GetCaseSensitiveOptions()
                                )
        options_sizer.Add(self.case_rb,0,wx.EXPAND | wx.ALIGN_CENTRE)
        self.case_rb.Bind(wx.EVT_RADIOBOX,self.OnCaseSensitivityChange)
        self.case_rb.SetToolTip(wx.ToolTip("Match column data case sensitive/insensitive"))

        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        options_sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.BOTTOM, 5)

        # Now add the radiobox indicating the type of matching we want to 
        # enforce for the column
        self.match_rb=wx.RadioBox(self, -1,
                                  label="String Matching:",
                                  pos=wx.DefaultPosition,
                                  size=wx.DefaultSize,
                                  style=wx.RA_SPECIFY_ROWS,
                                  choices=self.data.GetMatchTypeOptions()
                                  )
        options_sizer.Add(self.match_rb,0,wx.EXPAND | wx.ALL,5)
        self.match_rb.Bind(wx.EVT_RADIOBOX,self.OnMatchTypeChange)
        self.match_rb.SetToolTip(wx.ToolTip("Match column data exactly"))

        # Add the column options sizer to to the horizontal sizer containing 
        # the column picker so they should appear to the right and be stacked
        # top to bottom
        column_sizer.Add(options_sizer, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        sizer.Add(column_sizer, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        # I can't seem to get a border around the radiobox at the moment so I 
        # will add a dividing line instead
        line = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.BOTTOM, 5)


        ########################################################################
        ##### Options dictating what terms we want to Append/Replace/Remove ####
        ##### from the rows that match the column selection parameters      ####
        ########################################################################
        label = wx.StaticText(self, -1, "Terms To Add:")
        sizer.Add(label, 0, wx.EXPAND | wx.ALL, 5)

        # An item picker for the ontology terms
        self.term_picker=OntoPicker(self,-1, 
                           'Discarded Terms:', 'Selected Terms:',
                             ipStyle=IP_REMOVE_FROM_CHOICES)

        self.term_picker.SetItemObjects(self.data.GetUnselectedTerms())
        self.term_picker.SetSelectionObjects(self.data.GetSelectedTerms())
        self.term_picker.Bind(EVT_IP_SELECTION_CHANGED, self.OnTermSelectionChange)
        sizer.Add(self.term_picker, 1, wx.EXPAND | wx.ALL ,5)

        # Dividing line between the widgets on the dialog and the test
        # checkbox
        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)

        #######################################################################
        # The rest of the widgets are the buttons on the bottom of the dialog #
        #######################################################################
        btnsizer=wx.StdDialogButtonSizer()

        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        
        # The OK button on the bottom of the toolbar
        self.ok_btn=wx.Button(self, label="Run", id=wx.ID_OK)
        self.ok_btn.SetHelpText("The OK button completes the dialog")
        btnsizer.AddButton(self.ok_btn)

        # The cancel button
        self.cancel_btn=wx.Button(self, wx.ID_CANCEL)
        self.cancel_btn.SetHelpText("Do not perform any updates and return to the main program")
        btnsizer.AddButton(self.cancel_btn)

        # This needs to be called when using wx.StdDialogButtonSizer
        btnsizer.Realize()

        # Add the button sixer to the main sixer so they appear below everything
        # else
        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        self.SetSizer(sizer)

        # This makes the dialog fit to the screen (i think??)
        sizer.Fit(self)

        # Now make sure the radio boxes are displaying the correct data or are
        # disabled accordingly
        self.UpdateColumnWidgets()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Other methods and event bindings for the dialog
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    '''
    Called when the text is modified on the seach text TextCtrl
    '''
    def OnSearchTextChanged(self,e):
        print("TEXT_CHANGED")
        # Get the column this will probably only succeed when we have
        # one column in the picker (as this will be deemed to be highlighted)
        # see GetHighlightedSelectionObjects() for details
        try:
            print("GOT COLUMN")
            column=self.column_picker.GetHighlightedSelectionObjects()[0]
            column.SetValue(self.search_text_box.GetValue())
        except IndexError:
            print("COULD NOT GET COLUMN")
            column=None
        
    '''
    Called ehen the user toggles the checkbox
    '''
    def OnTestChange(self,e):
#        print(e.IsChecked())
        self.data.SetIsTest(e.IsChecked())

    '''
    Called when the user shifts an item in the column picker
    '''
    def OnColumnSelectionChange(self, e):
        self.data.SetUnselectedColumns(self.column_picker.GetItemObjects())
        self.data.SetSelectedColumns(self.column_picker.GetSelectionObjects())

        # Get the column this will probably only succeed when we have
        # one column in the picker (as this will be deemed to be highlighted)
        # see GetHighlightedSelectionObjects() for details
        try:
            column=self.column_picker.GetHighlightedSelectionObjects()[0]
        except IndexError:
            column=None

        self.UpdateColumnWidgets()

        # This will update the The search string based on the except/contains 
        # radiobox selection
        self.ModifySearchString(column)

    '''
    Called when the user shifts an item in the term picker
    '''
    def OnTermSelectionChange(self,e):
        self.data.SetUnselectedTerms(self.term_picker.GetItemObjects())
        self.data.SetSelectedTerms(self.term_picker.GetSelectionObjects())

    '''
    Called whn An item is selected in the Destination listbox of the column 
    picker
    '''
    def OnColumnSelection(self,e):
        self.UpdateColumnWidgets()

        # Get the column this will probably only succeed when we have
        # one column in the picker (as this will be deemed to be highlighted)
        # see GetHighlightedSelectionObjects() for details
        try:
            column=self.column_picker.GetHighlightedSelectionObjects()[0]
        except IndexError:
            column=None

        # This will update the The search string based on the except/contains 
        # radiobox selection
        self.ModifySearchString(column)

    '''
    Called when the user toggles the case-sensitivity radio box
    '''
    def OnCaseSensitivityChange(self,event):
        column=self.column_picker.GetHighlightedSelectionObjects()[0]
        column.SetCaseSensitiveString(event.GetString())

    '''
    Called when the user toggles the match-type radio box
    '''
    def OnMatchTypeChange(self,event):
        # Update the currently selected column with the users
        # choice
        column=self.column_picker.GetHighlightedSelectionObjects()[0]
        column.SetMatchTypeString(event.GetString())

        # This will update the
        self.ModifySearchString(column)

    '''
    Caled when the user clicks the update type radiobox
    '''
    def OnUpdateTypeChange(self,event):
        self.data.SetUpdateTypeString(event.GetString())

    '''
    Updates the column selection radiobox widgets according to the selection
    in the column picker
    '''
    def UpdateColumnWidgets(self):
        #################################################################
        # First see what (if anything) is selected in the column picker
        # if nothing is selected or there are multiple selections then
        # disable the associated radio boxes as we could get out of sync
        #################################################################
        # Get the current selections but diving into the inners of the
        # item picker
        selections=self.column_picker.GetHighlightedSelectionObjects()

        # Disable the OK/TEST buttons if there are no selections in either 
        # the columns or the terms list
        if len(self.column_picker.GetSelectionObjects())==0 or len(self.term_picker.GetSelectionObjects())==0:
            self.ok_btn.Disable()
        else:
            self.ok_btn.Enable()

        if len(selections)!=1:
#            self.search_text_box.SetValue("")
            # Using change value should on generate an event where as SetValue would
            # we only want to capture changes that the user makes on the program
            self.search_text_box.ChangeValue("")
            self.search_text_box.Disable()
            self.case_rb.Disable()
            self.match_rb.Disable()
        else:
            # If we have a valid selection then get the column object
            # that is selected and enable the radio boxes so we can
            # display the currently selected data in them
            self.search_text_box.Enable()
            self.case_rb.Enable()
            self.match_rb.Enable()
            
            column=selections[0]
#            self.search_text_box.SetValue(column.GetValue())
            self.search_text_box.ChangeValue(column.GetValue())
            self.case_rb.SetStringSelection(column.GetCaseSensitiveString())
            self.match_rb.SetStringSelection(column.GetMatchTypeString())
    
    '''
    Will modify the search string regexp based on the exact/contains
    text selection
    '''
    def ModifySearchString(self,column):
        # if the column does not exist then just return
        if column.__class__.__name__!="Column":
#            print(column.__class__.__name__)
            return

        # Also, as we are dealing with regexps now in the bulk update
        # I will either add or remove ^$ from the string.
        search_string=self.search_text_box.GetValue()

        # Also of the search string does not contain anything then return
        # Then we want to match against blank space
        if len(search_string)==0:
            search_string="^$"
            self.search_text_box.ChangeValue(search_string)
            column.SetValue(search_string)
            return

        if self.match_rb.GetString(self.match_rb.GetSelection())==mapvar.BU_EXACT_TEXT:
            # As the search string is user editable we have to check they have
            # not already added them
            if search_string[0]!="^":
                search_string="^"+search_string
            if search_string[-1]!="$":
                search_string=search_string+"$"
        if self.match_rb.GetString(self.match_rb.GetSelection())==mapvar.BU_CONTAINS_TEXT:
            search_string=re.sub("^\^","",search_string)
            search_string=re.sub("\$$","",search_string)
        
        self.search_text_box.ChangeValue(search_string)
        column.SetValue(search_string)

'''
A class that acts as a container for passing the bulk update options to and 
from the bulk update options dialog. It just requires the column names in 
the data. Note that these can't (or at least shouldn't) be changed after
creation.
'''
class BulkUpdateOptions(object):
    def __init__(self,all_columns):
        self.selected_cols=[]
        self.all_columns=[]
        self.row=None
        
        # These are the update types
        self.update_types=[mapvar.BU_TEST,mapvar.BU_APPEND,mapvar.BU_REPLACE,mapvar.BU_REMOVE]
        self.update_type=self.update_types[0]

        # Loop through all the columns and create column objects
        for c,i in enumerate(all_columns):
            self.all_columns.append(self.Column(i,c,"Exact",False))

        # Selected columns and unselected columns
        self.selected_columns=[]
        self.unselected_columns=self.all_columns

        # These won't contain any data until row data is added
        self.unselected_terms=[]
        self.selected_terms=[]

#        # The default is to have the test run chkbox checked
#        self.is_test=True

    '''
    Used during debugging just to print the contents of the object
    '''
    def __str__(self):
        string=[]
#        string.append("Is Test: %s" % self.is_test)
        string.append("Update Type: %s" % self.update_type)
        for i in self.selected_columns:
            string.append("Selected Column: %s" % i)
        for i in self.unselected_columns:
            string.append("Unselected Column: %s" % i)
        for i in self.selected_terms:
            string.append("Selected Term: %s" % i.get_formatted_value())
        for i in self.unselected_terms:
            string.append("Unselected Term: %s" % i.get_formatted_value())
        return("\n".join(string))

    '''
    Returns a list of the selected terms
    '''
    def GetSelectedTerms(self):
        return self.selected_terms

    '''
    Returns a list of unselected terms
    '''
    def GetUnselectedTerms(self):
        return self.unselected_terms

    '''
    Sets the terms that are selected via the gui
    takes a list
    '''
    def SetSelectedTerms(self,terms):
        self.selected_terms=terms
        
    '''
    Sets the terms that are not selected via the gui, this really should
    be delt with via logic in the SetSelectedTerms method. takes a list
    '''
    def SetUnselectedTerms(self,terms):
        self.unselected_terms=terms

    '''
    Returns a list of the selected columns
    '''
    def GetSelectedColumns(self):
        return self.selected_columns

    '''
    Returns a list of unselected columns
    '''
    def GetUnselectedColumns(self):
        return self.unselected_columns

    '''
    Sets the columns that are selected via the gui
    takes a list
    '''
    def SetSelectedColumns(self,columns):
        self.selected_columns=columns
        
    '''
    Sets the columns that are not selected via the gui, this really should
    be delt with via logic in the SetSelectedColumns method. takes a list
    '''
    def SetUnselectedColumns(self,columns):
        self.unselected_columns=columns

    '''
    Returns a string giving the current update selection
    '''
    def GetUpdateTypeString(self):
        return self.update_type

    '''
    Sets the update type string, there are no error checks in here but
    it should not get out of sync as the gui only has toggle radio boxes
    '''
    def SetUpdateTypeString(self,string):
        self.update_type=string

    '''
    Returns a list containing the column case sensitivity options
    '''
    def GetCaseSensitiveOptions(self):
        return self.all_columns[0].case_sensitive_options

    '''
    Returns a list containing the column match type options
    '''
    def GetMatchTypeOptions(self):
        return self.all_columns[0].match_type_options

    '''
    Adds the row to the bulk update options. This should be the currently
    selected row of the grid with all the data/ Ontology mappings associated
    with it
    '''
    def SetRow(self,row):
        self.row=row

        # Selected and unselected terms
        self.selected_terms=self.row.GetOntologyMappings()
        self.unselected_terms=[]

        # Now we loop through all the columns and the row data and assign the
        # row data to the required column
        for c,i in enumerate(self.all_columns):
            # The data we give the row is a regexp escaped string. They will
            # be able to edit this as they wish in the dialog
            i.SetValue(re.escape(str(self.row.row_data[c])))

#            i.SetValue(self.row.row_data[c])
        

    '''
    Represents a column in the bulk update options I have added this a sub-class
    so I can implement the get_formatted_value method to return the column name
    this allows me to use the onto picker item picker widget to hold/manipulate
    the data
    '''
    class Column(object):
        def __init__(self,name,index,match_type,case_sensitive):
            self.name=name
            self.index=index
            self.case_sensitive_options=[mapvar.BU_CASE_SENSITIVE,mapvar.BU_CASE_INSENSITIVE]
            self.match_type_options=[mapvar.BU_EXACT_TEXT,mapvar.BU_CONTAINS_TEXT]
            self.SetCaseSensitiveIndex(1)
            self.SetMatchTypeIndex(0)
            self.value=""
        
        '''
        Used for debugging to see the contents of the object
        '''
        def __str__(self):
            return ",".join([self.name,
                             str(self.value),
                             str(self.index),
                             self.case_sensitive_string,
                             self.match_type_string])

        def SetValue(self,value):
            self.value=value

        def GetValue(self):
            return self.value
        
        '''
        Return the index of column (i.e. the column number)
        '''
        def GetIndex(self):
            return self.index

        '''
        Set the index of a column (i.e. a column number)
        '''
        def SetIndex(self,index):
            self.index=index
        
        '''
        Sets the compiled regular expression
        '''
        def SetRegex(self,regex):
            self.regex=regex

        '''
        Gets the compiled regular expression
        '''
        def GetRegex(self):
            return self.regex
        
        def GetPickerValue(self):
            return self.name

        def GetCaseSensitiveString(self):
            return self.case_sensitive_string

        def GetMatchTypeString(self):
            return self.match_type_string

        def SetCaseSensitiveString(self,option):
            self.case_sensitive_index=self.case_sensitive_options.index(option)
            self.case_sensitive_string=option

        def SetCaseSensitiveIndex(self,index):
            self.case_sensitive_string=self.case_sensitive_options[index]
            self.case_sensitive_index=index

        def SetMatchTypeString(self,option):
            self.match_type_index=self.match_type_options.index(option)
            self.match_type_string=option

        def SetMatchTypeIndex(self,index):
            self.match_type_string=self.match_type_options[index]
            self.match_type_index=index


class OntoPicker(ItemsPicker):
    def __init__(self,
                 parent,
                 id=wx.ID_ANY,
                 label='',
                 selectedLabel='',
                 ipStyle=IP_DEFAULT_STYLE,
                 *args, **kw):
        
        super(OntoPicker,self).__init__(parent,
                                        id=id,
                                        choices=[],
                                        label=label,
                                        selectedLabel=selectedLabel,
                                        ipStyle=ipStyle,
                                        *args, **kw)
        
        # Will hold the selection and the items ontology objects
#        self.selections_obj={}
#        self.items_obj={}
        self.obj_mapper={}
        self.added=False

    '''
    Event binding to the Add button in the Item Picker. This is overridden from
    the super class and calls the superclass
    '''
    def _OnAdd(self,e):
        self.added=True
        self.source_selections=self._source.GetSelections()
        self.dest_selections=None
        super(OntoPicker,self)._OnAdd(e)
        if self._source.GetCount()>0:
            self._source.SetFocus()
            self._source.SetSelection(min(self._source.GetCount()-1,self.source_selections[0]))

    '''
    Event binding to the remove button in the Item Picker. This is overridden from
    the super class and calls the superclass
    '''
    def _OnRemove(self,e):
        self.added=False
        self.source_selections=None
        self.dest_selections=self._dest.GetSelections()
        super(OntoPicker,self)._OnRemove(e)

        if self._dest.GetCount()>0:
            self._dest.SetFocus()
            self._dest.SetSelection(min(self._dest.GetCount()-1,self.dest_selections[0]))

    '''
    Sets the ontologies to the selections box in the item picker. These will
    be assigned ontology terms
    '''
    def SetSelectionObjects(self,obj):
#        self.obj_mapper={}
#        pp.pprint(obj)
        s=[]
        for i in obj:
            val=i.GetPickerValue()
            self.obj_mapper[val]=i
            s.append(val)
        self.SetSelections(s)
#        pp.pprint(self.selections_obj)


    '''
    Gets the all the objects in the selections box
    '''
    def GetSelectionObjects(self):
        return [self.obj_mapper[i] for i in self.GetSelections()]
            

    '''
    Gets the currently highlighted objects in the selections box
    '''
    def GetHighlightedSelectionObjects(self):
        # Get the selections
        selections=self._dest.GetSelections() 
#        print("ItemPicker Selections")
#        print(selections)
        all_objects=self.GetSelectionObjects()
#        print("ItemPicker Objects")
#        print(all_objects)

        # I have noticed there are cases when the selection is down to
        # one thing but self._dest.GetSelections() does not give any
        # values so if (particularly after something has been removed)
        # So this is a fudge to make it return the correct thing
        if len(selections)==0 and len(all_objects)==1:
            return all_objects

        return [all_objects[i] for i in selections]
        

    '''
    Sets the ontologies to the Items box in the item picker. These will be 
    search results or previously discarded items
    '''
    def SetItemObjects(self,obj):
#        self.obj_mapper={}
#        pp.pprint(obj)
        items=[]
        for i in obj:
            val=i.GetPickerValue()
            self.obj_mapper[val]=i
            items.append(val)
        self.SetItems(items)

        
    '''
    Gets the objects in the items box
    '''
    def GetItemObjects(self):
        return [self.obj_mapper[i] for i in self.GetItems()]


'''
A class that does all the interaction with the ontology database, this has been 
updated to innteract with the new ontology database
'''
class OntoDb(object):
    def __init__(self,ontodb,echo=False):
        self.db=create_engine(ontodb,echo=echo)
        self.conn=self.db.connect()

        # Need to check for an SQLite connection
        try:
            self.conn.execute("PRAGMA case_sensitive_like=OFF")
        except ProgrammingError:
            pass

        # Tables, columns etc. in SQL alchemy are known as metadata
        self.meta = MetaData(bind=self.conn)
        self.meta.reflect()

    '''
    Close the database connection
    '''
    def Close(self):
        self.conn.close()

    '''
    Get All the modifers that are selected
    '''
    def GetSelectedModifers(self):
        res=self.GetModifierData()
        return [i.mod_name for i in res if i.selected==1]        

    '''
    Get all the modifers as strings
    '''
    def GetModifiers(self):
        res=self.GetModifierData()
        return [i.mod_name for i in res]

    '''
    Get all the modifier SQLAlchemy results
    '''
    def GetModifierData(self):
        mod=self.meta.tables['modifier']
        s=select([mod.c.mod_id,mod.c.mod_name,mod.c.selected]).select_from(mod).order_by(mod.c.mod_name)
        res=self.conn.execute(s)
        return res

    '''
    Returns the ontologies that have selected=1 in the ontology table
    '''
    def GetSelectedOntologies(self):
        res=self.GetOntologyData()
        return [i.ontology for i in res if i.selected==1]

    '''
    Returns ontology names associated with the database as a list of strings
    '''
    def GetOntologies(self):
        res=self.GetOntologyData()
        return [i.ontology for i in res]

    '''
    Gets all the data from the ontology table
    '''
    def GetOntologyData(self):
        ont=self.meta.tables['ontology']
        s=select([ont.c.ontology_id,ont.c.ontology,ont.c.branch_length,ont.c.file_path,ont.c.selected]).select_from(ont).order_by(ont.c.ontology)
        res=self.conn.execute(s)
        return res

    '''
    SELECT c.class_id,o.preferred_label,t.ontology
    FROM ontology_class o
    JOIN class c
    ON c.class_pk=o.class_pk
    JOIN ontology t
    ON t.ontology_id=o.ontology_id
    WHERE o.preferred_label LIKE "%interferon%"

    split search splits the search terms on word boundaries and then searches
    for the separated words with an and clause
    '''
    def search_like_terms(self,term,search_ontology,split_search=True):
        oc=self.meta.tables['ontology_class']
        cl=self.meta.tables['class']
        on=self.meta.tables['ontology']
        results=[]

#        terms=[term]
        likes=[]
        string="%"+term+"%"
        likes.append(oc.c.preferred_label.like(string))
        if split_search==True:
            terms=re.split("\s+",term)

            # Build the like string
            likes=[]
            for i in terms:
                string="%"+i+"%"
                likes.append(oc.c.preferred_label.like(string))
        likes.append(on.c.ontology.in_(search_ontology))
#        where_term=None
#        where_term=likes[0]
#        where_term=likes
#        if len(likes)>1:
#        print("IN HERE")
        where_term=and_(*likes)
            

#        term="%"+term+"%"
#        s=select([cl.c.class_id,oc.c.preferred_label,on.c.ontology]).select_from(oc.join(cl,cl.c.class_pk==oc.c.class_pk).join(on,on.c.ontology_id==oc.c.ontology_id)).where(oc.c.preferred_label.like(term))
#        s=select([cl.c.class_id,oc.c.preferred_label,on.c.ontology]).select_from(oc.join(cl,cl.c.class_pk==oc.c.class_pk).join(on,on.c.ontology_id==oc.c.ontology_id)).where(where_term)

        s=select([cl.c.class_id,oc.c.preferred_label,on.c.ontology]).\
           select_from(oc.join(cl,cl.c.class_pk==oc.c.class_pk).\
           join(on,on.c.ontology_id==oc.c.ontology_id)).\
           where(where_term)

#        print(s)
        for r in self.conn.execute(s):
            results.append(OntologyTerm(r.preferred_label,r.ontology,id=r.class_id))
        return results

    '''
    Update ontology selection status
    '''
    def UpdateOntologySelection(self,ont_name,status):
        on=self.meta.tables['ontology']
        u=on.update().where(on.c.ontology==ont_name).values(selected=status)
        self.conn.execute(u)

    '''
    Update modifier selection status
    '''
    def UpdateModifierSelection(self,mod_name,status):
        mod=self.meta.tables['modifier']
        u=mod.update().where(mod.c.mod_name==mod_name).values(selected=status)
        self.conn.execute(u)

    '''
    Inserts a new modifier term
    '''
    def InsertNewModifier(self,new_mod_name):
        mod=self.meta.tables['modifier']
        i=mod.insert().values(mod_name=new_mod_name)
        self.conn.execute(i)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# END OF ONTODB CLASS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# '''
# A class that does all the interaction with the ontology database
# '''
# class OntoDb(object):
#     def __init__(self,ontodb,echo=False):
#         self.db=create_engine(ontodb,echo=echo)
#         self.conn=self.db.connect()

#         # Need to check for an SQLite connection
#         self.conn.execute("PRAGMA case_sensitive_like=OFF")

#         # Tables, columns etc. in SQL alchemy are known as metadata
#         self.meta = MetaData(bind=self.conn)
#         self.meta.reflect()

#     def search_like_terms(self,term):
#         mesh_term=self.meta.tables['mesh_term']
#         results=[]
#         term="%"+term+"%"
#         s=select([mesh_term.c.mesh_id,mesh_term.c.mesh_term]).select_from(mesh_term).where(mesh_term.c.mesh_term.like(term))
#         for r in self.conn.execute(s):
#             results.append(OntologyTerm(r['mesh_term'],"MESH",id=r['mesh_id']))
#         return results


'''
A right click popup menu that is displayed when the user right clicks on the 
text display
'''
class SearchPopupMenu(wx.Menu):
    def __init__(self,search_term,frame):
        wx.Menu.__init__(self)

        self.frame=frame
        self.search_term=search_term
        display_term=search_term
        if len(search_term)>10:
            display_term=search_term[0:10]+"..."
        
    
        item = wx.MenuItem(self, wx.NewId(), 'Search "%s"' % display_term)
        self.AppendItem(item)
        self.Bind(wx.EVT_MENU, self.OnSearch, item)

    def OnSearch(self, event):
        self.frame.DoSearch(self.search_term)

'''
Holds the options for a smart search to find matching terms
'''
class SmartSearchOptions(object):
    def __init__(self):
        self.search_row=None
        self.selected_ontologies=set()
        self.selected_cols=set()

    '''
    The smart search requires all the data in a row to be provided
    '''
    def SetRow(self,row):
        self.search_row=row

    '''
    Return the current row in the options
    '''
    def GetRow(self):
        return self.search_row

    '''
    Return s set of the selected columns
    '''
    def GetSelectedColumns(self):
        return self.selected_cols

    '''
    Add a colummn to selected column set
    '''
    def AddColumn(self,col):
        self.selected_cols.add(col)

    '''
    Remove a column from the selected column set
    '''
    def RemoveColumn(self,col):
        try:
            self.selected_cols.remove(col)
        except KeyError:
            pass

    '''
    return a set of selected ontologies
    '''
    def GetSelectedOntologies(self):
        return self.selected_ontologies
        
    '''
    Adds an ontology name to the set of ontologies
    '''
    def AddOntology(self,ont):
        self.selected_ontologies.add(ont)

    '''
    Removes an Ontology from the selected ontology set
    '''
    def RemoveOntology(self,ont):
        try:
            self.selected_ontologies.remove(ont)
        except KeyError:
            pass

'''
Controls the display setup and logic behind the smart search dialog
'''
class SmartSearchDialog(wx.Dialog):
    def __init__(
            self, parent, ID, 
            title="", 
            data=None, 
            ontodb=None,
            size=wx.DefaultSize, 
            pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE
            ):

        # Instead of calling wx.Dialog.__init__ we precreate the dialog
        # so we can set an extra style that must be set before
        # creation, and then we create the GUI object using the Create
        # method.
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        # This next step is the most important, it turns this Python
        # object into the real wrapper of the dialog (instead of pre)
        # as far as the wxPython extension is concerned.
        self.PostCreate(pre)
        self.data=data
        self.db=ontodb
        self.row=self.data.GetRow()

        ont_names=self.db.GetOntologies()
        selected_names=self.data.GetSelectedOntologies()
        
        # Just in case a difference between the options and the database has 
        # occured, it shouldn't but you never know
        selected_names=selected_names.intersection(ont_names)
        
        # If there are no selected names then make all selected
        if len(selected_names)==0:
            selected_names=ont_names

        # Now continue with the normal construction of the dialog
        # contents
        # The main sizer we will add everything into
        sizer=wx.BoxSizer(wx.VERTICAL)

        st=wx.StaticText(self,-1,"Select Ontologies to search with data from selected columns and/or terms. Any selected search results are appended to the existing terms for the row")
        sizer.Add(st,0,wx.EXPAND | wx.ALL,5)

        # A horizontal sizer is then added that will contain checklist boxes
        chklist_sizer=wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(chklist_sizer,1,wx.EXPAND | wx.ALL,5)

        # An Ontology SELECTOR
        ont_list_sizer=wx.BoxSizer(wx.VERTICAL)
        chklist_sizer.Add(ont_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Ontologies:")
        ont_list_sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.ont_list=wx.CheckListBox(self, -1, (80, 50), wx.DefaultSize, ont_names)
        self.Bind(wx.EVT_CHECKLISTBOX, self.OnOntologyChange, self.ont_list)
        self.ont_list.SetCheckedStrings(selected_names)
        ont_list_sizer.Add(self.ont_list,0,wx.EXPAND | wx.ALL,5)
#        chklist_sizer.Add(self.ont_list,0,wx.EXPAND | wx.ALL,5)

        cols=self.row.GetColNames()
        selected_cols=self.data.GetSelectedColumns()
        selected_cols=selected_cols.intersection(cols)

        if len(selected_cols)==0:
            selected_cols=cols

        # A data column selector
        col_list_sizer=wx.BoxSizer(wx.VERTICAL)
        chklist_sizer.Add(col_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Columns:")
        col_list_sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.col_list=wx.CheckListBox(self, -1, (80, 50), wx.DefaultSize,cols)
        self.Bind(wx.EVT_CHECKLISTBOX, self.OnColumnChange, self.col_list)
        self.col_list.SetCheckedStrings(selected_cols)
#        chklist_sizer.Add(self.col_list,0,wx.EXPAND | wx.ALL,5)
        col_list_sizer.Add(self.col_list,1,wx.EXPAND | wx.ALL,5)
        
        # A term selector, this is derived from the current row
        term_list_sizer=wx.BoxSizer(wx.VERTICAL)
        chklist_sizer.Add(term_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Terms:")
        term_list_sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.mappings=list(self.row.GetOntologyMappings())
        self.mapping_strings=[i.GetPickerValue() for i in self.mappings]
        self.term_list=wx.CheckListBox(self, -1, (80, 50), wx.DefaultSize, self.mapping_strings)
#        self.Bind(wx.EVT_CHECKLISTBOX, self.OnColumnChange, self.col_list)
        self.term_list.SetCheckedStrings(self.mapping_strings)
#        chklist_sizer.Add(self.term_list,1,wx.EXPAND | wx.ALL,5)
        term_list_sizer.Add(self.term_list,1,wx.EXPAND | wx.ALL,5)


        # Create an item picker for the columns
        st=wx.StaticText(self,-1,"Pick Terms From Search Results:")
        sizer.Add(st,0,wx.EXPAND | wx.ALL,5)

        self.search_picker=OntoPicker(self,-1, 
                           'Search Results:', 'Assigned Terms:',
                             ipStyle=IP_REMOVE_FROM_CHOICES)
        sizer.Add(self.search_picker,1,wx.EXPAND | wx.ALL,5)

        self.search_btn=wx.Button(self, label="Search")
        sizer.Add(self.search_btn,0,wx.EXPAND | wx.ALL,5)
        self.Bind(wx.EVT_BUTTON,self.OnSearch,self.search_btn)
        # Add the data to the column picker according to what is in the bulk
        # update options object
#        self.column_picker.SetItemObjects(self.data.GetUnselectedColumns())
#        self.column_picker.SetSelectionObjects(self.data.GetSelectedColumns())

        # Bind
#        self.column_picker.Bind(EVT_IP_SELECTION_CHANGED, self.OnColumnSelectionChange)
#        self.column_picker._dest.Bind(wx.EVT_LISTBOX,self.OnColumnSelection)

        #######################################################################
        # The rest of the widgets are the buttons on the bottom of the dialog #
        #######################################################################
        btnsizer=wx.StdDialogButtonSizer()

        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        
        # The OK button on the bottom of the toolbar
        self.ok_btn=wx.Button(self, label="Apply", id=wx.ID_OK)
        self.ok_btn.SetHelpText("The OK button completes the dialog")
        btnsizer.AddButton(self.ok_btn)

        # The cancel button
        self.cancel_btn=wx.Button(self, wx.ID_CANCEL)
        self.cancel_btn.SetHelpText("Do not perform any updates and return to the main program")
        btnsizer.AddButton(self.cancel_btn)

        # This needs to be called when using wx.StdDialogButtonSizer
        btnsizer.Realize()

        # Add the button sixer to the main sixer so they appear below everything
        # else
        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        self.SetSizer(sizer)

        # This makes the dialog fit to the screen (i think??)
        sizer.Fit(self)

        # Update the buttons on the bottom according to the selection. i.e. if
        # there is not enough selections to carry out the search then the search
        # and apply buttons are greyed out
        self.UpdateDialogButtons()

    '''
    Called when the user selects or de selects an ontology in the ontology 
    chklist, this will update the options so that the ontology chklist will
    maintain a persistant selection during different calls to smart search
    '''
    def OnOntologyChange(self,e):
        index = e.GetSelection()
        label = self.ont_list.GetString(index)

        if self.ont_list.IsChecked(index):
            self.data.AddOntology(label)
        else:
            self.data.RemoveOntology(label)

        self.ont_list.SetSelection(index)    # so that (un)checking also selects (moves the highlight)
        self.UpdateDialogButtons()

    '''
    Called when the user selects of deselects a column
    '''
    def OnColumnChange(self,e):
        index = e.GetSelection()
        label = self.col_list.GetString(index)

        if self.col_list.IsChecked(index):
            self.data.AddColumn(label)
        else:
            self.data.RemoveColumn(label)

        self.col_list.SetSelection(index)    # so that (un)checking also selects (moves the highlight)
        self.UpdateDialogButtons()
        
    '''
    Called when the user clicks the search button
    '''
    def OnSearch(self,e):
        print("Start Search!")
        # Grab any term selection as ontology objects, these are not stored in 
        # the search options as they will change each time it is used
        terms=[self.mappings[i].value for i in self.term_list.GetChecked()]
        
        # now grab the column data
        coldata=[self.row.row_data[i] for i in self.col_list.GetChecked()]
        self.DoSmartSearch(terms,coldata)    
#        for i in coldata:
#            print(i)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Other methods and event bindings for the dialog
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def DoSmartSearch(self,terms,col):
        for i in terms:
            print(i)
#            tokens=nltk.word_tokenize(i.translate(None, string.punctuation))
            tokens=nltk.word_tokenize(i)
            print(tokens)
            tokens=[j for j in tokens if j not in string.punctuation]
            print(tokens)
            filtered=[w for w in tokens if not w in stopwords.words('english')]
            print(filtered)

    '''
    Makes sure that the button activity makes sense 
    '''
    def UpdateDialogButtons(self):
        if len(self.ont_list.GetChecked())==0 or (len(self.col_list.GetChecked())==0 and len(self.term_list.GetChecked())==0):
            self.search_btn.Disable()
        else:
            self.search_btn.Enable()

        if len(self.search_picker.GetSelectionObjects())==0:
            self.ok_btn.Disable()
        else:
            self.ok_btn.Enable()

'''
Stores the global options for the application, uses the database. In theory
I could just use the database for the options but I wanted to put into a 
separate class as I might add other non database things in future
'''
class OntologyOptions(object):
    def __init__(self,db):
        self.db=db

        # Initialise the selections, this is so we are not continually accessing
        # the database. This is important as each row in the grid has access to
        # the options and so they "know" what terms,ontologies to advertise. We 
        # do not really want all the rows tintinually doing database queries
        # every time the GUI is refreshed
        self.ontologies=self.db.GetOntologies()
        self.selected_ontologies=set(self.db.GetSelectedOntologies())
        self.unselected_ontologies=set(self.ontologies).difference(self.selected_ontologies)

        self.modifiers=self.db.GetModifiers()
        self.selected_modifiers=set(self.db.GetSelectedModifers())
        self.unselected_modifiers=set(self.modifiers).difference(self.selected_modifiers)

        self.new_modifiers=set()
        self.file_ontologies=set()

    '''
    Get a list of ontologies that have been marked as selected in the GUI
    '''
    def GetSelectedOntologies(self):
        return self.selected_ontologies

    '''
    Return a set of ontologies that are unselected
    '''
    def GetUnselectedOntologies(self):
        return unselected_ontologies

    '''
    Get a list of all ontologies     
    '''
    def GetOntologies(self):
        return self.ontologies

    '''
    Add an ontology to the selected ontology set
    '''
    def AddOntology(self,ont):
        self.selected_ontologies.add(ont)
        self.unselected_ontologies.remove(ont)

    '''
    Remove an ontology from the selected ontoogy set
    '''
    def RemoveOntology(self,ont):
        self.selected_ontologies.remove(ont)
        self.unselected_ontologies.add(ont)

    '''
    File Ontologies are ontology names that are preexisting in the opened
    data file (that will be annotated). It is possible for this to happen
    and the program does not make an issue of this. However, for display and
    search purposes the program needs to know about them so the method 
    below is a mechanism for that. file_ont should be a set
    '''
    def AddFileOntologies(self,file_ont):
        # Filter out any file ontologies that are present in the database
        self.file_ontologies=file_ont.difference(self.ontologies)

        # Now add to the ontologies
        self.ontologies=list(self.file_ontologies.union(self.ontologies))

        # We also add them as selected
        self.selected_ontologies=self.selected_ontologies.union(self.file_ontologies)

    '''
    Get a set of all modifiers that have been selected in the GUI
    '''
    def GetSelectedModifiers(self):
        return self.selected_modifiers

    '''
    Return a set of modifiers that are unselected
    '''
    def GetUnselectedModifiers(self):
        return self.unselected_modifiers

    '''
    Get a list of all modifiers available
    '''
    def GetModifiers(self):
        return self.modifiers

    '''
    Add a modifiers to the selected modifiers list
    '''
    def AddModifier(self,mod):
        self.selected_modifiers.add(mod)
        self.unselected_modifiers.remove(mod)

    '''
    Remove a modifiers from the selected modifiers list
    '''
    def RemoveModifier(self,mod):
        self.selected_modifiers.remove(mod)
        self.unselected_modifiers.add(mod)

    '''
    Add any modifiers that are found in the data file (but not the database)
    to the options. See comments for AddFileOntologies
    '''
    def AddFileModifiers(self,file_mod):
        # Filter out any file modifiers that are present in the database
        self.file_modifiers=file_mod.difference(self.modifiers)

        # Now add to the modifiers
        self.modifiers=list(self.file_modifiers.union(self.modifiers))

        # We also add them as selected
        self.selected_modifiers=self.selected_modifiers.union(self.file_modifiers)

    '''
    Adds a new modifier to the modifiers set and selected modifiers. Note that
    it is also added to the new_modifiers set. This is so it can be written to
    the database at the end of execution. All format checks are done via the 
    GUI dialog so mod only contain words,digits,_ or -
    '''
    def NewModifier(self,mod):
        self.new_modifiers.add(mod)
        self.modifiers.append(mod)
        self.selected_modifiers.add(mod)

    '''
    Writes all the options to the database and adds any new modifiers to the
    modifiers table
    '''
    def Store(self):
        # First any new modifiers
        for i in self.new_modifiers:
#            print("NEW_MOD %s" % i)
            self.db.InsertNewModifier(i)
#            pass

        # First Update the selected ontologies
        for i in self.selected_ontologies.difference(self.file_ontologies):
#            print("SELECTED_ONT %s" % i)
            self.db.UpdateOntologySelection(i,True)
#            pass
                
        # Unselected ontologies
        for i in self.unselected_ontologies.difference(self.file_ontologies):
#            print("UNSELECTED_ONT %s" % i)
            self.db.UpdateOntologySelection(i,False)
#            pass

        # Selected Modifiers
        for i in self.selected_modifiers.difference(self.file_modifiers):
#            print("SELECTED_MOD %s" % i)
            self.db.UpdateModifierSelection(i,True)
#            pass
    
        # Unselected Modifiers
        for i in self.unselected_modifiers.difference(self.file_modifiers):
#            print("UNSELECTED_MOD %s" % i)
            self.db.UpdateModifierSelection(i,False)
#            pass


'''
Controls the display setup and logic behind the smart search dialog
'''
class OptionsDialog(wx.Dialog):
    def __init__(
            self, parent, ID, 
            title="", 
            data=None, 
            size=wx.DefaultSize, 
            pos=wx.DefaultPosition, 
            style=wx.DEFAULT_DIALOG_STYLE
            ):

        # Instead of calling wx.Dialog.__init__ we precreate the dialog
        # so we can set an extra style that must be set before
        # creation, and then we create the GUI object using the Create
        # method.
        pre = wx.PreDialog()
        pre.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
        pre.Create(parent, ID, title, pos, size, style)

        # This next step is the most important, it turns this Python
        # object into the real wrapper of the dialog (instead of pre)
        # as far as the wxPython extension is concerned.
        self.PostCreate(pre)
        self.data=data
#         self.db=ontodb
#         self.row=self.data.GetRow()

        ont_names=self.data.GetOntologies()
        selected_names=self.data.GetSelectedOntologies()
        
        # Just in case a difference between the options and the database has 
        # occured, it shouldn't but you never know
        selected_names=set(selected_names).intersection(ont_names)
        
        # If there are no selected names then make all selected
        if len(selected_names)==0:
            selected_names=ont_names

        # Now continue with the normal construction of the dialog
        # contents
        # The main sizer we will add everything into
        sizer=wx.BoxSizer(wx.VERTICAL)

        st=wx.StaticText(self,-1,"Select Ontologies and modifiers to appear in search/selected lists")
        sizer.Add(st,0,wx.EXPAND | wx.ALL,5)

        # A horizontal sizer is then added that will contain checklist boxes
        chklist_sizer=wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(chklist_sizer,1,wx.EXPAND | wx.ALL,5)

        # An Ontology SELECTOR
        ont_list_sizer=wx.BoxSizer(wx.VERTICAL)
        chklist_sizer.Add(ont_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Ontologies:")
        ont_list_sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.ont_list=wx.CheckListBox(self, -1, (80, 50), wx.DefaultSize, ont_names)
        self.Bind(wx.EVT_CHECKLISTBOX, self.OnOntologyChange, self.ont_list)
        self.ont_list.SetCheckedStrings(selected_names)
        ont_list_sizer.Add(self.ont_list,1, wx.EXPAND | wx.ALL,5)

        modifiers=self.data.GetModifiers()
        selected_modifiers=self.data.GetSelectedModifiers()
        selected_modifiers=set(selected_modifiers).intersection(modifiers)
#         cols=self.row.GetColNames()
#         selected_cols=self.data.GetSelectedColumns()
#         selected_cols=selected_cols.intersection(cols)

        if len(selected_modifiers)==0:
            selected_modifiers=modifiers

        # A data column selector
        mod_list_sizer=wx.BoxSizer(wx.VERTICAL)
        chklist_sizer.Add(mod_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Modifiers:")
        mod_list_sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.mod_list=wx.CheckListBox(self, -1, (80, 50), wx.DefaultSize,modifiers)
        self.Bind(wx.EVT_CHECKLISTBOX, self.OnModifierChange, self.mod_list)
        self.mod_list.SetCheckedStrings(selected_modifiers)
        mod_list_sizer.Add(self.mod_list,1,wx.EXPAND | wx.ALL,5)
        
        # A term selector, this is derived from the current row
#        add_mod_sizer=wx.BoxSizer(wx.VERTICAL)
#        chklist_sizer.Add(term_list_sizer,0,wx.EXPAND)
        st=wx.StaticText(self,-1,"Add New Modifier:")
        sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
#        st=wx.StaticText(self,-1,"New modifiers should not contain spaces or any of these . ; | , [ ] ( )")
        st=wx.StaticText(self,-1,"New modifiers should contain words,digits _ - and nothing else")

        sizer.Add(st,0,wx.EXPAND | wx.ALL,5)
        self.add_mod_txt=wx.TextCtrl(self,style=wx.TE_PROCESS_ENTER)
        self.add_mod_txt.Bind(wx.EVT_TEXT,self.OnTextEntry)
        self.add_mod_txt.Bind(wx.EVT_TEXT_ENTER,self.OnAddNewMod)
        sizer.Add(self.add_mod_txt,0,wx.EXPAND | wx.ALL,5)

        self.add_btn=wx.Button(self, label="Add Modifier")
        sizer.Add(self.add_btn,0,wx.EXPAND | wx.ALL,5)
        self.Bind(wx.EVT_BUTTON,self.OnAddNewMod,self.add_btn)
        self.add_btn.Disable()

        #######################################################################
        # The rest of the widgets are the buttons on the bottom of the dialog #
        #######################################################################
        btnsizer=wx.StdDialogButtonSizer()

        if wx.Platform != "__WXMSW__":
            btn = wx.ContextHelpButton(self)
            btnsizer.AddButton(btn)
        
        # The OK button on the bottom of the toolbar
        self.ok_btn=wx.Button(self, label="Ok", id=wx.ID_OK)
        self.ok_btn.SetHelpText("The OK button completes the dialog")
        btnsizer.AddButton(self.ok_btn)

#        # The cancel button
#        self.cancel_btn=wx.Button(self, wx.ID_CANCEL)
#        self.cancel_btn.SetHelpText("Do not perform any updates and return to the main program")
#        btnsizer.AddButton(self.cancel_btn)

        # This needs to be called when using wx.StdDialogButtonSizer
        btnsizer.Realize()

        # Add the button sixer to the main sixer so they appear below everything
        # else
        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        self.SetSizer(sizer)

        # This makes the dialog fit to the screen (i think??)
        sizer.Fit(self)

        # Update the buttons on the bottom according to the selection. i.e. if
        # there is not enough selections to carry out the search then the search
        # and apply buttons are greyed out
#        self.UpdateDialogButtons()

    '''
    Called when the user selects or deselects an ontology in the options
    '''
    def OnOntologyChange(self,e):
        index = e.GetSelection()
        label = self.ont_list.GetString(index)

        if self.ont_list.IsChecked(index):
            self.data.AddOntology(label)
        else:
            # We need at least one ontology selected so here we make sure of 
            # that and we reselect if not
            if len(self.ont_list.GetCheckedStrings())==0:
                self.ont_list.SetChecked([index])
            else:
                self.data.RemoveOntology(label)

        self.ont_list.SetSelection(index)    # so that (un)checking also selects (moves the highlight)
#        self.UpdateDialogButtons()

    '''
    Called when the user selects or deselects an ontology in the options
    '''
    def OnModifierChange(self,e):
        index = e.GetSelection()
        label = self.mod_list.GetString(index)

        if self.mod_list.IsChecked(index):
            self.data.AddModifier(label)
        else:
            # We need at least one ontology selected so here we make sure of 
            # that and we reselect if not
            if len(self.mod_list.GetCheckedStrings())==0:
                self.mod_list.SetChecked([index])
            else:
                self.data.RemoveModifier(label)

        self.mod_list.SetSelection(index)    # so that (un)checking also selects (moves the highlight)

    def OnTextEntry(self,e):
        proceed=self.CheckModifierText(self.add_mod_txt.GetValue())
        if proceed==True:
            self.add_btn.Enable()
        else:
            self.add_btn.Disable()

    '''
    Called when the user clicks the add modifier button or presses enter in the
    add modifier textbox
    '''
    def OnAddNewMod(self,e):
        text=self.add_mod_txt.GetValue()
        proceed=self.CheckModifierText(text)
        if proceed==True:
            self.data.NewModifier(text)
            self.mod_list.Append(text)
            selected_modifiers=self.data.GetSelectedModifiers()
            self.mod_list.SetCheckedStrings(selected_modifiers)
            self.add_mod_txt.Clear()
            self.OnTextEntry(e)
    
    '''
    Makes sure that the text entered into the add modifier box is 
    sensible
    '''
    def CheckModifierText(self,text):
        # Separated these out for debugging
        if text in self.data.GetModifiers():
#            print("present")
            return False
        elif len(text)==0:
#            print("ZERO")
            return False
#        if re.search(r'[\.\t\s,\|;\[\]\(\)]',text):
        elif re.match(r'^[\w-]+$',text):
#            print("ok")
            return True
        # Catch all for things not covered above
#        print("Catch ALL")
        return False


if __name__=="__main__":
##    TEST_BUTTON=1000
    # Version number
    version=0.1

    # Get the program name, this may be a symbolic link so get the
    # full path to the program and them the path to the program directory
    prog_name=sys.argv[0]
    prog_full_path=""

    # The program can be called in many different ways, via a link or directly called
    # with a full or relative path, so first see if it is being called via a soft link
    # If it is NOT a soft link then this will produce an OSError so we will then
    # get the absolute path and go from there
    try:
        prog_full_path=os.readlink(prog_name)
    except OSError:
        prog_full_path=os.path.abspath(prog_name)

    # Get the path to the program
    prog_path=os.path.dirname(prog_full_path)

    # Make a human readable program name, i.e. not the full path
    prog_name=os.path.basename(prog_name)

    # For debugging
    pp = pprint.PrettyPrinter()

    parser=argparse.ArgumentParser(description="A tool to assign ontology mappings to data")
    parser.add_argument("-f","--file",type=str,help="A text file with data to assign")
    parser.add_argument("-e","--existing-term-col",default=mapvar.DEFAULT_CURRENT_TERM_COL,type=str,help="The column name for a column that contains existing terms")
    parser.add_argument("-d","--file-delimiter",default=mapvar.DEFAULT_FILE_DELIMITER,type=str,help="The delimiter of the file")
    parser.add_argument("--ontology-delimiter",default=mapvar.DEFAULT_ONTOLOGY_DELIMITER,type=str,help="The delimiter of the ontology terms in the file, it is expected that they are flattened")    
    parser.add_argument("-o","--default-ontology",default=mapvar.DEFAULT_ONTOLOGY,type=str,help="The default (assumed) ontology used if the ontology name is not in [] next to each term")    
    parser.add_argument("-b","--base-dir",default=os.getcwd(),type=str,help="The base (or working) directory for the application")    
    parser.add_argument("ontodb",type=str,help="An ontology database to query for terms")
    parser.add_argument("outfile",type=str,nargs="?",help="An optional output file, if not supplied can be specified from the GUI")
    args=parser.parse_args()
#    print(args)

    # Initialise the file ontologies and modifiers to empty sets just in case
    # there is no file (note at the moment there has to be a file there is no 
    # other way of getting data into the application but in the future I will
    # add an open file option within the GUI)
    file_ontologies=set()
    file_modifiers=set()

    # Connect to the database and provide methods for queries
    ontodb=OntoDb(args.ontodb)
    
    # Initialise the ontology options
    ontology_options=OntologyOptions(ontodb)

    # If we have a file then build the data from it
    if args.file!=None:
        data,colnames,file_ontologies,file_modifiers=GetDataFromFile(args,ontology_options)
    
    # Provide the options any ontologies
    # that are not present in the database
    ontology_options.AddFileOntologies(file_ontologies)
    ontology_options.AddFileModifiers(file_modifiers)

    app=wx.App()  
    f=OntoFrame(data,args,colnames,ontodb,ontology_options)
    f.Show()
    app.MainLoop()
