# GWAS catalog parser

__version__: `0.1.1a0`

Helper scripts and API for parsing the GWAS catalog.

There is [online](https://cfinan.gitlab.io/gwas-catalog-parser/index.html) documentation for gwas-catalog-parser.

## Installation instructions
At present, gwas-catalog-parser is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c biopython -c conda-forge gwas-catalog-parser
```

There are currently builds for Python v3.8, v3.9 and v3.10 for Linux-64 and Mac-osx. Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/gwas-catalog-parser.git
cd gwas-catalog-parser
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.

## Next steps...
In addition to the installation a config file needs to be set up. This is an extension of the [genomic-config](https://gitlab.com/cfinan/genomic-config) configuration file. It details the locations of the UMLS database (not currently used), your e-mail address that is used for querying pubmed and the liftover chain file that is used to build a GRCh37 version of the catalog. The default name and location of this file is `~/.genomic_data.ini` and a minimal example is shown below:

```
# Make sure this file is not readable by anyone but you
[general]
email = joe.bloggs@hostname.com
# webdriver can also be in your $PATH
webdriver = /path/to/chromedriver

[species_synonyms]
homo_sapiens = human
homo sapiens = human
humans = human

[assembly_synonyms.human]
GRCh38 = b38
GRCh37 = b37

[umls_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# Also, don't forget to escape any % that are in the URL (with a second %)
umls.url = sqlite:////data/umls/umls_2022aa.db

# This is the default
[gwas_cat_conn]
# All connection options here should be prefixed with gwas_cat
gwas_cat.url = sqlite:////data/gwas_catalog/gwas_cat_grch38.db

# However, if you want separate GRCh38 and GRCh37 versions you can do something like
# this and supply the --gwas-catalog-section argument with gwas_cat_b38 or gwas_cat_b37
[gwas_cat_b38]
# All connection options here should be prefixed with gwas_cat
gwas_cat.url = sqlite:////data/gwas_catalog/gwas_cat_grch38.db

[gwas_cat_b37]
# All connection options here should be prefixed with gwas_cat
gwas_cat.url = sqlite:////data/gwas_catalog/gwas_cat_grch37.db

[chain_files.human.b38]
b37 = /path/to/chain_files/GRCh38_to_GRCh37.chain.gz
```

When initialising, the GWAS catalogue abbreviations are downloaded. Currently this uses Selanium and [Google Chrome](https://www.google.co.uk/chrome/) in headless mode. So chrome (or chromium if you prefer) and the corresponding [webdriver](https://chromedriver.chromium.org/downloads) must be available. The webdriver version must be matched to your chrome/chromium version. If the webdriver is not in your PATH it should be specified in the configuration file as shown above.

Initialisation will validate the genetic variants (unless ``--no-map-variants`` is on) and also download pubmed citation information and links to the source publications. If there is internet interruption then both of these processes will cache their downloads so the whole process will pick up from where it left off. Initialisation with variant mapping can take a long time ~8hrs. With no variant mapping it will probably take 30-60 mins.
