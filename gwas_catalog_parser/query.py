"""Functions to perform queries in the GWAS catalogue database.
"""
# Importing the version number (in __init__.py) and the package name
from gwas_catalog_parser import (
    __version__,
    __name__ as pkg_name,
    log,
    utils,
    query as q,
    constants as con,
    config,
    orm as o
)
from sqlalchemy.orm.exc import NoResultFound
from tqdm import tqdm
from datetime import datetime, date
from sqlalchemy.orm.exc import NoResultFound
import tempfile
import unicodedata
import argparse
import sys
import os
import csv
import hashlib
import gzip
import re
import shutil
import pprint as pp


_SCRIPT_NAME = "build-gwas-catalog"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``gwas_catalog_parser.build.build_gwas_catalog``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)
    args.cache_dir = os.path.expanduser(args.cache_dir)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    config_file = config.GwasCatalogConfig(
        args.config, args.assembly, cache_path=args.cache_dir,
        gwas_cat_db_section=args.gwas_catalog_section
    )
    logger.info(
        f"variant cache database path: {config_file.variant_cache_db_path()}"
    )
    try:
        gwas_catalog_query(config_file, tmpdir=args.tmpdir,
                           verbose=args.verbose,
                           map_variants=not args.no_map_variants)
        log.log_end(logger)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        config_file.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        '-o', '--outfile', action='count',
        help="The output file name, if not provided then will output to"
        " STDOUT. If the output file has a .gz extension then output will"
        " be gzipped compressed."
    )
    parser.add_argument(
        '-v', '--verbose', action='count',
        help="Indicate progress, use -vv for progress monitoring"
    )
    parser.add_argument(
        '-N', '--no-map-variants', action='store_true',
        help="Skip variant mapping as this can take a while but it"
        " useful to get variant alleles"
    )
    parser.add_argument(
        '-c', '--config', type=str, default=con.GWAS_CATALOG_CONFIG,
        help="An optional alternative location for the gwas catalog config"
        " file"
    )
    parser.add_argument(
        '--gwas-catalog-section', type=str,
        default=config.GwasCatalogConfig.CONFIG_GWAS_CAT_SECTION,
        help="The section name for the GWAS catalog database connection "
        "parameters"
    )
    parser.add_argument(
        '-T', '--tmpdir', type=str,
        help="An alternate temp file location. The default is the system "
        "temp location"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def gwas_catalog_query(config_file, tmpdir=None, verbose=False,
                       commit_every=10000, map_variants=True):
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genome_assembly(session):
    """Get the genome assembly option value.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    assembly : `str` or `NoneType`
        The genome assembly value, this will be NoneType if not set.
    """
    try:
        return session.query(o.Option.option_value).\
            filter(o.Option.option_type == con.ASSEMBLY_OPT).one()[0]
    except NoResultFound:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_version(session):
    """Get the Ensembl version of the gene definitions.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    ens_version : `str` or `NoneType`
        The Ensembl version of the gene definitions, this will be NoneType if
        not set, i.e. no gene definitions in the database.
    """
    try:
        return session.query(o.Option.option_value).\
            filter(o.Option.option_type == con.ENS_GENE_VERSION_OPT)\
            .one()[0]
    except NoResultFound:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_variant_from_hash(session, variant_hash):
    """Get any variants that match the ``variant_hash``.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object/
    variant_hash : `str`
        the 32 character MD5 sum of the variant details.

    Returns
    -------
    variants : `list` of `gwas_catalog_parser.orm.Variant`
        The variant info for the hash string.

    Raises
    ------
    sqlalchemy.orm.exc.NoResultFound
        If there is no match.
    """
    return session.query(o.Variant).\
        filter(o.Variant.variant_hash == variant_hash).one()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene(session, ensembl_gene_id):
    """Get the gene info for an ensembl gene ID.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object/

    Returns
    -------
    gene : `gwas_catalog_parser.orm.Gene`
        The gene info for the ensembl gene identifier.

    Raises
    ------
    sqlalchemy.orm.exc.NoResultFound
        If there is no match.
    """
    return session.query(o.Gene).\
        filter(o.Gene.ensembl_gene_id == ensembl_gene_id).one()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_pubmed(session, pubmed_id):
    """Get the pubmed info for a pubmed ID.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    pubmed : `gwas_catalog_parser.orm.Gene`
        The pubmed info for the pubmed identifier.

    Raises
    ------
    sqlalchemy.orm.exc.NoResultFound
        If there is no match.
    """
    return session.query(o.Pubmed).\
        filter(o.Pubmed.pubmed_id == pubmed_id).one()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_raw_assoc_count(session, loaded=None):
    """Get a count of the GWAS catalog raw association records.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object/
    loaded : `bool`, optional, default: `False`
        Should the count be restricted to any rows (``NoneType``),
        loaded rows (``True``) or unloaded rows ``False``.

    Returns
    -------
    count : `int`
        The number of raw records in the ``raw_association`` table.
    """
    if loaded is None:
        return session.query(o.RawAssociation).count()
    elif loaded is True:
        return session.query(o.RawAssociation).\
            filter(o.RawAssociation.loaded == True).count()
    elif loaded is False:
        return session.query(o.RawAssociation).\
            filter(o.RawAssociation.loaded == False).count()
    else:
        raise ValueError(f"unknown loaded value: {loaded}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_identifers(session, loaded=None):
    """Get all raw association IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    loaded : `bool`, optional, default: `False`
        Should the count be restricted to any rows (``NoneType``),
        loaded rows (``True``) or unloaded rows ``False``.

    Yields
    ------
    accession : `str`
        The GWAS catalogue accession value.
    md5_hash : `str`
        The MD5 hash for the record.
    """
    q = session.query(o.RawAssociation.gwas_catalog_accession,
                      o.RawAssociation.md5hash)

    if loaded is True:
        q = q.filter(o.RawAssociation.loaded == True)
    elif loaded is False:
        q = q.filter(o.RawAssociation.loaded == False)
    elif loaded is not None:
        raise ValueError(f"unknown loaded value: {loaded}")

    for acc, md5_hash in q:
        yield acc, md5_hash


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_pheno_class(session):
    """Get all the phenotype classes as a dictionary.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    phenotye_class : `dict`
        The keys are the phenotype class names and the values are the
        primary keys IDs.
    """
    return dict([(i.phenotype_class_name, i.phenotype_class_pk)
                 for i in session.query(o.PhenotypeClass)])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_all_gene_class(session):
    """Get all the gene classes as a dictionary.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    gene_class : `dict`
        The keys are the gene class names and the values are the
        primary keys IDs.
    """
    return dict([(i.gene_class_name, i.gene_class_pk)
                 for i in session.query(o.GeneClass)])
