"""SQLAlchemy ORM mappings for the GWAS catalog.
"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    Float,
    SmallInteger,
    DateTime,
    ForeignKey
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_all_tables(session):
    """Create all the ORM tables.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session where ``session.get_bind()`` will return a valid engine.
    """
    # Ensure all the tables are created
    Base.metadata.create_all(
        session.get_bind()
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Option(Base):
    """A representation of the ``option`` table. This holds version information
    for the import.

    Parameters
    ----------
    option_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    option_type : `str`
        What the ``option_value`` represents (length 15).
    option_value : `str`
        The value for the ``option_type`` (length 15).
    """
    __tablename__ = 'option'

    option_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    option_type = Column(
        String(15), nullable=False, doc="What the ``option_value`` represents."
    )
    option_value = Column(
        String(15), nullable=False, doc="The value for the ``option_type``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Gene(Base):
    """A representation of the ``genes`` lookup table. This will hold all
    annotated genes in Ensembl.

    Parameters
    ----------
    gene_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    chr_name : `str`
        The chromosome containing the gene (length 15).
    start_pos : `int`
        The chromosomal start position in bp.
    end_pos : `int`
        The chromosomal end position in bp.
    strand : `int`
        The strand of the gene 1/-1 with 0 being undefined.
    ensembl_gene_id : `str`
        The Ensembl gene identifier. This is indexed (length 15).
    version : `int`
        The gene version.
    name : `str`, optional, default: `NoneType`
        The display name for the gene (length 50).
    biotype : `str`, optional, default: `NoneType`
        The biotype of the gene (length 50).
    source : `str`, optional, default: `NoneType`
        The source for the gene annotation (length 50).
    score : `float`, optional, default: `NoneType`
        Not sure. I think this might be some sort of gene annotation score.
    gene_synonyms : `gwas_catalog_parser.orm.GeneSynonym`, optional,\
    default: `NoneType`
        The relationship link to the ``gene_synonym`` table.
    associated_gene : `gwas_catalog_parser.orm.AssociatedGene`, optional,\
    default: `NoneType`
        The relationship link to the ``associated_gene`` table.
    """
    __tablename__ = 'genes'

    gene_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    chr_name = Column(
        String(15), nullable=False, default='0',
        doc="The chromosome containing the gene"
    )
    start_pos = Column(
        Integer, nullable=False, default=0,
        doc="The chromosomal start position in bp."
    )
    end_pos = Column(
        Integer, nullable=False, default=0,
        doc="The chromosomal end position in bp."
    )
    strand = Column(
        Integer, nullable=False, default=0,
        doc="The strand of the gene 1/-1 with 0 being undefined."
    )
    ensembl_gene_id = Column(
        String(15), nullable=False, index=True,
        doc="The Ensembl gene identifier"
    )
    version = Column(Integer, nullable=False, default=0,
                     doc="The gene version.")
    name = Column(String(50), doc="The display name for the gene")
    biotype = Column(String(50), doc="The biotype of the gene.")
    source = Column(String(50), doc="The source for the gene annotation")
    score = Column(
        Float,
        doc="Not sure. I think this might be some sort of gene "
        "annotation score."
    )

    # #### relationships #####
    gene_synonyms = relationship(
        "GeneSynonym", back_populates="gene",
        doc="The relationship link to the ``gene_synonym`` table."
    )
    associated_gene = relationship(
        "AssociatedGene", back_populates="gene",
        doc="The relationship link to the ``associated_gene`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GeneSynonym(Base):
    """A representation of a ``gene_synonyms`` table. The different names that
    the gene is known by, these are downloaded from HGNC.

    Parameters
    ----------
    gene_syn_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    gene_pk : `int`
        The connection to the ``genes`` table. Foreign key to
        ``genes.gene_pk``.
    syn_type : `str`
        The synonym type (where is is from). This is indexed (length 25).
    syn_name : `str`
        The synonym name. This is indexed (length 250).
    genes : `gwas_catalog_parser.orm.Gene`, optional, default: `NoneType`
        The relationship link to the ``genes`` table.
    """
    __tablename__ = 'gene_synonyms'

    gene_syn_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    gene_pk = Column(
        Integer, ForeignKey('genes.gene_pk'), nullable=False,
        doc="The connection to the ``genes`` table."
    )
    syn_type = Column(
        String(25), nullable=False, index=True,
        doc="The synonym type (where is is from)"
    )
    syn_name = Column(String(250), nullable=False, index=True,
                      doc="The synonym name")

    # #### relationships #####
    gene = relationship(
        Gene, back_populates="gene_synonyms",
        doc="The relationship link to the ``genes`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GeneClass(Base):
    """A representation of the ``gene_class`` table. This stores how the
    associated gene was defined in the GWAS catalog.

    Parameters
    ----------
    gene_class_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    gene_class_name : `str`, optional, default: `NoneType`
        The phenotype class name (length 20).
    associated_gene : `gwas_catalog_parser.orm.AssociatedGene`, optional,\
    default: `NoneType`
        The relationship link to the ``associated_gene`` table.
    """
    __tablename__ = 'gene_class'

    gene_class_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True, doc="The main primary key column. Will be"
        " auto-incremented if not supplied."
    )
    gene_class_name = Column(String(20), unique=True,
                             doc="The phenotype class name")

    # #### relationships #####
    associated_gene = relationship(
        "AssociatedGene",
        back_populates="gene_class",
        doc="The relationship link to the ``associated_gene`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssociatedGene(Base):
    """A representation of a ``associated_gene`` table.

    Parameters
    ----------
    assoc_gene_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    association_pk : `int`
        The connection to the genes table. Foreign key to
        ``association.association_pk``.
    gene_pk : `int`
        The connection to the genes table. Foreign key to ``genes.gene_pk``.
    gene_class_pk : `int`
        The connection to the genes table. Foreign key to
        ``gene_class.gene_class_pk``.
    genes : `gwas_catalog_parser.orm.Gene`, optional, default: `NoneType`
        The relationship link to the ``genes`` table.
    association : `gwas_catalog_parser.orm.Association`, optional,\
    default: `NoneType`
        The relationship link to the ``association`` table.
    gene_class : `gwas_catalog_parser.orm.GeneClass`, optional, \
    default: `NoneType`
        The relationship link to the ``genes_class`` table.
    """
    __tablename__ = 'associated_gene'

    assoc_gene_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    association_pk = Column(
        Integer, ForeignKey('association.association_pk'),
        nullable=False, doc="The connection to the genes table."
    )
    gene_pk = Column(Integer, ForeignKey('genes.gene_pk'), nullable=False,
                     doc="The connection to the genes table.")
    gene_class_pk = Column(
        Integer, ForeignKey('gene_class.gene_class_pk'), nullable=False,
        doc="The connection to the genes table."
    )

    # #### relationships #####
    gene = relationship(
        Gene, back_populates="associated_gene",
        doc="The relationship link to the ``genes`` table."
    )
    association = relationship(
        'Association', back_populates="associated_gene",
        doc="The relationship link to the ``association`` table."
    )
    gene_class = relationship(
        GeneClass, back_populates="associated_gene",
        doc="The relationship link to the ``genes_class`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Variant(Base):
    """A representation of a ``variant`` table.

    Parameters
    ----------
    variant_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    chr_name : `str`
        The chromosome name for the valiant. This is indexed (length 15).
    start_pos : `int`
        The start position for the variant. This is indexed.
    ref_allele : `str`, optional, default: `NoneType`
        The reference allele for the variant (length 1000).
    alt_alleles : `str`, optional, default: `NoneType`
        The alternate alleles for the variant, this is a comma separated list
        if > 1 alt allele (length 1000).
    strand : `int`, optional, default: `NoneType`
        The strand for the variant, either 1 or -1.
    uni_id : `str`, optional, default: `NoneType`
        The universal variant identifier. This is the ``chr_start_<alleles in
        sort order>``. This is indexed (length 1000).
    var_id : `str`, optional, default: `NoneType`
        The variant identifier. This is indexed (length 255).
    minor_allele : `str`, optional, default: `NoneType`
        The minor allele, depending on how the variant data was obtained
        (positional query vs. name), this may not be present (length 255).
    global_maf : `float`, optional, default: `NoneType`
        The global MAF from Ensembl, depending on how the variant data was
        obtained (positional query vs. name), this may not be present.
    worst_consequence : `str`, optional, default: `NoneType`
        The worst consequence for the variant (length 50).
    nalleles : `int`, optional, default: `ColumnDefault(0)`
        The number of alleles at the variant site.
    variant_hash : `str`
        The variant MD5 hash from the processed input data. This is indexed
        (length 32).
    associated_variants : `gwas_catalog_parser.orm.AssociatedVariant`,\
    optional, default: `NoneType`
        The relationship link to the ``associated_variant`` table.
    """
    __tablename__ = 'variants'

    variant_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    chr_name = Column(
        String(15), nullable=False, index=True, default='0',
        doc="The chromosome name for the valiant"
    )
    start_pos = Column(
        Integer, nullable=False, index=True, default=0,
        doc="The start position for the variant."
    )
    ref_allele = Column(
        String(1000), doc="The reference allele for the variant"
    )
    alt_alleles = Column(
        String(1000), doc="The alternate alleles for the variant, this is"
        " a comma separated list if > 1 alt allele"
    )
    strand = Column(SmallInteger,
                    doc="The strand for the variant, either 1 or -1.")
    uni_id = Column(
        String(1000), index=True,
        doc="The universal variant identifier. This is the"
        " ``chr_start_<alleles in sort order>``."
    )
    var_id = Column(String(255), index=True, doc="The variant identifier")
    minor_allele = Column(
        String(255),
        doc="The minor allele, depending on how the variant data was obtained"
        " (positional query vs. name), this may not be present"
    )
    global_maf = Column(
        Float, doc="The global MAF from Ensembl, depending on how the"
        " variant data was obtained (positional query vs. name), this may not"
        " be present"
    )
    worst_consequence = Column(
        String(50), doc="The worst consequence for the variant"
    )
    nalleles = Column(
        Integer, default=0, doc="The number of alleles at the variant site."
    )
    variant_hash = Column(
        String(32), nullable=False, index=True,
        doc="The variant MD5 hash from the processed input data."
    )

    # #### relationships #####
    associated_variants = relationship(
        "AssociatedVariant", back_populates="variant",
        doc="The relationship link to the ``associated_variant`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawAssociation(Base):
    """A representation of the ``raw_association`` table. This table is the
    unprocessed GWAS catalog associations data imported into a table in the
    database.

    Parameters
    ----------
    raw_association_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    date_added_to_catalog : `date.datetime`, optional, default: `NoneType`
        The date when the record was added to the GWAS catalogue.
    pubmed_id : `int`, optional, default: `NoneType`
        The pubmed identifier associated with the study. Foreign key to
        ``pubmed.pubmed_id``.
    first_author : `str`, optional, default: `NoneType`
        The first author of the study (length 98).
    date : `date.datetime`, optional, default: `NoneType`
        The date of the study.
    journal : `str`, optional, default: `NoneType`
        The publication journal (length 51).
    link : `str`, optional, default: `NoneType`
        The URL link to the publication (length 46).
    study : `str`, optional, default: `NoneType`
        The study name (length text).
    disease_trait : `str`, optional, default: `NoneType`
        The disease or trait of the GWAS (length 180).
    initial_sample_size : `str`, optional, default: `NoneType`
        Sample size and ancestry description for stage 1 of GWAS (summing
        across multiple Stage 1 populations, if applicable) (length text).
    replication_sample_size : `str`, optional, default: `NoneType`
        Sample size and ancestry description for subsequent replication(s)
        (summing across multiple populations, if applicable) (length text).
    region : `str`, optional, default: `NoneType`
        Cytogenetic region associated with rs number (length 29).
    chr_id : `str`, optional, default: `NoneType`
        Chromosome number associated with rs number (length text).
    chr_pos : `str`, optional, default: `NoneType`
        Chromosomal position associated with rs number (length text).
    reported_genes : `str`, optional, default: `NoneType`
        Gene(s) reported by author (length text).
    mapped_gene : `str`, optional, default: `NoneType`
        Gene(s) mapped to the strongest SNP. If the SNP is located within a
        gene, that gene is listed. If the SNP is located within multiple genes,
        these genes are listed separated by commas. If the SNP is intergenic,
        the upstream and downstream genes are listed, separated by a hyphen
        (length text).
    upstream_gene_id : `str`, optional, default: `NoneType`
        Entrez Gene ID for nearest upstream gene to rs number, if not within
        gene (length 25).
    downstream_gene_id : `str`, optional, default: `NoneType`
        Entrez Gene ID for nearest downstream gene to rs number, if not within
        gene (length 25).
    snp_gene_ids : `str`, optional, default: `NoneType`
        Entrez Gene ID, if rs number within gene; multiple genes denotes
        overlapping transcripts (length text).
    upstream_gene_distance : `str`, optional, default: `NoneType`
        Distance in kb for nearest upstream gene to rs number, if not within
        gene (length 17).
    downstream_gene_distance : `str`, optional, default: `NoneType`
        Distance in kb for nearest downstream gene to rs number, if not within
        gene (length 17).
    strongest_snp_risk_allele : `str`, optional, default: `NoneType`
        SNP(s) most strongly associated with trait + risk allele (? for unknown
        risk allele). May also refer to a haplotype (length text).
    snps : `str`, optional, default: `NoneType`
        Strongest SNP; if a haplotype it may include more than one rs number
        (multiple SNPs comprising the haplotype) (length text).
    merged : `int`, optional, default: `NoneType`
        Denotes whether the SNP has been merged into a subsequent rs record (0
        = no; 1 = yes;).
    snp_id_current : `str`, optional, default: `NoneType`
        Current rs number (will differ from strongest SNP when merged = 1)
        (length 22).
    context : `str`, optional, default: `NoneType`
        Provides information on a variant’s predicted most severe functional
        effect from Ensembl (length text).
    intergenic : `str`, optional, default: `NoneType`
        Denotes whether SNP is in intergenic region (0 = no; 1 = yes) (length
        1).
    risk_allele_frequency : `str`, optional, default: `NoneType`
        Reported risk/effect allele frequency associated with strongest SNP in
        controls (if not available among all controls, among the control group
        with the largest sample size). If the associated locus is a haplotype
        the haplotype frequency will be extracted (length 39).
    p_value : `float`, optional, default: `NoneType`
        Reported p-value for strongest SNP risk allele (linked to dbGaP
        Association Browser). Note that p-values are rounded to 1 significant
        digit (for example, a published p-value of 4.8 x 10-7 is rounded to 5 x
        10-7).
    pvalue_mlog : `float`, optional, default: `NoneType`
        -log(p-value).
    p_value_text : `str`, optional, default: `NoneType`
        Information describing context of p-value (e.g. females, smokers)
        (length 222).
    or_or_beta : `str`, optional, default: `NoneType`
        Reported odds ratio or beta-coefficient associated with strongest SNP
        risk allele. Note that if an OR <1 is reported this is inverted, along
        with the reported allele, so that all ORs included in the Catalog are
        >1. Appropriate unit and increase/decrease are included for beta
        coefficients (length 22).
    ci_text : `str`, optional, default: `NoneType`
        Reported 95% confidence interval associated with strongest SNP risk
        allele, along with unit in the case of beta-coefficients. If 95% CIs
        are not published, we estimate these using the standard error, where
        available (length 67).
    platform_snps_passing_qc : `str`, optional, default: `NoneType`
        Genotyping platform manufacturer used in Stage 1; also includes
        notation of pooled DNA study design or imputation of SNPs, where
        applicable (length 72).
    cnv : `str`, optional, default: `NoneType`
        Study of copy number variation (yes/no) (length 1).
    mapped_trait : `str`, optional, default: `NoneType`
        Mapped Experimental Factor Ontology trait for this study (length text).
    mapped_trait_uri : `str`, optional, default: `NoneType`
        URI of the EFO trait (length text).
    gwas_catalog_accession : `str`, optional, default: `NoneType`
        Accession ID allocated to a GWAS Catalog study. This is indexed (length
        20).
    genotyping_technology : `str`, optional, default: `NoneType`
        Genotyping technology/ies used in this study, with additional array
        information (ex. Immunochip or Exome array) in brackets (length 96).
    loaded : `int`, optional, default: `ColumnDefault(0)`
        Indicates if the record has been processed into the main database. Not
        currently used.
    md5hash : `str`
        An MD5 check sum of the row, this is used to detect existing entries
        (length 32).
    import_date : `date.datetime`
        The date this entry was loaded into the database.
    pubmed : `gwas_catalog_parser.orm.Pubmed`, optional, default: `NoneType`
        The relationship link to the ``pubmed`` table.
    association : `gwas_catalog_parser.orm.Association`, optional, \
    default: `NoneType`
        The relationship link to the ``associations`` table.
    """
    __tablename__ = 'raw_association'

    raw_association_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    date_added_to_catalog = Column(
        DateTime, doc="The date when the record was added to the GWAS "
        "catalogue."
    )
    pubmed_id = Column(
        Integer, ForeignKey('pubmed.pubmed_id'),
        doc="The pubmed identifier associated with the study."
    )
    first_author = Column(String(98), doc="The first author of the study")
    date = Column(DateTime, doc="The date of the study.")
    journal = Column(String(51), doc="The publication journal")
    link = Column(String(46), doc="The URL link to the publication.")
    study = Column(Text, doc="The study name")
    disease_trait = Column(
        String(180), doc="The disease or trait of the GWAS."
    )
    initial_sample_size = Column(
        Text, doc="Sample size and ancestry description for stage 1 of GWAS"
        " (summing across multiple Stage 1 populations, if applicable)."
    )
    replication_sample_size = Column(
        Text, doc="Sample size and ancestry description for subsequent "
        "replication(s) (summing across multiple populations, if applicable)"
    )
    region = Column(
        String(29), doc="Cytogenetic region associated with rs number"
    )
    chr_id = Column(
        Text, doc="Chromosome number associated with rs number"
    )
    chr_pos = Column(
        Text, doc="Chromosomal position associated with rs number"
    )
    reported_genes = Column(Text, doc="Gene(s) reported by author")
    mapped_gene = Column(
        Text,
        doc="Gene(s) mapped to the strongest SNP. If the SNP is located "
        "within a gene, that gene is listed. If the SNP is located within"
        " multiple genes, these genes are listed separated by commas. If"
        " the SNP is intergenic, the upstream and downstream genes are"
        " listed, separated by a hyphen"
    )
    upstream_gene_id = Column(
        String(25), doc="Entrez Gene ID for nearest upstream gene to rs "
        "number, if not within gene"
    )
    downstream_gene_id = Column(
        String(25),
        doc="Entrez Gene ID for nearest downstream gene to rs number, if not "
        "within gene"
    )
    snp_gene_ids = Column(
        Text,
        doc="Entrez Gene ID, if rs number within gene; multiple genes "
        "denotes overlapping transcripts"
    )
    upstream_gene_distance = Column(
        String(17),
        doc="Distance in kb for nearest upstream gene to rs number, if not "
        "within gene"
    )
    downstream_gene_distance = Column(
        String(17),
        doc="Distance in kb for nearest downstream gene to rs number, if not"
        " within gene"
    )
    strongest_snp_risk_allele = Column(
        Text,
        doc="SNP(s) most strongly associated with trait + risk allele (? for"
        " unknown risk allele). May also refer to a haplotype"
    )
    snps = Column(
        Text, doc="Strongest SNP; if a haplotype it may include more than"
        " one rs number (multiple SNPs comprising the haplotype)"
    )
    merged = Column(
        Integer, doc="Denotes whether the SNP has been merged into a "
        "subsequent rs record (0 = no; 1 = yes;)"
    )
    snp_id_current = Column(
        String(22),
        doc="Current rs number (will differ from strongest SNP when "
        "merged = 1)"
    )
    context = Column(
        Text,
        doc="Provides information on a variant’s predicted most severe "
        "functional effect from Ensembl"
    )
    intergenic = Column(
        String(1), doc="Denotes whether SNP is in intergenic region (0 ="
        " no; 1 = yes)"
    )
    risk_allele_frequency = Column(
        String(39),
        doc="Reported risk/effect allele frequency associated with strongest "
        "SNP in controls (if not available among all controls, among the "
        "control group with the largest sample size). If the associated locus "
        "is a haplotype the haplotype frequency will be extracted"
    )
    p_value = Column(
        Float,
        doc="Reported p-value for strongest SNP risk allele (linked to dbGaP"
        " Association Browser). Note that p-values are rounded to 1 "
        "significant digit (for example, a published p-value of 4.8 x 10-7 is "
        "rounded to 5 x 10-7)."
    )
    pvalue_mlog = Column(Float, doc="-log(p-value).")
    p_value_text = Column(
        String(222),
        doc="Information describing context of p-value (e.g. females, smokers)"
    )
    or_or_beta = Column(
        String(22),
        doc="Reported odds ratio or beta-coefficient associated with strongest"
        " SNP risk allele. Note that if an OR <1 is reported this is inverted,"
        " along with the reported allele, so that all ORs included in the "
        "Catalog are >1. Appropriate unit and increase/decrease are included "
        "for beta coefficients"
    )
    ci_text = Column(
        String(67),
        doc="Reported 95% confidence interval associated with strongest SNP "
        "risk allele, along with unit in the case of beta-coefficients. If 95%"
        " CIs are not published, we estimate these using the standard error, "
        "where available"
    )
    platform_snps_passing_qc = Column(
        String(72),
        doc="Genotyping platform manufacturer used in Stage 1; also includes"
        " notation of pooled DNA study design or imputation of SNPs, where"
        " applicable"
    )
    cnv = Column(String(1), doc="Study of copy number variation (yes/no)")
    mapped_trait = Column(
        Text, doc="Mapped Experimental Factor Ontology trait for this study"
    )
    mapped_trait_uri = Column(Text, doc="URI of the EFO trait")
    gwas_catalog_accession = Column(
        String(20), index=True,
        doc="Accession ID allocated to a GWAS Catalog study"
    )
    genotyping_technology = Column(
        String(96),
        doc="Genotyping technology/ies used in this study, with additional"
        " array information (ex. Immunochip or Exome array) in brackets"
    )
    loaded = Column(
        SmallInteger, default=0,
        doc="Indicates if the record has been processed into the main"
        " database. Not currently used."
    )
    md5hash = Column(
        String(32), nullable=False,
        doc="An MD5 check sum of the row, this is used to detect existing"
        " entries."
    )
    import_date = Column(
        DateTime, nullable=False,
        doc="The date this entry was loaded into the database."
    )

    # #### Relationships #####
    pubmed = relationship(
        "Pubmed", back_populates="raw_association",
        doc="The relationship link to the ``pubmed`` table."
    )
    association = relationship(
        "Association", back_populates="raw_association",
        doc="The relationship link to the ``associations`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawStudy(Base):
    """A representation of the ``raw_study`` table. This table is the
    unprocessed GWAS catalog data study data imported into a table in the
    database.

    Parameters
    ----------
    raw_study_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    date_added_to_catalog : `date.datetime`, optional, default: `NoneType`
        The date when the record was added to the GWAS catalogue.
    pubmed_id : `int`, optional, default: `NoneType`
        The pubmed identifier associated with the study. Foreign key to
        ``pubmed.pubmed_id``.
    first_author : `str`, optional, default: `NoneType`
        The first author of the study (length 98).
    date : `date.datetime`, optional, default: `NoneType`
        The date of the study.
    journal : `str`, optional, default: `NoneType`
        The publication journal (length 51).
    link : `str`, optional, default: `NoneType`
        The URL link to the publication (length 46).
    disease_trait : `str`, optional, default: `NoneType`
        The disease trait (length 180).
    initial_sample_size : `str`, optional, default: `NoneType`
        Sample size and ancestry description for stage 1 of GWAS (summing
        across multiple Stage 1 populations, if applicable) (length text).
    replication_sample_size : `str`, optional, default: `NoneType`
        Sample size and ancestry description for subsequent replication(s)
        (summing across multiple populations, if applicable) (length text).
    platform_snps_passing_qc : `str`, optional, default: `NoneType`
        Genotyping platform manufacturer used in Stage 1; also includes
        notation of pooled DNA study design or imputation of SNPs, where
        applicable (length 72).
    association_count : `int`, optional, default: `NoneType`
        The number of associations in the GWAS catalog for this study.
    mapped_trait : `str`, optional, default: `NoneType`
        Mapped Experimental Factor Ontology trait for this study (length text).
    mapped_trait_uri : `str`, optional, default: `NoneType`
        URI of the EFO trait (length text).
    gwas_catalog_accession : `str`, optional, default: `NoneType`
        Accession ID allocated to a GWAS Catalog study. This is indexed (length
        20).
    genotyping_technology : `str`, optional, default: `NoneType`
        Genotyping technology/ies used in this study, with additional array
        information (ex. Immunochip or Exome array) in brackets (length 96).
    summary_stats_location : `str`, optional, default: `NoneType`
        The location of the summary statistics file (length text).
    submission_date : `date.datetime`, optional, default: `NoneType`
        The date the GWAS was submitted to the GWAS catalog.
    statistical_model : `str`, optional, default: `NoneType`
        Details of the statistical model used to determine association
        significance (length text).
    background_trait : `str`, optional, default: `NoneType`
        Any background trait(s) shared by all individuals in the GWAS (free
        text) (length text).
    mapped_background_trait : `str`, optional, default: `NoneType`
        Any background trait(s) shared by all individuals in the GWAS mapped to
        the EFO (length text).
    mapped_background_trait_uri : `str`, optional, default: `NoneType`
        Not currently used (length text).
    loaded : `int`, optional, default: `ColumnDefault(0)`
        Indicates if the record has been processed into the main database (not
        currently used).
    md5hash : `str`, optional, default: `NoneType`
        An MD5 check sum of the row, this is used to detect existing entries
        (length 32).
    import_date : `date.datetime`
        The date the raw data was loaded into the database.
    pubmed : `gwas_catalog_parser.orm.Pubmed`, optional, default: `NoneType`
        The relationship link to the ``pubmed`` table.
    study : `gwas_catalog_parser.orm.Study`, optional, default: `NoneType`
        The relationship link to the ``study`` table.
    """
    __tablename__ = 'raw_study'

    raw_study_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    date_added_to_catalog = Column(
        DateTime, doc="The date when the record was added to the GWAS "
        "catalogue."
    )
    pubmed_id = Column(
        Integer, ForeignKey('pubmed.pubmed_id'),
        doc="The pubmed identifier associated with the study."
    )
    first_author = Column(String(98), doc="The first author of the study")
    date = Column(DateTime, doc="The date of the study")
    journal = Column(String(51), doc="The publication journal")
    link = Column(String(46), doc="The URL link to the publication")
    study = Column(Text, doc="The study name")
    disease_trait = Column(String(180), doc="The disease trait")
    initial_sample_size = Column(
        Text,
        doc="Sample size and ancestry description for stage 1 of GWAS "
        "(summing across multiple Stage 1 populations, if applicable)."
    )
    replication_sample_size = Column(
        Text, doc="Sample size and ancestry description for subsequent "
        "replication(s) (summing across multiple populations, if "
        "applicable)"
    )
    platform_snps_passing_qc = Column(
        String(72), doc="Genotyping platform manufacturer used in Stage 1; "
        "also includes notation of pooled DNA study design or imputation of"
        " SNPs, where applicable"
    )
    association_count = Column(
        Integer, doc="The number of associations in the GWAS catalog for "
        "this study"
    )
    mapped_trait = Column(
        Text, doc="Mapped Experimental Factor Ontology trait for this study"
    )
    mapped_trait_uri = Column(Text, doc="URI of the EFO trait")
    gwas_catalog_accession = Column(
        String(20), index=True,
        doc="Accession ID allocated to a GWAS Catalog study"
    )
    genotyping_technology = Column(
        String(96),
        doc="Genotyping technology/ies used in this study, with additional"
        " array information (ex. Immunochip or Exome array) in brackets"
    )
    summary_stats_location = Column(
        Text, doc="The location of the summary statistics file"
    )
    submission_date = Column(
        DateTime, doc="The date the GWAS was submitted to the GWAS catalog."
    )
    statistical_model = Column(
        Text,
        doc="Details of the statistical model used to determine association"
        " significance"
    )
    background_trait = Column(
        Text,
        doc="Any background trait(s) shared by all individuals in the"
        " GWAS (free text)"
    )
    mapped_background_trait = Column(
        Text,
        doc="Any background trait(s) shared by all individuals in the"
        " GWAS mapped to the EFO"
    )
    mapped_background_trait_uri = Column(Text, doc="Not currently used")
    loaded = Column(
        SmallInteger, default=0,
        doc="Indicates if the record has been processed into the main"
        " database (not currently used)."
    )
    md5hash = Column(
        String(32),
        doc="An MD5 check sum of the row, this is used to detect "
        "existing entries"
    )
    import_date = Column(
        DateTime, nullable=False, doc="The date the raw data was loaded"
        " into the database."
    )

    # #### Relationships #####
    pubmed = relationship(
        "Pubmed", back_populates="raw_study",
        doc="The relationship link to the ``pubmed`` table."
    )
    study = relationship(
        "Study", back_populates="raw_study",
        doc="The relationship link to the ``study`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawAncestry(Base):
    """A representation of the ``raw_ancestry`` table. This table is the
    unprocessed GWAS catalog ancestry data imported into a table in the
    database.

    Parameters
    ----------
    raw_ancestry_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    gwas_catalog_accession : `str`, optional, default: `NoneType`
        The GWAS catalog accession number. This is indexed (length 20).
    pubmed_id : `int`, optional, default: `NoneType`
        The pubmed identifier associated with the study.
    first_author : `str`, optional, default: `NoneType`
        The first author of the study (length 98).
    date : `date.datetime`, optional, default: `NoneType`
        The date of the study.
    initial_sample_desc : `str`, optional, default: `NoneType`
        The discovery sample description (length text).
    replication_sample_desc : `str`, optional, default: `NoneType`
        The replication sample description (length text).
    stage : `str`, optional, default: `NoneType`
        Unknown (length text).
    sample_size : `int`, optional, default: `NoneType`
        The number of individuals.
    broad_ancestry : `str`, optional, default: `NoneType`
        The broad ancestral group of the cohort (length text).
    country_of_origin : `str`, optional, default: `NoneType`
        Not used (length text).
    country_of_recruit : `str`, optional, default: `NoneType`
        Country of recruitment of the individuals in the sample (length text).
    additional_info : `str`, optional, default: `NoneType`
        Any additional ancestry descriptors relevant to the sample description
        (length text).
    ancestry_descriptor : `str`, optional, default: `NoneType`
        The most detailed ancestry descriptor(s) for the sample (length text).
    is_founder : `str`, optional, default: `NoneType`
        Description of a founder or genetically isolated population (length
        100).
    no_cases : `int`, optional, default: `NoneType`
        The number of cases in this broad ancestry group.
    no_controls : `int`, optional, default: `NoneType`
        The number of controls in this broad ancestry group.
    sample_desc : `str`, optional, default: `NoneType`
        Additional sample information required for the interpretation of result
        (length text).
    cohorts : `str`, optional, default: `NoneType`
        List of cohort(s) represented in the discovery sample (length text).
    cohort_ref : `str`, optional, default: `NoneType`
        List of cohort specific identifier(s) issued to this research study
        (length text).
    loaded : `int`, optional, default: `ColumnDefault(0)`
        Indicates if the record has been processed into the main database (not
        currently used).
    md5hash : `str`
        An MD5 check sum of the row, this is used to detect existing entries
        (length 32).
    import_date : `date.datetime`
        The date the row was added to the database.
    """
    __tablename__ = 'raw_ancestry'

    raw_ancestry_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    gwas_catalog_accession = Column(
        String(20), index=True, doc="The GWAS catalog accession number."
    )
    pubmed_id = Column(
        Integer, doc="The pubmed identifier associated with the study."
    )
    first_author = Column(String(98), doc="The first author of the study.")
    date = Column(DateTime, doc="The date of the study.")
    initial_sample_desc = Column(
        Text, doc="The discovery sample description."
    )
    replication_sample_desc = Column(
        Text, doc="The replication sample description."
    )
    stage = Column(Text, doc="Unknown.")
    sample_size = Column(Integer, doc="The number of individuals.")
    broad_ancestry = Column(
        Text, doc="The broad ancestral group of the cohort"
    )
    country_of_origin = Column(Text, doc="Not used")
    country_of_recruit = Column(
        Text, doc="Country of recruitment of the individuals in the sample"
    )
    additional_info = Column(
        Text,
        doc="Any additional ancestry descriptors relevant to the sample"
        " description"
    )
    ancestry_descriptor = Column(
        Text, doc="The most detailed ancestry descriptor(s) for the sample"
    )
    is_founder = Column(
        String(100),
        doc="Description of a founder or genetically isolated population"
    )
    no_cases = Column(
        Integer, doc="The number of cases in this broad ancestry group."
    )
    no_controls = Column(
        Integer, doc="The number of controls in this broad ancestry group."
    )
    sample_desc = Column(
        Text, doc="Additional sample information required for the "
        "interpretation of result"
    )
    cohorts = Column(
        Text, doc="List of cohort(s) represented in the discovery sample"
    )
    cohort_ref = Column(
        Text, doc="List of cohort specific identifier(s) issued to this "
        "research study."
    )
    loaded = Column(
        SmallInteger, default=0, doc="Indicates if the record has been"
        " processed into the main database (not currently used)."
    )
    md5hash = Column(
        String(32), nullable=False, doc="An MD5 check sum of the row, this"
        " is used to detect existing entries."
    )
    import_date = Column(DateTime, nullable=False,
                         doc="The date the row was added to the database")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Pubmed(Base):
    """A representation of the ``pubmed`` lookup table.

    Parameters
    ----------
    pubmed_id : `int`
        The main primary key column. Although this is set to auto increment, it
        will be populated by actual pubmed IDs.
    pubmed_central_id : `str`, optional, default: `NoneType`
        Any pubmed central identifiers association with the publication (length
        15).
    pub_year : `int`, optional, default: `NoneType`
        The publication year.
    pub_month : `str`, optional, default: `NoneType`
        The publication month (length 10).
    title : `str`, optional, default: `NoneType`
        The title of the publication (length text).
    abstract : `str`, optional, default: `NoneType`
        The abstract of the publication (length text).
    first_author : `str`, optional, default: `NoneType`
        The first author  of the publication (length 100).
    last_author : `str`, optional, default: `NoneType`
        The last author  of the publication (length 100).
    date : `str`, optional, default: `NoneType`
        The publication date (length 20).
    journal_title : `str`, optional, default: `NoneType`
        The title of the publication journal (length 255).
    journal_provider : `str`, optional, default: `NoneType`
        The title of the publication journal provider (publisher) (length 255).
    download_url : `str`, optional, default: `NoneType`
        The NLM eLink URL to the journal website (length 255).
    doi : `str`, optional, default: `NoneType`
        The DOI URL (length 255).
    summary_string : `str`, optional, default: `NoneType`
        The summary string (keywords??) (length 255).
    nmesh_terms : `int`, optional, default: `NoneType`
        The number of mesh terms association with the citation.
    raw_study : `gwas_catalog_parser.orm.RawStudy`, optional, \
    default: `NoneType`
        The relationship link to the ``raw_study`` table.
    raw_association : `gwas_catalog_parser.orm.RawAssociation`, optional, \
    default: `NoneType`
        The relationship link to the ``raw_association`` table.
    mesh : `gwas_catalog_parser.orm.Mesh`, optional, default: `NoneType`
        The relationship link to the ``mesh`` table.
    study : `gwas_catalog_parser.orm.Study`, optional, default: `NoneType`
        The relationship link to the ``study`` table.
    """
    __tablename__ = "pubmed"

    pubmed_id = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Although this is set to auto"
        " increment, it will be populated by actual pubmed IDs."
    )
    pubmed_central_id = Column(
        String(15),
        doc="Any pubmed central identifiers association with the publication."
    )
    pub_year = Column(Integer, doc="The publication year.")
    pub_month = Column(String(10), doc="The publication month")
    title = Column(Text, doc="The title of the publication")
    abstract = Column(Text, doc="The abstract of the publication")
    first_author = Column(
        String(100), doc="The first author  of the publication"
    )
    last_author = Column(
        String(100), doc="The last author  of the publication"
    )
    date = Column(String(20), doc="The publication date")
    journal_title = Column(
        String(255), doc="The title of the publication journal"
    )
    journal_provider = Column(
        String(255), doc="The title of the publication journal "
        "provider (publisher)"
    )
    download_url = Column(
        String(255), doc="The NLM eLink URL to the journal website"
    )
    doi = Column(String(255), doc="The DOI URL")
    summary_string = Column(
        String(255), doc="The summary string (keywords??)"
    )
    nmesh_terms = Column(
        Integer, doc="The number of mesh terms association with the citation."
    )

    # #### Relationships #####
    raw_study = relationship(
        RawStudy, back_populates="pubmed",
        doc="The relationship link to the ``raw_study`` table."
    )
    raw_association = relationship(
        RawAssociation, back_populates="pubmed",
        doc="The relationship link to the ``raw_association`` table."
    )
    mesh = relationship(
        "Mesh", back_populates="pubmed",
        doc="The relationship link to the ``mesh`` table."
    )
    study = relationship(
        "Study", back_populates="pubmed",
        doc="The relationship link to the ``study`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Mesh(Base):
    """A representation of the ``mesh`` table. These are MeSH terms associated
    with the pubmed table.

    Parameters
    ----------
    mesh_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    pubmed_id : `int`, optional, default: `NoneType`
        The connection to the pubmed table. Foreign key to
        ``pubmed.pubmed_id``.
    cui : `str`, optional, default: `NoneType`
        The UMLS concept ID for the MeSH term (length 8).
    term : `str`, optional, default: `NoneType`
        The mesh term name. This is indexed (length 255).
    ncui : `int`, optional, default: `NoneType`
        The number of AUIs associated with the ``cui``.
    probcui : `float`, optional, default: `NoneType`
        The probability that the cui would be returned from a random query of
        the UMLS MRCONSO table.
    semantic_types : `str`, optional, default: `NoneType`
        The semantic types associated with the cui, this is a pipe separated
        string (length 255).
    all_terms : `str`, optional, default: `NoneType`
        The UMLS terms associated with the CUI (length text).
    mesh_term : `str`, optional, default: `NoneType`
        Not sure in the difference between this and ``term`` (length 255).
    modifiers : `str`, optional, default: `NoneType`
        The modifier strings for the MeSH term, this is a ``/`` separated
        string (length 255).
    major_term : `int`, optional, default: `NoneType`
        A flag to indicate if the mesh term was a major subject of the
        publication.
    pubmed : `gwas_catalog_parser.orm.Pubmed`, optional, default: `NoneType`
        The relationship link to the ``pubmed`` table.
    """
    __tablename__ = "mesh"

    mesh_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    pubmed_id = Column(
        Integer, ForeignKey('pubmed.pubmed_id'),
        doc="The connection to the pubmed table."
    )
    cui = Column(String(8), doc="The UMLS concept ID for the MeSH term")
    term = Column(String(255), index=True, doc="The mesh term name")
    ncui = Column(
        Integer, doc="The number of AUIs associated with the ``cui``."
    )
    probcui = Column(
        Float, doc="The probability that the cui would be returned from"
        " a random query of the UMLS MRCONSO table."
    )
    semantic_types = Column(
        String(255),
        doc="The semantic types associated with the cui, this is a pipe"
        " separated string"
    )
    all_terms = Column(Text, doc="The UMLS terms associated with the CUI")
    mesh_term = Column(
        String(255),
        doc="Not sure in the difference between this and ``term``"
    )
    modifiers = Column(
        String(255),
        doc="The modifier strings for the MeSH term, this is a ``/`` "
        "separated string"
    )
    major_term = Column(
        SmallInteger,
        doc="A flag to indicate if the mesh term was a major subject of "
        "the publication."
    )

    pubmed = relationship(
        "Pubmed", back_populates="mesh",
        doc="The relationship link to the ``pubmed`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Association(Base):
    """A representation of the ``association`` table.

    Parameters
    ----------
    association_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    raw_association_pk : `int`
        . Foreign key to ``raw_association.raw_association_pk``.
    study_pk : `int`
        The link back to the ``study`` table. Foreign key to
        ``study.study_pk``.
    effect_type : `str`, optional, default: `NoneType`
        The guess of the effect type, these should not be taken as correct
        (length 10).
    effect_size : `float`, optional, default: `NoneType`
        The effect size.
    effect_units : `str`, optional, default: `NoneType`
        The units of the effect size (length 100).
    standard_error : `float`, optional, default: `NoneType`
        The standard error of the effect size.
    pvalue : `float`, optional, default: `NoneType`
        The pvalue of the association.
    mlog_pvalue : `float`, optional, default: `NoneType`
        The -log10(pvalue) of the association.
    nvars : `int`, optional, default: `NoneType`
        The number of variants that are tied to the association. Most will be 1
        but some associations are haplotypes so will have more (see
        ``assoc_type``).
    ci_lower : `float`, optional, default: `NoneType`
        The lower bound for the confidence interval.
    ci_upper : `float`, optional, default: `NoneType`
        The upper bound for the confidence interval.
    ci_text : `str`, optional, default: `NoneType`
        Any free text associated with the confidence intervals (length 255).
    effect_direction : `int`, optional, default: `NoneType`
        The effect direction 1/-1.
    assoc_type : `str`
        The type of the association (length 1) (length 1).
    validated : `int`, optional, default: `NoneType`
        Has the association been validated 1/0 for yes/no.
    raw_association : `gwas_catalog_parser.orm.RawAssociation`, optional, \
    default: `NoneType`
        The relationship link to the ``raw_association`` table.
    associated_variant : `gwas_catalog_parser.orm.AssociatedVariant`, \
    optional, default: `NoneType`
        The relationship link to the ``associated_variant`` table.
    study : `gwas_catalog_parser.orm.Study`, optional, default: `NoneType`
        The relationship link to the ``study`` table.
    associated_gene : `gwas_catalog_parser.orm.AssociatedGene`, \
    optional, default: `NoneType`
        The relationship link to the ``associated_gene`` table.
    """
    __tablename__ = 'association'

    association_pk = Column(
        Integer, nullable=False, primary_key=True,
        autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    raw_association_pk = Column(
        Integer,
        ForeignKey('raw_association.raw_association_pk'),
        nullable=False, doc=""
    )
    study_pk = Column(
        Integer, ForeignKey('study.study_pk'),
        nullable=False, doc="The link back to the ``study`` table."
    )
    effect_type = Column(
        String(10),
        doc="The guess of the effect type, these should not be taken as "
        "correct."
    )
    effect_size = Column(Float, doc="The effect size.")
    effect_units = Column(String(100), doc="The units of the effect size")
    standard_error = Column(
        Float, doc="The standard error of the effect size."
    )
    pvalue = Column(Float, doc="The pvalue of the association.")
    mlog_pvalue = Column(Float, doc="The -log10(pvalue) of the association.")
    nvars = Column(
        Integer,
        doc="The number of variants that are tied to the association. Most "
        "will be 1 but some associations are haplotypes so will have more (see"
        " ``assoc_type``)."
    )
    ci_lower = Column(
        Float, doc="The lower bound for the confidence interval.")
    ci_upper = Column(
        Float, doc="The upper bound for the confidence interval."
    )
    ci_text = Column(
        String(255),
        doc="Any free text associated with the confidence intervals"
    )
    effect_direction = Column(Integer, doc="The effect direction 1/-1.")
    assoc_type = Column(
        String(1), nullable=False,
        doc="The type of the association (length 1)."
    )
    validated = Column(
        SmallInteger, doc="Has the association been validated 1/0 for yes/no."
    )

    # #### relationships #####
    raw_association = relationship(
        RawAssociation, back_populates="association",
        doc="The relationship link to the ``raw_association`` table."
    )
    associated_variant = relationship(
        "AssociatedVariant", back_populates="association",
        doc="The relationship link to the ``associated_variant`` table."
    )
    study = relationship(
        "Study", back_populates="association",
        doc="The relationship link to the ``study`` table."
    )
    associated_gene = relationship(
        "AssociatedGene", back_populates="association",
        doc="The relationship link to the ``associated_gene`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class AssociatedVariant(Base):
    """A representation of the ``associated_variant`` table. This is a linker
    table between ``association`` and ``variant``.

    Parameters
    ----------
    associated_variant_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    association_pk : `int`, optional, default: `NoneType`
        The link back to the ``association`` table. Foreign key to
        ``association.association_pk``.
    variant_pk : `int`, optional, default: `NoneType`
        The link back to the ``variant`` table. Foreign key to
        ``variants.variant_pk``.
    effect_allele : `str`, optional, default: `NoneType`
        The effect allele (if known) (length 1000).
    other_allele : `str`, optional, default: `NoneType`
        The other allele (if known/calculated) (length 1000).
    effect_allele_freq : `float`, optional, default: `NoneType`
        The effect allele frequency.
    map_info : `int`
        The bitwise mapping information.
    decoded_map_info : `str`
        The decoded ``map_info``, this is a ``|`` separated string (length
        255).
    listed_var_id : `str`, optional, default: `NoneType`
        The variant ID of the input variant (length 100).
    is_validated : `int`
        Has the variant mapping reached the evidence level to be trusted.
    variants : `gwas_catalog_parser.orm.Variant`, optional, default: `NoneType`
        The relationship link to the ``variant`` table.
    association : `gwas_catalog_parser.orm.Association`, optional, \
    default: `NoneType`
        The relationship link to the ``association`` table.
    """
    __tablename__ = 'associated_variant'

    associated_variant_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    association_pk = Column(
        Integer, ForeignKey('association.association_pk'),
        doc="The link back to the ``association`` table."
    )
    variant_pk = Column(
        Integer, ForeignKey('variants.variant_pk'),
        doc="The link back to the ``variant`` table."
    )
    effect_allele = Column(String(1000), doc="The effect allele (if known).")
    other_allele = Column(
        String(1000), doc="The other allele (if known/calculated)."
    )
    effect_allele_freq = Column(Float, doc="The effect allele frequency.")
    map_info = Column(
        Integer, nullable=False, doc="The bitwise mapping information."
    )
    decoded_map_info = Column(
        String(255), nullable=False,
        doc="The decoded ``map_info``, this is a ``|`` separated string"
    )
    listed_var_id = Column(
        String(100), doc="The variant ID of the input variant."
    )
    is_validated = Column(
        SmallInteger, nullable=False, default=0,
        doc="Has the variant mapping reached the evidence level to be "
        "trusted."
    )

    # #### relationships #####
    variant = relationship(
        Variant, back_populates="associated_variants",
        doc="The relationship link to the ``variant`` table."
    )
    association = relationship(
        Association, back_populates="associated_variant",
        doc="The relationship link to the ``association`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Abbreviations(Base):
    """A representation of the ``abbreviation`` table, this is used to store
    all the abbreviations used in the GWAS catalogue.

    Parameters
    ----------
    abrv_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    abbreviation : `str`, optional, default: `NoneType`
        The abbreviation. This is indexed (length 255).
    expanded : `str`, optional, default: `NoneType`
        The description for the abbreviation. This is indexed (length 255).
    """
    __tablename__ = "abbreviations"

    abrv_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    abbreviation = Column(String(255), index=True, doc="The abbreviation.")
    expanded = Column(
        String(255), index=True, doc="The description for the abbreviation."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Study(Base):
    """A representation of the ``study`` table.

    Parameters
    ----------
    study_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    raw_study_pk : `int`, optional, default: `NoneType`
        The link back to the ``raw_study`` table. Foreign key to
        ``raw_study.raw_study_pk``.
    date_added_to_catalog : `date.datetime`
        The date when the record was added to the GWAS catalogue.
    pubmed_id : `int`
        The pubmed identifier associated with the study. Foreign key to
        ``pubmed.pubmed_id``.
    gwas_catalog_accession : `str`
        Accession ID allocated to a GWAS Catalog study. This is indexed (length
        20).
    association_count : `int`
        A lookup of the number of associations for the study.
    pubmed : `gwas_catalog_parser.orm.Pubmed`, optional, default: `NoneType`
        The relationship link to the ``pubmed`` table.
    phenotype : `gwas_catalog_parser.orm.Phenotype`, optional, \
    default: `NoneType`
        The relationship link to the ``phenotype`` table.
    raw_study : `gwas_catalog_parser.orm.RawStudy`, optional, \
    default: `NoneType`
        The relationship link to the ``raw_study`` table.
    association : `gwas_catalog_parser.orm.Association`, optional, \
    default: `NoneType`
        The relationship link to the ``association`` table.
    """
    __tablename__ = 'study'

    study_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    raw_study_pk = Column(
        Integer, ForeignKey('raw_study.raw_study_pk'),
        doc="The link back to the ``raw_study`` table."
    )
    date_added_to_catalog = Column(
        DateTime, nullable=False,
        doc="The date when the record was added to the GWAS catalogue."
    )
    pubmed_id = Column(
        Integer, ForeignKey('pubmed.pubmed_id'), nullable=False,
        doc="The pubmed identifier associated with the study."
    )
    gwas_catalog_accession = Column(
        String(20), nullable=False, index=True,
        doc="Accession ID allocated to a GWAS Catalog study"
    )
    association_count = Column(
        Integer, nullable=False,
        doc="A lookup of the number of associations for the study."
    )

    # #### relationships #####
    pubmed = relationship(
        "Pubmed", back_populates="study",
        doc="The relationship link to the ``pubmed`` table."
    )
    phenotype = relationship(
        "Phenotype", back_populates="study",
        doc="The relationship link to the ``phenotype`` table."
    )
    raw_study = relationship(
        "RawStudy", back_populates="study",
        doc="The relationship link to the ``raw_study`` table."
    )
    association = relationship(
        "Association", back_populates="study",
        doc="The relationship link to the ``association`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Phenotype(Base):
    """A representation of the ``phenotype`` table.

    Parameters
    ----------
    phenotype_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    study_pk : `int`, optional, default: `NoneType`
        The link back to the ``study`` table. Foreign key to
        ``study.study_pk``.
    phenotype_class_pk : `int`
        The link back to the ``phenotype_class`` table. Foreign key to
        ``phenotype_class.phenotype_class_pk``.
    phenotype_term_pk : `int`
        The link back to the ``phenotype_term`` table. Foreign key to
        ``phenotype_term.phenotype_term_pk``.
    phenotype_class : `gwas_catalog_parser.orm.PhenotypeClass`, optional, \
    default: `NoneType`
        The relationship link to the ``phenotype_class`` table.
    phenotype_term : `gwas_catalog_parser.orm.PhenotypeTerm`, optional, \
    default: `NoneType`
        The relationship link to the ``phenotype_term`` table.
    study : `gwas_catalog_parser.orm.Study`, optional, default: `NoneType`
        The relationship link to the ``study`` table.
    """
    __tablename__ = 'phenotype'

    phenotype_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    study_pk = Column(
        Integer, ForeignKey('study.study_pk'),
        doc="The link back to the ``study`` table."
    )
    phenotype_class_pk = Column(
        Integer, ForeignKey('phenotype_class.phenotype_class_pk'),
        nullable=False, doc="The link back to the ``phenotype_class`` table."
    )
    phenotype_term_pk = Column(
        Integer, ForeignKey('phenotype_term.phenotype_term_pk'),
        nullable=False, doc="The link back to the ``phenotype_term`` table."
    )

    phenotype_class = relationship(
        "PhenotypeClass", back_populates="phenotype",
        doc="The relationship link to the ``phenotype_class`` table."
    )
    phenotype_term = relationship(
        "PhenotypeTerm", back_populates="phenotype",
        doc="The relationship link to the ``phenotype_term`` table."
    )

    # #### relationships #####
    study = relationship(
        "Study", back_populates="phenotype",
        doc="The relationship link to the ``study`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PhenotypeClass(Base):
    """A representation of the ``phenotype_class`` table.

    Parameters
    ----------
    phenotype_class_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    phenotype_class_name : `str`, optional, default: `NoneType`
        The phenotype class name (length 20).
    phenotype : `gwas_catalog_parser.orm.Phenotype`, optional, \
    default: `NoneType`
        The relationship link to the ``phenotype`` table.
    """
    __tablename__ = 'phenotype_class'

    phenotype_class_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    phenotype_class_name = Column(
        String(20), unique=True, doc="The phenotype class name."
    )

    # #### relationships #####
    phenotype = relationship(
        "Phenotype", back_populates="phenotype_class",
        doc="The relationship link to the ``phenotype`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PhenotypeTerm(Base):
    """A representation of the ``phenotype_term`` table.

    Parameters
    ----------
    phenotype_term_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    code : `str`, optional, default: `NoneType`
        The phenotype code. This is indexed (length 50).
    term : `str`, optional, default: `NoneType`
        The phenotype term. This is indexed (length 255).
    phenotype : `gwas_catalog_parser.orm.Phenotype`, optional,\
    default: `NoneType`
        The relationship link to the ``phenotype`` table.
    phenotype_mapping : `gwas_catalog_parser.orm.PhenotypeMapping`,\
    optional, default: `NoneType`
        The relationship to the ``phenotype_mapping`` table.
    """
    __tablename__ = 'phenotype_term'

    phenotype_term_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    code = Column(String(50), index=True, doc="The phenotype code.")
    term = Column(String(255), index=True, doc="The phenotype term.")

    # #### relationships #####
    phenotype = relationship(
        "Phenotype", back_populates="phenotype_term",
        doc="The relationship link to the ``phenotype`` table."
    )
    phenotype_mapping = relationship(
        "PhenotypeMapping", back_populates="phenotype_term",
        doc="The relationship to the ``phenotype_mapping`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PhenotypeMapping(Base):
    """A representation of the ``phenotype_mapping`` table.

    Parameters
    ----------
    phenotype_mapping_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    phenotype_term_pk : `int`
        The foreign key link back to the phenotype_term table. Foreign key to
        ``phenotype_term.phenotype_term_pk``.
    match_term : `str`, optional, default: `NoneType`
        The matching part of the query term (length text).
    match_type : `str`, optional, default: `NoneType`
        A code to indicate it the match was exact or via metamap (length 1).
    match_start : `int`
        The start position of the substring.
    match_end : `int`
        The end position of the substring.
    cui : `str`, optional, default: `NoneType`
        The UMLS concept identifier. This is indexed (length 8).
    aui : `str`, optional, default: `NoneType`
        The UMLS atom identifier (length 8).
    sab : `str`, optional, default: `NoneType`
        The UMLS source abbreviation (length 40).
    code : `str`, optional, default: `NoneType`
        The source abbreviation identifier. This is indexed (length 100).
    tty : `str`, optional, default: `NoneType`
        The term type (length 40).
    rank : `int`, optional, default: `NoneType`
        The UMLS AUI rank.
    phenotype_term : `gwas_catalog_parser.orm.PhenotypeTerm`, optional,\
    default: `NoneType`
        The relationship back to the ``phenotype_term`` table.
    semantic_type_mapping : `gwas_catalog_parser.orm.SemanticTypeMapping`,\
    optional, default: `NoneType`
        The relationship back to the ``semantic_type_mapping`` table.
    """
    __tablename__ = 'phenotype_mapping'

    phenotype_mapping_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    phenotype_term_pk = Column(
        Integer, ForeignKey('phenotype_term.phenotype_term_pk'),
        nullable=False,
        doc="The foreign key link back to the phenotype_term table."
    )
    match_term = Column(Text, doc="The matching part of the query term.")
    match_type = Column(
        String(1), doc="A code to indicate it the match was exact or via "
        "metamap."
    )
    match_start = Column(
        Integer, nullable=False, default=-1,
        doc="The start position of the substring."
    )
    match_end = Column(
        Integer, nullable=False, default=-1,
        doc="The end position of the substring."
    )
    cui = Column(String(8), index=True, doc="The UMLS concept identifier.")
    aui = Column(String(8), doc="The UMLS atom identifier.")
    sab = Column(String(40), doc="The UMLS source abbreviation.")
    code = Column(
        String(100), index=True, doc="The source abbreviation identifier."
    )
    tty = Column(String(40), doc="The term type.")
    rank = Column(Integer, doc="The UMLS AUI rank.")

    # #### relationships #####
    phenotype_term = relationship(
        PhenotypeTerm, back_populates="phenotype_mapping",
        doc="The relationship back to the ``phenotype_term`` table."
    )
    semantic_type_mapping = relationship(
        "SemanticTypeMapping", back_populates="phenotype_mapping",
        doc="The relationship back to the ``semantic_type_mapping`` table"
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SemanticTypeMapping(Base):
    """Linker between phenotype mapping and semantic types.

    Parameters
    ----------
    semantic_type_mapping_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    phenotype_mapping_pk : `int`
        The link to the ``phenotype_mapping`` table. Foreign key to
        ``phenotype_mapping.phenotype_mapping_pk``.
    semantic_type_pk : `int`
        The link to the ``semantic_type`` table. Foreign key to
        ``semantic_type.semantic_type_pk``.
    phenotype_mapping : `gwas_catalog_parser.orm.PhenotypeMapping`, optional,\
    default: `NoneType`
        The relationship back to the ``phenotype_mapping`` table.
    semantic_type : `gwas_catalog_parser.orm.SemanticType`, optional, \
    default: `NoneType`
        The relationship back to the ``semantic_type`` table.
    """
    __tablename__ = 'semantic_type_mapping'

    semantic_type_mapping_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not "
        "supplied."
    )
    phenotype_mapping_pk = Column(
        Integer, ForeignKey('phenotype_mapping.phenotype_mapping_pk'),
        nullable=False, doc="The link to the ``phenotype_mapping`` table."
    )
    semantic_type_pk = Column(
        Integer, ForeignKey('semantic_type.semantic_type_pk'),
        nullable=False, doc="The link to the ``semantic_type`` table."
    )

    # #### relationships #####
    phenotype_mapping = relationship(
        PhenotypeMapping, back_populates="semantic_type_mapping",
        doc="The relationship back to the ``phenotype_mapping`` table."
    )
    semantic_type = relationship(
        "SemanticType", back_populates="semantic_type_mapping",
        doc="The relationship back to the ``semantic_type`` table."
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SemanticType(Base):
    """A representation of the ``semantic_type`` table.

    Parameters
    ----------
    semantic_type_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    tui : `str`
        The semantic type ID (length 4).
    semantic_type : `str`
        The semantic type term. This is indexed (length 50).
    semantic_type_mapping : `gwas_catalog_parser.orm.SemanticTypeMapping`, \
    optional, default: `NoneType`
        Relationship with ``semantic_type_mapping``.
    """
    __tablename__ = 'semantic_type'

    semantic_type_pk = Column(
        Integer, nullable=False, primary_key=True, autoincrement=True,
        doc="The main primary key column. Will be auto-incremented if not"
        " supplied."
    )
    tui = Column(
        String(4), nullable=False, unique=True, doc="The semantic type ID."
    )
    semantic_type = Column(
        String(50), nullable=False, unique=True, index=True,
        doc="The semantic type term."
    )

    # #### relationships #####
    semantic_type_mapping = relationship(
        "SemanticTypeMapping", back_populates="semantic_type", doc=""
    )


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class DownloadLinks(Base):
#     """A representation of the download files table that contains suspected
#     download links for a publication
#     """
#     __tablename__ = "download_links"

#     download_link_pk = Column(Integer, nullable=False, primary_key=True,
#                               autoincrement=True)
#     resource_name = Column(String(50))
#     resource_id = Column(String(50))
#     pubmed_id = Column(Integer, ForeignKey('pubmed.pubmed_id'))
#     trait_area = Column(String(255))
#     trait_subarea = Column(String(255))
#     trait_domain = Column(String(255))
#     trait = Column(String(255))
#     uniq_trait = Column(String(255))
#     genome_assembly = Column(String(255))
#     population = Column(String(255))
#     consortium = Column(String(255))
#     ncases = Column(Integer)
#     ncontrols = Column(Integer)
#     nsamples = Column(Integer)
#     file_download = Column(Text)
#     readme_download = Column(Text)
#     website = Column(Text)
#     notes = Column(Text)

#     pubmed = relationship("Pubmed",
#                           back_populates=__tablename__)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return print_orm_obj(self)
