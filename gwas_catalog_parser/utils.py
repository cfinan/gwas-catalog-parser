"""Misc functions that may be used by several different modules.
"""
from gwas_catalog_parser import constants as con
from datetime import datetime
from time import time
import tempfile
import os
import hashlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_float(indata):
    """Attempt to parse to a float if it fails then return ``NoneType``.

    Parameters
    ----------
    indata : `str`
        The input data to attempt to cast.

    Returns
    -------
    cast_indata : `float` or `NoneType`
        the cast input data.
    """
    try:
        return float(indata)
    except (TypeError, ValueError):
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(date):
    """Parse the GWAS catalog date string into a Python datetime object.

    Parameters
    ----------
    date_str : `str`
        A date with the format: ``YYYY-MM-DD``

    Returns
    -------
    date_obj : `datetime.DateTime`
        The processed date.
    """
    # Going for 2022-02-15
    format_data = "%Y-%m-%d"
    return datetime.strptime(date, format_data)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def decode_map_info(map_info):
    """Parse the GWAS catalog date string into a Python datetime object.

    Parameters
    ----------
    date_str : `str`
        A date with the format: ``YYYY-MM-DD``

    Returns
    -------
    date_obj : `datetime.DateTime`
        The processed date.
    """
    decoded = []
    if map_info > 0:
        for i in con.ALL_MAP_INFO:
            if i.bits & map_info == i.bits:
                decoded.append(i.desc)
        return decoded
    return [con.NO_MAP.desc]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def epoch_time():
    """Generate the current epoch time (i.e. seconds since the 1,1,1970)

    Returns
    -------
    epoch_time : `int`
        The epoch time in seconds.
    """
    # could also use:
    # int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds())
    return int(time())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def hash_assoc(chr_id, chr_pos, variant_risk_allele):
    """Generate an MD5 hash of the raw download variant data.

    Parameters
    ----------
    chr_id : `str` or `NoneType`
        The chromosome ID.
    chr_pos : `int` or `NoneType`
        the chromosome coordinates.
    variant_risk_allele : `str` or `NoneType`
        The variant ID concatinated on a - with the effect allele.

    Returns
    -------
    md55hash : `str`
        The 32 character hex hash for the parameters joined on a `|`, all
        parameters are made into byte strings.
    """
    row = [chr_id, chr_pos, variant_risk_allele]
    return hashlib.md5(
        con.MD5_JOIN.join([str(i) for i in row]).encode()
    ).hexdigest()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tmp_file(**kwargs):
    """Initialise a temp file to work with. This differs from
    `tempfile.mkstemp` as the temp file is closed and only the file name is
    returned.

    Parameters
    ----------
    **kwargs
        Any arguments usually passed to `tempfile.mkstemp`

    Returns
    -------
    tmpfile : `str`
        The name and path of the temp file.
    """
    tmp_file_obj, tmp_file_name = tempfile.mkstemp(**kwargs)
    os.close(tmp_file_obj)
    return tmp_file_name
