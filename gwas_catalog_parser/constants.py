"""Some constants used in the application.
"""
from collections import namedtuple
import os


GWAS_CATALOG_CONFIG = os.path.join(os.environ['HOME'], '.genomic_data.ini')
""" The location for the config file (`str`)
"""
ALLOW_EXISTING_ACCESSION = 10
"""When doing an update this is the number of rows that can be added where the
accession is already present in the database, so these are either instances
where an update has failed within an accession or the GWAS catalog
maintainers have updated the records but we have to be careful with these
as they could be a major pain in the arse, I need a good mechanism of
recognising duplicated rows (`int`)
"""

# ### Database option strings, these are option type values in the database ###
ASSEMBLY_OPT = "genome_assembly"
"""The name of the genome assembly option type in the
``gwas_catalog_parser.orm.Options`` table (`str`)
"""
ENS_GENE_VERSION_OPT = "gene_version"
"""The name of the Ensembl gene version option type in the
``gwas_catalog_parser.orm.Options`` table (`str`)
"""
# #############################################################################

# #############################################################################
UNKNOWN_ASSOC = "U"
"""A string for unknown association type (`str`)
"""
ADDITIVE_ASSOC = "A"
"""A string for additive association types, i.e. single variant
association (`str`)
"""
INTERATION_ASSOC = "I"
"""A string for interaction association types (`str`)
"""
HAPLOTYPE_ASSOC = "H"
"""A string for haplotype association types, i.e. multiple variants(`str`)
"""
OR_EFFECT_TYPE = "or"
"""Odds ratio effect type (`str`)
"""
BETA_EFFECT_TYPE = "beta"
"""Beta effect type (`str`)
"""
LOG_OR_EFFECT_TYPE = "log_or"
"""Log odds ratio effect type (`str`)
"""
UNKNOWN_EFFECT_TYPE = "unknown"
"""Unknown effect type (`str`)
"""
UNKNOWN_VARIANT_TYPE = "U"
"""Unknown variant type (`str`)
"""
SIMPLE_VARIANT_TYPE = "S"
"""Simple variant type, i.e. SNP or INDEL (`str`)
"""
HLA_VARIANT_TYPE = "H"
"""HLA variant type (`str`)
"""
# #############################################################################

# ############################# PUBMED CONSTANTS ##############################
MESH_JOIN = "|"
"""The join delimiter that is used when joining mesh modifiers (`str`)
"""
# #############################################################################

# #############################################################################
PHENO_CLASS_BACKGROUND = "background_trait"
"""The phenotype class name for a background trait (a phenotype shared by all
GWAS participants) (`str`)
"""
PHENO_CLASS_MAPPED_BACKGROUND = "mapped_background_trait"
"""The phenotype class name for a mapped background trait (a phenotype shared
by all GWAS participants but mapped to an EFO term) (`str`)
"""
PHENO_CLASS_MAIN = "disease_trait"
"""The phenotype class name for a GWAS trait (`str`)
"""
PHENO_CLASS_MAPPED_MAIN = "mapped_trait"
"""The phenotype class name for a GWAS trait mapped back to the EFO (`str`)
"""
PHENO_CLASS_ALL = [
    PHENO_CLASS_BACKGROUND,
    PHENO_CLASS_MAPPED_BACKGROUND,
    PHENO_CLASS_MAIN,
    PHENO_CLASS_MAPPED_MAIN
]
"""All phenotype classes (`list`) of (`str`)
"""
FREE_TEXT_CODE = "free_text"
"""The constant name for free text (uncoded) codes (`str`)
"""
# #############################################################################


# #############################################################################
GENE_CLASS_UPSTREAM = "upstream_gene_id"
"""The gene class name for an upstream gene (`str`)
"""
GENE_CLASS_DOWNSTREAM = "downstream_gene_id"
"""The gene class name for an downstream gene (`str`)
"""
GENE_CLASS_OVERLAP = "snp_gene_id"
"""The gene class name for an downstream gene (`str`)
"""
GENE_CLASS_REPORTED = "reported_genes"
"""The gene class name for an author_reported genedownstream gene (`str`)
"""
GENE_CLASS_ALL = [
    GENE_CLASS_UPSTREAM,
    GENE_CLASS_DOWNSTREAM,
    GENE_CLASS_OVERLAP,
    GENE_CLASS_REPORTED
]
"""All gene classes (`list`) of (`str`)
"""
# #############################################################################


# ############################# RAW FILE PARAMETERS ###########################
MD5_JOIN = "|"
"""The join delimiter that is used when creating a record MD5 sum (`str`)
"""
GWAS_CAT_DOWNLOAD_NULLS = ['', 'NR', 'NS']
"""Values that represent NULL values in the GWAS catalog (`list` of `str`)
"""
GWAS_CAT_DOWNLOAD_DELIMITER = "\t"
"""The delimiter of the downloaded GWAS catalog file (`str`)
"""
ASSOC_FILE_EXP_HEADER = [
    ('DATE ADDED TO CATALOG', 'date_added_to_catalog'),
    ('PUBMEDID', 'pubmed_id'),
    ('FIRST AUTHOR', 'first_author'),
    ('DATE', 'date'),
    ('JOURNAL', 'journal'),
    ('LINK', 'link'),
    ('STUDY', 'study'),
    ('DISEASE/TRAIT', 'disease_trait'),
    ('INITIAL SAMPLE SIZE', 'initial_sample_size'),
    ('REPLICATION SAMPLE SIZE', 'replication_sample_size'),
    ('REGION', 'region'),
    ('CHR_ID', 'chr_id'),
    ('CHR_POS', 'chr_pos'),
    ('REPORTED GENE(S)', 'reported_genes'),
    ('MAPPED_GENE', 'mapped_gene'),
    ('UPSTREAM_GENE_ID', 'upstream_gene_id'),
    ('DOWNSTREAM_GENE_ID', 'downstream_gene_id'),
    ('SNP_GENE_IDS', 'snp_gene_ids'),
    ('UPSTREAM_GENE_DISTANCE', 'upstream_gene_distance'),
    ('DOWNSTREAM_GENE_DISTANCE', 'downstream_gene_distance'),
    ('STRONGEST SNP-RISK ALLELE', 'strongest_snp_risk_allele'),
    ('SNPS', 'snps'),
    ('MERGED', 'merged'),
    ('SNP_ID_CURRENT', 'snp_id_current'),
    ('CONTEXT', 'context'),
    ('INTERGENIC', 'intergenic'),
    ('RISK ALLELE FREQUENCY', 'risk_allele_frequency'),
    ('P-VALUE', 'p_value'),
    ('PVALUE_MLOG', 'pvalue_mlog'),
    ('P-VALUE (TEXT)', 'p_value_text'),
    ('OR or BETA', 'or_or_beta'),
    ('95% CI (TEXT)', 'ci_text'),
    ('PLATFORM [SNPS PASSING QC]', 'platform_snps_passing_qc'),
    ('CNV', 'cnv'),
    ('MAPPED_TRAIT', 'mapped_trait'),
    ('MAPPED_TRAIT_URI', 'mapped_trait_uri'),
    ('STUDY ACCESSION', 'gwas_catalog_accession'),
    ('GENOTYPING TECHNOLOGY', 'genotyping_technology')
]
"""The expected header for the GWAS catalog v1.0.2 file and mappings back to
databse column names (`list` of (`str`, `str`))
"""
STUDY_FILE_EXP_HEADER = [
    ('DATE ADDED TO CATALOG', 'date_added_to_catalog'),
    ('PUBMED ID', 'pubmed_id'),
    ('FIRST AUTHOR', 'first_author'),
    ('DATE', 'date'),
    ('JOURNAL', 'journal'),
    ('LINK', 'link'),
    ('STUDY', 'study'),
    ('DISEASE/TRAIT', 'disease_trait'),
    ('INITIAL SAMPLE SIZE', 'initial_sample_size'),
    ('REPLICATION SAMPLE SIZE', 'replication_sample_size'),
    ('PLATFORM [SNPS PASSING QC]', 'platform_snps_passing_qc'),
    ('ASSOCIATION COUNT', 'association_count'),
    ('MAPPED_TRAIT', 'mapped_trait'),
    ('MAPPED_TRAIT_URI', 'mapped_trait_uri'),
    ('STUDY ACCESSION', 'gwas_catalog_accession'),
    ('GENOTYPING TECHNOLOGY', 'genotyping_technology'),
    ('SUMMARY STATS LOCATION', 'summary_stats_location'),
    ('SUBMISSION DATE', 'submission_date'),
    ('STATISTICAL MODEL', 'statistical_model'),
    ('BACKGROUND TRAIT', 'background_trait'),
    ('MAPPED BACKGROUND TRAIT', 'mapped_background_trait'),
    ('MAPPED BACKGROUND TRAIT URI', 'mapped_background_trait_uri')
]
"""The expected header for the GWAS catalog v1.0.3 study file and mappings back
to database column names (`list` of (`str`, `str`))
"""
ANCESTRY_FILE_EXP_HEADER = [
    ('STUDY ACCESSION', 'gwas_catalog_accession'),
    ('PUBMED ID', 'pubmed_id'),
    ('FIRST AUTHOR', 'first_author'),
    ('DATE', 'date'),
    ('INITIAL SAMPLE DESCRIPTION', 'initial_sample_desc'),
    ('REPLICATION SAMPLE DESCRIPTION', 'replication_sample_desc'),
    ('STAGE', 'stage'),
    ('NUMBER OF INDIVIDUALS', 'sample_size'),
    ('BROAD ANCESTRAL CATEGORY', 'broad_ancestry'),
    ('COUNTRY OF ORIGIN', 'county_of_origin'),
    ('COUNTRY OF RECRUITMENT', 'county_of_recruit'),
    ('ADDITIONAL ANCESTRY DESCRIPTION', 'additional_info'),
    ('ANCESTRY DESCRIPTOR', 'ancestry_descriptor'),
    ('FOUNDER/GENETICALLY ISOLATED POPULATION', 'is_founder'),
    ('NUMBER OF CASES', 'no_cases'),
    ('NUMBER OF CONTROLS', 'no_controls'),
    ('SAMPLE DESCRIPTION', 'sample_desc'),
    ('COHORT(S)', 'cohorts'),
    ('COHORT-SPECIFIC REFERENCE', 'cohort_ref')
]
"""The expected header for the GWAS catalog v1.0.3 ancestry file and mappings
back to database column names (`list` of (`str`, `str`))
"""
# #############################################################################

# ############################ MAPPING CODES ##################################
# Slightly more informative than a regular tuple
MapInfo = namedtuple("MapInfo", ['bits', 'desc'])
"""A named tuple for holding the mapping info (`namedtuple`)
"""
NO_MAP = MapInfo(0, 'NO_MAP')
"""A no mapping indicator (`MapInfo`)
"""
ID = MapInfo(1 << 1, 'ID')
"""A no mapping indicator (`MapInfo`)
"""
CHR = MapInfo(1 << 2, 'CHR')
"""A no mapping indicator (`MapInfo`)
"""
START = MapInfo(1 << 3, 'START')
"""A no mapping indicator (`MapInfo`)
"""
REF = MapInfo(1 << 4, 'EFFECT')
"""A no mapping indicator (`MapInfo`)
"""
ALT = MapInfo(1 << 5, 'OTHER')
"""A no mapping indicator (`MapInfo`)
"""
REF_FLIP = MapInfo(1 << 7, 'REF_FLIP')
"""A no mapping indicator (`MapInfo`)
"""
BAD_EFFECT = MapInfo(1 << 8, 'BAD_EFFECT')
"""A no mapping indicator (`MapInfo`)
"""
EVIDENCE_LEVEL = [
    # bits, is_validated, must be single site
    (CHR.bits | START.bits | REF.bits | ALT.bits, True, False),
    (CHR.bits | START.bits | REF.bits | ID.bits, True, False),
    (CHR.bits | START.bits | ID.bits, True, False),
    (ID.bits, True, False),
    (CHR.bits | START.bits, False, True)
]
ALL_MAP_INFO = [ID, CHR, START, REF, ALT, REF_FLIP, BAD_EFFECT]
# #############################################################################
