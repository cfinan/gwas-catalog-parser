"""Functions for mapping phenotypes against the UMLS.
"""
from gwas_catalog_parser import orm as o
from umls_tools import (
    mapping,
    query as uq,
    orm as uo
)
from tqdm import tqdm
from sqlalchemy.orm.exc import NoResultFound
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_umls_mappings(session, umls_session, metamap_server=None,
                         verbose=False, commit_every=50):
    """If the UMLS connection info is available then map the phenotype terms
    against it.

    This will only map terms that have not already been mapped.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object for the GWAS catalog database.
    umls_session : `sqlalchemy.Session`
        The SQLAlchemy session object for the UMLS database.
    metamap_server : `umls_tools.metamap.MetamapServer`, optional, default: `NoneType`
        The metamap server to perform metamap queries. If not provided then
        Metamap will not be used.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.

    Returns
    -------
    total : `int`
        The total number of queries.
    no_match : `int`
        The number of phenotype terms that have not been mapped in any way.
    exact_match : `int`
        The number of phenotype terms that have been directly mapped.
    substring_match : `int`
        The number of phenotype terms that have been mapped with a
        substring match.
    metamp_match : `int`
        The number of phenotype terms that have been mapped with Metamap.
    """
    total = 0
    nadded = 0
    match_type_count = {
        uq.NO_MATCH: 0, uq.EXACT_MATCH: 0, uq.SUBSTRING_MATCH: 0,
        uq.METAMAP_MATCH: 0
    }

    # Get a rank lookup dictionary
    ranks = uq.get_ranks(umls_session)
    mapper = uq.CuiMapper(umls_session, metamap_server=metamap_server)

    nadded = False
    for tui, sty in uq.get_all_sty(umls_session):
        try:
            session.query(o.SemanticType).\
                filter(o.SemanticType.tui == tui).one()
        except NoResultFound:
            nadded = True
            session.add(o.SemanticType(tui=tui, semantic_type=sty))

    if nadded is True:
        session.commit()

    # Cache the semantic types
    st_lookup = dict(
        [(st.semantic_type, st.semantic_type_pk)
         for st in session.query(o.SemanticType)]
    )
    totals, exact_match = _get_exact_matches(session, mapper, st_lookup, ranks, verbose=verbose)
    total += totals
    return (total, exact_match,
            match_type_count[uq.SUBSTRING_MATCH],
            match_type_count[uq.METAMAP_MATCH],
            match_type_count[uq.NO_MATCH])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_exact_matches(session, mapper, st_lookup, ranks, verbose=False,
                       commit_every=500):
    match_count = 0
    nadded = 0
    total = get_pheno_terms(session).count()
    tqdm_kwargs = dict(
        unit=" terms",
        desc="[info] exact mapping phenotype terms",
        leave=False,
        total=total
    )
    for row in tqdm(get_pheno_terms(session), **tqdm_kwargs):
        # umls_session.query(uo.MrConso).filter(uo.MrConso.STR == row.term).all()
        matches = mapper.match_term(row.term)
        if len(matches) > 1:
            match_count += 1
    #     match_type_count[match_type] += 1
        semantic_types = _get_semantic_type(mapper.session, matches)
        total += 1
        for r, start, end in matches:
            p = o.PhenotypeMapping(
                phenotype_term_pk=row.phenotype_term_pk,
                match_term=r.STR,
                match_type=uq.EXACT_MATCH,
                match_start=start,
                match_end=end,
                cui=r.CUI,
                aui=r.AUI,
                sab=r.SAB,
                code=r.CODE,
                tty=r.TTY,
                rank=ranks[(r.SAB, r.TTY)][0]
            )
            for sty in semantic_types[r.CUI]:
                stm = o.SemanticTypeMapping(
                    phenotype_mapping=p,
                    semantic_type_pk=st_lookup[sty.STY]
                )
                session.add(stm)
            nadded += 1
        if nadded > commit_every:
            session.commit()
            nadded = 0
    if nadded > 0:
        session.commit()
    return total, match_count


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_semantic_type(session, matches):
    """Query the semantic types for the matches, this gets them for each
    unique CUI.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for the UMLS database.
    matches : `list` of `umls_tools.orm.MrConso`
        The MRCONSO matches.

    Returns
    -------
    semantic_types : `dict`
        The keys are CUIs and the values are STYs.
    """
    seen_cuis = dict()
    for r, start, end in matches:
        if r.CUI not in seen_cuis:
            seen_cuis[r.CUI] = session.query(
                uo.MrSty.STY
            ).filter(
                uo.MrSty.CUI == r.CUI
            ).all()
    return seen_cuis


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_pheno_terms(session):
    """Get the phenotype term query.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for the GWAS catalog database.

    Returns
    -------
    query : `sqlalchemy.Query`
        The query to get all the unmapped phenotype terms.
    """
    return session.query(
        o.PhenotypeTerm
    ).join(
        o.PhenotypeMapping,
        o.PhenotypeTerm.phenotype_term_pk == o.PhenotypeMapping.phenotype_term_pk,
        isouter=True
    ).filter(
        o.PhenotypeMapping.phenotype_term_pk == None
    )
