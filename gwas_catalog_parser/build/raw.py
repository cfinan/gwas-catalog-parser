"""Deal with the importation of the GWAS catalog raw download data.
That is the study file, associations file and the ancestry file. These
are downloaded and stored in the database as "raw" unprocessed tables that are
used to import from.
"""
from gwas_catalog_parser import (
    utils,
    constants as con,
    orm as o
)
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound
from gwas_catalog_parser.build import download
from tqdm import tqdm
from datetime import date
import csv
import hashlib
import shutil
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _RawImporter(object):
    """Base class for importing a raw data table. Do not use directly.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    download_file : `str`
        A path to write the downloaded raw GWAS catalog data file.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.
    """
    DOWNLOAD_CLASS = None
    """The download class to use to get the raw data. Must be derived from:
    ``gwas_catalog_parser.build.downloads.BaseGwasRepFetch``.
    """
    EXPECTED_HEADER = None
    """The expected header of the download data and it's mapping back to
    database column names (`list` of (`str`, `str`)).
    """
    MSG_TYPE = ""
    """The message type for the class, this is used in verbose outputs (`str`)
    """
    TABLE_DEFINITION = None
    """The SQLAlchemy ORM table definition.
    """
    DELIMITER = "\t"
    """The delimiter of the downloaded file (`str`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, session, download_file, verbose=False,
                 commit_every=10000):
        self.session = session
        self.download_file = download_file
        self.verbose = verbose
        self.commit_every = commit_every

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_data(self):
        """Kick off the process of importing the raw data into the database.

        Returns
        -------
        add_count : `int`
            The number of completely new accessions added.
        update_count : `int`
            The number of new rows added to existing accessions (see Notes).

        Notes
        -----
        This will add any new records but currently is unable to locate changed
        records, these are treated as completely new records to an existing
        study accession. If this is happening, the ``update_count`` will
        be > 0.
        """
        # Will hold newly added reports and added to existing record counts
        add_count, update_count = 0, 0

        # Download the raw data table
        self.download_data()

        # Existing IDs id a tuple of (accession, hash), I am going to use the
        # combination of both to determine what needs updating, the hash will
        # be the main one but, if we have loads of new hashes tied to existing
        # accessions then this is potentially problematic at it means that
        # there have been large scale changes to the file that potentially make
        # it useless
        existing_ids = self.get_accession_hashes()
        downloaded_rows = self.count_download_rows()

        # So we start by opening the download file and seeing what rows need
        # importing into the raw data
        with open(self.download_file, 'r') as csvin:
            reader = csv.reader(csvin, delimiter=self.DELIMITER)
            header = next(reader)

            # The column names in the database table
            mapto = [new for old, new in self.EXPECTED_HEADER]

            tqdm_kwargs = dict(
                unit=" rows",
                desc=(f"[info] processing GWAS catalog {self.MSG_TYPE}"
                      " download..."),
                total=downloaded_rows,
                leave=False
            )

            # Will hold rows that we want to add to the database
            row_buffer = []
            for idx, row in enumerate(tqdm(reader, **tqdm_kwargs)):
                if len(row) != len(header):
                    raise IndexError(f"Incorrect row length at row '{idx}")
                # Make missing values None
                row = [i if i not in con.GWAS_CAT_DOWNLOAD_NULLS else None
                       for i in row]

                # Get the MD5 sum of the row whilst turning NoneTypes into
                # empty strings
                md5hash = hashlib.md5(
                    con.MD5_JOIN.join(
                        [i if i is not None else '' for i in row]
                    ).encode('utf-8')
                ).hexdigest()

                # turn into a dict
                mapped_row = dict([(i, row[c]) for c, i in enumerate(mapto)])

                # Determines if the record will eventually be added to the
                # database
                to_add = False

                # Each imported raw data row will be tagged with an import date
                update_date = date.today()

                try:
                    # Have we seen the accession or the md5 hash before
                    existing_ids[mapped_row['gwas_catalog_accession']].index(
                        md5hash
                    )
                except ValueError:
                    # The accession is not present, new record in study
                    # (potentially concerning)
                    to_add = True
                    update_count += 1
                except KeyError:
                    # The accession is not present, new study
                    to_add = True

                if to_add is True:
                    add_count += 1
                    # Attempt to reformat dates
                    for i in ['date_added_to_catalog', 'date']:
                        try:
                            mapped_row[i] = utils.parse_date(
                                mapped_row[i]
                            )
                        except KeyError:
                            # The field is not present in the imported dataset
                            pass
                    mapped_row['loaded'] = False
                    mapped_row['md5hash'] = md5hash
                    mapped_row['import_date'] = update_date
                    row_buffer.append(mapped_row)

                if len(row_buffer) == self.commit_every:
                    self.session.bulk_insert_mappings(
                        self.TABLE_DEFINITION, row_buffer
                    )
                    row_buffer = []
                    self.session.commit()

            # Process anything left in the row buffer
            if len(row_buffer) > 0:
                self.session.bulk_insert_mappings(
                    self.TABLE_DEFINITION, row_buffer
                )
                self.session.commit()
                row_buffer = []

        return add_count, update_count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download_data(self):
        """Initiate the data download.
        """
        # Download the raw file
        with self.DOWNLOAD_CLASS(self.download_file,
                                 verbose=self.verbose) as fetch:
            fetch.download()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def count_download_rows(self):
        """Count the number of rows in the downloaded file.

        Returns
        -------
        row_count : `int`
            The number of rows in the downloaded file.
        """
        # Calculate how many rows were downloaded
        with open(self.download_file, 'r') as infile:
            return sum([1 for i in infile]) - 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_accession_hashes(self):
        """Get the MD5 row hashes for the existing data in the row table.

        Returns
        -------
        row_hashes : `dict`
            The keys for the dictionary are gwas catalog accessions the values
            are MD5 hashes of the row contents (as stores in the raw table).
        """
        q = self.session.query(
            self.TABLE_DEFINITION.gwas_catalog_accession,
            self.TABLE_DEFINITION.md5hash
        )

        # Get the IDs of all the rows in the raw_download table
        existing_ids = dict()
        for accession, md5hash in q:
            try:
                existing_ids[accession].append(md5hash)
            except KeyError:
                existing_ids[accession] = [md5hash]
        return existing_ids


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawStudyImporter(_RawImporter):
    """For importing the raw study data.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    download_file : `str`
        A path to write the downloaded raw GWAS catalog data file.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.
    """
    DOWNLOAD_CLASS = download.GwasCatStudyFetch
    """The download class to use to get the raw data. Must be derived from:
    ``gwas_catalog_parser.build.downloads.GwasCatStudyFetch``.
    """
    EXPECTED_HEADER = con.STUDY_FILE_EXP_HEADER
    """The expected header of the download data and it's mapping back to
    database column names (`list` of (`str`, `str`)).
    """
    MSG_TYPE = "study"
    """The message type for the class, this is used in verbose outputs (`str`)
    """
    TABLE_DEFINITION = o.RawStudy
    """The SQLAlchemy ORM table definition (`gwas_catalog_parser.orm.RawStudy`).
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawAssocImporter(_RawImporter):
    """For importing the raw association data.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    download_file : `str`
        A path to write the downloaded raw GWAS catalog data file.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.
    """
    DOWNLOAD_CLASS = download.GwasCatAssocFetch
    """The download class to use to get the raw data. Must be derived from:
    ``gwas_catalog_parser.build.downloads.GwasCatAssocFetch``.
    """
    EXPECTED_HEADER = con.ASSOC_FILE_EXP_HEADER
    """The expected header of the download data and it's mapping back to
    database column names (`list` of (`str`, `str`)).
    """
    MSG_TYPE = "association"
    """The message type for the class, this is used in verbose outputs (`str`)
    """
    TABLE_DEFINITION = o.RawAssociation
    """The SQLAlchemy ORM table definition
    (`gwas_catalog_parser.orm.RawAssociation`).
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawAncestryImporter(_RawImporter):
    """For importing the raw association data.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    download_file : `str`
        A path to write the downloaded raw GWAS catalog data file.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.
    """
    DOWNLOAD_CLASS = download.GwasCatAncestryFetch
    """The download class to use to get the raw data. Must be derived from:
    ``gwas_catalog_parser.build.downloads.GwasCatAncestryFetch``.
    """
    EXPECTED_HEADER = con.ANCESTRY_FILE_EXP_HEADER
    """The expected header of the download data and it's mapping back to
    database column names (`list` of (`str`, `str`)).
    """
    MSG_TYPE = "ancestry"
    """The message type for the class, this is used in verbose outputs (`str`)
    """
    TABLE_DEFINITION = o.RawAncestry
    """The SQLAlchemy ORM table definition
    (`gwas_catalog_parser.orm.RawAncestry`).
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download_data(self):
        """A custom downloader (fudge) for the ancestry data.

        Notes
        -----
        The ancestry data file is currently poorly formatted with some rows
        being shorter than the header (no data in them). The importer will
        error on these. So this will download and then standardise the file
        columns prior to the import. It is written is such a way that it
        should not cause issues if they fix the problem.
        """
        super().download_data()

        tmpfile = utils.get_tmp_file()
        with open(self.download_file) as incsv:
            reader = csv.reader(incsv, delimiter=self.DELIMITER)
            header = next(reader)
            with open(tmpfile, 'wt') as outcsv:
                writer = csv.writer(outcsv, delimiter=self.DELIMITER)
                writer.writerow(header)
                for row in reader:
                    for idx in range(len(header)):
                        try:
                            row[idx]
                        except IndexError:
                            row.append(None)
                    writer.writerow(row)
        shutil.move(tmpfile, self.download_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_abbreviations(session, download_file):
    """Import and update the GWAS catalog abbreviations into the database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    download_file : `str`
        The path to the downloaded GWAS catalog data file (v1.0.2).

    Returns
    -------
    nadded : `int`
        The number of abbreviations that have been added.

    Notes
    -----
    This will not add anything that is already present (based on
    abbreviation and expanded meaning). However, it also does not check for
    obsolete abbreviations.
    """
    nadded = 0
    # Download the GWAS catalogue
    with download.DownloadAbbreviations(download_file, headless=True) as fetch:
        fetch.download()

    with open(download_file, 'rt') as csvin:
        reader = csv.reader(csvin, delimiter="\t")
        header = next(reader)
        for idx, row in enumerate(reader):
            # Sanity check
            if len(row) != len(header):
                raise IndexError(
                    f"header and row length are different: '{idx}'"
                )
            abrv, expand = [i.strip() for i in row]
            try:
                # test if already in Abbreviations
                session.query(o.Abbreviations).filter(
                    and_(
                        o.Abbreviations.abbreviation == abrv,
                        o.Abbreviations.expanded == expand
                    )
                ).one()
            except NoResultFound:
                # Not there
                session.add(
                    o.Abbreviations(
                        abbreviation=abrv, expanded=expand
                    )
                )
                nadded += 1
        session.commit()
    return nadded
