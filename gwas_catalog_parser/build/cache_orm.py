"""A variant cache OR. As the variant data is downloaded from the REST API,
this provides a mechanism to resume interrupted queries.
"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    SmallInteger
)

Base = declarative_base()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_all_tables(session):
    """Create all the ORM tables.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session where ``session.get_bind()`` will return a valid engine.
    """
    # Ensure all the tables are created
    Base.metadata.create_all(
        session.get_bind()
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Variant(Base):
    """A representation of a genetic variant in the cache.

    Parameters
    ----------
    variant_pk : `int`
        The main primary key column. Will be auto-incremented if not supplied.
    chr_name : `int`
        The chromosome name for the valiant (length 15).
    start_pos : `int`
        The start position for the variant.
    ref_allele : `str`
        The reference allele for the variant (length 255).
    alt_alleles : `str`
        The alternate alleles for the variant, this is a comma separated list
        if > 1 alt allele (length 1000).
    strand : `int`
        The strand for the variant, either 1 or -1.
    var_id : `str`
        The variant identifier (length 255).
    minor_allele : `str`
        The minor allele, depending on how the variant data was obtained
        (positional query vs. name), this may not be present (length 255).
    global_maf : `float`
        The global MAF from Ensembl, depending on how the variant data was
        obtained (positional query vs. name), this may not be present
        (length 255).
    worst_consequence : `str`
        The worst consequence for the variant (length 50).
    variant_hash : `str`
        The variant MD5 hash from the processed input data (length 32).
    """
    __tablename__ = 'variants'

    variant_pk = Column(Integer, nullable=False, primary_key=True,
                        autoincrement=True)
    chr_name = Column(String(15), nullable=False, default='0')
    start_pos = Column(Integer, nullable=False, default=0)
    ref_allele = Column(String(255))
    alt_alleles = Column(String(255))
    strand = Column(SmallInteger)
    var_id = Column(String(255))
    minor_allele = Column(String(255))
    global_maf = Column(Float)
    worst_consequence = Column(String(50))
    variant_hash = Column(String(32), nullable=False, index=True)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)
