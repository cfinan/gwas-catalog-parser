"""A set of classes/functions that handles the fetching or summary GWAS
databases from the web.
"""
from gwas_catalog_parser import (
    constants as con,
    utils
)
from urllib.parse import urlparse
from posixpath import basename
from lxml import html
# from pyaddons import ftp_helper, gzopen
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from tqdm import tqdm
import requests
import urllib
import pycurl
import json
import tempfile
import os
import sys
import ftplib
import shutil
import csv
import re
import glob
import gzip
import time
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseGwasRepFetch(object):
    """A base class that forms the backbone of fetcher classes for the various
    GWAS databases.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "file"
    """A label to add to the download progress (`str`)
    """
    EXPECTED_HEADER = []
    """The expected header of the downloaded trial (`list` of `str`)
    """
    DELIMITER = "\t"
    """The expected delimiter of the downloaded trial (`str`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, outfile, verbose=False, timeout=0.5):
        self.outfile = self._get_outfile(
            outfile, self.__class__.DOWNLOAD_URL
        )
        self.verbose = verbose
        self.md5 = ""
        self.timeout = timeout
        self._progress = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An open interface function
        """
        self._prev_download = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """A close interface function
        """
        if self._progress is not None:
            self._progress.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download(self, url=None):
        """Initialise the downloading of the data file.

        Parameters
        ----------
        url : `str`, optional, default: `NoneType`
            An optional URL, if not provided then the class default is used.
        """
        # The default is to use requests to do the download
        self._download(use_pycurl=True)

        # Now before we finish we check that the file that has been downloaded
        # has the expected header
        self.check_expected_header()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def check_expected_header(self):
        """Make sure the downloaded file has the expected header line and error
        out if not.

        Returns
        -------
        ok : `bool`
            This will always be ``True`` as ``False`` will result in a
            ``ValueError``.

        Raises
        ------
        ValueError
            If the header does not match the expected header.
        """
        with open(self.outfile) as csvin:
            reader = csv.reader(csvin, delimiter=self.__class__.DELIMITER)
            header = next(reader)

        if header != self.__class__.EXPECTED_HEADER:
            # pp.pprint(header)
            raise ValueError(
                "the header in the downloaded file is not expected"
            )

        return True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_outfile(self, ofn, url):
        """Check the output file name and generate one from the URL if
        necessary.

        Parameters
        ----------
        ofn : `str`
            A string containing the output file name.
        url : `str`
            A string containing the download url.

        Returns
        -------
        out_file_name : `str`
            A string containing a valid filename for the final download
            destination.
        """
        # Split the URL
        # See:
        # https://stackoverflow.com/questions/449775/
        # how-can-i-split-a-url-string-up-into-separate-parts-in-python
        parse_object = urlparse(url)
        filename_ext = basename(parse_object.path)
        filename, file_extension = os.path.splitext(filename_ext)

        # If nothing has been supplied then we return the file name from the
        # URL
        if ofn is None or ofn.endswith('/') or ofn.endswith('\\'):
            return filename_ext

        return ofn

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _download(self, use_pycurl=True):
        """Kicks off the downloading of the files.

        Parameters
        ----------
        use_pycurl : `bool`, optional, default: `True`
            Do we want to use pycurl to do the download or requests
            (``False``).
        """
        # Generate a temp file name
        ofn = self._mktemp()

        # The default is to perform the download via requests
        if use_pycurl is False:
            self._download_requests(self.__class__.DOWNLOAD_URL, ofn)
        else:
            self._download_pycurl(self.__class__.DOWNLOAD_URL, ofn)

        # Get an MD5 of the file and store it
        # self.md5 = gzopen.hash_for_file(ofn, algorithm="md5")

        # Do we want to doo column/header checks? Probably not here, that is
        # for the parser
        # Move the file to the desired location
        self._move_file(ofn, self.outfile)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _mktemp(self):
        """Make a temp file to same the file into.

        Returns
        -------
        out_file_name : `str`
            A string containing the temp file name.
        """
        # We download into the temp directory and then copy to the final path
        fd, ofn = tempfile.mkstemp(suffix=self.__class__.__name__)
        os.close(fd)

        return ofn

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _move_file(self, source, dest):
        """Move the file to the final location.

        Parameters
        ----------
        source : `str`
            A string of the source path.
        dest : `str`
            A string of the destination path.

        Raises
        ------
        IOError
            If the move operation fails.
        """
        # Now we move the downloaded file into the desired location
        # if we can't move it then we capture the error and clean up before
        # raising
        try:
            shutil.move(source, dest)
        except IOError:
            os.unlink(source)
            raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _download_requests(self, url, ofn):
        """Perform the download using the requests module.

        Parameters
        ----------
        url : `str`
            The URL to download from.
        ofn : `str`
            The output file name.

        Raises
        ------
        DownloadError
            If something goes wrong to the download.
        """
        # This sets self.ani and self.msg that will be used to relay progress
        # if verbose = True
        self._init_animate(url)

        # LOG start of the download
        self.ani.log_start()

        # Navigate to the URL
        r = requests.get(url, timeout=0.5)

        if r.status_code == 200:
            with open(ofn, 'wb') as f:
                for c, chunk in enumerate(r):
                    if (c % 1000) == 0:
                        self.ani.animate(self.msg)
                    download_size = sys.getsizeof(chunk)
                    self.ani.next(lines=download_size)
                    f.write(chunk)
        else:
            raise DownloadError(
                "download failed", r.status_code, self.__class__.__name__
            )
        self.ani.log_finish()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _download_pycurl(self, url, ofn):
        """Perform the download using the pycurl module.

        Parameters
        ----------
        url : `str`
            The URL to download from.
        ofn : `str`
            The output file name.

        Raises
        ------
        DownloadError
            If something goes wrong to the download.
        """
        # initialise the animator that will handle the progress if verbose=True
        # this will set self.ani and self.msg
        self._init_animate(url)

        # Open the temp file that we will download to
        with open(ofn, "wb") as fp:
            # Set up the pycurl parameter
            curl = pycurl.Curl()
            curl.setopt(curl.URL, url)
            curl.setopt(curl.NOPROGRESS, False)
            curl.setopt(curl.XFERINFOFUNCTION, self._update)
            curl.setopt(curl.FOLLOWLOCATION, 1)
            curl.setopt(curl.MAXREDIRS, 5)
            curl.setopt(curl.CONNECTTIMEOUT, 50)
            curl.setopt(curl.FTP_RESPONSE_TIMEOUT, 600)
            curl.setopt(curl.NOSIGNAL, 1)
            curl.setopt(curl.WRITEDATA, fp)

            try:
                # Start the download
                curl.perform()
            except pycurl.error as e:
                # We capture any errors and raise them as DownloadError
                raise DownloadError(
                    str(e),
                    curl.getinfo(curl.RESPONSE_CODE),
                    self.__class__.__name__
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_animate(self, url):
        """Initialise the animator and the animation message.

        Parameters
        ----------
        url : `str`
            The URL to download from.
        """
        self._prev_download = 0

        # We get the download size in bytes. Note that this uses PYCURL
        self.download_size = self._get_download_size_pycurl(url)

        # If we get a size, we might not if we get an incorrect response
        # then we set up a line animator that will show time remaining etc
        if self.download_size > 0:
            # size = round((self.download_size/1024)/1024, 0)
            size = int(self.download_size)
            msg = "[info] Downloading {0} MB {1}....".format(
                round((self.download_size/1024)/1024, 0),
                self.LABEL
            )
            self._progress = tqdm(
                total=size, desc=msg, unit=" bytes",
                disable=not self.verbose, leave=False
            )
        else:
            # Otherwise we fallback to the simple animator
            self._progress = tqdm(
                desc="Downloading file....",
                unit=" bytes",
                disable=not self.verbose
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_download_size_pycurl(self, url):
        """Attempt to get the download size of a file at a URL.

        Parameters
        ----------
        url : `str`
            The url to the file for size check.

        Returns
        -------
        size : `int`
            The size in bytes.
        """
        c = pycurl.Curl()
        c.setopt(c.URL, url)
        c.setopt(c.NOBODY, 1)
        c.perform()
        return c.getinfo(c.CONTENT_LENGTH_DOWNLOAD)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update(self, download_t, download_d, upload_t, upload_d):
        """The progress call back, The arguments here are supplied by pycurl.

        Parameters
        ----------
        download_t : ``
            The download total size (bytes)
        download_d : `int`
            The downloaded data size (bytes)
        upload_t : ``
            The upload total size (bytes)
        upload_d : `int`
            The upload data size (bytes)
        """
        update = download_d - self._prev_download
        self._prev_download = download_d
        if update > 0:
            try:
                self._progress.update(update)
            except RuntimeError:
                pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GraspRepFetch(BaseGwasRepFetch):
    """A fetcher for the GRASP repository.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GRASP file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "https://s3.amazonaws.com/NHLBI_Public/GRASP/GraspFullDataset2.zip"
    """The download URL for the grasp data (`str`)
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatStudyFetch(BaseGwasRepFetch):
    """A fetcher for the GWAS catalogue study data repository.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GWAS catalogue study file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "https://www.ebi.ac.uk/gwas/api/search/downloads/studies_new"
    """The download URL for the GWAS catalog study file 1.0.3 data (`str`)
    """
    EXPECTED_HEADER = [old for old, new in con.STUDY_FILE_EXP_HEADER]
    """The GWAS catalogue study file expected header for the v1.0.3 format.
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatAssocFetch(BaseGwasRepFetch):
    """A fetcher for the GWAS catalogue association data.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GWAS catalogue association file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "https://www.ebi.ac.uk/gwas/api/search/downloads/alternative"
    """The download URL for the GWAS catalog 1.0.2 association data (`str`)
    """
    EXPECTED_HEADER = [old for old, new in con.ASSOC_FILE_EXP_HEADER]
    """The GWAS catalogue expected header for the v1.0.2 association format.
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatAncestryFetch(BaseGwasRepFetch):
    """A fetcher for the GWAS catalogue ancestry data.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GWAS catalogue ancestry file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "https://www.ebi.ac.uk/gwas/api/search/downloads/ancestries_new"
    """The download URL for the GWAS catalog 1.0.3 ancestry data (`str`)
    """
    EXPECTED_HEADER = [old for old, new in con.ANCESTRY_FILE_EXP_HEADER]
    """The GWAS catalogue expected header for the v1.0.3 ancestry format.
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EnsemblGenesGrch38Fetch(BaseGwasRepFetch):
    """A fetcher for the Ensembl GRCh38 feature definitions.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GRCh38 GTF genes file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "http://ftp.ensembl.org/pub/release-108/gtf/homo_sapiens/Homo_sapiens.GRCh38.108.gtf.gz"
    """The download URL Ensembl build 38 genes (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def check_expected_header(self):
        """GTF files have a more complex header so we skip this test.
        """
        return True


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EnsemblGenesGrch37Fetch(BaseGwasRepFetch):
    """A fetcher for the Ensembl GRCh37 feature definitions.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "GRCh37 GTF genes file"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "http://ftp.ensembl.org/pub/grch37/release-107/gtf/homo_sapiens/Homo_sapiens.GRCh37.87.gtf.gz"
    """The download URL Ensembl build 37 genes (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def check_expected_header(self):
        """GTF files have a more complex header so we skip this test.
        """
        return True


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HgncFetch(BaseGwasRepFetch):
    """A fetcher for the HGNC gene definitions and synonyms.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    LABEL = "HGNC gene names"
    """A label to add to the download progress (`str`)
    """
    DOWNLOAD_URL = \
        "http://ftp.ebi.ac.uk/pub/databases/genenames/hgnc/tsv/hgnc_complete_set.txt"
    """The download URL for the HGNC gene names (`str`)
    """
    DELIMITER = "\t"
    """The expected delimiter for the file (`str`)
    """
    INTERNAL_DELIMITER = "|"
    """The internal delimiter for the file (`str`)
    """
    # The expected header in the GWAS catalogue
    EXPECTED_HEADER = [
        "hgnc_id", "symbol", "name", "locus_group", "locus_type", "status",
        "location", "location_sortable", "alias_symbol", "alias_name",
        "prev_symbol", "prev_name", "gene_group", "gene_group_id",
        "date_approved_reserved", "date_symbol_changed", "date_name_changed",
        "date_modified", "entrez_id", "ensembl_gene_id", "vega_id",
        "ucsc_id", "ena", "refseq_accession", "ccds_id", "uniprot_ids",
        "pubmed_id", "mgd_id", "rgd_id", "lsdb", "cosmic", "omim_id",
        "mirbase", "homeodb", "snornabase", "bioparadigms_slc",
        "orphanet", "pseudogene.org", "horde_id", "merops", "imgt", "iuphar",
        "kznf_gene_catalog", "mamit-trnadb", "cd", "lncrnadb", "enzyme_id",
        "intermediate_filament_db", "rna_central_ids", "lncipedia", "gtrnadb",
        "agr", "mane_select", "gencc"
    ]
    """The HGNC names file expected header. (`list` of `str`)
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasDbRepFetch(BaseGwasRepFetch):
    """A fetcher for the GWAS DB repository. Note this is not currently used.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    """
    DOWNLOAD_URL = "ftp://jjwanglab.org/GWASdb/gwasdb_20150819_snp_trait.gz"
    """The download URL for the GWASdb data (`str`)
    """


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseSeleniumFetch(object):
    """A base class that uses Selenium to get data, do not use directly. This
    uses Google chrome to interact with web pages.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    tmpdir : `str`, optional, default: `NoneType`
        The path to a temp directory for the downloads. If it is ``NoneType``
        then the system temp is used..
    driver_path : `str`, optional, default: `NoneType`
        The path to the chrome webdriver (if it is not in your path).
    download_dir : `str`, optional, default: `NoneType`
        The path to a downloads directory for the browser to use. If
        ``NoneType`` then the browser default will be used.
    headless : `bool`, optional, default: `False`
        Should the browser be headless (i.e. not physically open). This should
        be used on non-desktop systems.
    """
    LABEL = "file"
    """A label to add to the download progress (`str`)
    """
    EXPECTED_HEADER = []
    """The expected header of the downloaded trial (`list` of `str`)
    """
    DELIMITER = "\t"
    """The expected delimiter of the downloaded trial (`str`)
    """
    HOME_URL = "www.google.co.uk"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, outfile, verbose=False, timeout=0.5, tmpdir=None,
                 driver_path=None, download_dir=None, headless=False):
        self.outfile = outfile
        self.verbose = verbose
        self.md5 = ""
        self.timeout = timeout
        self.tmpdir = tmpdir
        self.driver_path = driver_path
        self.download_dir = download_dir
        self.headless = headless
        self._progress = None
        self._tmpfile = None
        self._fobj = None
        self._csv_writer = None
        self.webdriver = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An open interface function.
        """
        self._tmpfile = utils.get_tmp_file(dir=self.tmpdir)
        self._fobj = open(self._tmpfile, 'wt')
        self._csv_writer = csv.writer(self._fobj, delimiter="\t")
        self.initialise_chrome()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """A close interface function.
        """
        if self._progress is not None:
            self._progress.close()

        if self._fobj is not None:
            self._fobj.close()

            # re-locate the temp download to final location
            self._move_file(self._tmpfile, self.outfile)

        if self.webdriver is not None:
            self.webdriver.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialise_chrome(self):
        """Initialise a chrome webdriver to download the files. Note that I
        can't make this headless at the moment as downloading does not seem to
        work in headless mode.
        """
        chrome_options = Options()
        # Downloads do not work in headless mode - see here for potential
        # solutions
        # https://github.com/TheBrainFamily/chimpy/issues/108
        if self.headless is True:
            chrome_options.add_argument("--headless")

        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")

        if self.download_dir is not None:
            prefs = {"profile.default_content_settings.popups": 0,
                     "download.default_directory": self.download_dir,
                     "directory_upgrade": True}
            chrome_options.add_experimental_option("prefs", prefs)

        kwargs = {'options': chrome_options}
        if self.driver_path is not None:
            kwargs['executable_path'] = self.driver_path

        self.webdriver = webdriver.Chrome(**kwargs)
        self.go_home()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def go_home(self):
        """Navigate to the home URL.
        """
        # Navigate to the GWAS Atlas home page
        self.webdriver.get(self.HOME_URL)
        time.sleep(2)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download(self):
        """Initialise the down load process, this should be overridden.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _move_file(self, source, dest):
        """Move the file to the final location.

        Parameters
        ----------
        source : `str`
            A string of the source path.
        dest : `str`
            A string of the destination path.

        Raises
        ------
        IOError
            If the move operation fails.
        """
        # Now we move the downloaded file into the desired location
        # if we can't move it then we capture the error and clean up before
        # raising
        try:
            shutil.move(source, dest)
        except IOError:
            os.unlink(source)
            raise


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DownloadAbbreviations(BaseSeleniumFetch):
    """A base class that uses Selenium to get data, do not use directly. This
    uses Google chrome to interact with web pages.

    Parameters
    ----------
    outfile : `str`
        A string of the file path to save the file. The default is
        the file name from the url in the current working directory. If the
        user supplies a file path (i.e. the string endswith '/', then the
        url filename is used and saved in that directory.
    verbose : `bool`
        Shall we display download progress.
    timeout : `int` or `float`
        A positive ``int`` or ``float`` for the max time to wait for the server
        to respond before throwing a time out exception.
    tmpdir : `str`, optional, default: `NoneType`
        The path to a temp directory for the downloads. If it is ``NoneType``
        then the system temp is used..
    driver_path : `str`, optional, default: `NoneType`
        The path to the chrome webdriver (if it is not in your path).
    download_dir : `str`, optional, default: `NoneType`
        The path to a downloads directory for the browser to use. If
        ``NoneType`` then the browser default will be used.
    headless : `bool`, optional, default: `False`
        Should the browser be headless (i.e. not physically open). This should
        be used on non-desktop systems.
    """
    HOME_URL = "http://www.ebi.ac.uk/gwas/docs/abbreviations"
    """The URL that the browser will navigate to when opening (`str`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download(self):
        """Download the GWAS catalog abbreviation data and write to the output
        file.
        """
        # I am using selenium because of the java script, however, this is an
        # alternative
        # https://pypi.org/project/requests-html/
        # Useful info:
        # https://stackoverflow.com/questions/26393231/using-python-requests-with-javascript-pages
        # page = requests.get(url)
        # tree = html.fromstring(page.content)
        # This finds the section to the file downloads
        elements = self.webdriver.find_elements(
            By.XPATH,
            '//div[@class="sect2"]/div[@class="paragraph"]/p'
        )

        # writ the header
        self._csv_writer.writerow(['abbreviation', 'expanded'])
        for i in elements:
            i = i.text
            reg = re.match(r'(.+?):\s*(.*)$', i)
            self._csv_writer.writerow(
                [reg.group(1), reg.group(2)]
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_gwas_atlas_summary(webdriver, download_dir,
                                url="https://atlas.ctglab.nl",
                                download_sleep=10):
    """Download the GWAS atlas summary file. This has to use selenium because of
    the javascript involved. This is not currently used.

    Parameters
    ----------
    webdriver : `selenium.Webdriver`
        The selanium webdriver.
    download_dir : `str`
        The download directory used by the browser. This is needed to return
        the download file paths.
    url : `str`, optional, default: `https://atlas.ctglab.nl`
        The URL for the GWAS atlas, this should not need to be changed.
    download_sleep : `int`, optional, default: `10`
        The sleeptime in seconds to wait after clicking the download button.

    Returns
    -------
    file_paths : `list` of `str`
        The paths to the downloaded files.
    """

    # Navigate to the GWAS Atlas home page
    webdriver.get(url)
    time.sleep(2)

    # This finds the section to the file downloads
    element = webdriver.find_element_by_xpath(
        '//h4[text() = "Database release"]')

    # This finds the download location
    download = element.find_element_by_xpath(
        '//a[@class="release" and contains(text(), ".txt.gz")]')

    # Click the download location to get the file
    download.click()

    # The file is very small so should download fast, so I do not need any
    # expensive polling of the directory
    time.sleep(download_sleep)

    # Get the location of the downloaded file, this could generate an
    # IndexError of the file can't be found
    return glob.glob(os.path.join(download_dir, '*.txt.gz'))[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_gwas_atlas_summary(fn):
    """Parse the GWAS Atlas summary file. This is not currently used.

    Parameters
    ----------
    fn : `str`
        Th name of the GWAS atlas summary file.

    Yields
    ------
    gwas_atlas_row : `dict`
        A row from the GWAS atlas download.
    """
    col_map = [('id', 'resource_id', str), ('PMID', 'pubmed_id', int),
               ('ChapterLevel', 'trait_area', str),
               ('SubchapterLevel', 'trait_subarea', str),
               ('Domain', 'trait_domain', str), ('Trait', 'trait', str),
               ('uniqTrait', 'uniq_trait', str),
               ('Genome', 'genome_assembly', str),
               ('Population', 'population', str),
               ('Consortium', 'consortium', str),
               ('Ncase', 'ncases', int), ('Ncontrol', 'ncontrols', int),
               ('N', 'nsamples', int), ('File', 'file_download', str),
               ('Website', 'website', str),
               ('Note', 'notes', str)]
    resource_name = "gwas_atlas"

    with gzip.open(fn, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        header = next(reader)
        for i in reader:
            row = dict([(header[idx], i[idx]) for idx in range(len(header))])

            import_row = {}
            for rc, ic, cast in col_map:
                row[rc] = row[rc].strip()
                if row[rc] == '':
                    row[rc] = None
                try:
                    import_row[ic] = cast(row[rc])
                except (TypeError, ValueError):
                    # Could be bad pubmed IDs or NoneTypes
                    import_row[ic] = None
                    pass

            if import_row['pubmed_id'] is not None:
                import_row['resource_name'] = resource_name
                yield import_row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_grasp_summary(url="https://grasp.nhlbi.nih.gov/FullResults.aspx"):
    """Download all the abbreviations into a list of tuples. This is not
    currently used.

    Parameters
    ----------
    url : `str`, optional, default: `https://grasp.nhlbi.nih.gov/FullResults.aspx`
        The URL of the webpage, this should not need to change but is here just
        in case it does and the code is not updated in time.

    Yields
    ------
    record : `dict`
        A gwas grasp download record.
    """
    page = requests.get(url)
    tree = html.fromstring(page.content)
    tree.make_links_absolute(url)

    pmid_regex = re.compile(r'\?term=(?P<PMID>\d+)$')
    data_file_regex = re.compile(
        r'(?:Results File\s*\d+|Full Results are posted)',
        re.IGNORECASE)
    readme_file_regex = re.compile(r'Results Readme File', re.IGNORECASE)
    trait_regex = re.compile(r'Trait(?:s|\(s\))?\s*:\s*(.*)',
                             re.IGNORECASE)
    sample_regex = re.compile(r'Sample Size\s*(?:\(~n\))?:\s*(.*)',
                              re.IGNORECASE)

    readme_in_data_file_regex = re.compile(r'readme', re.IGNORECASE)

    # Loop through all the studies
    for i in tree.xpath('//div[@id="ctl00_cphBody_menuContent"]//p'):
        data_files = []
        readme_files = []
        pubmed_id = None

        for j in i.xpath(".//a"):
            try:
                link_element, link_type, link_url, n = list(j.iterlinks())[0]
            except IndexError:
                continue

            # Is this the pubmed ID link that we have to extract the
            # pubmed ID from
            try:
                is_pmid = pmid_regex.search(link_url)
            except (ValueError, TypeError):
                # could fail
                pass

            try:
                pubmed_id = is_pmid.group('PMID')
            except AttributeError:
                pass

            # Process the results files, some of these I have noticed are
            # actually readmes
            try:
                if data_file_regex.search(link_element.text):
                    if readme_in_data_file_regex.search(link_url):
                        readme_files.append(link_url)
                    else:
                        data_files.append(link_url)
                elif readme_file_regex.search(link_element.text):
                    # The the links that are explicitly readme files
                    readme_files.append(link_url)
            except TypeError:
                # Could be None
                pass

        traits = []
        samples = []
        try:
            # Now try to get the traits sample size etc...
            for t in i.itertext():
                t = t.strip()
                try:
                    trait = trait_regex.match(t)
                    traits.append(trait.group(1))
                except AttributeError:
                    pass

                try:
                    sample = sample_regex.match(t)
                    samples.append(re.sub(r',', '',
                                          sample.group(1)))
                except AttributeError:
                    pass

        except AttributeError:
            pass

        # Now compile all the data and yield, we may be able to assign
        # a trait with a file but there is no guanantee so will will
        # merge all the traits into one and produce an entry for each
        # data file
        if pubmed_id is not None:
            for d in data_files:
                try:
                    # Convert to an int the value error is just in case it is
                    # not castable to an int
                    pubmed_id = int(pubmed_id)
                except ValueError:
                    continue

                record = {'trait': '|'.join(traits),
                          'file_download': d,
                          'pubmed_id': pubmed_id,
                          'notes': [],
                          'readme_download': None,
                          'resource_name': 'grasp'}

                # If there is > 1 readme file then store the first
                # and drop the rest in the notes
                if len(readme_files) > 1:
                    record['readme_download'] = readme_files[0]
                    record['notes'].append('README: {0}'.format(
                        ','.join(readme_files[1:])))
                else:
                    try:
                        # If not then we attempt to store the first, however
                        # there could be None in which case we ignore
                        record['readme_download'] = readme_files[0]
                    except IndexError:
                        pass

                # For the samples, we attempt to store the first one as an
                # int and then if that fails we store the whole lot in the
                # notes
                try:
                    record['nsamples'] = int(samples[0])
                except IndexError:
                    record['nsamples'] = None
                except (TypeError, ValueError):
                    record['notes'].append(
                        'SAMPLES: {0}'.format(','.join(samples)))

                # Finally combine the notes
                record['notes'] = '|'.join(record['notes'])

                # Now make sure anything that is empty is make none
                for k, v in record.items():
                    if record[k] == '':
                        record[k] = None

                yield record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gwas_cat_full_data_count():
    """Get the number of full datasets available. Note this is not currently
    used.

    Returns
    -------
    full_data_count : `int`
        The number of full data directories.

    Notes
    -----
    A bit of a hacky way of getting the available full datasets, I could not
    find a way of doing this via the API, so I get a directory listing from the
    FTP server, this is likely to be an overestimation but good enough.
    """
    count_dir = 'pub/databases/gwas/summary_statistics/'

    # Connect to the FTP site
    ftp_site = "ftp.ebi.ac.uk"
    ftp = ftplib.FTP(ftp_site)
    ftp.login("anonymous", "")

    # Change the FTP directory to the current download directory to get a
    # sub directory listing
    ftp.cwd(count_dir)

    dir_list = []

    # get the contents of the directory
    ftp.dir(dir_list.append)
    return len(dir_list)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_gwas_cat_full_summary(
        url="https://www.ebi.ac.uk/gwas/api/search/summaryStatistics",
        download_max=5000):
    """Download GWAS catalog full data links. Note this is currently not used.

    Parameters
    ----------
    url : `str`, optional, default: `https://www.ebi.ac.uk/gwas/api/search/summaryStatistics`
        The URL of the webpage, this should not need to change but is here just
        in case it does and the code is not updated in time.
    download_max : `int`, optional, default: `5000`
        This is passed as a parameter to the URL to get more download data in
        one REST call.

    Yields
    ------
    record : `dict`
        A gwas catalog download record.
    """
    params = {'q': 'fullPvalueSet:true',
              'max': download_max,
              'fl': 'author,publicationDate,pubmedId,publication,title,'
              'traitName,associationCount,author_s,accessionId'}
    # url = '{0}?q=fullPvalueSet%3Atrue&max={1}'.format(url, download_max)

    # r = requests.get(url)
    r = requests.get(url, params=params)
    fulltext = json.loads(r.text)

    # Connect to the FTP site
    ftp_site = "ftp.ebi.ac.uk"
    ftp = ftplib.FTP(ftp_site)
    ftp.login("anonymous", "")

    # Loop through all the returned studies
    for i in fulltext['response']['docs']:
        resource_id = i['accessionId']
        pubmed_id = int(i['pubmedId'])

        # This generates variables that we will need to query the FTP server
        ftp_author = re.sub(r'\s+', '', i['authorAscii_s'])
        dir_name = "{0}_{1}_{2}".format(ftp_author, pubmed_id, resource_id)

        # The full download directory in here there are download files and
        # other directories that have the download data in them
        download_dir = "{0}{1}".format(
            "ftp://ftp.ebi.ac.uk/pub/databases/gwas/summary_statistics/",
            dir_name)

        # The FTP dir is the download dir with the ftp.ebi.ac.uk removed
        ftp_dir = '{0}{1}'.format('/pub/databases/gwas/summary_statistics/',
                                  dir_name)

        # Change the FTP directory to the current download directory to get a
        # sub directory listing
        ftp.cwd(ftp_dir)

        # Now get the files recursively
        file_listing = get_file_listing(ftp)
        for v in file_listing:
            record = {'pubmed_id': pubmed_id,
                      'resource_name': 'gwas_cat',
                      'resource_id': resource_id,
                      'trait': i['traitName_s'],
                      'file_download': "{0}{1}{2}".format(ftp_site,
                                                          ftp_dir,
                                                          v['name']),
                      'notes': 'DOWNLOAD_DIR: {0}'.format(download_dir)}
            yield record


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_file_listing(ftpobj):
    """recursively attempt to get a file listing from the current working
    directory of the ftpobj

    Parameters
    ----------
    ftpobj : `FTP`
        The FTP object generated by ``ftplib.FTP``.

    Returns
    -------
    files : `list` of `str`
        All the files that exist at or below the CWD of the ftpobj.
    """
    # Will hold the contents of the direcory listing
    dir_list = []

    # Will hold the contents of any sub directories that are found when
    # processing the current directory
    sub_dirs = []

    # Get the current working directory
    pwd = ftpobj.pwd()
    # joinpwd = pwd
    if not pwd.endswith(r'/'):
        pwd = "{0}/".format(pwd)

    # Get the directory content and make sure it is in the directory listing
    # this will be processed later to extract information
    ftpobj.dir(dir_list.append)

    # Now loop through the directory list and process the output, we also
    # tranverse to any other directories we find on the way
    for c, d in enumerate(dir_list):
        # print(c)
        # print(d)

        # Get all the groups
        try:
            lp = re.match(
                r'^(?P<dir>[-ld])(?P<permission>([-r][-w][-xs]){3})\s+'
                r'(?P<filecode>\d+)\s+(?P<owner>\w+)\s+(?P<group>\w+)\s+'
                r'(?P<size>\d+)\s+(?P<timestamp>((?P<month>\w{3})\s+'
                r'(?P<day>\d{2})\s+(?P<hour>\d{1,2}):(?P<minute>\d{2}))'
                r'|((?P<month2>\w{3})\s+(?P<day2>\d{2})\s+(?P<year2>'
                r'\d{4})))\s+(?P<name>.+)$', d
            )

            atts = lp.groupdict()
        except (AttributeError, TypeError):
            # If it is None, there presumably there is no listsing, so just
            # move on perhaps i should just change this to break?
            # print("CONTINUE AFTER")
            # print(d)
            continue

        # change the relative name to an absolute one
        # print("FILE LISTING PWD: '{0}'".format(pwd))
        # print("FILE LISTING ATTS")
        # print(atts['name'])
        atts['name'] = urllib.parse.urljoin(pwd, atts['name'])
        # pp.pprint(atts)

        if atts['dir'] == "d":
            # print(atts)
            # Move to the nested directory and then process
            ftpobj.cwd(atts['name'])
            sub_dirs.extend(get_file_listing(ftpobj))
        else:
            sub_dirs.append(atts)

    # Make sure the FTP object is back to where we started
    ftpobj.cwd(pwd)

    # Add any files in subdirs to the directory list
    # dir_list.extend(sub_dirs)
    return sub_dirs


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DownloadError(Exception):
    """Called when a GWAS repository can't be downloaded.

    Parameters
    ----------
    message : `str`
        The error message to output.
    response : `str`
        The response code from the webserver.
    class_name : `str`
        The name of the class that the search attribute is missing from.
    *args
        Arguments passed forward to ``Exception``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, response_code, class_name, *args):
        # Store attributes
        self.message = message
        self.response_code = response_code
        self.class_name = class_name

        # Call the Exception superclass
        super(DownloadError, self).__init__(message, *args)
