"""Functions to import gene data
"""
# Importing the version number (in __init__.py) and the package name
from gwas_catalog_parser import (
    query as q,
    constants as con,
    config,
    orm as o
)
from gwas_catalog_parser.build import (
    download
)
from tqdm import tqdm
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import literal, and_
import csv
import gzip
import re
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_gene_lookup(session, download_file, assembly, verbose=False):
    """Import gene definitions from ensembl if not present. Also add any orphan
    genes.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    assembly : `str`
        The geneome assembly we are building for either GRCh37 or GRCh38 (case
        sensitive).
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    tmpdir : `str`, optional, default: `NoneType`
        A directory to store the downloaded file, if not provided will default
        to the system temp location.

    Returns
    -------
    nensembl_genes : `int`
        The number of ensembl genes (downloaded), loaded into the database.
    """
    nensembl_genes = 0

    ens_version = q.get_gene_version(session)

    if ens_version is None:
        dl_class = None
        # Download and process
        if assembly == config.GwasCatalogConfig.B38_ASSEMBLY:
            dl_class = download.EnsemblGenesGrch38Fetch
        elif assembly == config.GwasCatalogConfig.B37_ASSEMBLY:
            dl_class = download.EnsemblGenesGrch37Fetch
        else:
            raise ValueError("unknown assembly value: {assembly}")

        with dl_class(download_file, verbose=verbose) as fetch:
            fetch.download()

        parser = parse_gtf(download_file)
        header = next(parser)

        try:
            if assembly != header['genome-version']:
                raise ValueError(
                    "assemblies do not match: {assembly} vs. "
                    "{header['genome-version']}"
                )
        except KeyError as e:
            raise KeyError(
                "unexpected gene file header gene file header"
            ) from e

        tqdm_kwargs = dict(
            unit=" rows",
            desc="[info] processing gene download..."
        )

        for feature in tqdm(parser, **tqdm_kwargs):
            if feature['feature'] == 'gene':
                del feature['feature']
                del feature['frame']
                egid = feature['id']
                del feature['id']
                feature['ensembl_gene_id'] = egid
                g = o.Gene(**feature)
                nensembl_genes += 1
                session.add(g)
        gene_assembly = o.Option(
            option_type=con.ENS_GENE_VERSION_OPT,
            option_value=assembly
        )
        session.add(gene_assembly)
        session.commit()

    return nensembl_genes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_orphan_genes(session, verbose=False):
    """Import orphan genes in the GWAS catalog that are not represented in the
    genes table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.

    Returns
    -------
    norphan_genes : `int`
        The number of unidentified gene IDs loaded into the database.

    Notes
    -----
    An orphan gene is any gene ID in the GWAS catalog tat is not in Ensembl.
    These are added so there is a link to the ID even if not valid.
    """
    norphans = 0
    test_genes = set()

    gq = session.query(
        o.RawAssociation.upstream_gene_id,
        o.RawAssociation.downstream_gene_id,
        o.RawAssociation.snp_gene_ids
    )
    for row in gq:
        for c in row:
            if c is not None:
                test_genes.update(
                    [g.strip() for g in c.split(',')]
                )
    tqdm_kwargs = dict(
        unit=" genes",
        desc="[info] testing for orphan genes...",
        disable=not verbose,
        leave=False
    )

    for g in tqdm(test_genes, **tqdm_kwargs):
        try:
            q.get_gene(session, g)
        except NoResultFound:
            norphans += 1
            session.add(get_orphan_gene(g))

    if norphans > 0:
        session.commit()
    return norphans


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_hgnc_synonyms(session, download_file, verbose=False):
    """Import the HGNC synonyms into the database. These are linked to the
    ``genes`` table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    tmpdir : `str`, optional, default: `NoneType`
        A directory to store the downloaded file, if not provided will default
        to the system temp location.

    Returns
    -------
    nsynonyms : `int`
        The number of gene synonyms added.
    norphan_genes : `int`
        The number of unidentified gene IDs loaded into the database.
    """
    nsynonyms = 0
    norphans = 0

    syn_count = session.query(o.GeneSynonym).count()
    if syn_count > 0:
        # If already loaded do not load again
        return nsynonyms, norphans

    columns = [
        ("hgnc_id", parse_str), ("symbol", parse_str),
        ("name", parse_str), ("alias_symbol", parse_delim),
        ("alias_name", parse_delim), ("prev_symbol", parse_delim),
        ("prev_name", parse_delim), ("entrez_id", parse_str),
        ("vega_id", parse_str), ("ucsc_id", parse_str),
        ("ena", parse_delim), ("refseq_accession", parse_delim),
        ("ccds_id", parse_delim), ("uniprot_ids", parse_delim),
        ("mgd_id", parse_delim), ("rgd_id", parse_delim),
        ("lsdb", parse_delim), ("cosmic", parse_delim),
        ("omim_id", parse_delim), ("mirbase", parse_delim),
        ("homeodb", parse_delim), ("snornabase", parse_delim),
        ("orphanet", parse_delim), ("pseudogene.org", parse_delim),
        ("horde_id", parse_delim), ("merops", parse_delim),
        ("imgt", parse_delim), ("iuphar", parse_end),
        ("kznf_gene_catalog", parse_delim), ("mamit-trnadb", parse_delim),
        ("cd", parse_delim), ("lncrnadb", parse_delim),
        ("enzyme_id", parse_delim), ("intermediate_filament_db", parse_delim),
        ("rna_central_ids", parse_delim), ("lncipedia", parse_delim),
        ("gtrnadb", parse_delim), ("agr", parse_delim),
        ("mane_select", parse_delim), ("gencc", parse_delim)
    ]

    with download.HgncFetch(download_file, verbose=verbose) as fetch:
        fetch.download()

    with open(download_file) as incsv:
        total = sum([1 for i in incsv])

    with open(download_file) as incsv:
        reader = csv.DictReader(incsv,
                                delimiter=download.HgncFetch.DELIMITER)
        tqdm_kwargs = dict(
            unit=" rows",
            desc="[info] processing HGNC synonyms...",
            disable=not verbose,
            total=total,
            leave=False
        )

        for row in tqdm(reader, **tqdm_kwargs):
            # pp.pprint(row)
            ens_genes = []
            if row["ensembl_gene_id"] == '':
                continue

            try:
                ensembl_ids = parse_delim(row["ensembl_gene_id"])

                for i in ensembl_ids:
                    try:
                        ens_genes.append(q.get_gene(session, i))
                    except NoResultFound:
                        norphans += 1
                        session.add(get_orphan_gene(i))
            except AttributeError:
                continue

            if len(ens_genes) == 0:
                continue

            syn_list = []
            for syn_type, parser in columns:
                if row[syn_type] == '' or row[syn_type] is None:
                    continue
                for syn_name in parser(row[syn_type]):
                    syn_list.append(
                        dict(syn_type=syn_type, syn_name=syn_name)
                    )
            for i in ens_genes:
                for j in syn_list:
                    nsynonyms += 1
                    session.add(o.GeneSynonym(gene=i, **j))
    session.commit()
    return nsynonyms, norphans


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_orphan_gene(ensembl_gene_id):
    """Generate an orphan gene entry from an ensembl gene ID.

    Parameters
    ----------
    ensembl_gene_id : `str`
        The ensembl gene identifier string.

    Returns
    -------
    orphan_gene : `gwas_catalog_parser.orm.Gene`
        The orphan gene entry, this is not attached to any session.
    """
    return o.Gene(ensembl_gene_id=ensembl_gene_id)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_end(field):
    """Remove the leader characters up to a ``:`` delimiter keeping the
    remaining digits.

    Parameters
    ----------
    field : `str`
        The data to process. i.e. ``ObjectId:12345``.

    Returns
    -------
    processed_field : `str`
        i.e. ``12345``
    """
    return [re.sub(r'\w+:\s*(\d+)', '\\1', i) for i in parse_delim(field)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str(field):
    """Take the field and make it into a list.

    Parameters
    ----------
    field : `str`
        The data to process.

    Returns
    -------
    processed_field : `list` of `str`
        The list of length 1.
    """
    return [field]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_delim(field):
    """Take the field and make it into a list by splitting on a pipe delimiter.

    Parameters
    ----------
    field : `str`
        The data to process.

    Returns
    -------
    processed_field : `list` of `str`
        The list of length 1.
    """
    return field.split(download.HgncFetch.INTERNAL_DELIMITER)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_gtf(infile):
    """Parse a GTF file. Not sure if this is a fully fledged parser as only
    tested in Ensembl versions.

    Parameters
    ----------
    infile : `str`
        The path to the GTF file.

    Yields
    ------
    gtf_rows : `dict`
        The first row yielded will be the header followed by data rows.
    """
    with gzip.open(infile, 'rt') as csvin:
        reader = csv.reader(csvin, delimiter="\t")
        header = dict()
        # Sort out the GTF header and yield it first
        for row in reader:
            if re.match(r'\s*#!', row[0]):
                value = row[0]
                value = re.sub(r'\s*#!', '', value)
                keyreg = re.match(r'^(?P<KEY>[-\w]+)\s+', value)
                key = keyreg.group('KEY')
                value = re.sub(r'^[-\w]+\s+', '', value)
                header[key] = value
            else:
                break
        yield header
        yield parse_gtf_row(row)

        for row in reader:
            # if row[2] == 'gene':
            yield parse_gtf_row(row)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_gtf_row(row):
    """Parse a GTF row.

    Parameters
    ----------
    row : `list` of `str`
        A row from the GTF file that needs processing.

    Returns
    -------
    processed_row : `dict`
        The parsed row, that includes expansion of the key/value pairs in the
        last column.
    """
    colnames = [
        'chr_name', 'source', 'feature', 'start_pos', 'end_pos',
        'score', 'strand', 'frame'
    ]
    row = [i if i != '.' else None for i in row]
    parsed_row = dict(
        [(colnames[idx], i) for idx, i in enumerate(row[:8])]
    )
    if parsed_row['strand'] == '+':
        parsed_row['strand'] = 1
    elif parsed_row['strand'] == '-':
        parsed_row['strand'] = -1
    parsed_row['start_pos'] = int(parsed_row['start_pos'])
    parsed_row['end_pos'] = int(parsed_row['end_pos'])

    if parsed_row['score'] is not None:
        parsed_row['score'] = float(parsed_row['score'])

    row[-1] = re.sub(r';$', '', row[-1])
    row[-1] = re.sub(r'(\w) "', '\\1="', row[-1])
    for i in row[-1].split(';'):
        values = i.strip().split('=')
        try:
            k, v = values
        except ValueError:
            pp.pprint(values)
            raise
        k = re.sub(r'(gene|exon|transcript)_', '', k)
        v = v.replace('"', '')
        parsed_row[k] = v

    if 'version' in parsed_row:
        parsed_row['version'] = int(parsed_row['version'])

    return parsed_row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_associated_genes(session, verbose=False, commit_every=10000):
    """Create an associated genes table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for querying/updating the database.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.

    Returns
    -------
    nadded : `int`
        The number of associated genes that were added.
    nmissed : `int`
        The number of auther reported genes that could not be mapped to an
        ensembl gene ID.
    """
    bad_genes = ['NR', 'NR x NR', 'x', 'intergenic', 'pseudogene']
    split = re.compile(r'[\s,]+')
    nadded = 0
    nassoc_gene = 0
    nmissed = 0

    import_gene_class(session)
    gene_class = q.get_all_gene_class(session)

    query = _gene_query(session)
    tqdm_kwargs = dict(
        unit=" genes",
        desc="[info] importing associated genes",
        disable=not verbose,
        leave=False,
        total=query.count()
    )
    for apk, cid, g, gc in tqdm(query.order_by(o.Association.association_pk),
                                **tqdm_kwargs):
        if g.lower().strip() in bad_genes:
            continue

        gpk = None
        if gc != con.GENE_CLASS_REPORTED:
            for i in split.split(g):
                # print(i)
                ens = session.query(
                    o.Gene.gene_pk,
                    o.Gene.chr_name,
                    o.Gene.start_pos
                ).filter(
                    o.Gene.ensembl_gene_id == i.strip()
                 ).one()
                gpk = ens.gene_pk
        else:
            for i in split.split(g):
                syns = session.query(
                    o.GeneSynonym.gene_pk,
                    o.GeneSynonym.syn_name,
                    o.Gene.chr_name,
                    o.Gene.start_pos
                ).join(
                    o.Gene, o.Gene.gene_pk == o.GeneSynonym.gene_pk
                ).filter(
                    and_(o.GeneSynonym.syn_name == i.strip(),
                         o.Gene.chr_name == cid)
                ).all()
                syns = set(syns)
                if len(syns) == 1:
                    gpk = list(syns)[0].gene_pk
        if gpk is not None:
            session.add(
                o.AssociatedGene(
                    association_pk=apk,
                    gene_pk=gpk,
                    gene_class_pk=gene_class[gc]
                )
            )
            nadded += 1
            nassoc_gene += 1
        else:
            nmissed += 1

        if nadded >= commit_every:
            session.commit()
            nadded = 0
    if nadded > 0:
        session.commit()

    return nassoc_gene, nmissed


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_gene_class(session):
    """Setup the constants for the gene class table. These will only be
    added if not present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Notes
    -----
    Gene classes are used to track the column where the gene ID came from.
    """
    gene_class = q.get_all_gene_class(session)

    for i in con.GENE_CLASS_ALL:
        if i not in gene_class:
            session.add(
                o.GeneClass(gene_class_name=i)
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _gene_query(session):
    """Get any associated genes from raw_associations that are not represented
    in the associated_gene table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for querying/updating the database.

    Returns
    -------
    query : `sqlalchemy.Query`
        A query for all the genes that 
    """
    gene_cols = [
        (o.RawAssociation.snp_gene_ids, con.GENE_CLASS_OVERLAP),
        (o.RawAssociation.upstream_gene_id, con.GENE_CLASS_UPSTREAM),
        (o.RawAssociation.downstream_gene_id, con.GENE_CLASS_DOWNSTREAM),
        (o.RawAssociation.reported_genes, con.GENE_CLASS_REPORTED),
    ]
    queries = []

    for t, c in gene_cols:
        queries.append(
            session.query(
                o.Association.association_pk,
                o.RawAssociation.chr_id,
                t,
                literal(c)
            ).join(
                o.RawAssociation,
                o.RawAssociation.raw_association_pk ==
                o.Association.raw_association_pk
            ).join(
                o.AssociatedGene,
                o.AssociatedGene.association_pk ==
                o.Association.association_pk,
                isouter=True
            ).filter(
                and_(o.AssociatedGene.association_pk == None,
                     t != None)
            )
        )
    return queries[0].union_all(*queries[1:])
