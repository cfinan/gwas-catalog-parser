"""Functions of extracting association data and pre-processing it and
importing.
"""
from gwas_catalog_parser import (
    constants as con,
    orm as o,
    utils
)
from gwas_catalog_parser.build import variants
from tqdm import tqdm
import re
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_associations(session, verbose=False, commit_every=10000):
    """Process and import the association data and link to variants.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of entities that are added to a ``sqlalchemy.Session``
        before the ``sqlalchemy.Session.commit`` is called.

    Returns
    -------
    nassocs : `int`
        The number of associations that were added to the table.
    """
    nadded = 0
    nassocs = 0

    # Do a left join against raw associations to determine the associations
    # that have not been processed
    query = session.query(o.RawAssociation).\
        join(
            o.Association,
            o.Association.raw_association_pk ==
            o.RawAssociation.raw_association_pk,
            isouter=True
        ).filter(
            o.Association.raw_association_pk == None
        )

    tqdm_kwargs = dict(
        unit=" associations",
        desc="[info] importing associations",
        disable=not verbose,
        leave=False,
        total=query.count()
    )

    # Querying the studies seems slow so I will cache the IDs in memory as
    # there is not that many
    study_lookup = dict(
        [i for i in session.query(o.Study.gwas_catalog_accession,
                                  o.Study.study_pk)]
    )

    # Loop through the unprocessed associations
    for rd in tqdm(query, **tqdm_kwargs):
        try:
            # Get the study for the association, in theory they should all be
            # represented but it is possible that they are not (error)
            # so we allow for that possibility
            study_pk = study_lookup[rd.gwas_catalog_accession]
        except KeyError:
            continue

        effect_type = effect_type_guess(
            rd.initial_sample_size,
            rd.replication_sample_size,
            rd.ci_text)
        effect_size, direction, units, se, ci_lower, ci_upper, ci_text = \
            process_effect_size(rd.or_or_beta, rd.ci_text)

        assoc_vars, assoc_type = variants.get_effect_allele(
            rd.strongest_snp_risk_allele
        )

        a = o.Association(
            raw_association_pk=rd.raw_association_pk,
            study_pk=study_pk,
            effect_type=effect_type,
            effect_size=effect_size,
            effect_units=units,
            standard_error=se,
            pvalue=utils.parse_float(rd.p_value),
            mlog_pvalue=utils.parse_float(rd.pvalue_mlog),
            nvars=len(assoc_vars),
            ci_lower=ci_lower,
            ci_upper=ci_upper,
            effect_direction=direction,
            assoc_type=assoc_type,
            validated=False
        )

        session.add(a)
        nadded += 1
        nassocs += 1
        if nadded == commit_every:
            session.commit()
    if nadded > 0:
        session.commit()

    return nassocs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def effect_type_guess(discovery_pop, replication_pop, ci_text):
    """Have a guess at the effect type based on data in the population fields
    and in the confidence interval test field.

    Parameters
    ----------
    discovery_pop : `str`
        The discovery population field.
    replication_pop : `str`
        The replication population field.
    ci_text : `str`
        The confidence interval text field.

    Returns
    -------
    effect_type : `str`
        An effect type guess, either, ``or``, ``log_or``, ``beta`` or
        ``unknown``.
    """

    # Count the evidence for an odds ratio and a beta estimate
    or_evidence = 0
    beta_evidence = 0

    # search the population fields for case/control designation
    for i in [discovery_pop, replication_pop]:
        # print(i)
        try:
            if re.search(r'\bcases?\b', i, re.IGNORECASE) and \
               re.search(r'\bcontrols?\b', i, re.IGNORECASE):
                or_evidence += 1
        except TypeError:
            # Could be None
            pass

    # Search the effect description for an indication that the effect estimate
    # is signed
    try:
        if re.search(r'increase|decrease', ci_text, re.IGNORECASE):
            beta_evidence += 1
    except TypeError:
        # could be none
        pass

    # Now evaluate the evidence and make a decision on the effect type
    # guess. If we have evidence for both then we are assuming a log_or
    if or_evidence > 0 and beta_evidence > 0:
        return con.LOG_OR_EFFECT_TYPE
    elif or_evidence > 0:
        return con.OR_EFFECT_TYPE
    elif beta_evidence > 0:
        return con.BETA_EFFECT_TYPE
    else:
        return con.UNKNOWN_EFFECT_TYPE


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_effect_size(effect_size, ci_text):
    """Post processing on the effect size columns.

    Parameters
    ----------
    effect_size :`str`
        The unsigned effect size (or_or_beta column) from the GWAS catalog
    ci_text :`str`
        The ci_text column from the GWAS catalog, this is used to determine the
        effect direction

    Returns
    -------
    effect_size : `float`
        The processed effect size. This may be signed.
    direction : `int`
        The direction either 1 or -1
    units : `str`
        The effect size units.
    se : `float`
        The standard error.
    ci_lower : `float`
        The confidence interval lower bound.
    ci_upper : `float`
        The confidence interval upper bound.
    ci_text : `str`
        The confidence interval description.

    Notes
    -----
    This updates beta values to be negative if that are decreasing in value.
    This also processes the 95% confidence interval into its upper/lower bounds
    and creates a direction of effect variable (for betas) which will either be
    1 or -1. The units for the beta will also be captured if they are
    available.
    """
    # Precompile as these will be used in a match and a sub
    ci_reg = re.compile(r'^\s*\[?(\d+(?:\.\d+))\s*-\s*(\d+(?:\.\d+))\]?')
    unit_reg = re.compile(r'(.*?)(increase|decrease)')

    # Attempt to match the CIs
    try:
        ci_match = ci_reg.match(ci_text)
    except TypeError:
        # ci_text is None
        try:
            effect_size = float(effect_size)
        except TypeError:
            pass

        return effect_size, None, None, None, None, None, None

    try:
        # This will fail with an attribute error if not present
        ci_lower = float(ci_match.group(1))
        ci_upper = float(ci_match.group(2))

        # Remove the CIs to make the following REGEXs easier
        ci_text = ci_reg.sub('', ci_text)

        # Calculate the standard error from the CIs
        se = (ci_upper - ci_lower) / 3.92
    except AttributeError:
        # If we fail then everything is None
        ci_lower = None
        ci_upper = None
        se = None

    # Now attempt to match the units and direction
    unit_match = unit_reg.search(ci_text)
    try:
        # The units may not be present so will end up being '' if not
        # (and direction is). If neither are present then we get an
        # AttributeError
        units = unit_match.group(1).strip()
        direction = unit_match.group(2)

        # Remove the units, anything that is left is flags
        ci_text = unit_reg.sub('', ci_text)

        # If units is empty make them None
        if units == '':
            units = None

        # Sign the effect size in the case of decreasing effect direction
        if direction == 'decrease' and effect_size is not None:
            effect_size = "-{0}".format(effect_size)
            direction = -1
        elif direction == 'increase':
            direction = 1
    except AttributeError:
        units = None
        direction = None

    try:
        # Make sure the effect size is a float
        # TODO: Catch AttributeError or TypeError
        effect_size = float(effect_size)
    except TypeError:
        # Effect size is None
        pass

    # Finally clean any flags and make None if there are not any
    ci_text = re.sub(r'[\(\)]', '', ci_text.strip())
    if ci_text == '':
        ci_text = None
    return effect_size, direction, units, se, ci_lower, ci_upper, ci_text
