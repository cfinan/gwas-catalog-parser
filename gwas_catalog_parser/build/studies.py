"""Functions for importing studies.
"""
from gwas_catalog_parser import (
    orm as o
)
from tqdm import tqdm


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_studies(session, verbose=False, commit_every=10000):
    """Using the ``raw_study`` and ``pubmed`` tables build a study table of
    newly added studies.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.

    Returns
    -------
    nstudies : `int`
        The number of studies that were added.

    Notes
    -----
    A study in the GWAS catalog has an accession number. A single pubmed ID
    might have multiple accessions for the different (sub)-studies that it
    undertakes. I would usually refer to these as analyses but in GWAS catalog
    parlance they are studies. In turn a study may have many associations.

    This will only import studies that are linked to pubmed IDs. In addition,
    the studies used in this package are the v.1.0.3 format, so they outnumber
    the associations (v1.0.2 format).

    Also, bad pubmed ID studies will not be imported (or ones that return no
    pubmed results). However, if the GWAS catalog corrects them they should
    eventually be included.

    This will only add new studies (accessions), it can't update any existing
    information. This may lead to a situation where a study content has been
    updated but the updated content will not be reflected in the database. This
    will need to be addressed.
    """
    nstudies = 0
    nadded = 0

    # Get the existing accessions
    existing_studies = set(
        [i[0] for i in session.query(o.Study.gwas_catalog_accession)]
    )
    existing_pmids = set(
        [i[0] for i in session.query(o.Pubmed.pubmed_id)]
    )

    # Get all the study info, we check if there are any missing during the
    # loop
    # TODO: Update to a left join
    query = session.query(
        o.RawStudy.raw_study_pk,
        o.RawStudy.pubmed_id,
        o.RawStudy.date_added_to_catalog,
        o.RawStudy.gwas_catalog_accession,
        o.RawStudy.association_count
    )

    tqdm_kwargs = dict(
        unit=" studies",
        desc="[info] testing/importing studies",
        disable=not verbose,
        leave=False,
        total=query.count()
    )

    for i in tqdm(query, **tqdm_kwargs):
        if i.gwas_catalog_accession not in existing_studies \
           and i.pubmed_id in existing_pmids:
            session.add(o.Study(
                raw_study_pk=i.raw_study_pk,
                pubmed_id=i.pubmed_id,
                date_added_to_catalog=i.date_added_to_catalog,
                gwas_catalog_accession=i.gwas_catalog_accession,
                association_count=i.association_count
            ))
            nadded += 1
            nstudies += 1
        if nadded == commit_every:
            session.commit()
            nadded = 0
    if nadded > 0:
        session.commit()
    return nstudies
