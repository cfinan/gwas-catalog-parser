"""Build the GWAS catalog data into a relational database.
"""
# Importing the version number (in __init__.py) and the package name
from gwas_catalog_parser import (
    __version__,
    __name__ as pkg_name,
    log,
    utils,
    query as q,
    constants as con,
    config,
    orm as o
)
from gwas_catalog_parser.build import (
    raw,
    associations,
    variants,
    download,
    pubmed,
    genes,
    studies,
    phenotypes,
    umls
)
from tqdm import tqdm
from datetime import datetime, date
from sqlalchemy.orm.exc import NoResultFound
import tempfile
import unicodedata
import argparse
import sys
import os
import csv
import hashlib
import gzip
import re
import shutil
import pprint as pp


_SCRIPT_NAME = "gwas-catalog-build"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``gwas_catalog_parser.build.build_gwas_catalog``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)
    args.cache_dir = os.path.expanduser(args.cache_dir)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    config_file = config.GwasCatalogConfig(
        args.config, args.assembly, cache_path=args.cache_dir,
        gwas_cat_db_section=args.gwas_catalog_section
    )
    logger.info(
        f"variant cache database path: {config_file.variant_cache_db_path()}"
    )
    try:
        build_gwas_catalog(config_file, tmpdir=args.tmpdir,
                           verbose=args.verbose,
                           map_variants=not args.no_map_variants)
        log.log_end(logger)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        config_file.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    # We have the opportunity to specify alternative configuration files
    parser.add_argument(
        '-v', '--verbose', action='count',
        help="Indicate progress, use -vv for progress monitoring"
    )
    parser.add_argument(
        '-N', '--no-map-variants', action='store_true',
        help="Skip variant mapping as this can take a while but it"
        " useful to get variant alleles"
    )
    parser.add_argument(
        '-c', '--config', type=str, default=con.GWAS_CATALOG_CONFIG,
        help="An optional alternative location for the gwas catalog config"
        " file"
    )
    parser.add_argument(
        '--gwas-catalog-section', type=str,
        default=config.GwasCatalogConfig.CONFIG_GWAS_CAT_SECTION,
        help="The section name for the GWAS catalog database connection "
        "parameters"
    )
    parser.add_argument(
        '--cache-dir', type=str, default="~/{0}".format(
            config.GwasCatalogConfig.DEFAULT_CACHE_PATH
        ),
        help="An alternate cache directory location."
    )
    parser.add_argument(
        '-T', '--tmpdir', type=str,
        help="An alternate temp file location. The default is the system "
        "temp location"
    )
    parser.add_argument(
        '-a', '--assembly', type=str,
        choices=config.GwasCatalogConfig.ALLOWED_ASSEMBLIES,
        default=config.GwasCatalogConfig.B38_ASSEMBLY,
        help="The human genome assembly version you want to build against"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_gwas_catalog(config_file, tmpdir=None, verbose=False,
                       commit_every=10000, map_variants=True):
    """The API entry point for building the GWAS catalogue.

    Parameters
    ----------
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object.
    tmpdir : `str`, optional, default: `NoneType`
        A directory to store tmp files, if not provided will default to the
        system temp location.
    verbose : `bool` or `int`, optional, default: `False`
        Report status, if > 1 then also report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of entities that are added to a ``sqlalchemy.Session``
        before the ``sqlalchemy.Session.commit`` is called.
    """
    # First we download the data file.
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    # Determine if we want to display download/import progress. This only
    # happens when verbose is > 1
    dl_verbose = False
    if int(verbose) > 1:
        dl_verbose = True

    # Get the session maker for the GWAS catalogue database
    gwas_cat_sm = config_file.get_gwas_cat_session_maker(must_exist=False)

    # Make sure all the tables exist in the database
    session = gwas_cat_sm()
    o.create_all_tables(session)

    # Will hold the UMLS session if the section is defined in the config file.
    umls_session = None

    # Get some basic stats on record counts
    total, nloaded, nunloaded = count_raw_data(session)
    logger.info(
        f"There are {total} raw association ({nloaded} "
        f"loaded/{nunloaded} unloaded)"
    )

    # Make sure the genome assembly option is set and/or valid
    set_assembly_value(session, config_file.assembly)

    # Create a single working directory to place the download files
    working_dir = tempfile.mkdtemp(dir=tmpdir)
    try:
        # Get the abbreviations
        abrv_count = 0
        logger.info("downloading abbreviations")
        tmpfile = utils.get_tmp_file(dir=working_dir)
        abrv_count = raw.import_abbreviations(session, tmpfile)
        logger.info(f"added {abrv_count} abbreviations")

        # Download and update study data
        _raw_downloader(session, raw.RawStudyImporter, working_dir,
                        'study', verbose=verbose, commit_every=commit_every)

        # Download and update association data
        _raw_downloader(session, raw.RawAssocImporter, working_dir,
                        'association', verbose=verbose,
                        commit_every=commit_every)

        # Download and update ancestry data
        _raw_downloader(session, raw.RawAncestryImporter, working_dir,
                        'ancestry', verbose=verbose,
                        commit_every=commit_every)

        # Pubmed
        npubmed, nbad_pubmed = 0, 0
        logger.info("Importing pubmed data - this may take some time...")
        npubmed, nbad_pubmed = pubmed.import_pubmed(
            session, config_file, verbose=dl_verbose, commit_every=commit_every
        )
        logger.info(
            f"There were {npubmed} pubmed entries added ({nbad_pubmed} "
            "bad IDs)"
        )

        # Import studies from raw_study
        nstudies = 0
        logger.info("Importing study data...")
        nstudies = studies.import_studies(
            session, verbose=dl_verbose, commit_every=commit_every
        )
        logger.info(
            f"There were {nstudies} studies entries added"
        )

        # Import phenotypes from raw_study and link with study table
        nphenos = 0
        logger.info("Importing phenotype data...")
        nphenos = phenotypes.import_phenotypes(
            session, verbose=dl_verbose, commit_every=commit_every
        )
        logger.info(
            f"There were {nphenos} phenotypes entries added"
        )

        # Import associations from raw_associations
        assoc_count = 0
        logger.info("inserting associations...")
        assoc_count = associations.import_associations(
            session, verbose=dl_verbose
        )
        logger.info(f"added {assoc_count} association rows")

        nmapped_vars, nunmapped_vars = 0, 0
        logger.info("inserting variants...")
        nmapped_vars, nunmapped_vars = variants.import_variants(
            session, config_file, verbose=dl_verbose,
            commit_every=commit_every, map_variants=map_variants
        )
        logger.info(
            f"added {nmapped_vars + nunmapped_vars} variants "
            f"({nmapped_vars} mapped, {nunmapped_vars} unmapped)"
        )

        # Setup genes
        nensembl_genes, norphan_genes, nsynonyms = 0, 0, 0
        logger.info("Importing genes from Ensembl...")
        tmpfile = utils.get_tmp_file(dir=working_dir)
        nensembl_genes = genes.import_gene_lookup(
            session, tmpfile, config_file.assembly, verbose=dl_verbose
        )

        logger.info("Importing orphan genes...")
        norphan_genes = genes.import_orphan_genes(session, verbose=dl_verbose)

        logger.info("Importing gene synonyms from HGNC...")
        tmpfile = utils.get_tmp_file(dir=working_dir)
        nsynonyms, norphans_syns = genes.import_hgnc_synonyms(
            session, tmpfile, verbose=dl_verbose
        )
        norphan_genes += norphans_syns

        logger.info(f"There were {nsynonyms} gene synonyms added")
        logger.info(
            f"There were {nensembl_genes + norphan_genes} genes added "
            f"({nensembl_genes} ensembl genes/{norphan_genes} orphans)"
        )

        logger.info("Importing associated genes...")
        nadded, nmissed = genes.import_associated_genes(
            session, verbose=dl_verbose, commit_every=commit_every
        )
        logger.info(
            f"There were {nadded} associated genes added ({nmissed} missed)"
        )
        total, nexact, nmetamap, nsubstring, nmissed = _umls_mapper(
            session, config_file, verbose=verbose, commit_every=50
        )
        for val, msg in [(nexact, "exact"), (nmetamap, "metamap"),
                         (nsubstring, "substring"), (nmissed, "missed")]:
            try:
                perc = round(val/total, 2)
            except ZeroDivisionError:
                perc = 0

            logger.info(f"There were {val} {msg} mappings ({perc}%)")
    finally:
        session.close()
        shutil.rmtree(working_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _umls_mapper(session, config_file, verbose=False, commit_every=50):
    """A helper function for UMLS mapping of phenotype terms.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for interacting with the database.
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object.
    verbose : `bool` or `int`, optional, default: `False`
        Report status, if > 1 then also report progress monitors.
    commit_every : `int`, optional, default: `50`
        The number of entities that are added to a ``sqlalchemy.Session``
        before the ``sqlalchemy.Session.commit`` is called.
    """
    total, nexact, nmetamap, nsubstring, nmissed = 0, 0, 0, 0, 0
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    dl_verbose = _dl_verbose(verbose)

    try:
        umls_sm = config_file.get_umls_session_maker()
        umls_session = umls_sm()
    except (FileNotFoundError, KeyError):
        # no umls version
        logger.warn("UMLS not available, skipping phenotype mapping...")
        return total, nexact, nmetamap, nsubstring, nmissed

    # Disable metamap server for the time being.
    metamap_server = None
    # try:
    #     metamap_server = config_file.get_metamap_server()
    # except KeyError:
    #     # no Metamap
    #     logger.warn("Metamap not available, will use exact match mapping...")
    #     metamap_server = None

    try:
        logger.info("Importing UMLS mappings...")
        return umls.import_umls_mappings(
            session, umls_session, metamap_server=metamap_server,
            verbose=dl_verbose, commit_every=commit_every
        )
    finally:
        umls_session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _raw_downloader(session, importer_class, working_dir, dl_type, verbose=False,
                    **kwargs):
    """A helper function for downloading and importing the raw table files from
    the GWAS catalog.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object for interacting with the database.
    importer_class : `class` inheriting from `gwas_catalog_parser.build.raw._RawImporter`
        The importer class.
    working_dir : `str`
        A directory to create temp files.
    dl_type : `str`
        The download type, this is used in verbose messages.
    verbose : `int` or `bool`, optional, default: `False`
        Should progress be reported, if > 1, then download progress will be
        reported.
    **kwargs
        Any other keyword arguments passed onto the ``importer_class``.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    dl_verbose = _dl_verbose(verbose)
    add_count, update_count = 0, 0
    tmpfile = utils.get_tmp_file(dir=working_dir)
    logger.info(f"downloading/updating the GWAS catalog {dl_type} data")

    # Import the downloaded rows into the raw_download table. Only missing rows
    # will be added
    importer = importer_class(session, tmpfile, verbose=dl_verbose, **kwargs)
    add_count, update_count = importer.import_data()
    logger.info(f"added {add_count} rows")

    if update_count > 0:
        logger.warning(f"rows added to existing studies: {update_count}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _dl_verbose(verbose):
    """Helper function to determine if the verbose count is high enough for
    verbose progress bars (must be > 1)

    Parameters
    ----------
    verbose : `int` or `bool`
        The verbosity.

    Returns
    -------
    progress_verbose : `bool`
        ``True`` if ``verbose`` > 1 ``False`` otherwise.
    """
    # Determine if we want to display download/import progress. This only
    # happens when verbose is > 1
    dl_verbose = False
    if int(verbose) > 1:
        dl_verbose = True
    return dl_verbose


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_assembly_value(session, assembly):
    """Make sure the assembly is set in the database and if so make sure the
    import assembly matches the database assembly.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    assembly : `str`
        The assembly build that the imported data should be set to, either
        GRCh38 or GRCh37.

    Raises
    ------
    ValueError
        If the import assembly does not match the database assembly.
    """
    assembly_value = q.get_genome_assembly(session)

    if assembly_value is None:
        a = o.Option(option_type=con.ASSEMBLY_OPT, option_value=assembly)
        session.add(a)
        session.commit()
        assembly_value = q.get_genome_assembly(session)

    if assembly_value != assembly:
        raise ValueError(
            "database assembly != import assembly:"
            f" {assembly_value} vs. {assembly}"
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def count_raw_data(session):
    """Count the total, loaded and unloaded rows in the ``raw_download`` table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    total : `int`
        The total number of rows in the ``raw_download`` table.
    nloaded : `int`
        The total number of loaded rows in the ``raw_download`` table.
    nunloaded : `int`
        The total number of unloaded rows in the ``raw_download`` table.
    """
    total = q.get_raw_assoc_count(session, loaded=None)
    nloaded = q.get_raw_assoc_count(session, loaded=True)
    nunloaded = total - nloaded
    return total, nloaded, nunloaded


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
