"""Functions of quering pubmed data and importing into the GWAS catalog
 database.
"""
from gwas_catalog_parser import (
    constants as con,
    orm as o
)
from tqdm import tqdm
from paper_scraper import pubmed
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_pubmed(session, config_file, verbose=False, commit_every=10000):
    """Import pubmed Ids into the pubmed lookup table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.

    Returns
    -------
    npubmed : `int`
        The number of pubmed IDs that were added.
    nbad_pubmed
        The number of pubmed IDs that produce no data when searched against
        pubmed.

    Notes
    -----
    This queries source pubmed IDs from the
    ``gwas_catalog_parser.orm.RawAssociation``,
    ``gwas_catalog_parser.orm.RawStudy`` and
    ``gwas_catalog_parser.orm.RawAncestry`` tables. Also, please note, this
    does not refresh any of the pubmed data.

    The pubmed downloaded will cache what it has downloaded, this is handy as
    pubmed has a habit of flaking out.
    """
    if config_file.get_email() is None:
        raise ValueError("no email address to query pubmed")

    nadded = 0
    npubmed = 0
    bad_pubmed = 0

    # Get all the available pubmed IDs from the GWAS catalog raw tables.
    tables = [
        o.RawStudy, o.RawAssociation, o.RawAncestry
    ]
    dl_pmids = set([])
    for i in tables:
        pubq = session.query(i.pubmed_id)
        dl_pmids.update([int(i.pubmed_id) for i in pubq])

    # Now get the pubmed IDs already in the pubmed table
    got_pmids = set([i.pubmed_id for i in session.query(o.Pubmed.pubmed_id)])

    # Get the pubmed IDs that are not represented
    to_get = dl_pmids.difference(got_pmids)

    if len(to_get) > 0:
        to_get = [str(i) for i in to_get]

        try:
            pubdl = pubmed.PubmedQuery(config_file.get_email())
            search_summary, medline_data, error_pmids = pubdl.search_pmids(
                to_get, verbose=verbose, elink=True
            )
        except IndexError as e:
            # So the pubmed IDs produce no search data here
            if e.args[0] == 'no PMIDs found':
                bad_pubmed = len(to_get)
                return npubmed, bad_pubmed

        tqdm_kwargs = dict(
            unit=" pubmed IDs",
            desc="[info] importing pubmed IDs",
            disable=not verbose,
            total=len(to_get),
            leave=False
        )
        for c in tqdm(medline_data, **tqdm_kwargs):
            session.add(get_pubmed_entry(c))
            nadded += 1
            npubmed += 1
            if nadded == commit_every:
                session.commit()
        if nadded > 0:
            session.commit()
    return npubmed, bad_pubmed


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_pubmed_entry(citation):
    """Create a database record for the citation record.

    Parameters
    ----------
    citation : `paper_scraper.pubmed.MedlineRecord`
        The medline citation.

    Returns
    -------
    entry : `gwas_catalog_parser.orm.Pubmed`
        A database pubmed ID row, complete with dependent MeSH term rows
        added.
    """
    outrow = citation.get_main_fields()
    try:
        mesh_terms = outrow.pop('mesh_terms')
    except KeyError:
        mesh_terms = []
    p = o.Pubmed(nmesh_terms=len(mesh_terms), **outrow)
    for term, modifiers, major in mesh_terms:
        if len(modifiers) > 0:
            modifiers = con.MESH_JOIN.join(modifiers)
        else:
            modifiers = None
        o.Mesh(
            mesh_term=term, modifiers=modifiers, major_term=major,
            pubmed=p
        )
    return p
