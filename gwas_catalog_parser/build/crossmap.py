"""Wrapper functions for running crossmap functions. Crossmap does not really
have an API.
"""
from cmmodule.utils import read_chain_file, map_coordinates
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_chain(chain_file):
    """A wrapping function to provide a constant interface to the crossmap
    `cmmodule.utils.read_chain_file` function.

    Parameters
    ----------
    chain_file : `str`
        The path to the chain file.

    Returns
    -------
    map_tree : `dict`
        The keys of the dict are chromosome names and the values are
        `bx.Interval` objects used to look for overlaps between the current
        genome build and the one being mapped to.
    target_chr_sizes : `dict`
        The sizes of the target chromosomes.
    source_chr_sizes : `dict`
        The sizes of the source chromosomes.
    """
    map_tree, target_chr_sizes, source_chr_sizes = read_chain_file(
        chain_file,
        print_table=False
    )
    return map_tree


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_crossmap(map_tree, chr_name, start_pos, end_pos, strand='+'):
    """Provides an interface to the crossmap `cmmodule.utils.map_coordinates`
    function that actually does the liftover.

    Parameters
    ----------
    map_tree : `dict`
        The keys of the dict are chromosome names and the values are
        `bx.Interval` objects used to look for overlaps between the current
        genome build and the one being mapped to
    chr_name : `str`
        The chromosome name of the query segment.
    start_pos : `int`
        The start position of the query segment.
    end_pos : `int`
        The end position of the query segment.
    strand : `str`, optional, default: `+`
        The strand for the query segment, note that the strands are '+' and '-'
        for forward and reverse strand.

    Returns
    -------
    mapping : `list`
        The first element of the list is the liftover chromosome, the second is
        the liftover start position and the third is the liftover end position.
        Note that this implementation only returns values for liftovers that
        do not span gaps (unlike crossmap it's self). If the region can't be
        lifted or spans a gap a NULL liftover is returned. This is '0' for
        chr_name and -1 for start position and end position.
    """
    try:
        mappings = map_coordinates(
            map_tree,
            chr_name,
            start_pos,
            end_pos,
            q_strand=strand,
            print_match=False
        ) or []
    except (TypeError, OverflowError, AttributeError):
        mappings = []

    if len(mappings)/2 == 1:
        return mappings[1]
    else:
        return ['0', -1, -1, '-']
