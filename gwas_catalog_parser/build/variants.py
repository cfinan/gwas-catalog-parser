"""Functions to import variant information into the database.
"""
from gwas_catalog_parser import (
    __version__,
    __name__ as pkg_name,
    log,
    query as q,
    constants as con,
    orm as o,
    config,
    utils
)
from gwas_catalog_parser.build import cache_orm as co
from tqdm import tqdm
from sqlalchemy.orm.exc import NoResultFound
from requests.exceptions import HTTPError
from operator import itemgetter
import unicodedata
import sys
import hashlib
import re
import argparse
import os
# import pprint as pp

_SCRIPT_NAME = "gwas-catalog-cache-variants"
"""The name of the script (`str`)
"""

# Use the module docstring for argparse
_DESC = """Cache the variant data from Ensembl. This can be used if the
Ensembl rest API failed during the main build process. You can just re-run the
main build process as it is designed to not import anything that already
exists. However, if the rest API is flaky it can be frustrating to re-download
the GWAS catalog files each time only to not use them. That is where this
script comes in. It will use the raw_association table to pre-cache all the
required variant data. It can be re-run multiple times until the cache is
complete. At that point you can re-run the main build process again. This
should only be used if you have an issue with the main build process crashing
because of rest errors. If you run before a build process then because there is
no imported data raw_association table then it will not find anything to cache.
"""
"""The program description (`str`)
"""


INTERACTION_ASSOC_REGEX = re.compile(
    r'^.+-[A-Za-z?-]+\s*[xX]\s*.+-[A-Za-z?-]+$'
)
"""A regular expression to recognise interaction variant associations
(re.Pattern)
"""
ASSOC_VAR_SPLIT_DEL_REGEX = re.compile(r'[;,]\s|\s[xX]\s')
"""Regular expression to recognise variant delimiters to split multi-variant
assocs (re.Pattern)
"""
EFFECT_ALLELE_EXTRACT_REGEX = re.compile(
    r'(?P<VAR>.+)-(?P<ALLELE>[A-Za-z?-]+)$'
)
"""Regular expression to recognise the effect allele and variant identifier for
extraction (re.Pattern)
"""
BASIC_DNA_REGEX = re.compile(r'^[ATCG]+$')
"""Basic DNA base pair regex (re.Pattern)
"""
RSID_REGEX = re.compile(r'^[rR][sS]\d+$')
"""A regular expression to recognise genetic variants with rs identifiers.
i.e. rs12345 (re.Pattern)
"""
CHR_POS_REGEX = re.compile(
    r'''
    (?:c(hr?)?:?)?
    (?P<CHR>[XY1-9][0-9]?|MT)
    [:\._]
    (?P<POS>\d+)
    (?:[:_](?P<A1>[ATCG]+)(?:_(?P<A2>[ATCG]+))?)?''',
    re.VERBOSE | re.IGNORECASE
)
"""A regular expression to capture chr:pos, chr:pos_A1, chr:pos:A1_A2,
with/without a leading chr or c on the chromosome. The captures are named
groups ``CHR``, ``POS``, ``A1``, ``A2`` (re.Pattern)
"""
HLA_REGEX = re.compile(
    r'''
    (?:H[LA]{2}[_-])?
    (?P<TYPE>\w+)
    \*?
    (?P<LOW>\d{2})
    :?
    (?P<MED>\d{2})?
    :?
    (?P<HIGH>\d{2})?
    ''',
    re.VERBOSE | re.IGNORECASE
)


_ALL_CHARS = (chr(i) for i in range(sys.maxunicode))
_CATEGORIES = {'Cc'}
CTRL_CHARS = ''.join(
    c for c in _ALL_CHARS if unicodedata.category(c) in _CATEGORIES
)
CTRL_CHARS_REGEX = re.compile('[%s]' % re.escape(CTRL_CHARS))
"""Identify any non-prinable unicode control characters. (`re.Pattern`)
"""
SPECIES = 'homo_sapiens'
"""The default ensembl species (`str`)
"""
ALLELE_SPLIT_DELIMITER = '/'
"""The default ensembl allele delimiter (`str`)
"""
ALLELE_JOIN_DELIMITER = ','
"""The default allele delimiter in the database (`str`)
"""
NO_ENSEMBL_MATCH = '-1'
"""The value for chromosome name if we do not have a match in Ensembl (`str`)
"""

# Hash keys
ENSEMBL_MAPPINGS = 'mappings'
"""The name of the mappings key in a POST query (`str`)
"""
ENSEMBL_VAR_ID_POST = 'name'
"""The variant name key in a variant name POST query (`str`)
"""
ENSEMBL_VAR_ID_GET = 'id'
"""The variant name key in a variant name GET query (`str`)
"""
ENSEMBL_MINOR_ALLELE = 'minor_allele'
"""The minor allele in a POST query (`str`)
"""
ENSEMBL_GLOBAL_MAF = 'MAF'
"""The variant global MAF in a POST query (`str`)
"""
ENSEMBL_CONSEQUENCE_POST = 'most_severe_consequence'
"""The variant consequence in a POST query (`str`)
"""
ENSEMBL_CONSEQUENCE_GET = 'consequence_type'
"""The variant consequence in a GET query (`str`)
"""
ENSMEBL_ALLELES_POST = 'allele_string'
"""The variant alleles in a POST query (`str`)
"""
ENSMEBL_ALLELES_GET = 'alleles'
"""The variant alleles in a GET query (`str`)
"""
ENSEMBL_CHR_NAME = 'seq_region_name'
"""The variant chromosome name in a GET/POST query (`str`)
"""
ENSEMBL_START_POS = 'start'
"""The variant start position in a GET/POST query (`str`)
"""
ENSEMBL_STRAND = 'strand'
"""The variant strand in a GET/POST query (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``gwas_catalog_parser.build.build_gwas_catalog``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)
    args.cache_dir = os.path.expanduser(args.cache_dir)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    config_file = config.GwasCatalogConfig(
        args.config, args.assembly, cache_path=args.cache_dir,
        gwas_cat_db_section=args.gwas_catalog_section
    )
    logger.info(
        f"variant cache database path: {config_file.variant_cache_db_path()}"
    )

    # Determine if we want to display download/import progress. This only
    # happens when verbose is > 1
    dl_verbose = False
    if int(args.verbose) > 1:
        dl_verbose = True

    # Get the session maker for the GWAS catalogue database
    gwas_cat_sm = config_file.get_gwas_cat_session_maker(must_exist=True)

    try:
        ncached = cache_variants(
            gwas_cat_sm(), config_file, commit_every=500,
            verbose=dl_verbose
        )
        logger.info(f'cached {ncached} variants')
        log.log_end(logger)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        config_file.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    # We have the opportunity to specify alternative configuration files
    parser = argparse.ArgumentParser(
        description=_DESC)
    parser.add_argument(
        '-v', '--verbose', action='count',
        help="Indicate progress, use -vv for progress monitoring"
    )
    parser.add_argument(
        '-c', '--config', type=str, default=con.GWAS_CATALOG_CONFIG,
        help="An optional alternative location for the gwas catalog config"
        " file"
    )
    parser.add_argument(
        '--gwas-catalog-section', type=str,
        default=config.GwasCatalogConfig.CONFIG_GWAS_CAT_SECTION,
        help="The section name for the GWAS catalog database connection "
        "parameters"
    )
    parser.add_argument(
        '--cache-dir', type=str, default="~/{0}".format(
            config.GwasCatalogConfig.DEFAULT_CACHE_PATH
        ),
        help="An alternate cache directory location."
    )
    parser.add_argument(
        '-a', '--assembly', type=str,
        choices=config.GwasCatalogConfig.ALLOWED_ASSEMBLIES,
        default=config.GwasCatalogConfig.B38_ASSEMBLY,
        help="The human genome assembly version you want to build against"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_variants(session, config_file, verbose=False, commit_every=1000):
    """Query variant information that is not already present in the database
    and cache ensembl lookups for mapping later.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object. Required to provide liftover info if
        build is GRCh37.
    verbose : `bool` or `int`, optional, default: `False`
        Report status.
    commit_every : `int`, optional, default: `1000`
        The number of entities that are added to a ``sqlalchemy.Session``
        before the ``sqlalchemy.Session.commit`` is called.

    Returns
    -------
    ncache : `int`
        The number of variants that have been cached.
    """
    # This is due to a bad query such as out of chromosome range or end>start
    # these are ignored (essentially a no mapping situation)
    error_msg = '400 Client Error: Bad Request for url'
    # Track the numbers added to a session and total cached
    nadded = 0
    ncached = 0

    # The max post size for an Ensembl variant name query
    post_size = 200

    # connect to the cache database
    sm = config_file.get_variant_cache_session_maker()
    cache_session = sm()
    # print(cache_session)
    # print(cache_session.bind)
    co.create_all_tables(cache_session)

    # initialise the ensembl rest client the genome assembly of the rest client
    # is sorted out by the config file
    rc = config_file.get_rest_client(cache=None)
    # print(rc.url)
    # sys.exit()
    # Get all the variant hashes in the cache so we can skip variants that have
    # already been processed. This should be ok for in memory as the numbers of
    # variants are ~200k
    hash_lookup = set(
        [i.variant_hash for i in cache_session.query(co.Variant.variant_hash)]
    )
    # A progress description for the variant generator
    desc = "[info] caching variants"

    # This holds rsIDs for the POST query of variant name
    rsid_buffer = []

    try:
        # Loop through all the variants we want to add
        for row in _variant_generator(session, config_file, desc=desc,
                                      verbose=verbose):
            assoc_pk, effect_allele, effect_allele_freq, var_hash, var_id, \
                chr_name, start_pos, end_pos, ref_allele, alt_allele, \
                var_type = row
            # Skip variants already added to the cache
            if var_hash in hash_lookup:
                # print("FOUND")
                continue
            # print("QUERYING")
            # If we have an rsID then add to the buffer for a variant name
            # query
            if var_id is not None and RSID_REGEX.match(var_id):
                rsid_buffer.append((var_id, var_hash))
            else:
                # No rsID check if we have enough info for a positional overlap
                # query
                if chr_name is not None and start_pos is not None:
                    try:
                        # Query against Ensembl
                        qv = rc.get_overlap_region(
                            'variation',
                            f"{chr_name}:{start_pos}-{end_pos}",
                            SPECIES
                        )
                        # print(qv)
                        # > 1 variant may overlap
                        for i in qv:
                            cache_session.add(co.Variant(
                                chr_name=i[ENSEMBL_CHR_NAME],
                                start_pos=i[ENSEMBL_START_POS],
                                ref_allele=i[ENSMEBL_ALLELES_GET][0],
                                alt_alleles=ALLELE_JOIN_DELIMITER.join(
                                    i[ENSMEBL_ALLELES_GET][1:]
                                ),
                                var_id=i[ENSEMBL_VAR_ID_GET],
                                worst_consequence=i[ENSEMBL_CONSEQUENCE_GET],
                                strand=i[ENSEMBL_STRAND],
                                variant_hash=var_hash
                            ))
                        if len(qv) == 0:
                            # No overlap variants then we add an empty
                            # record so we do not re-query upon failure
                            cache_session.add(co.Variant(
                                chr_name=NO_ENSEMBL_MATCH,
                                variant_hash=var_hash
                            ))
                            hash_lookup.add(var_hash)
                            nadded += 1
                            ncached += 1
                    except HTTPError as e:
                        # This could be a bad URL or lost connection, if a bad
                        # URL then we skip otherwise we raise
                        if e.args[0].startswith(error_msg):
                            cache_session.add(co.Variant(
                                chr_name=NO_ENSEMBL_MATCH,
                                variant_hash=var_hash
                            ))
                            hash_lookup.add(var_hash)
                            continue
                        else:
                            raise

            # If the buffer is full then we query and add the results
            if len(rsid_buffer) == post_size:
                added = _process_post(rc, cache_session, hash_lookup,
                                      rsid_buffer)
                nadded += added
                ncached += added
                rsid_buffer = []

            # If we have added enough data to the cache then we commit, as if
            # we fail we do not want to lose too many variants and have to
            # re-query
            if nadded >= commit_every:
                cache_session.commit()
                nadded = 0
    except Exception:
        cache_session.close()
        raise

    # Final lookup
    if len(rsid_buffer) > 0:
        added = _process_post(rc, cache_session, hash_lookup, rsid_buffer)
        nadded += added
        ncached += added
        rsid_buffer = []
        cache_session.commit()

    cache_session.close()
    return ncached


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_post(rc, session, hash_lookup, rsid_buffer):
    """Issue a post query for variant names and process the results into the
    cache.

    Parameters
    ----------
    rc : `ensembl_rest_client.rest.Client`
        The ensembl rest client for the appropriate genome assembly.
    session : `sqlalchemy.Session`
        The session object for the cache database.
    hash_lookup : `set` of `str`
        The previously added variant 32 character MD5 hashes.
    rsid_buffer : `list` of `tuple`
        The buffer if query rsIDs and their respective variant hashes.

    Returns
    -------
    nadded : `int`
        The number of variants added to the cache.
    """
    nadded = 0

    # Issue the rest query
    qv = rc.post_variation(
        SPECIES, [vid for vid, h in rsid_buffer]
    )

    # Go through the results, the REST API is nice enough to return a
    # dict where the keys are the query IDs and the values are the matching
    # data. This deals with the issue of rsId synonyms.
    for vid, h in rsid_buffer:
        try:
            variants = qv[vid]
        except KeyError:
            # Not returned, presumably no match
            session.add(co.Variant(chr_name=NO_ENSEMBL_MATCH, variant_hash=h))
            hash_lookup.add(h)
            nadded += 1
            continue

        # The returned variant may have multiple mappings (positions), i.e. in
        # some cases it can't be localised to a single area of the genome.
        # However, some fields are common to all mappings.
        var_id = variants[ENSEMBL_VAR_ID_POST]
        minor_allele = variants[ENSEMBL_MINOR_ALLELE]
        global_maf = variants[ENSEMBL_GLOBAL_MAF]
        consequence = variants[ENSEMBL_CONSEQUENCE_POST]
        for m in variants[ENSEMBL_MAPPINGS]:
            alleles = m[ENSMEBL_ALLELES_POST].split('/')
            ref_allele = alleles[0]
            alt_alleles = ','.join(alleles[1:])
            session.add(co.Variant(
                chr_name=m[ENSEMBL_CHR_NAME],
                start_pos=m[ENSEMBL_START_POS],
                ref_allele=ref_allele,
                alt_alleles=alt_alleles,
                var_id=var_id,
                worst_consequence=consequence,
                strand=m[ENSEMBL_STRAND],
                variant_hash=h,
                minor_allele=minor_allele,
                global_maf=global_maf
            ))
            nadded += 1
        hash_lookup.add(h)
    return nadded


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _variant_generator(session, config_file, verbose=False,
                       desc="[info] getting variants"):
    """Get variants associations that are not represented in the associations
    table, perform some initial processing on them and then yield them.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object. Required to provide liftover info if
        build is GRCh37.
    verbose : `bool` or `int`, optional, default: `False`
        Report status.
    desc : `str`
        A progress description to use when ``verbose`` is ``True``.

    Yields
    ------
    association_pk : `int`
        The association primary key for the variant data.
    effect_allele : `str`
        The association effect allele (if available).
    effect_allele_freq : `float` or `NoneType`
        The association effect allele frequency (if available).
    var_hash : `str`
        A 32 character string of representing the processed variant data.
    var_id : `str` or `NoneType`
        The processed variant identifier.
    chr_name : `str` or `NoneType`
        The processed chromosome name.
    start_pos : `int` or `NoneType`
        The processed start position.
    end_pos : `int` or `NoneType`
        The processed end position.
    ref_allele : `str` or `NoneType`
        The processed reference allele (if available).
    alt_allele : `str` or `NoneType`
        The processed alternate allele (if available).
    var_type : `str`
        The variant type `simple`, `hla`
    """
    assocq = session.query(
        o.Association.association_pk, o.RawAssociation.risk_allele_frequency,
        o.RawAssociation.chr_id, o.RawAssociation.chr_pos,
        o.RawAssociation.strongest_snp_risk_allele).\
        join(o.RawAssociation).\
        join(o.AssociatedVariant, isouter=True).\
        filter(o.RawAssociation.loaded == False).\
        filter(o.AssociatedVariant.association_pk == None)
    nassocs = assocq.count()

    # If the target assembly is b37 then we will lift down otherwise this will
    # pass through
    liftover_func = config_file.get_liftover_func()

    tqdm_kwargs = dict(
        unit=" variants",
        desc=desc,
        disable=not verbose,
        total=nassocs,
        leave=False
    )
    for rd in tqdm(assocq, **tqdm_kwargs):
        chr_name, start_pos, var_allele = [
            i if i is None else i.strip() for i in [
                rd.chr_id, rd.chr_pos, rd.strongest_snp_risk_allele
            ]
        ]

        try:
            start_pos = int(start_pos)
        except (ValueError, TypeError):
            start_pos = None

        variants, assoc_type = get_effect_allele(var_allele)
        for var_id, effect_allele in variants:
            new_var_id, extract_chr_name, extract_start_pos, ref_allele, \
                alt_allele, var_type = process_var_id(var_id)

            max_len = 1
            for i in [effect_allele, ref_allele, alt_allele]:
                if i is not None:
                    max_len = max(max_len, len(i))

            var_chr_name = chr_name or extract_chr_name
            var_start_pos = start_pos or extract_start_pos
            var_end_pos = (var_start_pos or 0) + max_len - 1

            var_chr_name, var_start_pos, var_end_pos = liftover_func(
                var_chr_name, var_start_pos, var_end_pos
            )

            var_hash = hash_variant(var_chr_name, var_start_pos, ref_allele,
                                    alt_allele, new_var_id)

            try:
                # Cast the effect allele freq into a float
                eaf = float(rd.risk_allele_frequency)
            except (ValueError, TypeError):
                # For stuff like this '0·341' and NoneType
                # TODO: sort out unicode decimals
                eaf = None

            yield (
                rd.association_pk,
                effect_allele,
                eaf,
                var_hash,
                new_var_id,
                var_chr_name,
                var_start_pos,
                var_end_pos,
                ref_allele,
                alt_allele,
                var_type
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_effect_allele(var_allele):
    """Extract the variant ID and effect allele from the combined field.

    Parameters
    ----------
    var_allele : `str`
        The variant ID and effect allele. i.e ``rs1234-T``. This could
        potentially be a delimited string with a ``;`` or ``,`` or ``x``.

    Returns
    -------
    parsed_vars : `list` of `tuple`
        The variants and effect alleles extracted from the string with the
        variant ID at ``[0]`` and effect allele at ``[1]``, any of these
        can be ``NoneType``.
    assoc_type : `str`
        The association type, either ``A``, ``I`` or ``H`` for additive,
        interaction or haplotype.
    """
    assoc_type = con.ADDITIVE_ASSOC
    if INTERACTION_ASSOC_REGEX.match(var_allele):
        assoc_type = con.INTERATION_ASSOC

    parsed_vars = []
    for v in ASSOC_VAR_SPLIT_DEL_REGEX.split(var_allele):
        variant = None
        allele = None
        m = EFFECT_ALLELE_EXTRACT_REGEX.match(v.strip())

        try:
            variant = m.group('VAR')
            allele = m.group('ALLELE').upper()
        except AttributeError:
            if var_allele is not None:
                variant = var_allele
            parsed_vars.append((variant, allele))
            continue

        if not BASIC_DNA_REGEX.match(allele):
            allele = None
        parsed_vars.append((variant, allele))

    if assoc_type == con.ADDITIVE_ASSOC and len(parsed_vars) > 1:
        assoc_type = con.HAPLOTYPE_ASSOC
    return parsed_vars, assoc_type


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def hash_variant(chr_name, start_pos, ref_allele, alt_allele, var_id):
    """Generate a hash of the salient variant information. These are used as
    indexes in the database.

    Parameters
    ----------
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    ref_allele : `str` or `NoneType`
        Any extracted reference alleles or ``NoneType`` if none are present.
    alt_allele : `str` or `NoneType`
        Any extracted alternate alleles or ``NoneType`` if none are present.
    var_id : `str` or `NoneType`
        A (potentially) processed variant identifier.

    Returns
    -------
    variant_hash : `str`
        The 32 character hash of the variant ID.
    """
    return hashlib.md5(
                con.MD5_JOIN.join(
                    [
                        str(chr_name), str(start_pos),
                        str(ref_allele), str(alt_allele),
                        str(var_id)
                    ]
                ).encode()
            ).hexdigest()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_var_id(var_id):
    """Process the variant ID to extract any information from it.

    Parameters
    ----------
    var_id : `str`
        The variant identifier.

    Returns
    -------
    var_id : `str`
        A (potentially) processed variant identifier.
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    ref_allele : `str` or `NoneType`
        Any extracted reference alleles or ``NoneType`` if none are present.
    alt_allele : `str` or `NoneType`
        Any extracted alternate alleles or ``NoneType`` if none are present.
    var_type : `str`
        The variant type, currently one of ``U``, ``S``, ``H`` for unknown,
        simple, HLA.
    """
    var_type = con.UNKNOWN_VARIANT_TYPE

    # Remove any unprintable characters
    var_id = CTRL_CHARS_REGEX.sub('', var_id)

    # remove any white space
    var_id = re.sub(r'\s+', '', var_id)

    if RSID_REGEX.match(var_id):
        var_type = con.SIMPLE_VARIANT_TYPE
        return var_id.lower(), None, None, None, None, var_type

    m = CHR_POS_REGEX.search(var_id)
    if m:
        var_type = con.SIMPLE_VARIANT_TYPE
        return (
            None, m.group('CHR'), int(m.group('POS')),
            m.group('A1'), m.group('A2'), var_type
        )

    m = HLA_REGEX.match(var_id)
    if m:
        var_type = con.HLA_VARIANT_TYPE
        alleles = m.group("LOW")
        for i in ['MED', 'HIGH']:
            try:
                alleles += m.group("HIGH")
            except TypeError:
                pass

        return (
            f"HLA-{m.group('TYPE')}*{alleles}", "6", None, alleles, None,
            var_type
        )

    return var_id, None, None, None, None, var_type


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_variants(session, config_file, map_variants=True, verbose=False,
                    commit_every=10000):
    """Go through unique association coordinates mapping the variant
    information as best as possible.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    config_file : `gwas_catalog_parser.config.GwasCatalogConfig`
        The configuration file object. Required to provide liftover info if
        build is GRCh37.
    map_variants : `bool` or `int`, optional, default: `True`
        Perform variant mapping/validation. This uses the ensembl rest API
        and will be slow, hence it is optional. This is useful if you want the
        variant alleles and to attempt to determine the non-effect allele
        (``other_allele``).
    verbose : `bool` or `int`, optional, default: `False`
        Report status.
    commit_every : `int`, optional, default: `10000`
        The number of entities that are added to a ``sqlalchemy.Session``
        before the ``sqlalchemy.Session.commit`` is called.

    Returns
    -------
    nmapped : `int`
        The number of variants that have been mapped
    nunmapped : `int`
        The number of variants that have NOT been mapped.
    """
    total = 0
    nmapped = 0
    nadded = 0

    desc = "[info] processing variants (no mapping)"
    cache_session = None
    variant_func = _variant_no_map
    if map_variants is True:
        # Start by caching the variants. We fix commit every here so we do not
        # lose too many variants upon failure
        cache_variants(
            session, config_file, commit_every=500, verbose=verbose
        )
        # connect to the cache database
        sm = config_file.get_variant_cache_session_maker()
        desc = "[info] processing variants (mapping)"
        variant_func = _variant_map
        cache_session = sm()

    # Loop through all the variants we want to add
    for row in _variant_generator(session, config_file,
                                  desc=desc, verbose=verbose):
        assoc_pk, effect_allele, effect_allele_freq, var_hash, var_id, \
            chr_name, start_pos, end_pos, ref_allele, alt_allele, \
            var_type = row
        v, map_info, is_valid, other_allele, nsites = variant_func(
            session, cache_session, var_hash, chr_name, start_pos,
            ref_allele, alt_allele, var_id, effect_allele
        )
        av = o.AssociatedVariant(
            association_pk=assoc_pk,
            effect_allele=effect_allele,
            other_allele=other_allele,
            effect_allele_freq=effect_allele_freq,
            map_info=map_info,
            decoded_map_info=con.MESH_JOIN.join(
                utils.decode_map_info(map_info)
            ),
            listed_var_id=var_id,
            is_validated=is_valid,
            variant=v,
        )
        session.add(av)
        total += 1
        nmapped += is_valid
        nadded += 1
        if nadded >= commit_every:
            session.commit()
            nadded = 0
    if nadded > 0:
        session.commit()
    return nmapped, total - nmapped


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _variant_no_map(session, cache_session, var_hash, chr_name, start_pos,
                    ref_allele, alt_allele, var_id, effect_allele):
    """Generate the variant for the association with no mapping info.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the GWAS catalog database.
    cache_session : `sqlalchemy.Session`
        A session object to interact with the variant cache database.
    var_hash : `str`
        A 32 character MD5 string for the parsed variant attributes from the
        GWAS catalog association data.
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    ref_allele : `str` or `NoneType`
        Any reference alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    alt_allele : `str` or `NoneType`
        Any alternate alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    var_id : `str` or `NoneType`
        A processed variant identifier from the GWAS catalogue data.
    effect_allele : `str` or `NoneType`
        Any effect alleles that have been ID'd. These will typically be
        parsed out of a ``strongest_snp_risk_allele``. If not known they will
        be ? in the association download but converted to ``NoneType`` before
        this function call.

    Returns
    -------
    variant : `gwas_catalog_parser.orm.Variant`
        The variant for the association. This will either be a previously added
        one or a newly generated one.
    map_info : `int`
        A bitwise mapping information integer (will be 0 in this case).
    is_valid : `bool`
        Did the variant meet the minimal evidence to be "mapped"
        (will be False in this case).
    other_allele : `str` or `NoneType`
        Any imputation of the other (non-effect) allele. Will be ``NoneType``
    nsites : `int`
        The number of sites that overlap the mapping. This is not that useful
        due to the way the queries happen so may be removed in future. (Will
        be 0 in this case).
    """
    variant = _get_variant(
        session, var_hash, chr_name, start_pos, ref_allele,
        alt_allele, var_id
    )
    return variant, con.NO_MAP.bits, False, None, 0


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _variant_map(session, cache_session, var_hash, chr_name, start_pos,
                 ref_allele, alt_allele, var_id, effect_allele):
    """Map an association variant against the variant cache database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the GWAS catalog database.
    cache_session : `sqlalchemy.Session`
        A session object to interact with the variant cache database.
    var_hash : `str`
        A 32 character MD5 string for the parsed variant attributes from the
        GWAS catalog association data.
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    ref_allele : `str` or `NoneType`
        Any reference alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    alt_allele : `str` or `NoneType`
        Any alternate alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    var_id : `str` or `NoneType`
        A processed variant identifier from the GWAS catalogue data.
    effect_allele : `str` or `NoneType`
        Any effect alleles that have been ID'd. These will typically be
        parsed out of a ``strongest_snp_risk_allele``. If not known they will
        be ? in the association download but converted to ``NoneType`` before
        this function call.

    Returns
    -------
    variant : `gwas_catalog_parser.orm.Variant`
        The variant for the association. This will either be a previously added
        one or a newly generated one.
    map_info : `int`
        A bitwise mapping information integer.
    is_valid : `bool`
        Did the variant meet the minimal evidence to be "mapped".
    other_allele : `str` or `NoneType`
        Any imputation of the other (non-effect) allele.
    nsites : `int`
        The number of sites that overlap the mapping. This is not that useful
        due to the way the queries happen so may be removed in future.
    """
    map_info = con.NO_MAP.bits
    is_valid = False

    # From all the available alleles attempt to define effect/other
    effect_allele, other_allele = _set_alleles(
        ref_allele, alt_allele, effect_allele
    )
    mappings = _get_evidence(
        cache_session, var_hash, chr_name, start_pos, effect_allele,
        other_allele, var_id
    )

    try:
        variant, map_info, is_valid, oa_guess = _eval_evidence(
            session, mappings
        )
        # Impute the other allele if possible
        other_allele = other_allele or oa_guess
    except KeyError:
        variant = _get_variant(
            session, var_hash, chr_name, start_pos, ref_allele,
            alt_allele, var_id
        )

    return variant, map_info, is_valid, other_allele, len(mappings)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_alleles(ref_allele, alt_allele, effect_allele):
    """Attempt to do some validation on the effect allele.

    Parameters
    ----------
    ref_allele : `str` or `NoneType`
        Any reference alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    alt_allele : `str` or `NoneType`
        Any alternate alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    effect_allele : `str` or `NoneType`
        Any effect alleles that have been ID'd. These will typically be
        parsed out of a ``strongest_snp_risk_allele``. If not known they will
        be ? in the association download but converted to ``NoneType`` before
        this function call.

    Returns
    -------
    effect_allele : `str` or `NoneType`
        Any effect alleles that have been ID'd. This is the same value as given.
    other_allele : `str` or `NoneType`
        Any non-effect alleles that have been ID'd. If both ref/alt are not
        ``NoneType``, and the effect allele is found in them, then the
        ``other_allele`` is set to the one the effect_allele did not match.
    """
    other_allele = None
    if effect_allele is not None:
        if effect_allele == ref_allele:
            other_allele = alt_allele
        elif effect_allele == alt_allele:
            other_allele = ref_allele

    return effect_allele, other_allele


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_evidence(cache_session, var_hash, chr_name, start_pos, effect_allele,
                  other_allele, var_id):
    """Generate mapping evidence for the variant against all of it's matches in
    the cache.

    Parameters
    ----------
    cache_session : `sqlalchemy.Session`
        A session object to interact with the variant cache database.
    var_hash : `str`
        A 32 character MD5 string for the parsed variant attributes from the
        GWAS catalog association data.
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    effect_allele : `str` or `NoneType`
        Any effect alleles that have been ID'd.
    other_allele : `str` or `NoneType`
        Any non-effect alleles that have been ID'd.
    var_id : `str` or `NoneType`
        A processed variant identifier from the GWAS catalogue data.

    Returns
    -------
    mappings : `list` of `tuple`
        The mapping evidence. Each tuple has: ``[0]`` The mapping bits
        (`int`), ``[1]`` The cache Variant object
        (`gwas_catalog_parser.build.cache_orm.Variant`), ``[2]`` The
        effect allele index position in the variant alleles (`int`) This
        will be -1 is not present, ``[3]`` The other allele index position
        in the variant alleles (`int`) This will be -1 is not present,
        ``[4]`` The alleles (`list` of `str`)
    """
    cv = cache_session.query(co.Variant).filter(
        co.Variant.variant_hash == var_hash
    ).all()
    mappings = []
    for v in cv:
        alleles = [v.ref_allele]

        try:
            alleles.extend(v.alt_alleles.split(ALLELE_JOIN_DELIMITER))
        except AttributeError:
            # alt_alleles can be None
            pass

        vmi = con.NO_MAP.bits
        vmi |= con.CHR.bits * (chr_name == v.chr_name)
        vmi |= con.START.bits * (start_pos == v.start_pos)
        vmi |= con.ID.bits * (var_id == v.var_id)
        ref_idx, alt_idx = -1, -1
        if effect_allele is not None:
            try:
                ref_idx = alleles.index(effect_allele)
                vmi |= con.REF.bits
            except ValueError:
                pass
            try:
                alt_idx = alleles.index(other_allele)
                vmi |= con.ALT.bits
            except ValueError:
                pass
        mappings.append([vmi, v, ref_idx, alt_idx, alleles])
    return mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _eval_evidence(session, mappings):
    """Evaluate the mapping evidence for an acceptable match.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the GWAS catalog database.
    mappings : `list` of `tuple`
        The mapping evidence. Each tuple has: ``[0]`` The mapping bits
        (`int`), ``[1]`` The cache Variant object
        (`gwas_catalog_parser.build.cache_orm.Variant`), ``[2]`` The
        effect allele index position in the variant alleles (`int`) This
        will be -1 is not present, ``[3]`` The other allele index position
        in the variant alleles (`int`) This will be -1 is not present,
        ``[4]`` The alleles (`list` of `str`)

    Returns
    -------
    variant : `gwas_catalog_parser.orm.Variant`
        The Variant mapping.
    mapping_bits : `int`
        The mapping bits.

    Raises
    ------
    KeyError
        If none of the mappings reach an acceptable level of evidence.
    """
    other_allele = None
    mappings.sort(key=itemgetter(0), reverse=True)
    for evidence, is_valid, single_site in con.EVIDENCE_LEVEL:
        for vmi, v, ridx, aidx, alleles in mappings:
            if evidence & vmi == evidence:
                if is_valid or (single_site & (len(mappings) == 1)):
                    if ridx > -1 and len(alleles) == 2:
                        other_allele = alleles[not ridx]
                    # We have a match
                    # vmi |= con.BAD_EFFECT.bits * ridx ==
                    vmi |= con.REF_FLIP.bits * (ridx > 0)
                    vh = hash_variant(
                        v.chr_name, v.start_pos, v.ref_allele,
                        v.alt_alleles, v.var_id
                    )
                    variant = _get_variant(
                        session, vh, v.chr_name, v.start_pos, v.ref_allele,
                        v.alt_alleles, v.var_id, nalleles=len(alleles),
                        strand=v.strand,
                        minor_allele=v.minor_allele, global_maf=v.global_maf,
                        worst_consequence=v.worst_consequence
                    )
                    return variant, vmi, is_valid, other_allele
    raise KeyError("no mapping")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_variant(session, var_hash, chr_name, start_pos, ref_allele,
                 alt_allele, var_id, nalleles=0, strand=1,
                 minor_allele=None, global_maf=None, worst_consequence=None):
    """Test to see if a variant exists in the database (using the
    ``var_hash``), if it doesn't, then create one (but not added to the
    session).

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the GWAS catalog database.
    var_hash : `str`
        A 32 character MD5 string for the parsed variant attributes from the
        GWAS catalog association data.
    chr_name : `str` or `NoneType`
        Any extracted chromosome names or ``NoneType`` if none are present.
    start_pos : `int` or `NoneType`
        Any extracted start positions or ``NoneType`` if none are present.
    ref_allele : `str` or `NoneType`
        Any reference alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    alt_allele : `str` or `NoneType`
        Any alternate alleles that have been ID'd. These will typically be
        parsed out of a chr:pos variant ID.
    var_id : `str` or `NoneType`
        A processed variant identifier from the GWAS catalogue data.
    strand : `int`, optional, default: `1`
        The strand of the variant.
    minor_allele : `str`, optional, default: `NoneType`
        The minor allele of the variant (if known).
    global_maf : `float`, optional, default: `NoneType`
        the gloabl (cross population) minor allele frequency of the variant.
    worst_consequence : `str`, optional, default: `NoneType`
        The worst VEP consequence of the variant.

    Returns
    -------
    variant : `gwas_catalog_parser.orm.Variant`
        The variant for the association. This will either be a previously added
        one or a newly generated one.
    """
    try:
        # Test if the variant is in the db
        v = q.get_variant_from_hash(session, var_hash)
    except NoResultFound:
        v = o.Variant(
            chr_name=chr_name, start_pos=start_pos,
            ref_allele=ref_allele, alt_alleles=alt_allele,
            var_id=var_id, variant_hash=var_hash,
            nalleles=nalleles,
            strand=strand, minor_allele=minor_allele,
            global_maf=global_maf, worst_consequence=worst_consequence
        )
        # session.add(v)
    return v


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
