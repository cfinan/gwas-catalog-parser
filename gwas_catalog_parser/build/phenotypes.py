"""Functions for processing and importing phenotypes.
"""
from gwas_catalog_parser import (
    constants as con,
    orm as o,
    query as q
)
from sqlalchemy import literal,  and_
from sqlalchemy.orm.exc import NoResultFound
from tqdm import tqdm
import re
import math
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_phenotypes(session, verbose=False, commit_every=10000):
    """Using the ``raw_study`` and ``study`` tables build a table of
    phenotypes.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress monitors.
    commit_every : `int`, optional, default: `10000`
        The number of records to hold in memory before committing to the
        database.

    Returns
    -------
    nphenotypes : `int`
        The number of phenotypes that were added.

    Notes
    -----
    A study in the GWAS catalog has an accession number. A single pubmed ID
    might have multiple accessions for the different (sub)-studies that it
    undertakes. I would usually refer to these as analyses but in GWAS catalog
    parlance they are studies. In turn a study may have many associations.

    Therefore, phenotypes should be defined at the study level in the GWAS
    catalog. There are four dedicated phenotype columns. Two with free text
    and two with coded terms. This will import data from al of them. Two of
    them have background traits, that is traits not actively studied in the
    GWAS but might be considered a comorbidity of all the study participants.

    There are other columns helpful to the phenotype such as the publication
    title and the ``ci_text`` column. However, these are a bit ad-hoc.

    This will only add new phenotype, it can't update any existing
    information. This may lead to a situation where a content has been updated
    but the updated content will not be reflected in the database. This
    will need to be addressed.
    """
    nphenotypes = 0
    nadded = 0
    import_pheno_class(session)
    pheno_class = q.get_all_pheno_class(session)
    # pp.pprint(pheno_class)
    query = pheno_query(session)

    # nrows = sum([query.count() for pc, query in query])
    nrows = query.count()
    progress = tqdm(
        unit=" phenotypes",
        desc="[info] importing phenotypes",
        disable=not verbose,
        leave=False,
        total=nrows
    )

    try:
        prev_study_pk = -1
        # Query the database
        for study_pk, acc, desc, code, pc in query:
            pheno_class_pk = pheno_class[pc]
            codes, descs = process_codes(code, desc)
            for d, c in zip(descs, codes):
                d = d.strip()

                try:
                    # First see if the phenotype term exists
                    pt = session.query(o.PhenotypeTerm).\
                        filter(and_(
                            o.PhenotypeTerm.code == c,
                            o.PhenotypeTerm.term == d
                        )).one()
                except NoResultFound:
                    # Then add
                    pt = o.PhenotypeTerm(code=c, term=d)

                p = o.Phenotype(
                    study_pk=study_pk,
                    phenotype_class_pk=pheno_class_pk,
                    phenotype_term=pt
                )
                session.add(p)
                nadded += 1
            if nadded >= commit_every and prev_study_pk != study_pk:
                session.commit()
                nadded = 0
            prev_study_pk = study_pk
            nphenotypes += 1
            progress.update(1)
        if nadded > 0:
            session.commit()
    finally:
        progress.close()

    return nphenotypes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_pheno_class(session):
    """Setup the constants for the phenotype class table. These will only be
    added if not present.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Notes
    -----
    Phenotype classes are used to track the column where the phenotype
    definition came from.
    """
    pheno_class = q.get_all_pheno_class(session)

    for i in con.PHENO_CLASS_ALL:
        if i not in pheno_class:
            session.add(
                o.PhenotypeClass(phenotype_class_name=i)
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def pheno_query(session):
    """Convenience function to build a big query to get the phenotype terms for
    studies that have not been imported into the phenotype table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object.

    Returns
    -------
    query : `sqlalchemy.Query`
        A union query for getting the four relevant phenotype columns.
    """
    q1 = session.query(
        o.Study.study_pk,
        o.RawStudy.gwas_catalog_accession,
        o.RawStudy.background_trait,
        literal(con.FREE_TEXT_CODE),
        literal(con.PHENO_CLASS_BACKGROUND)
    ).join(
        o.RawStudy, o.RawStudy.raw_study_pk == o.Study.raw_study_pk
    ).join(
        o.Phenotype, o.Study.study_pk == o.Phenotype.study_pk,
        isouter=True
    ).filter(
        and_(
            o.RawStudy.background_trait != None,
            o.Phenotype.study_pk == None
        )
    )
    q2 = session.query(
        o.Study.study_pk,
        o.RawStudy.gwas_catalog_accession,
        o.RawStudy.mapped_background_trait,
        o.RawStudy.mapped_background_trait_uri,
        literal(con.PHENO_CLASS_MAPPED_BACKGROUND)
    ).join(
        o.RawStudy, o.RawStudy.raw_study_pk == o.Study.raw_study_pk
    ).join(
        o.Phenotype, o.Study.study_pk == o.Phenotype.study_pk,
        isouter=True
    ).filter(
        and_(
            o.RawStudy.mapped_background_trait != None,
            o.Phenotype.study_pk == None
        )
    )
    q3 = session.query(
        o.Study.study_pk,
        o.RawStudy.gwas_catalog_accession,
        o.RawStudy.disease_trait,
        literal(con.FREE_TEXT_CODE),
        literal(con.PHENO_CLASS_MAIN)
    ).join(
        o.RawStudy, o.RawStudy.raw_study_pk == o.Study.raw_study_pk
    ).join(
        o.Phenotype, o.Study.study_pk == o.Phenotype.study_pk,
        isouter=True
    ).filter(
        and_(
            o.RawStudy.disease_trait != None,
            o.Phenotype.study_pk == None
        )
    )
    q4 = session.query(
        o.Study.study_pk,
        o.RawStudy.gwas_catalog_accession,
        o.RawStudy.mapped_trait,
        o.RawStudy.mapped_trait_uri,
        literal(con.PHENO_CLASS_MAPPED_MAIN)
    ).join(
        o.RawStudy, o.RawStudy.raw_study_pk == o.Study.raw_study_pk
    ).join(
        o.Phenotype, o.Study.study_pk == o.Phenotype.study_pk,
        isouter=True
    ).filter(
        and_(
            o.RawStudy.mapped_trait != None,
            o.Phenotype.study_pk == None
        )
    )
    query = q1.union_all(q2, q3, q4)
    return query


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_codes(code, desc):
    """Perform some pre-processing on the phenotype codes and descriptions.

    Parameters
    ----------
    code : `str`
        This will either be a constant string i.e. ``"free_text"``, or a URI
        for a coded term. These may be comma separated.
    desc : `str`
        The long form description.

    Returns
    -------
    codes : `list` of `str`
        The codes with and URIs stripped off.
    descs : `list` of `str`
        The descriptions corresponding to the codes.

    Notes
    -----
    The phenotype codes and URIs are , separated however, the dumb thing is
    they look to have unprotected commas in them!!! This function contains a
    fudge fixes most of the problems but it is approximate and some will be
    wrong. The correct solution is to download the EFO and validate the codes.
    """
    # Split the codes (they won't have any internal comma's so we
    # can see how many terms we should expect post splitting)
    codes = [i for i in code.split(',')]
    descs = [desc]
    # There are multiple codes/terms
    if len(codes) > 1:
        # Split the descriptions
        descs = [i for i in desc.split(',')]

        # If they are unbalanced then apply the fudge
        if len(codes) != len(descs):
            # If they are whole dividable, great, if not then
            # we front weight them and hope for the best
            x = math.ceil(len(descs) / len(codes))
            new_descs = []
            for n in range(0, len(descs), x):
                new_descs.append(",".join(descs[n:n+x]))
            descs = new_descs
    # Remove the URL
    codes = [
        re.sub(r'^http(s)?://.+/', '', i.strip()) for i in codes
    ]
    return codes, descs
