"""Handle interaction with the config file.
"""
from gwas_catalog_parser import (
    constants as c
)
from gwas_catalog_parser.build import (
    crossmap
)
try:
    # v1.4
    from sqlalchemy.engine import URL
except ImportError:
    # v1.3
    from sqlalchemy.engine.url import URL

from umls_tools.metamap import metamap
from genomic_config import ini_config
from sqlalchemy_config import config
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from ensembl_rest_client import client
from email_validator import validate_email
from pathlib import Path
import sqlalchemy_utils
import os


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GwasCatalogConfig(object):
    """An interface for the config file.

    Parameters
    ----------
    config_path : `str`
        The configuration file path.
    assembly : `str`
        The genome assembly to build against. Either ``GRCh37`` or ``GRCh38``.
    cache_path : `str`, optional, default: `NoneType`
        An alternate location for the cache. The default location is the root
        of the home directory.
    gwas_cat_db_section : `str`, optional, default: `NoneType`
        The config section name for the GWAS catalog database connection
        parameters. The default is ``gwas_cat_con``.
    """
    GENERAL_SECTION = "general"
    """The name general section in the configuration file (`str`)
    """
    WEBDRIVER_OPTION = "webdriver"
    """The name webdriver option key in the configuration file (`str`)
    """
    CONFIG_USER_EMAIL = "email"
    """The name of the user email option key in the configuration file (`str`)
    """
    CONFIG_ENSEMBL_REST = "ensembl"
    """The name of the ensembl REST API url option key in the configuration
    file (`str`)
    """
    METAMAP_OPTION = "metamap_version"
    """The name of the metamap version key in the configuration file (`str`)
    """
    CONFIG_UMLS_SECTION = "umls_conn"
    """The name UMLS database connection section in the configuration file
    (`str`)
    """
    CONFIG_UMLS_PREFIX = "umls."
    """The name of the SQLAlchemy connection setting prefix for the UMLS
    database (`str`)
    """
    CONFIG_GWAS_CAT_SECTION = "gwas_cat_conn"
    """The name GWAS catalog database connection section in the configuration
    file (`str`)
    """
    CONFIG_GWAS_CAT_PREFIX = "gwas_cat."
    """The name of the SQLAlchemy connection setting prefix for the GWAS
    catalogue database (`str`)
    """
    B37_ASSEMBLY = "GRCh37"
    """The name for the genome assembly version 37 (`str`)
    """
    B38_ASSEMBLY = "GRCh38"
    """The name for the genome assembly version 38 (`str`)
    """
    B37_REST = 'https://grch37.rest.ensembl.org'
    """The URL for the ensembl GRCh37 REST endpoint (`str`)
    """
    B38_REST = 'https://rest.ensembl.org'
    """The URL for the ensembl GRCh38 REST endpoint (`str`)
    """
    ALLOWED_ASSEMBLIES = [B37_ASSEMBLY, B38_ASSEMBLY]
    """The assemblies that are allowed to be given
    """
    VARIANT_CACHE_DB = "variants"
    """The base name for the variant cache database name, the genome assembly
    and extension are added at run time (`str`)
    """
    DEFAULT_CACHE_PATH = os.path.join('.cache', 'gwas_catalog')
    """The path to the cache directory (`str`)
    """
    META_ARGS = [
        '-L', '{0}', '-Z', '{0}', '-sAIy', '--negex', '--conj', '-V', 'USAbase'
    ]
    """The metamap arguments to use. ``{0}`` placeholders should be used for
    the version (`list` of `str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, config_path, assembly, cache_path=None,
                 gwas_cat_db_section=None):
        try:
            # Attempt to open the config file
            open(config_path).close()
        except FileNotFoundError as e:
            raise FileNotFoundError(
                "can't find configuration file"
            ) from e

        self.assembly = assembly
        self.config_path = config_path
        self.gwas_cat_db_section = gwas_cat_db_section or \
            self.CONFIG_GWAS_CAT_SECTION

        # Set the cache path to the user location or the default
        self.cache_path = Path(
            cache_path or os.path.join(
                os.environ['HOME'], self.DEFAULT_CACHE_PATH
            )
        )
        # Make sure the cache path exists
        self.cache_path.mkdir(parents=True, exist_ok=True)

        self.gen_config = ini_config.IniConfig(self.config_path)
        self.gen_config.open()
        self.config = self.gen_config.parser

        # Make sure the assembly is valid
        self.assembly_root = self.gen_config.get_assembly_synonym(
            'homo sapiens', self.assembly
        )
        if self.assembly not in self.ALLOWED_ASSEMBLIES and \
           self.assembly_root not in self.ALLOWED_ASSEMBLIES:
            raise ValueError(f"unknown assembly: {self.assembly}")

        self._metamap_server = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """An open interface function.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """A close interface function.
        """
        # If a metamap server has been opened then close it.
        if self._metamap_server is not None:
            pass
            # self._metamap_server.close()
            # self._metamap_server = None

        self.gen_config.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_metamap_server(self, args=None):
        """Get the metamap server object if the metamap version is defined.

        Returns
        -------
        metamap_version : `str`
            The metamap version number to use.
        args : `list` of `str`, optional, default: `NoneType`
            Metamap arguments to use. If not supplied then the config defaults
            are used.

        Returns
        -------
        metamap_server : `umls_tools.metamp.MetamapServer`
            A metamp server object to issue Metamap queries.

        Raises
        ------
        KeyError
            If the metamap version (or UMLS section) is not defined.
        """
        version = self.config[self.CONFIG_UMLS_SECTION][self.METAMAP_OPTION]

        if self._metamap_server is not None:
            return self._metamap_server
        args = args or self.META_ARGS
        args = [i.format(version) for i in args]
        self._metamap_server = metamap.MetamapServer(args=args)
        self._metamap_server.start()
        return self._metamap_server

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_umls_session_maker(self, must_exist=True):
        """Get the SQLAlchemy session maker for the UMLS database.

        Parameters
        ----------
        must_exist : `bool`, optional, default: `True`
            Is the database expected to exist, if this is ``True`` and it does
            not then an error will be raised.

        Returns
        -------
        session_maker : `sqlalchemy.SessionMaker`
            A session maker object that can be used to generate UMLS database
            sessions.

        Raises
        ------
        KeyError
            If the UMLS section is not defined.
        """
        conn_args = config.get_sqlalchemy_args(
            self.config, self.CONFIG_UMLS_SECTION, self.CONFIG_UMLS_PREFIX
        )
        sm = config.get_sqlalchemy_conn(
            conn_args, self.CONFIG_UMLS_PREFIX
        )
        if must_exist is True and config.database_exists(sm) is False:
            raise FileNotFoundError("database does not exist")

        return sm

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_gwas_cat_session_maker(self, must_exist=True):
        """Get the SQLAlchemy session maker for the GWAS catalog database.

        Parameters
        ----------
        must_exist : `bool`, optional, default: `True`
            Is the database expected to exist, if this is ``True`` and it does
            not then an error will be raised.

        Returns
        -------
        session_maker : `sqlalchemy.SessionMaker`
            A session maker object that can be used to generate GWAS catalog
            database sessions.
        """
        conn_args = config.get_sqlalchemy_args(
            self.config, self.gwas_cat_db_section, self.CONFIG_GWAS_CAT_PREFIX,
        )
        sm = config.get_sqlalchemy_conn(
            conn_args, self.CONFIG_GWAS_CAT_PREFIX,
        )
        if must_exist is True and config.database_exists(sm) is False:
            raise FileNotFoundError("database does not exist")

        return sm

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_variant_cache_session_maker(self):
        """Get the SQLAlchemy session maker for the variant cache database.

        Returns
        -------
        session_maker : `sqlalchemy.SessionMaker`
            A session maker object that can be used to generate cache database
            sessions.
        """
        db_backend = 'sqlite'
        cache_db_path = self.variant_cache_db_path()
        try:
            url = URL.create(database=cache_db_path, drivername=db_backend)
        except AttributeError:
            url = URL(database=cache_db_path, drivername=db_backend)

        if sqlalchemy_utils.database_exists(url) is False:
            sqlalchemy_utils.create_database(url)

        engine = create_engine(url)

        # If we get here then we are good to continue, so we create a session
        return sessionmaker(bind=engine)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def variant_cache_db_path(self):
        """Get the path to the variant cache database.

        Returns
        -------
        cache_db_path : `str`
            The absolute path to the variant cache database.
        """
        db_basename = "{0}_{1}.db".format(
            self.VARIANT_CACHE_DB, self.assembly.lower()
        )
        return os.path.join(self.cache_path, db_basename)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_email(self):
        """Get the e-mail address, this is used to access pubmed remotely.

        Returns
        -------
        email : `str` or `NoneType`
            An email address to access pubmed or ``NoneType`` if not provided.
        """
        try:
            email = self.config[self.GENERAL_SECTION][self.CONFIG_USER_EMAIL]
            email_obj = validate_email(email)
            return email_obj.email
        except KeyError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_rest_client(self, **kwargs):
        """get the ensembl REST server to use.

        Parameters
        ----------
        **kwargs
            Any keyword arguments passed onto
            ``esembl_rest_client.client.Rest``.

        Returns
        -------
        rest_client : `esembl_rest_client.client.Rest`
            A rest client object to query ensembl with. The correct URL is used
            for the required assembly.
        """
        try:
            url = self.config[self.GENERAL_SECTION][self.CONFIG_ENSEMBL_REST]
            return client.Rest(url=url)
        except KeyError:
            if self.assembly == self.B37_ASSEMBLY:
                return client.Rest(url=self.B37_REST, **kwargs)
            elif self.assembly == self.B38_ASSEMBLY:
                return client.Rest(url=self.B38_REST, **kwargs)
            else:
                raise ValueError(f"unknown assembly: {self.assembly}")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_liftover_func(self):
        """Get a function that can be called to liftover the variants.

        Returns
        -------
        liftover_func : `function`
            The function that is called to liftover (or not).

        Notes
        -----
        Liftover is only required if assembly is GRCh37. If the assembly is
        GRCh38 a dummy pass-through function is returned.
        """
        if self.assembly == self.B37_ASSEMBLY:
            lift_file = self.assembly_root = self.gen_config.get_chain_file(
                'homo sapiens', self.B38_ASSEMBLY, self.B37_ASSEMBLY
            )
            self._map_tree = crossmap.read_chain(lift_file)
            return self.b37_liftdown
        return self.dummy_liftover

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def b37_liftdown(self, chr_name, start_pos, end_pos):
        """The function to lift the coordinates down to GRCh37 from GRCh38 (the
        GWAS catalog coordinates).

        Parameters
        ----------
        chr_name : `str`
            The chromosome to be lifted over.
        start_pos : `int`
            The start position to be lifted over.
        end_pos : `int`
            The end position to be lifted over.

        Returns
        -------
        chr_name : `str`
            The lifted over ``chr_name``.
        start_pos : `int`
            The lifted over ``start_pos``.
        end_pos : `int`
            The lifted over ``end_pos``.

        Notes
        -----
        If the coordinates can't be lifted over then chr_name will be ``"0"``,
        ``start_pos`` and ``end_pos`` will be -1.
        """
        return crossmap.run_crossmap(
            self._map_tree, chr_name, start_pos, end_pos
        )[:3]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def dummy_liftover(self, chr_name, start_pos, end_pos):
        """A dummy passthrough non-liftover function.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to be passed through.
        start_pos : `int`
            The start position to be passed through.
        end_pos : `int`
            The end position to be passed through.

        Returns
        -------
        chr_name : `str`
            Same as passed.
        start_pos : `int`
            Same as passed.
        end_pos : `int`
            Same as passed.
        """
        return chr_name, start_pos, end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_webdriver(self):
        """Attempt to get the webdriver path that may be in the config file. If
        not it is assumed to be in the PATH.

        Returns
        -------
        webdriver_path : `str` or `NoneType`
            The path to the webdriver or ``NoneType`` if not defined.
        """
        try:
            return self.config[self.GENERAL_SECTION][self.WEBDRIVER_OPTION]
        except KeyError:
            pass
